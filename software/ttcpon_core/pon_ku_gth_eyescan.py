#!/usr/bin/env python

import csv
import datetime
import math
import time
import logging
from random import randint

logger_pon = logging.getLogger('ttc_pon')

# -------------------------------------------------------------
#  ----------------- Eye Scan Functions ---------------------
# -------------------------------------------------------------

# Those parameters are not intended to be changed by the user
# The eye scan functionality is provided here as a mere example of DRP interface communication
# If a user wishes to implement his/her own eye scan, they are welcome

def rx_eye_scan_masksetup(drp_master):
    """
        ***************************************************************************
        Sets ES_QUAL_MASK and ES_SDATA_MASK qualifiers for 40b interface
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        ***************************************************************************
    """
    # ES_QUAL_MASK is all 1's for statistical eye 
    for i in range(0x44,0x49):
        drp_master.drp_write(i,2**16-1)

    drp_master.drp_write(0x49,0)
    drp_master.drp_write(0x4A,0)
    drp_master.drp_write(0x4B,0xFF00)
    drp_master.drp_write(0x4C,2**16-1)
    drp_master.drp_write(0x4D,2**16-1)

def rx_eye_scan_prescale(drp_master, prescale_factor):
    """
        ***************************************************************************
        Prescale choice
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
            prescale_factor  : prescale value			
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x3C)[1]
    stat = stat & 0xFFE0
    stat = stat | (prescale_factor & 0x001F)		
    return drp_master.drp_write(0x3C,stat)

def rx_eye_scan_enable(drp_master):
    """
        ***************************************************************************
        Sets ES_EYE_SCAN_EN=1 and ES_ERRDET_EN=1 for a statistical eye
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x3C)[1]
    stat_new = stat | 0x0300
    if(stat_new!=stat):
        return drp_master.drp_write(0x3C,stat_new)
    else:
        return 0

def rx_eye_scan_run(drp_master):
    """
        ***************************************************************************
        Sets ES_CONTROL[5:0]=1
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x3C)[1]
    stat_new = stat | (0x0400)
    return drp_master.drp_write(0x3C,stat_new)

def rx_eye_scan_stop(drp_master):
    """
        ***************************************************************************
        Sets ES_CONTROL[5:0]=0
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x3C)[1]
    stat_new = stat & (0x03FF)
    return drp_master.drp_write(0x3C,stat_new)

def rx_eye_scan_horz_offset(drp_master, horz_offset):
    """
        ***************************************************************************
        Sets Horizontal offset for eye diagram
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
            horz_offset      : horizontal offset for eye scan			
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x4F)[1]
    horz_offset_cp2 = horz_offset		
    if(horz_offset<0):
        horz_offset_cp2 = -1*horz_offset_cp2
        horz_offset_cp2 = horz_offset_cp2^0xFFF + 1
    stat_new = (stat&0x000F | horz_offset_cp2<<4)
    return drp_master.drp_write(0x4F, stat_new)

def rx_eye_scan_vert_offset(drp_master, vert_offset, step_volt):
    """
        ***************************************************************************
        Sets Horizontal offset for eye diagram
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
            step_volt        :0 - 1.5mV/div
                              1 - 1.8mV/div
                              2 - 2.2mV/div
                              3 - 2.8mV/div
        return:
            success
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x97)[1]
    vert_offset_cp1 = vert_offset		
    if(vert_offset<0):
        vert_offset_cp1 = -1*vert_offset_cp1		
        vert_offset_cp1 = vert_offset_cp1 | 0x100
    
    vert_offset_cp1 = vert_offset_cp1 << 2
    vert_offset_cp1 = vert_offset_cp1 |(step_volt&0x0003)
    stat_new = (stat&0xF800 | vert_offset_cp1)
    return drp_master.drp_write(0x97, stat_new)

def rx_eye_scan_read_fsm(drp_master):
    """
        ***************************************************************************
        Read es_control_status
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            es_control_status
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x153)[1]
    stat_new = (stat & (0x000F))
    return stat_new

def rx_eye_scan_read_errors(drp_master):
    """
        ***************************************************************************
        Read number of errors in statistical eye
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            number of errors
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x151)[1]
    return stat

def rx_eye_scan_read_samples(drp_master):
    """
        ***************************************************************************
        Read number of samples in statistical eye
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
        return:
            number of samples
        ***************************************************************************
    """
    stat = drp_master.drp_read(0x152)[1]
    return stat

def rx_eye_scan(drp_master, eyescan_file_name, save_to_file=1):
    """
        ***************************************************************************
        Performs eye scan in KintexUltrascale-GTH configured as ONU
        Requires DRP interface to be working properly
        Optionally saves to file
        ***************************************************************************

        ***************************************************************************
        arg:
            drp_master       : Object with the methods drp_read, drp_write as defined in the OltCore and OnuCore classes
            eyescan_file_name: File to save the eye scan
            save_to_file     : (1/0)

        return:
            ber_matrix       : matrix (list of lists) containing the eye scan results
        ***************************************************************************
    """
    c_STATE_WAIT   = 0b0001
    c_STATE_RESET  = 0b0010
    c_STATE_COUN   = 0b0110
    c_STATE_END    = 0b0101
    c_STATE_ARMED  = 0b1010
    c_STATE_READ   = 0b1001

    # Hard parameters (do not change)
    c_GTH_INT_DATAWIDTH = 40 #datapath width
    c_PRESCALE_VALUE    = 5
    c_OFFSET_VOLTAGE    = 3 # 2.8mV/div
    ber_matrix = []

    rx_eye_scan_masksetup(drp_master)
    rx_eye_scan_prescale(drp_master, c_PRESCALE_VALUE)		
    rx_eye_scan_stop(drp_master)		

    stat = rx_eye_scan_enable(drp_master)

    for vert_offset in range(127,-127,-4):
        ber_matrix.append([])		
        rx_eye_scan_vert_offset(drp_master, vert_offset, c_OFFSET_VOLTAGE)		
        line_print = ''
        for horz_offset in range(-32,32):				
            rx_eye_scan_horz_offset(drp_master, horz_offset)
            rx_eye_scan_run(drp_master)
            stat = 0
			
            # wait scan is done (sample acc saturated)
            while((stat&0x0001)==0):
                stat = rx_eye_scan_read_fsm(drp_master)

            errs = rx_eye_scan_read_errors(drp_master)
            bits = rx_eye_scan_read_samples(drp_master)
            errs_scaled = errs				
            bits_scaled = bits*c_GTH_INT_DATAWIDTH*(2**(1+c_PRESCALE_VALUE))
            ber = (errs_scaled/bits_scaled) if (errs_scaled>0) else (1/bits_scaled)
            ber_matrix[-1].append(ber)
            rx_eye_scan_stop(drp_master)
            #print(ber)
            if(errs_scaled==0): append_char=' '
            else : append_char = '*'			
            line_print = line_print + append_char
        logger_pon.info(line_print)

    # save eyescan to file
    if(save_to_file):		
        with open(eyescan_file_name + '.csv', 'w') as csvfile:
            fieldnames = []
            for i in range(0, len(ber_matrix[-1])):
                fieldnames += ['hor' + str(i)]

            for i in range(0, len(fieldnames)):
                # format 20 spaces for each value
                fieldnames[i] = '%20s' % fieldnames[i]

            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                delimiter=',',
                lineterminator='\n')
            writer.writeheader()

            dict_write = {}
            for j in range(0, len(ber_matrix)):
                for i in range(0, len(ber_matrix[-1])):
                    dict_write[fieldnames[i]] = "%20.15f" % ber_matrix[j][i]

                writer.writerow(dict_write)
        logger_pon.info('Finished Saving eye scan to file ' + eyescan_file_name + '.csv')
