#!/usr/bin/env python

####################################################
### Example design for board config of:          ###
###     one OLT in KCU105 + TTC-PON FMC          ###
####################################################

####################################################
###          Python Native Packages              ###
####################################################
import logging
import time

####################################################
###              PON Exdsg cores                 ###
####################################################
from ttcpon_core.ttcpon_exdsg import OltExdsg

####################################################
###             PON Board Config                 ###
####################################################
import ttcpon_board_config.pon_fmc_i2c_program as program_fmc

def main(olt):

    ####################################################
    ###          Parameters for Application          ###
    ####################################################
    INIT_OLT                        = 1  # possible values: 1 or 0

    ####################################################
    ###        Start TTC-PON Ex. Application         ###
    ####################################################
    logger_pon.info('Started TTC-PON TEST application')

    ####################################################
    ###          Initialization of OLT               ###
    ####################################################
    # Program OLT
    if(INIT_OLT):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM OLT-FMC --------------------')	
        program_fmc.program_clkmux(olt, 0xFF)
        program_fmc.program_si570(olt, 1)
        olt.exd_core_reset()
        time.sleep(1)		
        program_fmc.program_leds(olt, 0x00)
        program_fmc.monitor_sfp(olt,1)
        logger_pon.info('                                                         ')
        time.sleep(1)

if __name__ == '__main__':
    # Logger TTC-PON CORE definition:
    # create logger with 'ttc_pon' application
    logger_pon = logging.getLogger('ttc_pon')
    logger_pon.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('./logger/ttcpon_log.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s',
        datefmt='%d-%m-%y %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger_pon.addHandler(fh)
    logger_pon.addHandler(ch)

    # Logger TTC-PON EXDSG definition:
    # create logger with 'ttc_pon' application
    logger_exd_pon = logging.getLogger('ttc_pon_tests')
    logger_exd_pon.setLevel(logging.DEBUG)
    # add the handlers to the logger
    logger_exd_pon.addHandler(fh)
    logger_exd_pon.addHandler(ch)

    # Open sockets
    # open OLT socket
    olt = OltExdsg('127.0.0.1', 8555)

    main(olt)
