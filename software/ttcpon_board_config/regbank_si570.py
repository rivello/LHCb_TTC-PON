# 
# regbank_si570.py
# 
#  Created on: 06 April 2017
#      Author: ebrandao
#      OBS: THIS CONFIGURATION IS UNIQUE PER FMC
# 

regbank_si570 = [
        [0x89, 0x10], # Freeze the DCO by setting Freeze DCO = 1 (bit 4 of register 137)
        [0x07, 0xE0],
        [0x08, 0x42],
        [0x09, 0xE3],
        [0x0A, 0x35],
        [0x0B, 0xA2],
        [0x0C, 0xBA],
        [0x89, 0x00], # Unfreeze the DCO by setting Freeze DCO = 0 (bit 4 of register 137)
        [0x87, 0x40], # Assert the NewFreq bit (bit 6 of register 135)
    ];



