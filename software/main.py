#!/usr/bin/env python

####################################################
### Example design for:                          ###
###     one OLT in KCU105 + TTC-PON FMC          ###
###     ONU1 in KCU105 + TTC-PON FMC             ###
###     ONU2 in A10GX  + on-board SFP            ###
####################################################

####################################################
###          Python Native Packages              ###
####################################################
import logging
import time

####################################################
###              PON Exdsg cores                 ###
####################################################
from ttcpon_core.ttcpon_exdsg import (OltExdsg, OnuExdsg)

####################################################
###            PON-ONU Eye diagrams              ###
####################################################
import ttcpon_core.pon_ku_gth_eyescan as pon_ku_gth_eyescan

####################################################
###             PON Monitor Plot                 ###
####################################################

####################################################
###             PON Board Config                 ###
####################################################
import ttcpon_board_config.pon_fmc_i2c_program as program_fmc
import ttcpon_board_config.init_onu_fmc as init_onu_fmc
import ttcpon_board_config.pon_a10gx_i2c_program as program_a10gx

def main(olt, onu):

    ####################################################
    ###          Parameters for Application          ###
    ####################################################
    INIT_OLT                        = 1  # possible values: 1 or 0		
    INIT_ONU                        = 1  # possible values: 1 or 0
    PERFORM_ONU_EYE_SCAN            = 1  # possible values: 1 or 0
    PERFORM_FULL_CALIBRATION        = 1  # possible values: 1 or 0
    RESET_SYSTEM_BEFORE_CALIBRATION = 1  # possible values: 1 or 0		
    WAIT_TIME_AFTER_CALIBRATION     = 60 # unit (s)
    PERFORM_OLT_MONITORING          = 1  # possible values: 1 or 0

    ####################################################
    ###        Start TTC-PON Ex. Application         ###
    ####################################################
    logger_pon.info('Started TTC-PON TEST application')

    ####################################################
    ###          Initialization of OLT/ONUs          ###
    ####################################################
    # Program OLT
    if(INIT_OLT):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM OLT-FMC --------------------')	
        program_fmc.program_clkmux(olt, 0xFF)
        program_fmc.program_si570(olt, 1)
        olt.exd_core_reset()
        time.sleep(2)
        program_fmc.program_leds(olt, 0x00)		
        program_fmc.monitor_sfp(olt,1)
        logger_pon.info('                                                         ')
        time.sleep(1)

    ### INIT - ONU_1 - Mounted on FMC - KCU105 board (Kintex Ultrascale)
    if(INIT_ONU):		
        logger_pon.info('---------------------------------------------------------')		
        logger_pon.info('------------------- PROGRAM ONU1-FMC --------------------')
        logger_pon.info('---------------------------------------------------------')	
        init_onu_fmc.onu_init(onu[0], 1)
        logger_pon.info('                                                         ')		
        time.sleep(1)
    if(PERFORM_ONU_EYE_SCAN):	
        logger_pon.info('---------------------- Eye Scan ONU1 --------------------')
        pon_ku_gth_eyescan.rx_eye_scan(onu[0],'./calibration_config/eyescan_onu1', 1)
        logger_pon.info('                                                         ')	
        time.sleep(1)

    ### INIT - ONU_2 - Mounted on on-board SFP connector - A10GX Board  (Arria10)
    if(INIT_ONU):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM ONU2-A10GX ------------------')
        onu[1].exd_set_addr(2)	
        program_a10gx.monitor_sfp(onu[1], 1)
        logger_pon.info('                                                         ')		
        time.sleep(1)
    if(PERFORM_ONU_EYE_SCAN):	
        logger_pon.info('- Eye scan feature not yet supported for ONU-A10 device')
        logger_pon.info('                                                         ')
        time.sleep(1)

	####################################################
    ###     Reset System and perform calibration     ###
	####################################################
    if(RESET_SYSTEM_BEFORE_CALIBRATION):	
        olt.exd_core_reset()
        time.sleep(2)
        for i in range(0, len(onu)):
            onu[i].exd_core_reset()
        time.sleep(2)

    if(PERFORM_FULL_CALIBRATION):
        logger_pon.info('Initializing System Upstream Calibration')
        onu_list = olt.full_calibration_run([1,2])
        olt.save_onu_config_csv(onu_list,'./calibration_config/onu_config')
        olt.save_olt_config_csv('./calibration_config/olt_config')

        # Wait time after calibration
        logger_pon.info('Waiting ' + str(WAIT_TIME_AFTER_CALIBRATION) + 's before system monitoring...')	
        t0 = time.time()
        deltaT = 0	
        while(deltaT<=WAIT_TIME_AFTER_CALIBRATION):
            deltaT = time.time() - t0

	####################################################
    ###        Start OLT monitoring application      ###
	####################################################
    if(PERFORM_OLT_MONITORING):
        olt.clear_olt_monitor_status()
        for onu_idx in range(0,len(onu)):
            olt.clear_onu_monitor_status(onu_idx+1)

        olt_list = olt.full_olt_monitor_run()
        olt.save_olt_monitor_csv(olt_list, './calibration_config/olt_monitor')

        for onu_idx in range(0,len(onu)):
            onu_dict = olt.full_onu_monitor_run(onu_idx+1)
            olt.save_onu_monitor_csv(onu_dict, './calibration_config/onu' + str(onu_idx+1) + '_monitor')

if __name__ == '__main__':
    # Logger TTC-PON CORE definition:
    # create logger with 'ttc_pon' application
    logger_pon = logging.getLogger('ttc_pon')
    logger_pon.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('./logger/ttcpon_log.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s',
        datefmt='%d-%m-%y %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger_pon.addHandler(fh)
    logger_pon.addHandler(ch)

    # Logger TTC-PON EXDSG definition:
    # create logger with 'ttc_pon' application
    logger_exd_pon = logging.getLogger('ttc_pon_tests')
    logger_exd_pon.setLevel(logging.DEBUG)
    # add the handlers to the logger
    logger_exd_pon.addHandler(fh)
    logger_exd_pon.addHandler(ch)

    # Open sockets
    # open OLT socket
    olt = OltExdsg('127.0.0.1', 8555)

    # open ONU socket
    onu = [	
        OnuExdsg('127.0.0.1', 8556),
        OnuExdsg('127.0.0.1', 2540)]

    main(olt, onu)
