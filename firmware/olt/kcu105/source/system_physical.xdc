# System 300MHz clock
set_property IOSTANDARD LVDS [get_ports CLK_300MHZ_P]
set_property PACKAGE_PIN AK17 [get_ports CLK_300MHZ_P]

# FMC I2C on FMC_HPC_LA01_CC_P/N
set_property PACKAGE_PIN G9 [get_ports iic_fmc_scl_io]
set_property IOSTANDARD LVCMOS18 [get_ports iic_fmc_scl_io]
set_property PACKAGE_PIN F9 [get_ports iic_fmc_sda_io]
set_property IOSTANDARD LVCMOS18 [get_ports iic_fmc_sda_io]

# FMC I2C_RESET
set_property PACKAGE_PIN D13 [get_ports {IIC_FMC_GPO[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {IIC_FMC_GPO[0]}]
# FMC PLL_RESET
set_property PACKAGE_PIN C13 [get_ports {IIC_FMC_GPO[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {IIC_FMC_GPO[1]}]

# GTH refclk
set_property PACKAGE_PIN K6 [get_ports FMC_HPC_GBTCLK0_M2C_C_P]
set_property PACKAGE_PIN K5 [get_ports FMC_HPC_GBTCLK0_M2C_C_N]

# MGT location constraint
set_property LOC GTHE3_CHANNEL_X0Y16 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[2].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
set_property PACKAGE_PIN E3 [get_ports FMC_HPC_DP0_M2C_N]

# FMC-SFP control connections
# TX DIS
set_property PACKAGE_PIN J10 [get_ports FMC_HPC_LA02_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA02_N]

# RSSI TRI
set_property PACKAGE_PIN K10 [get_ports FMC_HPC_LA02_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA02_P]

# RXSD
set_property PACKAGE_PIN A12 [get_ports FMC_HPC_LA03_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA03_N]

# TX FAULT
set_property PACKAGE_PIN A13 [get_ports FMC_HPC_LA03_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA03_P]

# MOD ABS
set_property PACKAGE_PIN K12 [get_ports FMC_HPC_LA04_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA04_N]

# RXRESET
set_property PACKAGE_PIN L12 [get_ports FMC_HPC_LA04_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA04_P]

# SMA GPIO
set_property PACKAGE_PIN G27 [get_ports SMA_GPIO_N_OUT]
set_property IOSTANDARD LVCMOS18 [get_ports SMA_GPIO_N_OUT]
# set_property PACKAGE_PIN H27 [get_ports SMA_GPIO_P_OUT]
# set_property IOSTANDARD LVCMOS18 [get_ports SMA_GPIO_P_OUT]

# Generate ILA for example design