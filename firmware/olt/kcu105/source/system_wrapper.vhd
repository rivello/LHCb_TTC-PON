--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Thu Mar 12 10:47:21 2020
--Host        : pcepese47 running 64-bit major release  (build 9200)
--Command     : generate_target system_wrapper.bd
--Design      : system_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_wrapper is
  port (
    CLK_300MHZ_N : in STD_LOGIC;
    CLK_300MHZ_P : in STD_LOGIC;
    FMC_HPC_DP0_C2M_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_P : out STD_LOGIC;
    FMC_HPC_DP0_M2C_N : in STD_LOGIC;
    FMC_HPC_DP0_M2C_P : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_P : in STD_LOGIC;
    FMC_HPC_LA02_N : out STD_LOGIC;
    FMC_HPC_LA02_P : out STD_LOGIC;
    FMC_HPC_LA03_N : in STD_LOGIC;
    FMC_HPC_LA03_P : in STD_LOGIC;
    FMC_HPC_LA04_N : in STD_LOGIC;
    FMC_HPC_LA04_P : out STD_LOGIC;
    IIC_FMC_GPO : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SMA_GPIO_N_OUT : out STD_LOGIC;
    iic_fmc_scl_io : inout STD_LOGIC;
    iic_fmc_sda_io : inout STD_LOGIC
  );
end system_wrapper;

architecture STRUCTURE of system_wrapper is
  component system is
  port (
    FMC_HPC_LA03_N : in STD_LOGIC;
    FMC_HPC_LA04_N : in STD_LOGIC;
    FMC_HPC_LA03_P : in STD_LOGIC;
    iic_fmc_sda_io : inout STD_LOGIC;
    iic_fmc_scl_io : inout STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_P : in STD_LOGIC;
    FMC_HPC_DP0_M2C_N : in STD_LOGIC;
    FMC_HPC_DP0_M2C_P : in STD_LOGIC;
    FMC_HPC_LA02_P : out STD_LOGIC;
    FMC_HPC_LA04_P : out STD_LOGIC;
    FMC_HPC_LA02_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_P : out STD_LOGIC;
    SMA_GPIO_N_OUT : out STD_LOGIC;
    IIC_FMC_GPO : out STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK_300MHZ_N : in STD_LOGIC;
    CLK_300MHZ_P : in STD_LOGIC
  );
  end component system;
begin
system_i: component system
     port map (
      CLK_300MHZ_N => CLK_300MHZ_N,
      CLK_300MHZ_P => CLK_300MHZ_P,
      FMC_HPC_DP0_C2M_N => FMC_HPC_DP0_C2M_N,
      FMC_HPC_DP0_C2M_P => FMC_HPC_DP0_C2M_P,
      FMC_HPC_DP0_M2C_N => FMC_HPC_DP0_M2C_N,
      FMC_HPC_DP0_M2C_P => FMC_HPC_DP0_M2C_P,
      FMC_HPC_GBTCLK0_M2C_C_N => FMC_HPC_GBTCLK0_M2C_C_N,
      FMC_HPC_GBTCLK0_M2C_C_P => FMC_HPC_GBTCLK0_M2C_C_P,
      FMC_HPC_LA02_N => FMC_HPC_LA02_N,
      FMC_HPC_LA02_P => FMC_HPC_LA02_P,
      FMC_HPC_LA03_N => FMC_HPC_LA03_N,
      FMC_HPC_LA03_P => FMC_HPC_LA03_P,
      FMC_HPC_LA04_N => FMC_HPC_LA04_N,
      FMC_HPC_LA04_P => FMC_HPC_LA04_P,
      IIC_FMC_GPO(1 downto 0) => IIC_FMC_GPO(1 downto 0),
      SMA_GPIO_N_OUT => SMA_GPIO_N_OUT,
      iic_fmc_scl_io => iic_fmc_scl_io,
      iic_fmc_sda_io => iic_fmc_sda_io
    );
end STRUCTURE;
