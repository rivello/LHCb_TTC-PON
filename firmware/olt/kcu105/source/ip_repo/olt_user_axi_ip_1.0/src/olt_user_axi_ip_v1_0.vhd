--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_user_axi_ip_v1_0.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
library UNISIM;                         -- TO BE MODIFIED FOR FINAL VERSION
use UNISIM.vcomponents.all;             -- TO BE MODIFIED FOR FINAL VERSION
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT user AXI wrapper (olt_user_axi_ip_v1_0)
--
--! @brief OLT user logic wrapper
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 19\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_user_axi_ip_v1_0
--============================================================================
entity olt_user_axi_ip_v1_0 is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line


    -- Parameters of Axi Slave Bus Interface S_OLT_USER_AXI
    C_S_OLT_USER_AXI_DATA_WIDTH : integer := 32;
    C_S_OLT_USER_AXI_ADDR_WIDTH : integer := 9
    );
  port (
    -- Ports of Axi Slave Bus Interface S_OLT_USER_AXI
    s_olt_user_axi_aclk    : in  std_logic;
    s_olt_user_axi_aresetn : in  std_logic;
    s_olt_user_axi_awaddr  : in  std_logic_vector(C_S_OLT_USER_AXI_ADDR_WIDTH-1 downto 0);
    s_olt_user_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_olt_user_axi_awvalid : in  std_logic;
    s_olt_user_axi_awready : out std_logic;
    s_olt_user_axi_wdata   : in  std_logic_vector(C_S_OLT_USER_AXI_DATA_WIDTH-1 downto 0);
    s_olt_user_axi_wstrb   : in  std_logic_vector((C_S_OLT_USER_AXI_DATA_WIDTH/8)-1 downto 0);
    s_olt_user_axi_wvalid  : in  std_logic;
    s_olt_user_axi_wready  : out std_logic;
    s_olt_user_axi_bresp   : out std_logic_vector(1 downto 0);
    s_olt_user_axi_bvalid  : out std_logic;
    s_olt_user_axi_bready  : in  std_logic;
    s_olt_user_axi_araddr  : in  std_logic_vector(C_S_OLT_USER_AXI_ADDR_WIDTH-1 downto 0);
    s_olt_user_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_olt_user_axi_arvalid : in  std_logic;
    s_olt_user_axi_arready : out std_logic;
    s_olt_user_axi_rdata   : out std_logic_vector(C_S_OLT_USER_AXI_DATA_WIDTH-1 downto 0);
    s_olt_user_axi_rresp   : out std_logic_vector(1 downto 0);
    s_olt_user_axi_rvalid  : out std_logic;
    s_olt_user_axi_rready  : in  std_logic;
    ------------------------------------------------------

    -- Users to add ports here
    -- core connections
    olt_clk_sys_i        : in  std_logic;
    olt_core_reset_o     : out std_logic;
    olt_interrupt_i      : in  std_logic;
    olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
    olt_clk_trxusr240_i  : in  std_logic;
    olt_tx_data_o        : out std_logic_vector(199 downto 0);
    olt_tx_data_strobe_o : out std_logic;
    olt_rx_locked_i      : in  std_logic;
    olt_rx_data_i        : in  std_logic_vector(55 downto 0);
    olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
    olt_rx_data_strobe_i : in  std_logic;
    olt_rx_data_error_i  : in  std_logic;
    olt_mgt_pll_lock_i   : in  std_logic;
    olt_mgt_tx_ready_i   : in  std_logic;
    olt_mgt_rx_ready_i   : in  std_logic;

    -- Others ----------                
    tx_match_flag_o : out std_logic
    ----------------------------------          

    );
end olt_user_axi_ip_v1_0;

--============================================================================
--! Architecture declaration for olt_user_axi_ip_v1_0
--============================================================================
architecture structural of olt_user_axi_ip_v1_0 is

  -- Control
  signal slv_reg_ctrl : t_regbank32b(127 downto 0);
  -- Status
  signal slv_reg_stat : t_regbank32b(127 downto 0);

  -- component declaration
  component olt_user_axi_ip_v1_0_S_OLT_USER_AXI is
    generic (
      C_S_AXI_DATA_WIDTH : integer := 32;
      C_S_AXI_ADDR_WIDTH : integer := 9
      );
    port (

      -- Control
      slv_reg_ctrl_o : out t_regbank32b(127 downto 0);
      -- Status
      slv_reg_stat_i : in  t_regbank32b(127 downto 0);

      S_AXI_ACLK    : in  std_logic;
      S_AXI_ARESETN : in  std_logic;
      S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_AWVALID : in  std_logic;
      S_AXI_AWREADY : out std_logic;
      S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_WSTRB   : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
      S_AXI_WVALID  : in  std_logic;
      S_AXI_WREADY  : out std_logic;
      S_AXI_BRESP   : out std_logic_vector(1 downto 0);
      S_AXI_BVALID  : out std_logic;
      S_AXI_BREADY  : in  std_logic;
      S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_ARVALID : in  std_logic;
      S_AXI_ARREADY : out std_logic;
      S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_RRESP   : out std_logic_vector(1 downto 0);
      S_AXI_RVALID  : out std_logic;
      S_AXI_RREADY  : in  std_logic
      );
  end component olt_user_axi_ip_v1_0_S_OLT_USER_AXI;

  component olt_user_logic_exdsg is
    port(
      -- core connections
      olt_clk_sys_i        : in  std_logic;
      olt_interrupt_i      : in  std_logic;
      olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
      olt_clk_trxusr240_i  : in  std_logic;
      olt_tx_data_o        : out std_logic_vector(199 downto 0);
      olt_tx_data_strobe_o : out std_logic;
      olt_rx_locked_i      : in  std_logic;
      olt_rx_data_i        : in  std_logic_vector(55 downto 0);
      olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
      olt_rx_data_strobe_i : in  std_logic;
      olt_rx_data_error_i  : in  std_logic;

      olt_mgt_pll_lock_i : in  std_logic;
      olt_mgt_tx_ready_i : in  std_logic;
      olt_mgt_rx_ready_i : in  std_logic;
      olt_core_reset_o   : out std_logic;

      -- Control AXI --
      s_olt_user_axi_aclk : in  std_logic;
      ctrl_reg_i          : in  t_regbank32b(127 downto 0);
      stat_reg_o          : out t_regbank32b(127 downto 0);
      ---------------------------------

      -- Others ----------
      tx_match_flag_o : out std_logic
      ----------------------------------
      );
  end component olt_user_logic_exdsg;

--============================================================================
--! Architecture begin for olt_user_axi_ip_v1_0
--============================================================================
begin

  -- Instantiation of Axi Bus Interface S_OLT_USER_AXI
  olt_user_axi_ip_v1_0_S_OLT_USER_AXI_inst : olt_user_axi_ip_v1_0_S_OLT_USER_AXI
    generic map (
      C_S_AXI_DATA_WIDTH => C_S_OLT_USER_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => C_S_OLT_USER_AXI_ADDR_WIDTH
      )
    port map (

      -- Control
      slv_reg_ctrl_o => slv_reg_ctrl,
      -- Status
      slv_reg_stat_i => slv_reg_stat,

      S_AXI_ACLK    => s_olt_user_axi_aclk,
      S_AXI_ARESETN => s_olt_user_axi_aresetn,
      S_AXI_AWADDR  => s_olt_user_axi_awaddr,
      S_AXI_AWPROT  => s_olt_user_axi_awprot,
      S_AXI_AWVALID => s_olt_user_axi_awvalid,
      S_AXI_AWREADY => s_olt_user_axi_awready,
      S_AXI_WDATA   => s_olt_user_axi_wdata,
      S_AXI_WSTRB   => s_olt_user_axi_wstrb,
      S_AXI_WVALID  => s_olt_user_axi_wvalid,
      S_AXI_WREADY  => s_olt_user_axi_wready,
      S_AXI_BRESP   => s_olt_user_axi_bresp,
      S_AXI_BVALID  => s_olt_user_axi_bvalid,
      S_AXI_BREADY  => s_olt_user_axi_bready,
      S_AXI_ARADDR  => s_olt_user_axi_araddr,
      S_AXI_ARPROT  => s_olt_user_axi_arprot,
      S_AXI_ARVALID => s_olt_user_axi_arvalid,
      S_AXI_ARREADY => s_olt_user_axi_arready,
      S_AXI_RDATA   => s_olt_user_axi_rdata,
      S_AXI_RRESP   => s_olt_user_axi_rresp,
      S_AXI_RVALID  => s_olt_user_axi_rvalid,
      S_AXI_RREADY  => s_olt_user_axi_rready
      );

  -- Add user logic here
  cmp_olt_user_logic_exdsg : olt_user_logic_exdsg
    port map(
      -- core connections
      olt_clk_sys_i        => olt_clk_sys_i ,
      olt_core_reset_o     => olt_core_reset_o ,
      olt_interrupt_i      => olt_interrupt_i ,
      olt_status_sticky_i  => olt_status_sticky_i ,
      olt_clk_trxusr240_i  => olt_clk_trxusr240_i ,
      olt_tx_data_o        => olt_tx_data_o ,
      olt_tx_data_strobe_o => olt_tx_data_strobe_o ,
      olt_rx_locked_i      => olt_rx_locked_i,
      olt_rx_data_i        => olt_rx_data_i ,
      olt_rx_onu_addr_i    => olt_rx_onu_addr_i ,
      olt_rx_data_strobe_i => olt_rx_data_strobe_i ,
      olt_rx_data_error_i  => olt_rx_data_error_i ,

      olt_mgt_pll_lock_i => olt_mgt_pll_lock_i ,
      olt_mgt_tx_ready_i => olt_mgt_tx_ready_i ,
      olt_mgt_rx_ready_i => olt_mgt_rx_ready_i ,

      -- Control AXI --
      s_olt_user_axi_aclk => s_olt_user_axi_aclk ,
      ctrl_reg_i          => slv_reg_ctrl ,
      stat_reg_o          => slv_reg_stat ,
      ---------------------------------

      -- Others ----------
      tx_match_flag_o => tx_match_flag_o
      ----------------------------------

      );

  -- User logic ends

end structural;
