--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_olt_axi_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_olt_package_static.all;
use work.pon_olt_package_modifiable.all;
--! Xilinx Packages
library UNISIM;
use UNISIM.vcomponents.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT wrapper with control via AXI4-Lite (pon_olt_axi_wrapper)
--
--! @brief OLT core wrapper interfacing
--  Wrapper interfacing:
--  If user needs to perform another interfacing type, this wrapper can be modified accordingly
--
--  TX interfacing   : olt_core in frame_interfacing mode (see olt_core.vhd) (next step: implement 40MHz fixed latency interfacing similar to GBT as example)
--  RX interfacing   : olt_core rx interfacing
--  CTRL interfacing : AXI4-Lite
--
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 19\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for pon_olt_axi_wrapper
--============================================================================
entity pon_olt_axi_wrapper is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line


    -- Parameters of Axi Slave Bus Interface S_OLT
    C_S00_AXI_DATA_WIDTH : integer := 32;
    C_S00_AXI_ADDR_WIDTH : integer := 9
    );
  port (
    -- AXI interface
    s_olt_axi_aclk    : in  std_logic;
    s_olt_axi_aresetn : in  std_logic;
    s_olt_axi_awaddr  : in  std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s_olt_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_olt_axi_awvalid : in  std_logic;
    s_olt_axi_awready : out std_logic;
    s_olt_axi_wdata   : in  std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s_olt_axi_wstrb   : in  std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s_olt_axi_wvalid  : in  std_logic;
    s_olt_axi_wready  : out std_logic;
    s_olt_axi_bresp   : out std_logic_vector(1 downto 0);
    s_olt_axi_bvalid  : out std_logic;
    s_olt_axi_bready  : in  std_logic;
    s_olt_axi_araddr  : in  std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s_olt_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_olt_axi_arvalid : in  std_logic;
    s_olt_axi_arready : out std_logic;
    s_olt_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s_olt_axi_rresp   : out std_logic_vector(1 downto 0);
    s_olt_axi_rvalid  : out std_logic;
    s_olt_axi_rready  : in  std_logic;

    -- Sys clk / reset
    olt_clk_sys_i    : in std_logic;
    olt_core_reset_i : in std_logic;

    -- mgt 240MHz ref clk
    olt_clk_ref240_i : in std_logic;

    -- Soft-configurable interrupt output
    olt_interrupt_o     : out std_logic;
    olt_status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------

    -- data in/out
    olt_clk_trxusr240_o  : out std_logic;
    olt_tx_data_i        : in  std_logic_vector(199 downto 0);
    olt_tx_data_strobe_i : in  std_logic;

    olt_rx_locked_o      : out std_logic;
    olt_rx_data_o        : out std_logic_vector(55 downto 0);
    olt_rx_onu_addr_o    : out std_logic_vector(7 downto 0);
    olt_rx_data_strobe_o : out std_logic;
    olt_rx_data_error_o  : out std_logic;

    -- status
    olt_mgt_tx_ready_o : out std_logic;
    olt_mgt_rx_ready_o : out std_logic;
    olt_mgt_pll_lock_o : out std_logic;

    -- serial
    olt_tx_p_o         : out std_logic;
    olt_tx_n_o         : out std_logic;
    olt_rx_p_i         : in  std_logic;
    olt_rx_n_i         : in  std_logic;
    olt_sfp_rx_sd_i    : in  std_logic;
    olt_sfp_tx_fault_i : in  std_logic;
    olt_sfp_mod_abs_i  : in  std_logic;
    olt_sfp_rx_reset_o : out std_logic;
    olt_sfp_rssi_tri_o : out std_logic;
    olt_sfp_tx_dis_o   : out std_logic;

    -- i2c interfacing --
    olt_i2c_sda_io : inout std_logic;
    olt_i2c_scl_io : inout std_logic
    -------------------------             
    );
end entity pon_olt_axi_wrapper;


--============================================================================
--! Architecture declaration for pon_olt_axi_wrapper
--============================================================================
architecture structural of pon_olt_axi_wrapper is

  --! Functions

  --! Constants

  --! Signal declaration
  signal axi_stat_reg        : t_regbank32b(127 downto 0);
  signal axi_ctrl_reg        : std_logic_vector(31 downto 0);
  signal axi_ctrl_reg_strobe : std_logic_vector(127 downto 0);

  -- global from olt_mgt                                                                                         
  signal olt_mgt_clk_trxusr240 : std_logic;
  signal olt_mgt_tx_ready      : std_logic;
  signal olt_mgt_rx_ready      : std_logic;
  signal olt_mgt_pll_lock      : std_logic;

  -- olt_core <-> olt_mgt_wrapper      
  signal olt_core_to_mgt_drp_wr        : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_drp_wr_strobe : std_logic;
  signal olt_mgt_to_core_drp_monitor   : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_rx_phase_ctrl : std_logic_vector(31 downto 0);
  signal olt_mgt_to_core_rx_phase_stat : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_tx_phase_ctrl : std_logic_vector(31 downto 0);
  signal olt_mgt_to_core_tx_phase_stat : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_tx_pol        : std_logic;
  signal olt_core_to_mgt_rx_pol        : std_logic;
  signal olt_core_to_mgt_tx_data       : std_logic_vector(39 downto 0);
  signal olt_mgt_to_core_rx_data       : std_logic_vector(39 downto 0);
  signal olt_core_to_mgt_reset         : std_logic;

  -- olt_core <-> i2c_master
  signal olt_core_to_i2c_master_i2c_req      : std_logic;
  signal olt_core_to_i2c_master_i2c_rnw      : std_logic;
  signal olt_core_to_i2c_master_i2c_rb2      : std_logic;
  signal olt_core_to_i2c_master_i2c_slv_addr : std_logic_vector(6 downto 0);
  signal olt_core_to_i2c_master_i2c_reg_addr : std_logic_vector(7 downto 0);
  signal olt_core_to_i2c_master_i2c_wr_data  : std_logic_vector(7 downto 0);

  signal i2c_master_to_olt_core_i2c_done     : std_logic;
  signal i2c_master_to_olt_core_i2c_error    : std_logic;
  signal i2c_master_to_olt_core_i2c_drop_req : std_logic;
  signal i2c_master_to_olt_core_i2c_rd_data  : std_logic_vector(15 downto 0);

  -- i2c_master
  signal i2c_master_sdi     : std_logic;
  signal i2c_master_sdo     : std_logic;
  signal i2c_master_sda_ena : std_logic;
  signal i2c_master_scl_ena : std_logic;
  signal i2c_master_scl     : std_logic;

  signal ni2c_master_sda_ena : std_logic;
  signal ni2c_master_scl_ena : std_logic;

  --! Component declaration    
  component olt_core is
    port (
      -- global input signals --
      clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      core_reset_i : in std_logic;      --! Sync to clk_sys_i
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         : in  std_logic;
      manager_stat_reg_o    : out t_regbank32b(c_OLT_NBR_CTRL_REGS-1 downto 0);
      manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      manager_ctrl_strobe_i : in  std_logic_vector(c_OLT_NBR_CTRL_REGS-1 downto 0);
      --------------------------

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------

      -- data user in/out -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_i        : in std_logic_vector(c_OLT_TX_USR_DATA_WIDTH-1 downto 0);
      tx_data_strobe_i : in std_logic;

      rx_locked_o      : out std_logic;
      rx_data_o        : out std_logic_vector(c_OLT_RX_USR_DATA_WIDTH-1 downto 0);
      rx_onu_addr_o    : out std_logic_vector(7 downto 0);
      rx_data_strobe_o : out std_logic;
      rx_data_error_o  : out std_logic;
      --------------------------

      -- MGT connections --
      -- Clocks
      clk_trxusr240_i : in std_logic;   --! MGT txusr clock

      -- Status/Ctrl
      mgt_reset_o  : out std_logic;
      mgt_tx_pol_o : out std_logic;
      mgt_rx_pol_o : out std_logic;

      -- Xilinx only
      mgt_drp_wr_o        : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o : out std_logic;
      mgt_drp_monitor_i   : in  std_logic_vector(31 downto 0);
      mgt_rx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_phase_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i : in  std_logic_vector(31 downto 0);

      mgt_tx_ready_i : in std_logic;
      mgt_rx_ready_i : in std_logic;
      mgt_pll_lock_i : in std_logic;

      -- Data
      mgt_tx_data_o : out std_logic_vector(c_OLT_TX_WORD_WIDTH-1 downto 0);
      mgt_rx_data_i : in  std_logic_vector(c_OLT_RX_WORD_WIDTH-1 downto 0);
      -------------------------

      -- transceiver ctrl/stat connections --
      sfp_rx_sd_i    : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_rx_reset_o : out std_logic;
      sfp_rssi_tri_o : out std_logic;
      sfp_tx_dis_o   : out std_logic;
      -------------------------    

      -- i2c interfacing --
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------
      );
  end component olt_core;

  component olt_mgt_wrapper is
    port (
      -- global input signals --        
      clk_sys_i   : in std_logic;
      clk_ref_i   : in std_logic;
      mgt_reset_i : in std_logic;
      -------------------------

      -- global output signals --               
      clk_trxusr_o : out std_logic;
      -------------------------

      -- status/control --
      drp_wr_i            : in  std_logic_vector(31 downto 0);
      drp_wr_strobe_i     : in  std_logic;
      drp_monitor_o       : out std_logic_vector(31 downto 0);
      mgt_rx_phase_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_rx_phase_stat_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_o : out std_logic_vector(31 downto 0);

      txpolarity_i   : in  std_logic;
      rxpolarity_i   : in  std_logic;
      txfsmrstdone_o : out std_logic;
      rxfsmrstdone_o : out std_logic;
      pll_lock_o     : out std_logic;
      ------------------------- 

      -- data in/out --         
      tx_data_i : in  std_logic_vector(39 downto 0);
      rx_data_o : out std_logic_vector(39 downto 0);

      rx_p_i : in  std_logic;
      rx_n_i : in  std_logic;
      tx_p_o : out std_logic;
      tx_n_o : out std_logic
      ------------------------- 
      );
  end component olt_mgt_wrapper;

  component i2c_interface is
    generic (
      g_CLOCK_PERIOD : integer range 5 to 20 := 10;    -- clock period in ns
      g_SCL_PERIOD   : integer               := 10000  -- SCL period in ns
      );
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      : in std_logic;
      i2c_rnw_i      : in std_logic;
      i2c_rb2_i      : in std_logic;
      i2c_slv_addr_i : in std_logic_vector(6 downto 0);
      i2c_reg_addr_i : in std_logic_vector(7 downto 0);
      i2c_wr_data_i  : in std_logic_vector(7 downto 0);

      i2c_done_o     : out std_logic;
      i2c_error_o    : out std_logic;
      i2c_drop_req_o : out std_logic;
      i2c_rd_data_o  : out std_logic_vector(15 downto 0);
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     : in  std_logic;
      i2c_sdo_o     : out std_logic;
      i2c_sda_ena_o : out std_logic;
      i2c_scl_o     : out std_logic;
      i2c_scl_ena_o : out std_logic
      -------------------------
      );

  end component i2c_interface;

  component olt_control_axi_wrapper is
    generic (
      -- Users to add parameters here

      -- User parameters ends
      -- Do not modify the parameters beyond this line

      -- Parameters of Axi Slave Bus Interface olt_AXI
      C_OLT_AXI_DATA_WIDTH : integer := 32;
      C_OLT_AXI_ADDR_WIDTH : integer := 9
      );
    port (
      -- Users to add ports here
      stat_reg_i        : in  t_regbank32b(127 downto 0);
      ctrl_reg_o        : out std_logic_vector(31 downto 0);
      ctrl_reg_strobe_o : out std_logic_vector(127 downto 0);

      -- User ports ends
      -- Do not modify the ports beyond this line

      -- Ports of Axi Slave Bus Interface olt_AXI
      olt_axi_aclk    : in  std_logic;
      olt_axi_aresetn : in  std_logic;
      olt_axi_awaddr  : in  std_logic_vector(C_OLT_AXI_ADDR_WIDTH-1 downto 0);
      olt_axi_awprot  : in  std_logic_vector(2 downto 0);
      olt_axi_awvalid : in  std_logic;
      olt_axi_awready : out std_logic;
      olt_axi_wdata   : in  std_logic_vector(C_OLT_AXI_DATA_WIDTH-1 downto 0);
      olt_axi_wstrb   : in  std_logic_vector((C_OLT_AXI_DATA_WIDTH/8)-1 downto 0);
      olt_axi_wvalid  : in  std_logic;
      olt_axi_wready  : out std_logic;
      olt_axi_bresp   : out std_logic_vector(1 downto 0);
      olt_axi_bvalid  : out std_logic;
      olt_axi_bready  : in  std_logic;
      olt_axi_araddr  : in  std_logic_vector(C_OLT_AXI_ADDR_WIDTH-1 downto 0);
      olt_axi_arprot  : in  std_logic_vector(2 downto 0);
      olt_axi_arvalid : in  std_logic;
      olt_axi_arready : out std_logic;
      olt_axi_rdata   : out std_logic_vector(C_OLT_AXI_DATA_WIDTH-1 downto 0);
      olt_axi_rresp   : out std_logic_vector(1 downto 0);
      olt_axi_rvalid  : out std_logic;
      olt_axi_rready  : in  std_logic
      );
  end component olt_control_axi_wrapper;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component olt_core
  --============================================================================   
  cmp_olt_core : olt_core
    port map(
      -- global input signals --
      clk_sys_i    => olt_clk_sys_i,
      core_reset_i => olt_core_reset_i,
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         => s_olt_axi_aclk,
      manager_stat_reg_o    => axi_stat_reg,
      manager_ctrl_reg_i    => axi_ctrl_reg,
      manager_ctrl_strobe_i => axi_ctrl_reg_strobe,
      --------------------------

      -- Soft-configurable interrupt output
      interrupt_o     => olt_interrupt_o,
      status_sticky_o => olt_status_sticky_o,
      -------------------------         

      -- data user in/out --
      tx_data_i        => olt_tx_data_i,
      tx_data_strobe_i => olt_tx_data_strobe_i,

      rx_locked_o      => olt_rx_locked_o,
      rx_data_o        => olt_rx_data_o,
      rx_onu_addr_o    => olt_rx_onu_addr_o,
      rx_data_strobe_o => olt_rx_data_strobe_o,
      rx_data_error_o  => olt_rx_data_error_o,
      --------------------------

      -- MGT connections --
      -- Clocks
      clk_trxusr240_i => olt_mgt_clk_trxusr240,

      -- Status/Ctrl
      mgt_reset_o  => olt_core_to_mgt_reset,
      mgt_tx_pol_o => olt_core_to_mgt_tx_pol,
      mgt_rx_pol_o => olt_core_to_mgt_rx_pol,

      mgt_drp_wr_o        => olt_core_to_mgt_drp_wr,
      mgt_drp_wr_strobe_o => olt_core_to_mgt_drp_wr_strobe,
      mgt_drp_monitor_i   => olt_mgt_to_core_drp_monitor,
      mgt_rx_phase_ctrl_o => olt_core_to_mgt_rx_phase_ctrl,
      mgt_rx_phase_stat_i => olt_mgt_to_core_rx_phase_stat,
      mgt_tx_phase_ctrl_o => olt_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_i => olt_mgt_to_core_tx_phase_stat,

      mgt_tx_ready_i => olt_mgt_tx_ready,
      mgt_rx_ready_i => olt_mgt_rx_ready,
      mgt_pll_lock_i => olt_mgt_pll_lock,

      -- Data              
      mgt_tx_data_o => olt_core_to_mgt_tx_data,
      mgt_rx_data_i => olt_mgt_to_core_rx_data,
      -------------------------

      -- transceiver ctrl/stat connections --
      sfp_rx_sd_i    => olt_sfp_rx_sd_i,
      sfp_tx_fault_i => olt_sfp_tx_fault_i,
      sfp_mod_abs_i  => olt_sfp_mod_abs_i,
      sfp_rx_reset_o => olt_sfp_rx_reset_o,
      sfp_rssi_tri_o => olt_sfp_rssi_tri_o,
      sfp_tx_dis_o   => olt_sfp_tx_dis_o,
      -------------------------

      -- i2c interfacing --
      i2c_req_o      => olt_core_to_i2c_master_i2c_req,
      i2c_rnw_o      => olt_core_to_i2c_master_i2c_rnw,
      i2c_rb2_o      => olt_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_o => olt_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_o => olt_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_o  => olt_core_to_i2c_master_i2c_wr_data,

      i2c_done_i     => i2c_master_to_olt_core_i2c_done,
      i2c_error_i    => i2c_master_to_olt_core_i2c_error,
      i2c_drop_req_i => i2c_master_to_olt_core_i2c_drop_req,
      i2c_rd_data_i  => i2c_master_to_olt_core_i2c_rd_data
      -------------------------         
      );

  --============================================================================
  -- Component instantiation
  --! Component olt_mgt_wrapper
  --============================================================================   
  cmp_olt_mgt_wrapper : olt_mgt_wrapper
    port map(
      -- global input signals --        
      clk_sys_i           => olt_clk_sys_i,
      clk_ref_i           => olt_clk_ref240_i,
      mgt_reset_i         => olt_core_to_mgt_reset,
      -------------------------
      -- global output signals --               
      clk_trxusr_o        => olt_mgt_clk_trxusr240,
      -------------------------
      -- status/control --
      drp_wr_i            => olt_core_to_mgt_drp_wr,
      drp_wr_strobe_i     => olt_core_to_mgt_drp_wr_strobe,
      drp_monitor_o       => olt_mgt_to_core_drp_monitor,
      mgt_rx_phase_ctrl_i => olt_core_to_mgt_rx_phase_ctrl,
      mgt_rx_phase_stat_o => olt_mgt_to_core_rx_phase_stat,
      mgt_tx_phase_ctrl_i => olt_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_o => olt_mgt_to_core_tx_phase_stat,

      txpolarity_i   => olt_core_to_mgt_tx_pol,
      rxpolarity_i   => olt_core_to_mgt_rx_pol,
      txfsmrstdone_o => olt_mgt_tx_ready,
      rxfsmrstdone_o => olt_mgt_rx_ready,
      pll_lock_o     => olt_mgt_pll_lock,
      ------------------------- 

      -- data in/out --         
      tx_data_i => olt_core_to_mgt_tx_data,
      rx_data_o => olt_mgt_to_core_rx_data,

      rx_p_i => olt_rx_p_i,
      rx_n_i => olt_rx_n_i,
      tx_p_o => olt_tx_p_o,
      tx_n_o => olt_tx_n_o
      ------------------------- 
      );

  --============================================================================
  -- Component instantiation
  --! Component i2c_interface (i2c_master)
  --============================================================================   
  cmp_i2c_interface : i2c_interface
    generic map(
      g_CLOCK_PERIOD => c_OLT_PERIOD_SYS_CLK,
      g_SCL_PERIOD   => c_OLT_SCL_PERIOD
      )
    port map(
      -- global input signals --
      clk_i   => olt_clk_sys_i,
      reset_i => olt_core_reset_i,
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      => olt_core_to_i2c_master_i2c_req,
      i2c_rnw_i      => olt_core_to_i2c_master_i2c_rnw,
      i2c_rb2_i      => olt_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_i => olt_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_i => olt_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_i  => olt_core_to_i2c_master_i2c_wr_data,

      i2c_done_o     => i2c_master_to_olt_core_i2c_done,
      i2c_error_o    => i2c_master_to_olt_core_i2c_error,
      i2c_drop_req_o => i2c_master_to_olt_core_i2c_drop_req,
      i2c_rd_data_o  => i2c_master_to_olt_core_i2c_rd_data,
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     => i2c_master_sdi,
      i2c_sdo_o     => i2c_master_sdo,
      i2c_sda_ena_o => i2c_master_sda_ena,
      i2c_scl_o     => i2c_master_scl,
      i2c_scl_ena_o => i2c_master_scl_ena
      -------------------------
      );

  ni2c_master_sda_ena <= not i2c_master_sda_ena;
  ni2c_master_scl_ena <= not i2c_master_scl_ena;

  -- IOBUF: Simple Bi-directional Buffer
  -- UltraScale
  -- Xilinx HDL Libraries Guide, version 2014.1
  IOBUF_inst : IOBUF
    port map (
      O  => i2c_master_sdi,             -- 1-bit output: Buffer output
      I  => i2c_master_sdo,             -- 1-bit input: Buffer input
      IO => olt_i2c_sda_io,  -- 1-bit inout: Buffer inout (connect directly to top-level port)
      T  => ni2c_master_sda_ena         -- 1-bit input: 3-state enable input
      );
  -- End of IOBUF_inst instantiation

  -- OBUFT: Simple 3-state Output Buffer
  -- UltraScale
  -- Xilinx HDL Libraries Guide, version 2014.1
  OBUFT_inst : OBUFT
    port map (
      O => olt_i2c_scl_io,  -- 1-bit output: Buffer output (connect directly to top-level port)
      I => i2c_master_scl,              -- 1-bit input: Buffer input
      T => ni2c_master_scl_ena          -- 1-bit input: 3-state enable input
      );
  -- End of OBUFT_inst instantiation    


  --============================================================================
  -- Component instantiation
  --! Component olt_control_axi_wrapper
  --============================================================================          
  cmp_olt_control_axi_wrapper : olt_control_axi_wrapper
    generic map(
      -- Users to add parameters here

      -- User parameters ends
      -- Do not modify the parameters beyond this line

      -- Parameters of Axi Slave Bus Interface olt_AXI
      C_OLT_AXI_DATA_WIDTH => C_S00_AXI_DATA_WIDTH,
      C_OLT_AXI_ADDR_WIDTH => C_S00_AXI_ADDR_WIDTH
      )
    port map(
      -- Users to add ports here
      stat_reg_i        => axi_stat_reg,
      ctrl_reg_o        => axi_ctrl_reg,
      ctrl_reg_strobe_o => axi_ctrl_reg_strobe,

      -- User ports ends
      -- Do not modify the ports beyond this line

      -- Ports of Axi Slave Bus Interface olt_AXI
      olt_axi_aclk    => s_olt_axi_aclk ,
      olt_axi_aresetn => s_olt_axi_aresetn ,
      olt_axi_awaddr  => s_olt_axi_awaddr ,
      olt_axi_awprot  => s_olt_axi_awprot ,
      olt_axi_awvalid => s_olt_axi_awvalid ,
      olt_axi_awready => s_olt_axi_awready ,
      olt_axi_wdata   => s_olt_axi_wdata ,
      olt_axi_wstrb   => s_olt_axi_wstrb ,
      olt_axi_wvalid  => s_olt_axi_wvalid ,
      olt_axi_wready  => s_olt_axi_wready ,
      olt_axi_bresp   => s_olt_axi_bresp ,
      olt_axi_bvalid  => s_olt_axi_bvalid ,
      olt_axi_bready  => s_olt_axi_bready ,
      olt_axi_araddr  => s_olt_axi_araddr ,
      olt_axi_arprot  => s_olt_axi_arprot ,
      olt_axi_arvalid => s_olt_axi_arvalid ,
      olt_axi_arready => s_olt_axi_arready ,
      olt_axi_rdata   => s_olt_axi_rdata ,
      olt_axi_rresp   => s_olt_axi_rresp ,
      olt_axi_rvalid  => s_olt_axi_rvalid ,
      olt_axi_rready  => s_olt_axi_rready
      );

  --============================================================================
  --Output
  --============================================================================  
  olt_clk_trxusr240_o <= olt_mgt_clk_trxusr240;
  olt_mgt_tx_ready_o  <= olt_mgt_tx_ready;
  olt_mgt_rx_ready_o  <= olt_mgt_rx_ready;
  olt_mgt_pll_lock_o  <= olt_mgt_pll_lock;
  
end structural;
