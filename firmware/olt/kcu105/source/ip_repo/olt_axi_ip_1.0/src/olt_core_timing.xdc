#Input clocks are constrained in top level in order to avoid clock path break

########### Common Constraints ###########
# Mailbox constraining
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_a2b_togflag_reg/C} ] -to [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_b_ready_reg*/D}]
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_a_reg*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_b_reg*/D}] 4.000

set_false_path -to [get_pins -hier -filter {NAME =~ *axi_rdata_reg*/D}] 

# False paths related to synchronizers
set_false_path -to [get_cells -hier -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
set_false_path -to [get_cells -hier -filter {NAME =~ *reset_synchronizer*inst/rst_in_*_reg}]

########### OLT constraints ###########
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/rx_ctrl_data_buf_mngrsync_reg*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/rx_ctrl_data_error_mngrsync_reg*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/*txsync_meta*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/*rxsync_meta*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/*syssync_meta*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/*mngrsync_meta*/D}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_control/*mngrsync_meta*/CE}]
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_olt_phy_control/*syssync_meta*/D}]

########### Tx/Rx Phase Aligner Constraints ###########
# Synchronizers internal to tx_phase_aligner
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_tx_phase_aligner/*meta*/D}]

# Latched with a done signal
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_o*/D}]

# Reset fifo fill pd after changing value of phase_detector_max from FSM
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/hits_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/done_reg/D}]

# Synchronizers internal to rx_phase_aligner
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_rx_phase_aligner/*meta*/D}]

# Latched with a done signal
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_rx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_o*/D}]

# Reset fifo fill pd after changing value of phase_detector_max from FSM
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_rx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_rx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_fifo_fill_level_acc/hits_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_rx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_rx_phase_aligner/cmp_fifo_fill_level_acc/done_reg/D}]
