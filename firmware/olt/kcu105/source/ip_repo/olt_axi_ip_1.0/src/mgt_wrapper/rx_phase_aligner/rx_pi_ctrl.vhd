--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file rx_pi_ctrl.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, HPTD
-- --
-------------------------------------------------------------------------------
--
-- unit name: Rx Phase Interpolator Controller (rx_pi_ctrl)
--
--! @brief Receiver phase interpolator controller for GTH/GTY (Ultrascale/Ultrascale plus - UG576 and UG578)
--! - This block provides a simple interface for controlling the phase interpolator of Xilinx devices
--! - The control of receiver phase interpolator only makes sense in the case where the CDR is not activated
--! - The control can only be made via DRP
--! - The address of DRP control could be potentially different for different devices, check your user guide for more information
--!   This address corresponds to the parameter RXCDR_CFG3 
--!   Default is the address of a GTH Ultrascale as this is what was tested
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 31\10\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 31\10\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for rx_pi_ctrl
--==============================================================================
entity rx_pi_ctrl is
  generic(
    g_DRP_ADDR_RXCDR_CFG3  : std_logic_vector(8 downto 0) := ("000010001");  --! Check the transceiver user guide of your device for this address
    g_RATIO_RXFREQ_SYSFREQ : integer                      := 10  --! This parameter can be calculated as RXUSRCLK2_FREQ / CLKSYS_FREQ, it is recommended to keep to at least 5 for safety reasons
    );
  port (
    -- User Interface   
    clk_sys_i    : in  std_logic;       --! system clock input
    reset_i      : in  std_logic;       --! active high sync. reset
    strobe_i     : in  std_logic;  --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
    inc_ndec_i   : in  std_logic;  --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
    phase_step_i : in  std_logic_vector(3 downto 0);  --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)       
    done_o       : out std_logic;  --! pulse synchronous to clk_sys_i to indicate a phase shift was performed
    phase_o      : out std_logic_vector(6 downto 0);  --! phase shift accumulated

    -- MGT interface                                      
    -- Receiver PI ports - see Xilinx transceiver User Guide for more information
    rxcdrhold_o   : out std_logic;      --! CDR hold       
    rxcdrovrden_o : out std_logic;      --! CDR override

    -- DRP interface - see Xilinx transceiver User Guide for more information
    -- obs1: connect clk_sys_i to drpclk        
    drpaddr_o : out std_logic_vector(8 downto 0);  --! For devices with a 10-bit DRP address interface, connect MSB to '0'
    drpen_o   : out std_logic;          --! DRP enable transaction
    drpdi_o   : out std_logic_vector(15 downto 0);  --! DRP data write
    drprdy_i  : in  std_logic;          --! DRP finished transaction
    drpdo_i   : in  std_logic_vector(15 downto 0);  --! DRP data read; not used nowadays, write only interface
    drpwe_o   : out std_logic           --! DRP write enable
    );
end rx_pi_ctrl;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of rx_pi_ctrl is

  --! Attribute declaration

  --! Constant declaration

  --! Signal declaration  
  -- ==============================================================================
  -- =============================== DRP interface ================================
  -- ==============================================================================
  -- phase accumulator
  signal phase_acc : unsigned(phase_o'range);
  signal strobe_r  : std_logic;         --rising edge detector for strobe

  -- CDR control
  signal rxcdrovrden : std_logic;
  signal rxcdrhold   : std_logic;

  -- FSM to control Rx PI via DRP control
  -- obs: Two write a new phase value for the receiver PI via DRP:
  --        (1) Write new value to bits 9:3 of RXCDR_CFG3 - Wait at least one rxusrclk (WAIT_RXUSRCLK0)
  --        (2) HOLD = 1, OVR=1 - Wait at least one rxusrclk
  --        (3) HOLD = 0, OVR=1 - Wait at least one rxusrclk
  --        (4) HOLD = 1, OVR=1 - Wait at least one rxusrclk
  --        (5) HOLD = 1, OVR=0 - Wait at least one rxusrclk      
  type   t_DRP_RX_PI_FSM is (IDLE, PHASE_ACCU, REGISTER_PHASE_DRP, WAIT_REGISTER_PHASE_DRP, WAIT_AFTER_DRP_REGISTER, CDR_PRE_HOLD1_OVR1, CDR_HOLD0_OVR1, CDR_HOLD1_OVR1, CDR_HOLD1_OVR0, DONE_DRP);
  signal drp_rx_pi_state : t_DRP_RX_PI_FSM;

begin

  -- =============================================================================
  -- ==================================== DRP interface ==========================
  -- =============================================================================

  --============================================================================
  -- Process p_strobe_r
  --!  Delays strobe
  --! read:  strobe_i\n
  --! write: strobe_r\n
  --! r/w:   \n
  --============================================================================  
  p_strobe_r : process(clk_sys_i)
  begin
    if(rising_edge(clk_sys_i)) then
      strobe_r <= strobe_i;
    end if;
  end process p_strobe_r;

  --============================================================================
  -- Process p_drp_tx_pi_fsm
  --! FSM for Tx PI control via DRP
  --! read:  strobe_i, strobe_r, drprdy_i\n
  --! write: -\n
  --! r/w:   drp_rx_pi_state\n
  --============================================================================  
  p_drp_tx_pi_fsm : process(clk_sys_i)
    variable v_wait_register_cntr : integer range 0 to g_RATIO_RXFREQ_SYSFREQ;
  begin
    if(rising_edge(clk_sys_i)) then
      if(reset_i = '1') then
        drp_rx_pi_state      <= IDLE;
        v_wait_register_cntr := 0;
      else
        case drp_rx_pi_state is
          when IDLE =>
            if(strobe_i = '1' and strobe_r = '0') then
              drp_rx_pi_state <= PHASE_ACCU;
            end if;
            v_wait_register_cntr := 0;
          when PHASE_ACCU =>
            drp_rx_pi_state      <= REGISTER_PHASE_DRP;
            v_wait_register_cntr := 0;
          when REGISTER_PHASE_DRP =>
            drp_rx_pi_state      <= WAIT_REGISTER_PHASE_DRP;
            v_wait_register_cntr := 0;
          when WAIT_REGISTER_PHASE_DRP =>
            if(drprdy_i = '1') then
              drp_rx_pi_state <= WAIT_AFTER_DRP_REGISTER;
            end if;
            v_wait_register_cntr := 0;
          when WAIT_AFTER_DRP_REGISTER =>
            if(v_wait_register_cntr = g_RATIO_RXFREQ_SYSFREQ) then
              drp_rx_pi_state      <= CDR_PRE_HOLD1_OVR1;
              v_wait_register_cntr := 0;
            else
              v_wait_register_cntr := v_wait_register_cntr + 1;
            end if;
          when CDR_PRE_HOLD1_OVR1 =>
            if(v_wait_register_cntr = g_RATIO_RXFREQ_SYSFREQ) then
              drp_rx_pi_state      <= CDR_HOLD0_OVR1;
              v_wait_register_cntr := 0;
            else
              v_wait_register_cntr := v_wait_register_cntr + 1;
            end if;
          when CDR_HOLD0_OVR1 =>
            if(v_wait_register_cntr = g_RATIO_RXFREQ_SYSFREQ) then
              drp_rx_pi_state      <= CDR_HOLD1_OVR1;
              v_wait_register_cntr := 0;
            else
              v_wait_register_cntr := v_wait_register_cntr + 1;
            end if;
          when CDR_HOLD1_OVR1 =>
            if(v_wait_register_cntr = g_RATIO_RXFREQ_SYSFREQ) then
              drp_rx_pi_state      <= CDR_HOLD1_OVR0;
              v_wait_register_cntr := 0;
            else
              v_wait_register_cntr := v_wait_register_cntr + 1;
            end if;
          when CDR_HOLD1_OVR0 =>
            if(v_wait_register_cntr = g_RATIO_RXFREQ_SYSFREQ) then
              drp_rx_pi_state      <= DONE_DRP;
              v_wait_register_cntr := 0;
            else
              v_wait_register_cntr := v_wait_register_cntr + 1;
            end if;
          when DONE_DRP =>
            drp_rx_pi_state <= IDLE;
          when others =>
            drp_rx_pi_state      <= IDLE;
            v_wait_register_cntr := 0;
        end case;
      end if;
    end if;
  end process p_drp_tx_pi_fsm;

  -- Tie static DRP signals
  drpaddr_o             <= g_DRP_ADDR_RXCDR_CFG3;
  drpdi_o(15 downto 10) <= (others => '0');
  drpdi_o(2 downto 0)   <= (others => '0');

  -- DRP signals controlled via FSM
  drpdi_o(9 downto 3) <= std_logic_vector(phase_acc);
  drpen_o             <= '1' when (drp_rx_pi_state = REGISTER_PHASE_DRP) else '0';
  drpwe_o             <= '1' when (drp_rx_pi_state = REGISTER_PHASE_DRP) else '0';

  rxcdrovrden   <= '1'         when (drp_rx_pi_state = CDR_PRE_HOLD1_OVR1 or drp_rx_pi_state = CDR_HOLD0_OVR1 or drp_rx_pi_state = CDR_HOLD1_OVR1) else '0';
  rxcdrhold     <= '0'         when (drp_rx_pi_state = CDR_HOLD0_OVR1)                                                                             else '1';
  rxcdrovrden_o <= rxcdrovrden when rising_edge(clk_sys_i);
  rxcdrhold_o   <= rxcdrhold   when rising_edge(clk_sys_i);

  --============================================================================
  -- Process p_phase_acc
  --!  Increments or decrements phase accumulator
  --! read:  drp_rx_pi_state, inc_ndec_i, phase_step_i\n
  --! write: \n
  --! r/w:   phase_acc\n
  --============================================================================  
  p_phase_acc : process(clk_sys_i)
  begin
    if(rising_edge(clk_sys_i)) then
      if(reset_i = '1') then
        phase_acc <= to_unsigned(0, phase_acc'length);
      else
        if(drp_rx_pi_state = PHASE_ACCU) then
          if(inc_ndec_i = '1') then
            phase_acc <= phase_acc + unsigned(phase_step_i);
          else
            phase_acc <= phase_acc - unsigned(phase_step_i);
          end if;
        else
          phase_acc <= phase_acc;
        end if;
      end if;
    end if;
  end process p_phase_acc;

  phase_o <= std_logic_vector(phase_acc);
  done_o  <= '1' when (drp_rx_pi_state = DONE_DRP) else '0';

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
