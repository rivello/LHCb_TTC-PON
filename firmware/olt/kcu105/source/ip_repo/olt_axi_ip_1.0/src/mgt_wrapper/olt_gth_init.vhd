--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_gth_init.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT GTH Initialization (olt_gth_init)
--
--! @brief OLT GTH Initialization block
--! <further description>
--!
--! @author Csaba Soos - csaba.soos@cern.ch
--! @date 10\02\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 10\02\2016 - CS - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_gth_init
--============================================================================
entity olt_gth_init is
  generic (
    FREERUN_FREQUENCY    : positive := 100;
    TX_TIMER_DURATION_US : positive := 30000;
    RX_TIMER_DURATION_US : positive := 130000);
  port (
    clk_freerun_in  : in  std_logic;
    reset_all_in    : in  std_logic;
    tx_init_done_in : in  std_logic;
    rx_init_done_in : in  std_logic;
    rx_data_good_in : in  std_logic;
    reset_all_out   : out std_logic;
    reset_rx_out    : out std_logic;
    init_done_out   : out std_logic;
    retry_ctr_out   : out std_logic_vector (3 downto 0));
end olt_gth_init;

--============================================================================
--! Architecture declaration for olt_gth_init
--============================================================================
architecture rtl of olt_gth_init is

  component reset_synchronizer
    port (
      clk_in  : in  std_logic;
      rst_in  : in  std_logic;
      rst_out : out std_logic
      );
  end component;
  component bit_synchronizer
    port (
      clk_in : in  std_logic;
      i_in   : in  std_logic;
      o_out  : out std_logic
      );
  end component;

  signal reset_all_sync    : std_logic;
  signal tx_init_done_sync : std_logic;
  signal rx_init_done_sync : std_logic;
  signal rx_data_good_sync : std_logic;
  signal timer_clr         : std_logic                  := '1';
  signal timer_ctr         : integer range 0 to 2**25-1 := 0;
  signal tx_timer_sat      : std_logic                  := '0';
  signal rx_timer_sat      : std_logic                  := '0';
  signal retry_ctr_incr    : std_logic;
  signal retry_ctr         : integer range 0 to 15      := 0;

  type   t_sm_states is (ST_START, ST_TX_WAIT, ST_RX_WAIT, ST_MONITOR);
  signal sm_init        : t_sm_states := ST_START;
  signal sm_init_active : std_logic;

--============================================================================
--! Architecture begin for olt_gth_init
--============================================================================
begin

  reset_synchronizer_reset_all_inst : reset_synchronizer
    port map (
      clk_in  => clk_freerun_in,
      rst_in  => reset_all_in,
      rst_out => reset_all_sync);


  bit_synchronizer_tx_init_done_inst : bit_synchronizer
    port map (
      clk_in => clk_freerun_in,
      i_in   => tx_init_done_in,
      o_out  => tx_init_done_sync);

  bit_synchronizer_rx_init_done_inst : bit_synchronizer
    port map (
      clk_in => clk_freerun_in,
      i_in   => rx_init_done_in,
      o_out  => rx_init_done_sync);

  bit_synchronizer_rx_data_good_inst : bit_synchronizer
    port map (
      clk_in => clk_freerun_in,
      i_in   => rx_data_good_in,
      o_out  => rx_data_good_sync);

  p_timers : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (timer_clr = '1') then
        timer_ctr    <= 0;
        tx_timer_sat <= '0';
        rx_timer_sat <= '0';
      else
        if (timer_ctr = (TX_TIMER_DURATION_US * FREERUN_FREQUENCY)) then
          tx_timer_sat <= '1';
        end if;
        if (timer_ctr = (RX_TIMER_DURATION_US * FREERUN_FREQUENCY)) then
          rx_timer_sat <= '1';
        else
          timer_ctr <= timer_ctr + 1;
        end if;
        
      end if;
    end if;
  end process;

  p_retry_ctr : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (retry_ctr_incr = '1' and retry_ctr < 15) then
        retry_ctr <= retry_ctr + 1;
      end if;
    end if;
  end process;
  retry_ctr_out <= std_logic_vector(to_unsigned(retry_ctr, 4));

  p_init_fsm : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (reset_all_sync = '1') then
        timer_clr      <= '1';
        reset_all_out  <= '0';
        reset_rx_out   <= '0';
        retry_ctr_incr <= '0';
        init_done_out  <= '0';
        sm_init_active <= '1';
        sm_init        <= ST_START;
      else
        case (sm_init) is
          
          when ST_START =>
            if (sm_init_active = '1') then
              timer_clr      <= '1';
              reset_all_out  <= '0';
              reset_rx_out   <= '0';
              retry_ctr_incr <= '0';
              sm_init        <= ST_TX_WAIT;
            end if;
            
          when ST_TX_WAIT =>
            if (tx_init_done_sync = '1') then
              timer_clr <= '1';
              sm_init   <= ST_RX_WAIT;
            elsif (tx_timer_sat = '1') then
              timer_clr      <= '1';
              reset_all_out  <= '1';
              retry_ctr_incr <= '1';
              sm_init        <= ST_START;
            else
              timer_clr <= '0';
            end if;
            
          when ST_RX_WAIT =>
            if (rx_timer_sat = '1') then
              if (rx_init_done_sync = '1' and rx_data_good_sync = '1') then
                init_done_out <= '1';
                sm_init       <= ST_MONITOR;
              else
                timer_clr      <= '1';
                reset_rx_out   <= '1';
                retry_ctr_incr <= '1';
                sm_init        <= ST_START;
              end if;
            else
              timer_clr <= '0';
            end if;
            
          when ST_MONITOR =>
            if (rx_init_done_sync = '0' or rx_data_good_sync = '0') then
              init_done_out  <= '0';
              timer_clr      <= '1';
              reset_rx_out   <= '1';
              retry_ctr_incr <= '1';
              sm_init        <= ST_START;
            end if;
            
          when others =>
            sm_init <= ST_START;
            
        end case;
      end if;
    end if;
  end process;
end rtl;
