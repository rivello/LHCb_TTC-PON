
################################################################
# This is a generated script based on design: system
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source system_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcku040-ffva1156-2-e
   set_property BOARD_PART xilinx.com:kcu105:part0:1.1 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name system

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

  # Add USER_COMMENTS on $design_name
  set_property USER_COMMENTS.comment_0 "OLT Example design" [get_bd_designs $design_name]

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:util_ds_buf:2.1\
cern.ch:user:olt_axi_ip:1.0\
cern.ch:user:olt_user_axi_ip:1.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:ip:clk_wiz:6.0\
xilinx.com:ip:jtag_axi:1.2\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: hier_jtag_axi_master_exdsg
proc create_hier_cell_hier_jtag_axi_master_exdsg { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_jtag_axi_master_exdsg() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M01_AXI


  # Create pins
  create_bd_pin -dir I -from 0 -to 0 -type clk CLK_300MHZ_N
  create_bd_pin -dir I -from 0 -to 0 -type clk CLK_300MHZ_P
  create_bd_pin -dir O -type clk clk_out1
  create_bd_pin -dir O locked

  # Create instance: axi_interconnect_1, and set properties
  set axi_interconnect_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_1 ]
  set_property -dict [ list \
   CONFIG.NUM_MI {2} \
 ] $axi_interconnect_1

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {33.330000000000005} \
   CONFIG.CLKIN2_JITTER_PS {100.0} \
   CONFIG.CLKOUT1_DRIVES {BUFG} \
   CONFIG.CLKOUT1_JITTER {101.475} \
   CONFIG.CLKOUT1_PHASE_ERROR {77.836} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {100} \
   CONFIG.CLKOUT2_DRIVES {Buffer} \
   CONFIG.CLKOUT3_DRIVES {Buffer} \
   CONFIG.CLKOUT4_DRIVES {Buffer} \
   CONFIG.CLKOUT5_DRIVES {Buffer} \
   CONFIG.CLKOUT6_DRIVES {Buffer} \
   CONFIG.CLKOUT7_DRIVES {Buffer} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {Custom} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {10.000} \
   CONFIG.MMCM_CLKIN1_PERIOD {3.333} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {10.000} \
   CONFIG.MMCM_DIVCLK_DIVIDE {3} \
   CONFIG.PHASESHIFT_MODE {WAVEFORM} \
   CONFIG.PRIM_IN_FREQ {300} \
   CONFIG.PRIM_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.RESET_PORT {resetn} \
   CONFIG.RESET_TYPE {ACTIVE_LOW} \
   CONFIG.SECONDARY_IN_FREQ {100.000} \
   CONFIG.SECONDARY_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.USE_BOARD_FLOW {true} \
   CONFIG.USE_INCLK_SWITCHOVER {false} \
   CONFIG.USE_PHASE_ALIGNMENT {true} \
 ] $clk_wiz_0

  # Create instance: jtag_axi_0, and set properties
  set jtag_axi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:jtag_axi:1.2 jtag_axi_0 ]
  set_property -dict [ list \
   CONFIG.PROTOCOL {2} \
 ] $jtag_axi_0

  # Create instance: util_ds_buf_1, and set properties
  set util_ds_buf_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_1 ]

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_pins M00_AXI] [get_bd_intf_pins axi_interconnect_1/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_1_M01_AXI [get_bd_intf_pins M01_AXI] [get_bd_intf_pins axi_interconnect_1/M01_AXI]
  connect_bd_intf_net -intf_net jtag_axi_0_M_AXI [get_bd_intf_pins axi_interconnect_1/S00_AXI] [get_bd_intf_pins jtag_axi_0/M_AXI]

  # Create port connections
  connect_bd_net -net CLK_125MHZ_N_1 [get_bd_pins CLK_300MHZ_N] [get_bd_pins util_ds_buf_1/IBUF_DS_N]
  connect_bd_net -net CLK_125MHZ_P_A_1 [get_bd_pins CLK_300MHZ_P] [get_bd_pins util_ds_buf_1/IBUF_DS_P]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins clk_out1] [get_bd_pins axi_interconnect_1/ACLK] [get_bd_pins axi_interconnect_1/M00_ACLK] [get_bd_pins axi_interconnect_1/M01_ACLK] [get_bd_pins axi_interconnect_1/S00_ACLK] [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins jtag_axi_0/aclk]
  connect_bd_net -net util_ds_buf_1_IBUF_OUT [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins util_ds_buf_1/IBUF_OUT]
  connect_bd_net -net xlconstant_0_dout1 [get_bd_pins clk_wiz_0/resetn] [get_bd_pins xlconstant_1/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins locked] [get_bd_pins axi_interconnect_1/ARESETN] [get_bd_pins axi_interconnect_1/M00_ARESETN] [get_bd_pins axi_interconnect_1/M01_ARESETN] [get_bd_pins axi_interconnect_1/S00_ARESETN] [get_bd_pins clk_wiz_0/locked] [get_bd_pins jtag_axi_0/aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set CLK_300MHZ_N [ create_bd_port -dir I -type clk -freq_hz 300000000 CLK_300MHZ_N ]
  set CLK_300MHZ_P [ create_bd_port -dir I -type clk -freq_hz 300000000 CLK_300MHZ_P ]
  set FMC_HPC_DP0_C2M_N [ create_bd_port -dir O FMC_HPC_DP0_C2M_N ]
  set FMC_HPC_DP0_C2M_P [ create_bd_port -dir O FMC_HPC_DP0_C2M_P ]
  set FMC_HPC_DP0_M2C_N [ create_bd_port -dir I FMC_HPC_DP0_M2C_N ]
  set FMC_HPC_DP0_M2C_P [ create_bd_port -dir I FMC_HPC_DP0_M2C_P ]
  set FMC_HPC_GBTCLK0_M2C_C_N [ create_bd_port -dir I FMC_HPC_GBTCLK0_M2C_C_N ]
  set FMC_HPC_GBTCLK0_M2C_C_P [ create_bd_port -dir I FMC_HPC_GBTCLK0_M2C_C_P ]
  set FMC_HPC_LA02_N [ create_bd_port -dir O FMC_HPC_LA02_N ]
  set FMC_HPC_LA02_P [ create_bd_port -dir O FMC_HPC_LA02_P ]
  set FMC_HPC_LA03_N [ create_bd_port -dir I FMC_HPC_LA03_N ]
  set FMC_HPC_LA03_P [ create_bd_port -dir I FMC_HPC_LA03_P ]
  set FMC_HPC_LA04_N [ create_bd_port -dir I FMC_HPC_LA04_N ]
  set FMC_HPC_LA04_P [ create_bd_port -dir O FMC_HPC_LA04_P ]
  set IIC_FMC_GPO [ create_bd_port -dir O -from 1 -to 0 IIC_FMC_GPO ]
  set SMA_GPIO_N_OUT [ create_bd_port -dir O SMA_GPIO_N_OUT ]
  set iic_fmc_scl_io [ create_bd_port -dir IO iic_fmc_scl_io ]
  set iic_fmc_sda_io [ create_bd_port -dir IO iic_fmc_sda_io ]

  # Create instance: hier_jtag_axi_master_exdsg
  create_hier_cell_hier_jtag_axi_master_exdsg [current_bd_instance .] hier_jtag_axi_master_exdsg

  # Create instance: mgt_diff_clk_buffer, and set properties
  set mgt_diff_clk_buffer [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 mgt_diff_clk_buffer ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {IBUFDSGTE} \
 ] $mgt_diff_clk_buffer

  # Create instance: olt_axi_ip_0, and set properties
  set olt_axi_ip_0 [ create_bd_cell -type ip -vlnv cern.ch:user:olt_axi_ip:1.0 olt_axi_ip_0 ]
  set_property -dict [ list \
   CONFIG.C_S00_AXI_ADDR_WIDTH {9} \
 ] $olt_axi_ip_0

  # Create instance: olt_user_axi_ip_0, and set properties
  set olt_user_axi_ip_0 [ create_bd_cell -type ip -vlnv cern.ch:user:olt_user_axi_ip:1.0 olt_user_axi_ip_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {3} \
   CONFIG.CONST_WIDTH {2} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_pins hier_jtag_axi_master_exdsg/M00_AXI] [get_bd_intf_pins olt_user_axi_ip_0/S_OLT_USER_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_1_M01_AXI [get_bd_intf_pins hier_jtag_axi_master_exdsg/M01_AXI] [get_bd_intf_pins olt_axi_ip_0/s_olt_axi]

  # Create port connections
  connect_bd_net -net CLK_125MHZ_N_1 [get_bd_ports CLK_300MHZ_N] [get_bd_pins hier_jtag_axi_master_exdsg/CLK_300MHZ_N]
  connect_bd_net -net CLK_125MHZ_P_A_1 [get_bd_ports CLK_300MHZ_P] [get_bd_pins hier_jtag_axi_master_exdsg/CLK_300MHZ_P]
  connect_bd_net -net FMC_HPC_LA03_N_1 [get_bd_ports FMC_HPC_LA03_N] [get_bd_pins olt_axi_ip_0/olt_sfp_rx_sd_i]
  connect_bd_net -net FMC_HPC_LA03_P1_1 [get_bd_ports FMC_HPC_LA04_N] [get_bd_pins olt_axi_ip_0/olt_sfp_mod_abs_i]
  connect_bd_net -net FMC_HPC_LA03_P_1 [get_bd_ports FMC_HPC_LA03_P] [get_bd_pins olt_axi_ip_0/olt_sfp_tx_fault_i]
  connect_bd_net -net Net2 [get_bd_ports iic_fmc_sda_io] [get_bd_pins olt_axi_ip_0/olt_i2c_sda_io]
  connect_bd_net -net Net3 [get_bd_ports iic_fmc_scl_io] [get_bd_pins olt_axi_ip_0/olt_i2c_scl_io]
  connect_bd_net -net REFCLK_N_IN_1 [get_bd_ports FMC_HPC_GBTCLK0_M2C_C_N] [get_bd_pins mgt_diff_clk_buffer/IBUF_DS_N]
  connect_bd_net -net REFCLK_P_IN_1 [get_bd_ports FMC_HPC_GBTCLK0_M2C_C_P] [get_bd_pins mgt_diff_clk_buffer/IBUF_DS_P]
  connect_bd_net -net RX_N_IN_1 [get_bd_ports FMC_HPC_DP0_M2C_N] [get_bd_pins olt_axi_ip_0/olt_rx_n_i]
  connect_bd_net -net RX_P_IN_1 [get_bd_ports FMC_HPC_DP0_M2C_P] [get_bd_pins olt_axi_ip_0/olt_rx_p_i]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins hier_jtag_axi_master_exdsg/clk_out1] [get_bd_pins olt_axi_ip_0/olt_clk_sys_i] [get_bd_pins olt_axi_ip_0/s_olt_axi_aclk] [get_bd_pins olt_user_axi_ip_0/olt_clk_sys_i] [get_bd_pins olt_user_axi_ip_0/s_olt_user_axi_aclk]
  connect_bd_net -net olt_axi_ip_0_olt_clk_trxusr240_o [get_bd_pins olt_axi_ip_0/olt_clk_trxusr240_o] [get_bd_pins olt_user_axi_ip_0/olt_clk_trxusr240_i]
  connect_bd_net -net olt_axi_ip_0_olt_interrupt_o [get_bd_pins olt_axi_ip_0/olt_interrupt_o] [get_bd_pins olt_user_axi_ip_0/olt_interrupt_i]
  connect_bd_net -net olt_axi_ip_0_olt_mgt_pll_lock_o [get_bd_pins olt_axi_ip_0/olt_mgt_pll_lock_o] [get_bd_pins olt_user_axi_ip_0/olt_mgt_pll_lock_i]
  connect_bd_net -net olt_axi_ip_0_olt_mgt_rx_ready_o [get_bd_pins olt_axi_ip_0/olt_mgt_rx_ready_o] [get_bd_pins olt_user_axi_ip_0/olt_mgt_rx_ready_i]
  connect_bd_net -net olt_axi_ip_0_olt_mgt_tx_ready_o [get_bd_pins olt_axi_ip_0/olt_mgt_tx_ready_o] [get_bd_pins olt_user_axi_ip_0/olt_mgt_tx_ready_i]
  connect_bd_net -net olt_axi_ip_0_olt_rx_data_error_o [get_bd_pins olt_axi_ip_0/olt_rx_data_error_o] [get_bd_pins olt_user_axi_ip_0/olt_rx_data_error_i]
  connect_bd_net -net olt_axi_ip_0_olt_rx_data_o [get_bd_pins olt_axi_ip_0/olt_rx_data_o] [get_bd_pins olt_user_axi_ip_0/olt_rx_data_i]
  connect_bd_net -net olt_axi_ip_0_olt_rx_data_strobe_o [get_bd_pins olt_axi_ip_0/olt_rx_data_strobe_o] [get_bd_pins olt_user_axi_ip_0/olt_rx_data_strobe_i]
  connect_bd_net -net olt_axi_ip_0_olt_rx_locked_o [get_bd_pins olt_axi_ip_0/olt_rx_locked_o] [get_bd_pins olt_user_axi_ip_0/olt_rx_locked_i]
  connect_bd_net -net olt_axi_ip_0_olt_rx_onu_addr_o [get_bd_pins olt_axi_ip_0/olt_rx_onu_addr_o] [get_bd_pins olt_user_axi_ip_0/olt_rx_onu_addr_i]
  connect_bd_net -net olt_axi_ip_0_olt_sfp_rssi_tri_o [get_bd_ports FMC_HPC_LA02_P] [get_bd_pins olt_axi_ip_0/olt_sfp_rssi_tri_o]
  connect_bd_net -net olt_axi_ip_0_olt_sfp_rx_reset_o [get_bd_ports FMC_HPC_LA04_P] [get_bd_pins olt_axi_ip_0/olt_sfp_rx_reset_o]
  connect_bd_net -net olt_axi_ip_0_olt_sfp_tx_dis_o [get_bd_ports FMC_HPC_LA02_N] [get_bd_pins olt_axi_ip_0/olt_sfp_tx_dis_o]
  connect_bd_net -net olt_axi_ip_0_olt_status_sticky_o [get_bd_pins olt_axi_ip_0/olt_status_sticky_o] [get_bd_pins olt_user_axi_ip_0/olt_status_sticky_i]
  connect_bd_net -net olt_axi_ip_0_olt_tx_n_o [get_bd_ports FMC_HPC_DP0_C2M_N] [get_bd_pins olt_axi_ip_0/olt_tx_n_o]
  connect_bd_net -net olt_axi_ip_0_olt_tx_p_o [get_bd_ports FMC_HPC_DP0_C2M_P] [get_bd_pins olt_axi_ip_0/olt_tx_p_o]
  connect_bd_net -net olt_user_axi_ip_0_olt_core_reset_o [get_bd_pins olt_axi_ip_0/olt_core_reset_i] [get_bd_pins olt_user_axi_ip_0/olt_core_reset_o]
  connect_bd_net -net olt_user_axi_ip_0_olt_tx_data_o [get_bd_pins olt_axi_ip_0/olt_tx_data_i] [get_bd_pins olt_user_axi_ip_0/olt_tx_data_o]
  connect_bd_net -net olt_user_axi_ip_0_olt_tx_data_strobe_o [get_bd_pins olt_axi_ip_0/olt_tx_data_strobe_i] [get_bd_pins olt_user_axi_ip_0/olt_tx_data_strobe_o]
  connect_bd_net -net olt_user_axi_ip_0_tx_match_flag_o [get_bd_ports SMA_GPIO_N_OUT] [get_bd_pins olt_user_axi_ip_0/tx_match_flag_o]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins mgt_diff_clk_buffer/IBUF_OUT] [get_bd_pins olt_axi_ip_0/olt_clk_ref240_i]
  connect_bd_net -net xlconstant_0_dout [get_bd_ports IIC_FMC_GPO] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins hier_jtag_axi_master_exdsg/locked] [get_bd_pins olt_axi_ip_0/s_olt_axi_aresetn] [get_bd_pins olt_user_axi_ip_0/s_olt_user_axi_aresetn]

  # Create address segments
  assign_bd_address -offset 0x44A00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces hier_jtag_axi_master_exdsg/jtag_axi_0/Data] [get_bd_addr_segs olt_axi_ip_0/s_olt_axi/reg0] -force
  assign_bd_address -offset 0x44A10000 -range 0x00010000 -target_address_space [get_bd_addr_spaces hier_jtag_axi_master_exdsg/jtag_axi_0/Data] [get_bd_addr_segs olt_user_axi_ip_0/S_OLT_USER_AXI/S_OLT_USER_AXI_reg] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


