# Copied from Echo_Server example design in Tcl / modif. by EBSM
# JTAG_CONTROL_Server --
#	Open the server listening socket
#	and enter the Tcl event loop
#
# Arguments:
#	port	The server's port number

proc JTAG_CONTROL_Server {port} {
    set s [socket -server JTAG_CONTROLAccept $port]
    vwait forever
}

# JTAG_CONTROL_Accept --
#	Accept a connection from a new client.
#	This is called after a new socket connection
#	has been created by Tcl.
#
# Arguments:
#	sock	The new socket connection to the client
#	addr	The client's IP address
#	port	The client's port number
	
proc JTAG_CONTROLAccept {sock addr port} {
    global JTAG_CONTROLLER

    # Record the client's information

    puts "Accept $sock from $addr port $port"
    set JTAG_CONTROLLER(addr,$sock) [list $addr $port]

    # Ensure that each "puts" by the server
    # results in a network transmission

    fconfigure $sock -buffering line

    # Set up a callback for when the client sends data

    fileevent $sock readable [list JTAG_CONTROL $sock]
}

# JTAG_CONTROL --
#	This procedure is called when the server
#	can read data from the client
#
# Arguments:
#	sock	The socket connection to the client

proc JTAG_CONTROL {sock} {
    global JTAG_CONTROLLER
    
    set gpio_rt gpio_rt
    set gpio_wt gpio_wt
	
    # Check end of file or abnormal connection drop,
    # then JTAG_CONTROLLER data back to the client.

    if {[eof $sock] || [catch {gets $sock line}]} {
	close $sock
	puts "Close $JTAG_CONTROLLER(addr,$sock)"
	unset JTAG_CONTROLLER(addr,$sock)
    } else {
		################################ Process Data #################################
		set rcvd_cmd $line
        set ope [lindex $rcvd_cmd 0]
		set addr [lindex $rcvd_cmd 1]
		set data [lindex $rcvd_cmd 2]
		set cmdlen [lindex $rcvd_cmd 3]
		# Offset for slaves
        set addr [expr  $addr + 0x44a00000]
        set addr [format %x $addr]
		#puts "$ope, $addr, $data"
        switch $ope {
          "r" {
              #puts "Read register command"
			  #Rd
			  set_property CMD.ADDR $addr [get_hw_axi_txns $gpio_rt]
			  set_property CMD.LEN 1 [get_hw_axi_txns $gpio_rt]
			  run_hw_axi [get_hw_axi_txns $gpio_rt]
			  set data [get_property DATA [get_hw_axi_txns $gpio_rt]]
			  puts $sock $data
		  }
		  
          "w" {
              #puts "Write register command"
              #Wr	  
			  set_property CMD.ADDR $addr [get_hw_axi_txns $gpio_wt]
			  set_property CMD.LEN 1 [get_hw_axi_txns $gpio_wt]
			  set_property DATA $data [get_hw_axi_txns $gpio_wt]
			  run_hw_axi [get_hw_axi_txns $gpio_wt]
			  puts $sock 1
		  }

          "sysmon_refresh" {
              puts "Refresh Sysmon"                  
              refresh_hw_sysmon  [get_hw_sysmons *]
              puts $sock 1
          }
 
          "sysmon_r" {
              puts "Read SysMon property"
              set probes [list $addr]                                                
              set data [get_property $probes [lindex [get_hw_sysmons *] 0]]
              puts $sock [lindex $data 0]
          }

          default {
            puts "Command not recognizable"
			puts $sock -1
          }
        }
	}
}


#Open Hardware Target
open_hw
connect_hw_server
open_hw_target {localhost:3121/xilinx_tcf/Digilent/210308956779}
set_property PROGRAM.FILE {../pon_olt/pon_olt.runs/impl_1/system_wrapper.bit} [lindex [get_hw_devices] 0]
set_property PROBES.FILE  {../pon_olt/pon_olt.runs/impl_1/debug_nets.ltx} [lindex [get_hw_devices] 0]
current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]

#Create Transactions
set gpio_rt gpio_rt
set gpio_wt gpio_wt
create_hw_axi_txn $gpio_rt [get_hw_axis hw_axi_1] -type read -address 0x00000000
create_hw_axi_txn $gpio_wt [get_hw_axis hw_axi_1] -type write -address 0x00000000 -data {00000000}

puts "############ JTAG_CONTROL - TEST CONTROL ################"
puts "# Socket Port: 8555                                     #"
puts "# IP Address (localhost): 127.0.0.1                     #"
puts "#########################################################"

JTAG_CONTROL_Server 8555
vwait forever
