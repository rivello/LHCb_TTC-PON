################################################################
# Vivado script to generate the PON OLT example design
################################################################
#start_gui
set path [pwd]
create_project pon_olt {./pon_olt} -part xcku040-ffva1156-2-e -force
set_property BOARD_PART xilinx.com:kcu105:part0:1.1 [current_project]
set_property target_language VHDL [current_project]
set_property ip_repo_paths {./source/ip_repo} [current_project]
update_ip_catalog
source ./source/system_bd.tcl

##################################################################
# DESIGN PROCs
##################################################################

regenerate_bd_layout
save_bd_design

import_files -norecurse ./source/system_wrapper.vhd
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

add_files -fileset constrs_1 -norecurse ./source/system_physical.xdc
add_files -fileset constrs_1 -norecurse ./source/system_timing.xdc

import_files -fileset constrs_1 ./source/system_physical.xdc
import_files -fileset constrs_1 ./source/system_timing.xdc

generate_target  {synthesis  implementation}  [get_files  ./pon_olt/pon_olt.srcs/sources_1/bd/system/system.bd]

set_property IS_ENABLED FALSE [get_files -filter {FILE_TYPE == XDC && SCOPED_TO_REF =~system_mgt_diff_clk_buffer_0}]
set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none [get_runs synth_1]

save_bd_design
reset_run synth_1
launch_runs synth_1
wait_on_run synth_1

launch_runs impl_1
wait_on_run impl_1
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1
