REM ################################################################################
REM # Vivado batch file to create the TTC-PON OLT project.
REM # This batch file uses the default Xilinx installation path.
REM #
REM ################################################################################

call C:\\Xilinx\\Vivado\\2019.2\\.\\bin\\vivado.bat -mode batch -source ttc_pon_olt_kcu105.tcl

del *.jou
del *.log
pause