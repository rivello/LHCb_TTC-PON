--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_user_arria10_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT user wrapper for Arria10 design (olt_user_arria10_wrapper)
--
--! @brief OLT user wrapper
--! Example OLT user logic top design
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 24\11\2020
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 24\11\2020 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo More comments \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_user_arria10_wrapper
--============================================================================
entity olt_user_arria10_wrapper is
  port (
    -- Ports of Avalon Slave Bus Interface S_ONU_USER
    avalon_clk_i         : in  std_logic;
    avalon_reset         : in  std_logic;
    avalon_address       : in  std_logic_vector(6 downto 0);
    avalon_byteenable    : in  std_logic_vector(3 downto 0);
    avalon_readdata      : out std_logic_vector(31 downto 0);
    avalon_writedata     : in  std_logic_vector(31 downto 0);
    avalon_read          : in  std_logic;
    avalon_write         : in  std_logic;
    avalon_readdatavalid : out std_logic;


    -- Users to add ports here
    -- core connections
    olt_clk_sys_i        : in  std_logic;
    olt_core_reset_o     : out std_logic;
    olt_interrupt_i      : in  std_logic;
    olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
    olt_clk_trxusr240_i  : in  std_logic;
    olt_tx_data_o        : out std_logic_vector(199 downto 0);
    olt_tx_data_strobe_o : out std_logic;
    olt_rx_locked_i      : in  std_logic;
    olt_rx_data_i        : in  std_logic_vector(55 downto 0);
    olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
    olt_rx_data_strobe_i : in  std_logic;
    olt_rx_data_error_i  : in  std_logic;
    olt_mgt_pll_lock_i   : in  std_logic;
    olt_mgt_tx_ready_i   : in  std_logic;
    olt_mgt_rx_ready_i   : in  std_logic;

    -- Others ----------                
    tx_match_flag_o : out std_logic
    ----------------------------------          

    );
end olt_user_arria10_wrapper;

--============================================================================
--! Architecture declaration for olt_user_arria10_wrapper
--============================================================================
architecture structural of olt_user_arria10_wrapper is

  -- ! Signal declaration
  -- Control / Status
  signal slv_reg_ctrl  : t_regbank32b(127 downto 0);
  signal avalon_read_r : std_logic;
  signal slv_reg_stat  : t_regbank32b(127 downto 0);

  -- component declaration
  component olt_user_logic_exdsg is
    port(
      -- core connections
      olt_clk_sys_i        : in  std_logic;
      olt_interrupt_i      : in  std_logic;
      olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
      olt_clk_trxusr240_i  : in  std_logic;
      olt_tx_data_o        : out std_logic_vector(199 downto 0);
      olt_tx_data_strobe_o : out std_logic;
      olt_rx_locked_i      : in  std_logic;
      olt_rx_data_i        : in  std_logic_vector(55 downto 0);
      olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
      olt_rx_data_strobe_i : in  std_logic;
      olt_rx_data_error_i  : in  std_logic;

      olt_mgt_pll_lock_i : in  std_logic;
      olt_mgt_tx_ready_i : in  std_logic;
      olt_mgt_rx_ready_i : in  std_logic;
      olt_core_reset_o   : out std_logic;

      -- Control AXI --
      s_olt_user_axi_aclk : in  std_logic;
      ctrl_reg_i          : in  t_regbank32b(127 downto 0);
      stat_reg_o          : out t_regbank32b(127 downto 0);
      ---------------------------------

      -- Others ----------
      tx_match_flag_o : out std_logic
      ----------------------------------
      );
  end component olt_user_logic_exdsg;

--============================================================================
--! Architecture begin for olt_user_arria10_wrapper
--============================================================================
begin

  -- Add user logic here
  cmp_olt_user_logic_exdsg : olt_user_logic_exdsg
    port map(
      -- core connections
      olt_clk_sys_i        => olt_clk_sys_i ,
      olt_core_reset_o     => olt_core_reset_o ,
      olt_interrupt_i      => olt_interrupt_i ,
      olt_status_sticky_i  => olt_status_sticky_i ,
      olt_clk_trxusr240_i  => olt_clk_trxusr240_i ,
      olt_tx_data_o        => olt_tx_data_o ,
      olt_tx_data_strobe_o => olt_tx_data_strobe_o ,
      olt_rx_locked_i      => olt_rx_locked_i,
      olt_rx_data_i        => olt_rx_data_i ,
      olt_rx_onu_addr_i    => olt_rx_onu_addr_i ,
      olt_rx_data_strobe_i => olt_rx_data_strobe_i ,
      olt_rx_data_error_i  => olt_rx_data_error_i ,

      olt_mgt_pll_lock_i => olt_mgt_pll_lock_i ,
      olt_mgt_tx_ready_i => olt_mgt_tx_ready_i ,
      olt_mgt_rx_ready_i => olt_mgt_rx_ready_i ,

      -- Control AXI --
      s_olt_user_axi_aclk => avalon_clk_i ,
      ctrl_reg_i          => slv_reg_ctrl ,
      stat_reg_o          => slv_reg_stat ,
      ---------------------------------

      -- Others ----------
      tx_match_flag_o => tx_match_flag_o
      ----------------------------------

      );

  -- User logic ends

  --============================================================================
  -- Process p_reg_rd_wr
  --! 
  --! read:  \n
  --! write: \n
  --============================================================================            
  p_reg_rd_wr : process (avalon_clk_i) is
  begin
    if avalon_clk_i'event and avalon_clk_i = '1' then
      if(avalon_reset = '1') then
        avalon_readdatavalid <= '0';
      else
        if(avalon_write = '1') then
          for i in avalon_byteenable'range loop
            if(avalon_byteenable(i) = '1') then
              slv_reg_ctrl(to_integer(unsigned(avalon_address(6 downto 0))))((i+1)*8-1 downto i*8) <= avalon_writedata((i+1)*8-1 downto i*8);
            end if;
          end loop;
        end if;

        avalon_read_r <= avalon_read;
        if(avalon_read = '1' and avalon_read_r = '0') then  --rising edge
          avalon_readdata      <= slv_reg_stat(to_integer(unsigned(avalon_address(6 downto 0))));
          avalon_readdatavalid <= '1';
        else
          avalon_readdatavalid <= '0';
        end if;
      end if;
    end if;
  end process p_reg_rd_wr;

end architecture structural;
