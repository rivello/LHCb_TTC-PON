--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_olt_model_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_olt_package_static.all;
use work.pon_olt_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT high level model wrapper for simulation (pon_olt_model_wrapper)
--
--! @brief OLT core wrapper interfacing
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 19\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for pon_olt_model_wrapper
--============================================================================
entity pon_olt_model_wrapper is
  generic (
    g_UI_PERIOD : time := 0.100 ns
    );
  port (
    -- Control interface
    olt_clk_manager_i     : in  std_logic;
    olt_ctrl_reg_i        : in  std_logic_vector(31 downto 0);
    olt_ctrl_reg_strobe_i : in  std_logic_vector(127 downto 0);
    olt_stat_reg_o        : out t_regbank32b(127 downto 0);

    -- Sys clk / reset
    olt_clk_sys_i    : in std_logic;
    olt_core_reset_i : in std_logic;

    -- mgt 240MHz ref clk
    olt_clk_ref240_i : in std_logic;

    -- Soft-configurable interrupt output
    olt_interrupt_o     : out std_logic;
    olt_status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------

    -- data in/out
    olt_clk_trxusr240_o  : out std_logic;
    olt_tx_data_i        : in  std_logic_vector(199 downto 0);
    olt_tx_data_strobe_i : in  std_logic;

    olt_rx_locked_o      : out std_logic;
    olt_rx_data_o        : out std_logic_vector(55 downto 0);
    olt_rx_onu_addr_o    : out std_logic_vector(7 downto 0);
    olt_rx_data_strobe_o : out std_logic;
    olt_rx_data_error_o  : out std_logic;

    -- status
    olt_mgt_tx_ready_o : out std_logic;
    olt_mgt_rx_ready_o : out std_logic;
    olt_mgt_pll_lock_o : out std_logic;

    -- serial
    olt_tx_p_o         : out std_logic;
    olt_tx_n_o         : out std_logic;
    olt_rx_p_i         : in  std_logic;
    olt_rx_n_i         : in  std_logic;
    olt_sfp_rx_sd_i    : in  std_logic;
    olt_sfp_tx_fault_i : in  std_logic;
    olt_sfp_mod_abs_i  : in  std_logic;
    olt_sfp_rx_reset_o : out std_logic;
    olt_sfp_rssi_tri_o : out std_logic;
    olt_sfp_tx_dis_o   : out std_logic;

    -- i2c interfacing --
    olt_i2c_sda_io : inout std_logic;   --! not implemented in high_level model
    olt_i2c_scl_io : inout std_logic    --! not implemented in high_level model
    -------------------------             
    );
end entity pon_olt_model_wrapper;

--============================================================================
--! Architecture declaration for pon_olt_model_wrapper
--============================================================================
architecture high_level of pon_olt_model_wrapper is

  --! Functions

  --! Constants

  --! Signal declaration
  -- global from olt_mgt                                                                                         
  signal olt_mgt_clk_trxusr240 : std_logic;
  signal olt_mgt_tx_ready      : std_logic;
  signal olt_mgt_rx_ready      : std_logic;
  signal olt_mgt_pll_lock      : std_logic;

  -- olt_core <-> olt_mgt_wrapper      
  signal olt_core_to_mgt_drp_wr        : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_drp_wr_strobe : std_logic;
  signal olt_mgt_to_core_drp_monitor   : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_rx_phase_ctrl : std_logic_vector(31 downto 0);
  signal olt_mgt_to_core_rx_phase_stat : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_tx_phase_ctrl : std_logic_vector(31 downto 0);
  signal olt_mgt_to_core_tx_phase_stat : std_logic_vector(31 downto 0);
  signal olt_core_to_mgt_tx_pol        : std_logic;
  signal olt_core_to_mgt_rx_pol        : std_logic;
  signal olt_core_to_mgt_tx_data       : std_logic_vector(39 downto 0);
  signal olt_mgt_to_core_rx_data       : std_logic_vector(39 downto 0);
  signal olt_core_to_mgt_reset         : std_logic;

  -- olt_core <-> i2c_master
  signal olt_core_to_i2c_master_i2c_req      : std_logic;
  signal olt_core_to_i2c_master_i2c_rnw      : std_logic;
  signal olt_core_to_i2c_master_i2c_rb2      : std_logic;
  signal olt_core_to_i2c_master_i2c_slv_addr : std_logic_vector(6 downto 0);
  signal olt_core_to_i2c_master_i2c_reg_addr : std_logic_vector(7 downto 0);
  signal olt_core_to_i2c_master_i2c_wr_data  : std_logic_vector(7 downto 0);

  signal i2c_master_to_olt_core_i2c_done     : std_logic                     := '0';
  signal i2c_master_to_olt_core_i2c_error    : std_logic                     := '0';
  signal i2c_master_to_olt_core_i2c_drop_req : std_logic                     := '0';
  signal i2c_master_to_olt_core_i2c_rd_data  : std_logic_vector(15 downto 0) := x"0000";

  -- i2c_master
  signal i2c_master_sdi     : std_logic;
  signal i2c_master_sdo     : std_logic;
  signal i2c_master_sda_ena : std_logic;
  signal i2c_master_scl_ena : std_logic;
  signal i2c_master_scl     : std_logic;

  --! Component declaration    
  component olt_core is
    port (
      -- global input signals --
      clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      core_reset_i : in std_logic;      --! Sync to clk_sys_i
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         : in  std_logic;
      manager_stat_reg_o    : out t_regbank32b(c_OLT_NBR_CTRL_REGS-1 downto 0);
      manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      manager_ctrl_strobe_i : in  std_logic_vector(c_OLT_NBR_CTRL_REGS-1 downto 0);
      --------------------------

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------

      -- data user in/out -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_i        : in std_logic_vector(c_OLT_TX_USR_DATA_WIDTH-1 downto 0);
      tx_data_strobe_i : in std_logic;

      rx_locked_o      : out std_logic;
      rx_data_o        : out std_logic_vector(c_OLT_RX_USR_DATA_WIDTH-1 downto 0);
      rx_onu_addr_o    : out std_logic_vector(7 downto 0);
      rx_data_strobe_o : out std_logic;
      rx_data_error_o  : out std_logic;
      --------------------------

      -- MGT connections --
      -- Clocks
      clk_trxusr240_i : in std_logic;   --! MGT txusr clock

      -- Status/Ctrl
      mgt_reset_o  : out std_logic;
      mgt_tx_pol_o : out std_logic;
      mgt_rx_pol_o : out std_logic;

      -- Xilinx only
      mgt_drp_wr_o        : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o : out std_logic;
      mgt_drp_monitor_i   : in  std_logic_vector(31 downto 0);
      mgt_rx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_phase_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i : in  std_logic_vector(31 downto 0);

      mgt_tx_ready_i : in std_logic;
      mgt_rx_ready_i : in std_logic;
      mgt_pll_lock_i : in std_logic;

      -- Data
      mgt_tx_data_o : out std_logic_vector(c_OLT_TX_WORD_WIDTH-1 downto 0);
      mgt_rx_data_i : in  std_logic_vector(c_OLT_RX_WORD_WIDTH-1 downto 0);
      -------------------------

      -- transceiver ctrl/stat connections --
      sfp_rx_sd_i    : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_rx_reset_o : out std_logic;
      sfp_rssi_tri_o : out std_logic;
      sfp_tx_dis_o   : out std_logic;
      -------------------------    

      -- i2c interfacing --
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------
      );
  end component olt_core;

  component olt_mgt_wrapper is
    generic (
      g_UI_PERIOD : time := 0.100 ns
      );
    port (
      -- global input signals --        
      clk_sys_i           : in  std_logic;
      clk_ref_i           : in  std_logic;
      mgt_reset_i         : in  std_logic;
      -------------------------
      -- global output signals --               
      clk_trxusr_o        : out std_logic;
      -------------------------
      -- status/control --
      drp_wr_i            : in  std_logic_vector(31 downto 0);
      drp_wr_strobe_i     : in  std_logic;
      drp_monitor_o       : out std_logic_vector(31 downto 0);
      mgt_rx_phase_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_rx_phase_stat_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_o : out std_logic_vector(31 downto 0);

      txpolarity_i   : in  std_logic;
      rxpolarity_i   : in  std_logic;
      txfsmrstdone_o : out std_logic;
      rxfsmrstdone_o : out std_logic;
      pll_lock_o     : out std_logic;
      ------------------------- 

      -- data in/out --         
      tx_data_i : in  std_logic_vector(39 downto 0);
      rx_data_o : out std_logic_vector(39 downto 0);

      rx_p_i : in  std_logic;
      rx_n_i : in  std_logic;
      tx_p_o : out std_logic;
      tx_n_o : out std_logic
      ------------------------- 
      );
  end component olt_mgt_wrapper;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component olt_core
  --============================================================================   
  cmp_olt_core : olt_core
    port map(
      -- global input signals --
      clk_sys_i    => olt_clk_sys_i,
      core_reset_i => olt_core_reset_i,
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         => olt_clk_manager_i,
      manager_stat_reg_o    => olt_stat_reg_o,
      manager_ctrl_reg_i    => olt_ctrl_reg_i,
      manager_ctrl_strobe_i => olt_ctrl_reg_strobe_i,
      --------------------------

      -- Soft-configurable interrupt output
      interrupt_o     => olt_interrupt_o,
      status_sticky_o => olt_status_sticky_o,
      -------------------------         

      -- data user in/out --
      tx_data_i        => olt_tx_data_i,
      tx_data_strobe_i => olt_tx_data_strobe_i,

      rx_locked_o      => olt_rx_locked_o,
      rx_data_o        => olt_rx_data_o,
      rx_onu_addr_o    => olt_rx_onu_addr_o,
      rx_data_strobe_o => olt_rx_data_strobe_o,
      rx_data_error_o  => olt_rx_data_error_o,
      --------------------------

      -- MGT connections --
      -- Clocks
      clk_trxusr240_i => olt_mgt_clk_trxusr240,

      -- Status/Ctrl
      mgt_reset_o  => olt_core_to_mgt_reset,
      mgt_tx_pol_o => olt_core_to_mgt_tx_pol,
      mgt_rx_pol_o => olt_core_to_mgt_rx_pol,

      mgt_drp_wr_o        => olt_core_to_mgt_drp_wr,
      mgt_drp_wr_strobe_o => olt_core_to_mgt_drp_wr_strobe,
      mgt_drp_monitor_i   => olt_mgt_to_core_drp_monitor,
      mgt_rx_phase_ctrl_o => olt_core_to_mgt_rx_phase_ctrl,
      mgt_rx_phase_stat_i => olt_mgt_to_core_rx_phase_stat,
      mgt_tx_phase_ctrl_o => olt_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_i => olt_mgt_to_core_tx_phase_stat,

      mgt_tx_ready_i => olt_mgt_tx_ready,
      mgt_rx_ready_i => olt_mgt_rx_ready,
      mgt_pll_lock_i => olt_mgt_pll_lock,

      -- Data              
      mgt_tx_data_o => olt_core_to_mgt_tx_data,
      mgt_rx_data_i => olt_mgt_to_core_rx_data,
      -------------------------

      -- transceiver ctrl/stat connections --
      sfp_rx_sd_i    => olt_sfp_rx_sd_i,
      sfp_tx_fault_i => olt_sfp_tx_fault_i,
      sfp_mod_abs_i  => olt_sfp_mod_abs_i,
      sfp_rx_reset_o => olt_sfp_rx_reset_o,
      sfp_rssi_tri_o => olt_sfp_rssi_tri_o,
      sfp_tx_dis_o   => olt_sfp_tx_dis_o,
      -------------------------

      -- i2c interfacing --
      i2c_req_o      => olt_core_to_i2c_master_i2c_req,
      i2c_rnw_o      => olt_core_to_i2c_master_i2c_rnw,
      i2c_rb2_o      => olt_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_o => olt_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_o => olt_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_o  => olt_core_to_i2c_master_i2c_wr_data,

      i2c_done_i     => i2c_master_to_olt_core_i2c_done,
      i2c_error_i    => i2c_master_to_olt_core_i2c_error,
      i2c_drop_req_i => i2c_master_to_olt_core_i2c_drop_req,
      i2c_rd_data_i  => i2c_master_to_olt_core_i2c_rd_data
      -------------------------         
      );

  --============================================================================
  -- Component instantiation
  --! Component olt_mgt_wrapper
  --============================================================================   
  cmp_olt_mgt_wrapper : olt_mgt_wrapper
    generic map(
      g_UI_PERIOD => g_UI_PERIOD
      )
    port map(
      -- global input signals --        
      clk_sys_i           => olt_clk_sys_i,
      clk_ref_i           => olt_clk_ref240_i,
      mgt_reset_i         => olt_core_to_mgt_reset,
      -------------------------
      -- global output signals --               
      clk_trxusr_o        => olt_mgt_clk_trxusr240,
      -------------------------
      -- status/control --
      drp_wr_i            => olt_core_to_mgt_drp_wr,
      drp_wr_strobe_i     => olt_core_to_mgt_drp_wr_strobe,
      drp_monitor_o       => olt_mgt_to_core_drp_monitor,
      mgt_rx_phase_ctrl_i => olt_core_to_mgt_rx_phase_ctrl,
      mgt_rx_phase_stat_o => olt_mgt_to_core_rx_phase_stat,
      mgt_tx_phase_ctrl_i => olt_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_o => olt_mgt_to_core_tx_phase_stat,

      txpolarity_i   => olt_core_to_mgt_tx_pol,
      rxpolarity_i   => olt_core_to_mgt_rx_pol,
      txfsmrstdone_o => olt_mgt_tx_ready,
      rxfsmrstdone_o => olt_mgt_rx_ready,
      pll_lock_o     => olt_mgt_pll_lock,
      ------------------------- 

      -- data in/out --         
      tx_data_i => olt_core_to_mgt_tx_data,
      rx_data_o => olt_mgt_to_core_rx_data,

      rx_p_i => olt_rx_p_i,
      rx_n_i => olt_rx_n_i,
      tx_p_o => olt_tx_p_o,
      tx_n_o => olt_tx_n_o
      ------------------------- 
      );

  --============================================================================
  --Output
  --============================================================================  
  olt_clk_trxusr240_o <= olt_mgt_clk_trxusr240;
  olt_mgt_tx_ready_o  <= olt_mgt_tx_ready;
  olt_mgt_rx_ready_o  <= olt_mgt_rx_ready;
  olt_mgt_pll_lock_o  <= olt_mgt_pll_lock;

end high_level;
