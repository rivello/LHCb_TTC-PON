--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file pon_up_sc_rx.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages

-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Slow control decoder design for TTC-PON upstream (pon_up_sc_rx)
--
--! @brief Slow control decoder design for TTC-PON upstream
--! - It assembles the control byte received into frames
--! - Identify when a header is received
--! - Output to user the received frame
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 03\05\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 03\05\2017 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for pon_up_sc_rx
--==============================================================================
entity pon_up_sc_rx is
  generic(
    g_HEADER : std_logic_vector(2 downto 0) := "110"
    );
  port (
    clk_i             : in  std_logic;  --! clock input
    reset_i           : in  std_logic;  --! active high sync. reset
    byte_rcvd_i       : in  std_logic;  --! control scheduling
    data_error_i      : in  std_logic;  --! detected error 8b10b
    data_i            : in  std_logic_vector(15 downto 0);  --! encoded input data
    onu_addr_i        : in  std_logic_vector(7 downto 0);  --! address of ONU to be checked
    data_o            : out std_logic_vector(28 downto 0);  --! user output data
    rcvd_ctrl_frame_o : out std_logic;  --! user control 
    rcvd_ctrl_error_o : out std_logic  --! error detected in ctrl frame - 8b10b or in SOF field
    );
end pon_up_sc_rx;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of pon_up_sc_rx is

  --! Signal declaration
  signal data_r       : std_logic_vector(8*3-1 downto 0);
  signal header       : std_logic_vector(2 downto 0);
  signal data_error_r : std_logic_vector(3-1 downto 0);

  signal header_detect   : std_logic;
  signal header_detect_r : std_logic;
  
begin


  --============================================================================
  -- Process p_data_r
  --! input data register and sr
  --! read: clk_i, reset_i, byte_rcvd_i, data_i, data_error_i\n
  --! write: - \n
  --! r/w: data_r, data_error_r \n
  --============================================================================  
  p_data_r : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        data_r       <= (others => '0');
        data_error_r <= (others => '0');
      else
        if(byte_rcvd_i = '1' and data_i(7 downto 0) = onu_addr_i) then
          data_r       <= data_r(data_r'left-8 downto 0)&data_i(15 downto 8);
          data_error_r <= data_error_r(data_error_r'left-1 downto 0)&data_error_i;
        end if;
      end if;
    end if;
  end process p_data_r;

  --============================================================================
  -- Process p_header_detect_r
  --! delay to identify rising edge of header_detect
  --! read: clk_i, header_detect\n
  --! write: header_detect_r\n
  --! r/w: - \n
  --============================================================================  
  p_header_detect_r : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      header_detect_r <= header_detect;
    end if;
  end process p_header_detect_r;

  header        <= data_r(23)&data_r(15)&data_r(7);
  header_detect <= '1' when (header = g_HEADER) else '0';

  --============================================================================
  -- Process p_interface_user_out
  --! output user interface
  --! read: clk_i, reset_i, onu_addr_i, data_r, header_detect, header_detect_r, data_error_r\n
  --! write: data_o, rcvd_ctrl_frame_o, rcvd_ctrl_error_o\n
  --! r/w: - \n
  --============================================================================  
  p_interface_user_out : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        data_o            <= (others => '0');
        rcvd_ctrl_frame_o <= '0';
        rcvd_ctrl_error_o <= '0';
      else
        rcvd_ctrl_frame_o <= '0';
        rcvd_ctrl_error_o <= '0';
        if(header_detect_r = '0' and header_detect = '1') then
          data_o(7 downto 0)   <= onu_addr_i;
          data_o(14 downto 8)  <= data_r(6 downto 0);
          data_o(21 downto 15) <= data_r(14 downto 8);
          data_o(28 downto 22) <= data_r(22 downto 16);
          rcvd_ctrl_frame_o    <= '1';
          if(data_error_r /= "000") then
            rcvd_ctrl_error_o <= '1';
          end if;
        end if;
      end if;
    end if;
  end process p_interface_user_out;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
