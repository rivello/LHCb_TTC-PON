--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file rx_fine_phase_measurement.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Fine phase measurement FSM logic (rx_fine_phase_measurement)
--
--! @brief Rx fine phase measurement logic
--! - Implements fine phase measurement algorithm for TTC-PON
--! 
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 31\10\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 31\10\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for rx_fine_phase_measurement
--==============================================================================
entity rx_fine_phase_measurement is
    generic (
        g_SWEEP_FINE_PHASE_MEASUREMENT : integer := 512 -- 64*4*2 OLT_UI * 4 * 2 = 2*ONU_UI (This value should be bigger than one and smaller than 1000)
    );
    port (
        -- clock/reset
        clk_sys_i      : in std_logic; --! System clock
        clk_trxusr240_i : in std_logic; --! MGT interfacing clock - 240MHz
        reset_i        : in std_logic; --! Reset synchronous to clk_trxusr240_i
        enable_i       : in std_logic; --! Enable logic

        -- User interface
        rx_latch_measurement_i  : in std_logic;                    --! Latch measurement		
        number_averaging_i      : in std_logic_vector(5 downto 0); --! Number of averaged measurements to be performed (static)	
        onu_addr_i              : in std_logic_vector(7 downto 0); --! Address of ONU for which phase measurement is currently being performed

        -- Measurement status out
        results_meas_o           : out std_logic_vector(63 downto 0); --! Phase measurement Bus
        results_meas_p_o         : out std_logic;                     --! Pulse to indicate a new value is present in results_meas_o bus		
        results_meas_latched_o   : out std_logic_vector(63 downto 0); --! Phase measurement latched
        meas_idle_o              : out std_logic;                     --! Phase measurement is in a idle state        		

        -- PON interface
        master_heartbeat_i      : in std_logic;                     --! OLT heartbeat		
        calibration_on_i        : in std_logic;                     --! Calibration is ON			
        rx_heartbeat_header_i   : in std_logic;                     --! Heartbeat header used during calibration
        rx_normal_header_i      : in std_logic;                     --! Normal header used in normal run of the system
        modulo_position_i       : in std_logic_vector(15 downto 0); --! Modulo in which position measurement is based
        coarse_position_i       : in std_logic_vector(15 downto 0); --! Coarse position measured with coarse counter with low resolution (clk_trxusr240_i period)
        fine_position_i         : in std_logic_vector(6 downto 0);  --! Fine position measured by BOS with high resolution (BOS_UI)
        rx_onu_addr_i           : in std_logic_vector(7 downto 0);  --! ONU Received address

        -- low-level interface for phase shift
        ps_strobe_o         : out  std_logic;                   --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
        ps_inc_ndec_o       : out std_logic;                    --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
        ps_phase_step_o     : out std_logic_vector(3 downto 0); --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)	   
        ps_done_i           : in std_logic                      --! pulse synchronous to clk_sys_i to indicate a phase shift was performed

    );
end rx_fine_phase_measurement;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of rx_fine_phase_measurement is

  attribute keep : string;

  --! Function declaration

  --! Constant declaration
  constant c_MEASURE_TRIAL_CNTR_MAX : integer :=5;

  --! Signal declaration
  -- FSM to control the rx fine phase measurement
  -- The phase measurement is divided into two steps:
  -- 	UP   : phase shifting upwards
  -- 	DN   : phase shifting backwards
  type t_PHASE_MEASURE_FSM is (IDLE,                 --  |<-------------.
                                                     --  |              | 
                               ACC_AVG             , --  |<---------.   |
                                                     --  v          |   |
                               UP_PHASE_ACC        , --  |------.   |   |
                                                     --  |      |   |   |
                                                     --  |<---. |   |   | 
							                         --  v    | |   |   |													 
							   UP_SHIFT_RX         , --  |    | |   |   |
							                         --  v    | |   |   |
							   UP_WAIT_HB          , --  |    | |   |   | 
                                                     --  v    | |   |   | 
                               UP_MEASURE          , --  |    | |   |   |
                                                     --  |----. |   |   | 							   
							                         --         |   |   |
                                                     --  |<-----.   |   |													 
                               DN_PHASE_ACC        , --  |<-----.   |   |							   
							                         --  v      |   |   | 
							   DN_SHIFT_RX         , --  |      |   |   | 
							                         --  v      |   |   |
							   DN_WAIT_HB          , --  |      |   |   | 
                                                     --  v      |   |   | 
                               DN_MEASURE          , --  |      |   |   |
                                                     --  |------.---.   | 						   
							                         --  v              |
                               FINISH_MEASURE        --(?)--------------.
						   );
						   
  signal phase_measure_state : t_PHASE_MEASURE_FSM;

  -- number of Rx PI shifts
  signal rx_shift_acc       : integer range 0 to (g_SWEEP_FINE_PHASE_MEASUREMENT + 1);

  -- high resolution phase accumulator
  signal coarse_position_scale : unsigned(coarse_position_i'left+7 downto 0);
  signal modulo_position_scale : unsigned(modulo_position_i'left+7 downto 0);
  signal first_phase_ismod     : std_logic;
  signal first_phase_iszero    : std_logic; 
  
  signal onu_phase_acc         : signed(47 downto 0);

  -- Config for measurement
  signal onu_addr_r         : std_logic_vector(7 downto 0);
  signal calibration_on_r   : std_logic;
  signal number_averaging_r : unsigned(number_averaging_i'range);

  -- number of averaging currently performed
  signal number_averaging   : unsigned(number_averaging_i'range);

  -- counter to check if measurement has failed
  signal measure_trial_cntr : integer range 0 to 7;
  signal measure_done_mem   : std_logic;
  signal measure_success    : std_logic;

  -- clear/latch results

  -- sync. for interface with rx_phase_aligner
  signal ps_done_r                  : std_logic; -- rising edge detector
  signal ps_done_toggle             : std_logic := '0';
  signal ps_done_toggle_rxsync_meta : std_logic;
  attribute keep of ps_done_toggle_rxsync_meta : signal is "true";
  signal ps_done_toggle_rxsync_r    : std_logic;
  signal ps_done_toggle_rxsync_r2   : std_logic;
  signal ps_done_rxsync             : std_logic;

  signal ps_inc_ndec                : std_logic;
  signal ps_inc_ndec_r              : std_logic;
  signal ps_inc_ndec_syssync_meta   : std_logic;
  attribute keep of ps_inc_ndec_syssync_meta : signal is "true";    
  signal ps_inc_ndec_syssync_r      : std_logic;

  signal ps_strobe                  : std_logic;
  signal ps_strobe_r                : std_logic;  
  signal ps_strobe_syssync_meta     : std_logic;
  attribute keep of ps_strobe_syssync_meta : signal is "true";  
  signal ps_strobe_syssync_r        : std_logic;
  signal ps_strobe_syssync_r2       : std_logic;

  -- heartbeat window
  signal heartbeat_pipe             : std_logic_vector(4 downto 0);
  signal heartbeat_start            : std_logic;
  signal heartbeat_stop             : std_logic;
  signal heartbeat_toggle           : std_logic := '0';

  -- header selection
  signal rx_header : std_logic;

  -- phase measurement results
  signal results_meas_r : std_logic_vector(results_meas_o'range);
  signal rx_latch_measurement_r : std_logic;

  signal meas_idle_comb : std_logic;                     --! Phase measurement is in a idle state    
  signal meas_idle_r    : std_logic;
  signal meas_idle_syssync_meta : std_logic;
  attribute keep of meas_idle_syssync_meta : signal is "true";
  
  signal meas_idle_syssync_r    : std_logic;

	
begin

    --============================================================================
    -- Main FSM algorithm
    --============================================================================  
    --============================================================================
    -- Process p_phase_measure_fsm
    --! Main FSM for the algorithm flow control
    --! read:  \n
    --! write: -\n
    --! r/w:   phase_measure_state\n
    --============================================================================  
    p_phase_measure_fsm : process(clk_trxusr240_i)
    begin
      if(rising_edge(clk_trxusr240_i)) then   
        if(reset_i='1') then   
          phase_measure_state <= IDLE;
        else
            case phase_measure_state is
              when IDLE =>
                  if(enable_i='1') then				  
                      phase_measure_state <= ACC_AVG;
                  end if;

              when ACC_AVG =>		  
                  phase_measure_state <= UP_PHASE_ACC;
              when UP_PHASE_ACC =>
                  if(rx_shift_acc = g_SWEEP_FINE_PHASE_MEASUREMENT) then 
                      phase_measure_state <= DN_PHASE_ACC;				  
                  else				  
                      phase_measure_state <= UP_SHIFT_RX;
                  end if;
              when UP_SHIFT_RX =>
                  if(ps_done_rxsync='1') then 			  
                      phase_measure_state <= UP_WAIT_HB;
                  end if;				  
              when UP_WAIT_HB =>
                  if(heartbeat_start='1') then			  
                      phase_measure_state <= UP_MEASURE;
                  end if;
              when UP_MEASURE =>
                  if(heartbeat_stop='1') then
                      if(measure_done_mem = '1' or measure_trial_cntr = c_MEASURE_TRIAL_CNTR_MAX) then				  
                         phase_measure_state <= UP_PHASE_ACC;
                      end if;
                  end if;
				  
              when DN_PHASE_ACC =>
                  if(rx_shift_acc = 1) then -- OLT_UI * 4 * 2 = 2*ONU_UI
                      if(number_averaging = number_averaging_r) then				  
                          phase_measure_state <= FINISH_MEASURE;
                      else
                          phase_measure_state <= ACC_AVG;
                      end if;					  
                  else				  
                      phase_measure_state <= DN_SHIFT_RX;
                  end if;
              when DN_SHIFT_RX =>
                  if(ps_done_rxsync='1') then 			  
                      phase_measure_state <= DN_WAIT_HB;
                  end if;				  
              when DN_WAIT_HB =>
                  if(heartbeat_start='1') then			  
                      phase_measure_state <= DN_MEASURE;
                  end if;
              when DN_MEASURE =>
                  if(heartbeat_stop='1') then
                      if(measure_done_mem = '1' or measure_trial_cntr = c_MEASURE_TRIAL_CNTR_MAX) then				  
                         phase_measure_state <= DN_PHASE_ACC;
                      end if;
                  end if;

              when FINISH_MEASURE =>
                  phase_measure_state <= IDLE;

    	     when others =>
                  phase_measure_state <= IDLE;
    	  end case;						
      end if;
    end if;  
    end process p_phase_measure_fsm;

    meas_idle_comb <= '1' when (phase_measure_state = IDLE) else '0';
	meas_idle_r    <= meas_idle_comb when rising_edge(clk_trxusr240_i);
    meas_idle_syssync_meta    <= meas_idle_r when rising_edge(clk_sys_i);
    meas_idle_syssync_r       <= meas_idle_syssync_meta when rising_edge(clk_sys_i);	
    meas_idle_o <= meas_idle_syssync_r;
	
    --============================================================================
    -- Interface with Rx phase shifter
    --============================================================================  
    ps_phase_step_o <= "0001"; -- always fine resolution
    ps_inc_ndec     <= '0' when (phase_measure_state=DN_SHIFT_RX or phase_measure_state=DN_PHASE_ACC or phase_measure_state=DN_MEASURE) else '1';
    ps_inc_ndec_r   <= ps_inc_ndec when rising_edge(clk_trxusr240_i);

    -- sync. to sysclk
    ps_inc_ndec_syssync_meta <= ps_inc_ndec_r            when rising_edge(clk_sys_i);
    ps_inc_ndec_syssync_r    <= ps_inc_ndec_syssync_meta when rising_edge(clk_sys_i);
    ps_inc_ndec_o            <= ps_inc_ndec_syssync_r    when rising_edge(clk_sys_i);
	
    ps_strobe       <= '1' when (phase_measure_state=UP_SHIFT_RX or phase_measure_state=DN_SHIFT_RX) else '0';
    ps_strobe_r     <= ps_strobe   when rising_edge(clk_trxusr240_i);

    -- sync. to sysclk
    ps_strobe_syssync_meta <= ps_strobe_r            when rising_edge(clk_sys_i);
    ps_strobe_syssync_r    <= ps_strobe_syssync_meta when rising_edge(clk_sys_i);
    ps_strobe_syssync_r2   <= ps_strobe_syssync_r    when rising_edge(clk_sys_i);
    ps_strobe_o            <= ps_strobe_syssync_r2   when rising_edge(clk_sys_i);

    -- sync. from sysclk
    p_sync_ps_done_sys : process(clk_sys_i)
    begin
        if(rising_edge(clk_sys_i)) then
          ps_done_r <= ps_done_i;
          if(ps_done_i='1' and ps_done_r='0') then
		    ps_done_toggle <= not ps_done_toggle;
          end if;
        end if;
    end process p_sync_ps_done_sys;

    ps_done_toggle_rxsync_meta <= ps_done_toggle             when rising_edge(clk_trxusr240_i);
    ps_done_toggle_rxsync_r    <= ps_done_toggle_rxsync_meta when rising_edge(clk_trxusr240_i);
    ps_done_toggle_rxsync_r2   <= ps_done_toggle_rxsync_r    when rising_edge(clk_trxusr240_i);
    ps_done_rxsync             <= (ps_done_toggle_rxsync_r xor ps_done_toggle_rxsync_r2) when rising_edge(clk_trxusr240_i);

    --============================================================================
    -- Increase heartbeat window
    --============================================================================
    --============================================================================
    -- Process p_heartbeat_window
    --! Increase heartbeat window with start and stop signals for phase measurement
    --! read:  master_heartbeat_i\n
    --! write: heartbeat_start, heartbeat_stop\n
    --! r/w:   heartbeat_pipe\n
    --============================================================================  	
    p_heartbeat_window : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            if(master_heartbeat_i='1') then
                heartbeat_toggle <= not heartbeat_toggle;
            end if;
            if(heartbeat_pipe(0) = '1' and heartbeat_toggle='0') then
                heartbeat_start <='1';
            else
                heartbeat_start <='0';	
            end if;
            if(heartbeat_pipe(heartbeat_pipe'left) = '1' and heartbeat_toggle='1') then
                heartbeat_stop <='1';
            else
                heartbeat_stop <='0';	
            end if;
            heartbeat_pipe <= heartbeat_pipe(heartbeat_pipe'left-1 downto 0)&master_heartbeat_i;
        end if;
    end process p_heartbeat_window;

    --============================================================================
    -- Averaging counter
    --============================================================================
    --============================================================================
    -- Process p_number_averaging_cntr
    --! Averaging counter
    --! read:  phase_measure_state\n
    --! write: -\n
    --! r/w:   number_averaging\n
    --============================================================================  	
    p_number_averaging_cntr : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            if(phase_measure_state = IDLE) then
                number_averaging <= to_unsigned(0,number_averaging'length);			
            elsif(phase_measure_state = ACC_AVG) then
                number_averaging <= number_averaging + to_unsigned(1,number_averaging'length);
            end if;
        end if;
    end process p_number_averaging_cntr;

    --============================================================================
    -- Trial counter
    --============================================================================
    --============================================================================
    -- Process p_number_averaging_cntr
    --! Averaging counter
    --! read:  phase_measure_state\n
    --! write: measure_success\n
    --! r/w:   number_averaging\n
    --============================================================================  	
    p_measure_trial_cntr : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then		
            if(phase_measure_state = UP_MEASURE or phase_measure_state = DN_MEASURE) then
                if(heartbeat_stop='1') then
                    measure_trial_cntr <= measure_trial_cntr + 1;
                end if;					
            else
                measure_trial_cntr <= 0;	
            end if;
            if(phase_measure_state=IDLE) then
                measure_success <= '1';
            elsif(measure_trial_cntr=c_MEASURE_TRIAL_CNTR_MAX) then
                measure_success <= '0';
            end if;
        end if;
    end process p_measure_trial_cntr;

    --============================================================================
    -- Receiver shift accumulator
    --============================================================================
    --============================================================================
    -- Process p_number_averaging_cntr
    --! Averaging counter
    --! read:  phase_measure_state\n
    --! write: -\n
    --! r/w:   rx_shift_acc\n
    --============================================================================  	
    p_rx_shift_acc : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            if(phase_measure_state = IDLE) then
                rx_shift_acc <= 0;	
            elsif(phase_measure_state = UP_PHASE_ACC) then
                rx_shift_acc <= rx_shift_acc + 1;
            elsif(phase_measure_state = DN_PHASE_ACC) then
                rx_shift_acc <= rx_shift_acc - 1;
            end if;
        end if;
    end process p_rx_shift_acc;

    --============================================================================
    -- High resolution phase measurement
    --============================================================================
    coarse_position_scale <= unsigned("00"&coarse_position_i&"00000") + unsigned("0000"&coarse_position_i&"000"); -- scaling factor is 40 = 32+8
    modulo_position_scale <= unsigned("00"&modulo_position_i&"00000") + unsigned("0000"&modulo_position_i&"000"); -- scaling factor is 40 = 32+8

    rx_header <= rx_heartbeat_header_i when calibration_on_i='1' else rx_normal_header_i;
    --============================================================================
    -- Process p_number_averaging_cntr
    --! Averaging counter
    --! read:  phase_measure_state, modulo_position_scale, coarse_position_scale, fine_position_i\n
    --! write: -\n
    --! r/w:   onu_phase_acc, measure_done_mem,first_phase_iszero,first_phase_ismod\n
    --============================================================================  	
    p_phase_detector_acc : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            if(phase_measure_state = IDLE) then
                onu_phase_acc      <= (others => '0');
                measure_done_mem   <= '0';
                first_phase_iszero <= '0';
                first_phase_ismod  <= '0';
            elsif(phase_measure_state = UP_PHASE_ACC or phase_measure_state = DN_PHASE_ACC) then
                onu_phase_acc    <= onu_phase_acc;			
                measure_done_mem <= '0';
                first_phase_iszero <= first_phase_iszero;
                first_phase_ismod  <= first_phase_ismod ;
            elsif(phase_measure_state = UP_MEASURE or phase_measure_state=DN_MEASURE) then		
                if(rx_header = '1') then
                    if(onu_addr_r = rx_onu_addr_i and measure_done_mem = '0') then
                        -- This logic avoid wraps up of the phase measurement counter					
                        if(rx_shift_acc = 1) then -- first shift 
                          if(coarse_position_scale = to_unsigned(0,coarse_position_scale'length)) then
                            first_phase_iszero <= '1';
                          elsif(unsigned(coarse_position_i) = (unsigned(modulo_position_i) - to_unsigned(1,modulo_position_i'length))) then
                            first_phase_ismod <= '1';
                          end if;						  
                        end if;
                        if(first_phase_ismod='1' and (coarse_position_scale = to_unsigned(0,coarse_position_scale'length))) then
                          onu_phase_acc  <= onu_phase_acc + resize(signed('0'&modulo_position_scale), onu_phase_acc'length) + resize(signed('0'&coarse_position_scale), onu_phase_acc'length) + resize(signed('0'&unsigned(fine_position_i)), onu_phase_acc'length);
                        elsif(first_phase_iszero='1' and (unsigned(coarse_position_i) = (unsigned(modulo_position_i) - to_unsigned(1,modulo_position_i'length)))) then
                          onu_phase_acc  <= onu_phase_acc - resize(signed('0'&modulo_position_scale), onu_phase_acc'length) + resize(signed('0'&coarse_position_scale), onu_phase_acc'length) + resize(signed('0'&unsigned(fine_position_i)), onu_phase_acc'length);
                        else
                          onu_phase_acc  <= onu_phase_acc + resize(signed('0'&signed('0'&coarse_position_scale)), onu_phase_acc'length) + resize(signed('0'&unsigned(fine_position_i)), onu_phase_acc'length);				
                        end if;
                        measure_done_mem <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process p_phase_detector_acc;

    --============================================================================
    -- Latches config for measurement only when in IDLE state
    --============================================================================
    --============================================================================
    -- Process p_latch_config
    --! Averaging counter
    --! read:  phase_measure_state, onu_addr_i, number_averaging_i, calibration_on_i\n
    --! write: onu_addr_r, number_averaging_r, calibration_on_r\n
    --! r/w:   \n
    --============================================================================  	
    p_latch_config : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            if(phase_measure_state = IDLE) then
                calibration_on_r   <= calibration_on_i;
                onu_addr_r         <= onu_addr_i;
                number_averaging_r <= unsigned(number_averaging_i);	 			
            end if;
        end if;
    end process p_latch_config;

    --============================================================================
    -- Latch results for consistency
    --============================================================================
    --============================================================================
    -- Process p_latch_results
    --! Latch results bus
    --! read:  rx_latch_measurement_i, phase_measure_state, onu_addr_r, onu_phase_acc\n
    --! write: results_meas_latched_o\n
    --! r/w:   results_meas_r, rx_latch_measurement_r\n
    --============================================================================  	
    p_latch_results : process(clk_trxusr240_i)
    begin
        if(rising_edge(clk_trxusr240_i)) then
            rx_latch_measurement_r <= rx_latch_measurement_i;
            if(rx_latch_measurement_i='1' and rx_latch_measurement_r = '0') then
                results_meas_latched_o <= results_meas_r;
                results_meas_r         <= (others => '0');
                results_meas_p_o       <= '0';				
            elsif(phase_measure_state = FINISH_MEASURE)then
                results_meas_r(47 downto 0)  <= std_logic_vector(onu_phase_acc);
                results_meas_r(55 downto 48) <=	onu_addr_r;
                results_meas_r(56)           <= measure_success;
                results_meas_r(63 downto 57) <= (others => '0');
                results_meas_p_o <= '1';
            else
                results_meas_p_o <= '0';			
			end if;
        end if;
    end process p_latch_results;

    results_meas_o <= results_meas_r;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================