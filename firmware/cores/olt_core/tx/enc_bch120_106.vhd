--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file enc_bch120_106.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: BCH(120,106) encoder design (enc_bch120_106)
--
--! @brief Forward Error Correction of type BCH(120,106) shortened from BCH(127,113)
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 02\09\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 02\09\2016 - EBSM - Created\n
--! 27\02\2017 - EBSM - Added package\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for enc_bch120_106
--==============================================================================
entity enc_bch120_106 is
  port (
    clk_i       : in  std_logic;        --! clock input
    reset_i     : in  std_logic;        --! active high sync. reset
    start_enc_i : in  std_logic;        --! control scheduling
    data_i      : in  std_logic_vector(39 downto 0);  --! input data
    data_o      : out std_logic_vector(39 downto 0)   --! encoded output data
    );
end enc_bch120_106;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of enc_bch120_106 is

  --! Signal declaration   
  signal parity_bits : std_logic_vector(13 downto 0);

begin

  --============================================================================
  -- Component instantiation
  --! Component cmp_poly_div
  --! Calculates r(x) = rem(data_i/(g1(x).g3(x)))
  --============================================================================   
  cmp_poly_div : poly_div
    generic map(
      g_DIVISOR_POLY   => c_BCH120_106_GEN13_POLY,
      g_DIVIDEND_WIDTH => 40
      )
    port map(
      clk_i       => clk_i,
      reset_i     => start_enc_i,
      enable_i    => '1',
      dividend_i  => data_i,
      quotient_o  => open,
      remainder_o => parity_bits
      );

  p_output_reg : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        data_o <= (others => '0');
      elsif(start_enc_i = '1') then
        data_o <= parity_bits&data_i(25 downto 0);
      else
        data_o <= data_i;
      end if;
    end if;
  end process p_output_reg;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
