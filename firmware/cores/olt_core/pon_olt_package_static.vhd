--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_olt_package_static.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT package definitions (pon_olt_package_static)
--
--! @brief OLT package
--! Those parameters shall never be changed by the user
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Package definition for pon_olt_package_static
--============================================================================
package pon_olt_package_static is

  --============================================================================
  -- OLT Data in/out
  --============================================================================   
  constant c_OLT_USR_TX_FRAME_WIDTH : integer := 200;
  constant c_OLT_USR_RX_FRAME_WIDTH : integer := 56;
  constant c_OLT_TX_WORD_WIDTH      : integer := 40;
  constant c_OLT_RX_WORD_WIDTH      : integer := 40;

  constant c_OLT_RX_USR_DATA_WIDTH : integer := c_OLT_USR_RX_FRAME_WIDTH;
  constant c_OLT_TX_USR_DATA_WIDTH : integer := c_OLT_USR_TX_FRAME_WIDTH;

  constant c_OLT_TX_CTRL_DATA_WIDTH : integer := 4;
  constant c_OLT_RX_CTRL_DATA_WIDTH : integer := 8;
  --============================================================================        

  --============================================================================
  -- MANAGEMENT / CONTROL
  --============================================================================
  constant c_OLT_NBR_CTRL_REGS : integer := 128;
  --============================================================================        

  --============================================================================
  -- OLT SFP physical parameters
  --============================================================================
  constant c_SFP_RX_RESET_WIDTH_TXCLK_PERIOD : integer := 6;        --! 6*4.16=25ns
  constant c_SFP_TX_DISABLE_RESET_TIME       : integer := 100;      --! OLT SFP Tx disable time in ms upon reset  
  constant c_RSSI_TRG_WIDTH                  : integer := 600;      --! RSSI trigger pulse width in ns
  constant c_RSSI_WAIT_TIME                  : integer := 500000;   --! RSSI conversion time in ns
  --============================================================================

  --============================================================================
  -- Clock parameters related to system/MGT
  --============================================================================
  constant c_RATIO_BCCLK_TRXCLK        : integer := 6;     --! 25ns / 4.166ns = 6
  constant c_BURST_LENGTH_OLT_RXUSRCLK : integer := 30;    --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
  --============================================================================

  --============================================================================
  -- Rx Lock Parameters
  --============================================================================  
  constant c_WINDOW_BURST_COUNTING     : integer := 32*4;  --! number of bursts to be checked to consider locked
  constant c_GOOD_BURST_TO_LOCK        : integer := 32*2;  --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
  constant c_GOOD_BURST_TO_UNLOCK      : integer := 32/2;  --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
  --============================================================================

  --============================================================================
  -- Rx Fine phase measurement parameters
  --============================================================================  
  constant c_SWEEP_FINE_PHASE_MEASUREMENT : integer := 512; -- 64*4*2 OLT_UI * 4 * 2 = 2*ONU_UI (This value should be bigger than one and smaller than 1000)
  --============================================================================

end pon_olt_package_static;