--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file dec_8b10b_wrapper.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: 8b10b decoder wrapper 
--
--! @brief Instantiates two 8b10b decoder with running disparity check error (from http://asics.chuckbenz.com/#My_open_source_8b10b_encoderdecoder)
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date -
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! decoder8b10b.v
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 08\05\2017 - EBSM - Created\n
--! <extended description>
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for dec_8b10b_wrapper
--============================================================================ 
entity dec_8b10b_wrapper is
  generic(
    g_NBR_8B10B_DEC : integer := 2      
    );
  port(
    clk_i   : in  std_logic;          --! falling edge triggered
    reset_i   : in  std_logic;
    datain_i  : in  std_logic_vector(10*g_NBR_8B10B_DEC-1 downto 0);
    dataout_o : out std_logic_vector(8*g_NBR_8B10B_DEC-1 downto 0);
    dataisk_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0);
    disperr_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0);
    codeerr_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0)
    );
end dec_8b10b_wrapper;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of dec_8b10b_wrapper is

  component decoder8b10b is
    port(
      datain   : in  std_logic_vector(9 downto 0);
      dispin   : in  std_logic;
      dataout  : out std_logic_vector(7 downto 0);
      commaout : out std_logic;
      dispout  : out std_logic;
      code_err : out std_logic;
      disp_err : out std_logic
      );
  end component decoder8b10b;

  --! Signal declaration
  signal disp_in_s    : std_logic_vector((g_NBR_8B10B_DEC-1) downto 0);
  signal disp_fbk_reg : std_logic;
  signal disp_out_s   : std_logic_vector((g_NBR_8B10B_DEC-1) downto 0);

  signal dataout_s : std_logic_vector(8*g_NBR_8B10B_DEC-1 downto 0);
  signal dataisk_s : std_logic_vector((g_NBR_8B10B_DEC-1) downto 0);
  signal codeerr_s : std_logic_vector((g_NBR_8B10B_DEC-1) downto 0);
  signal disperr_s : std_logic_vector((g_NBR_8B10B_DEC-1) downto 0);

  signal datain_inv  : std_logic_vector(datain_i'range);
  signal dataout_inv : std_logic_vector(dataout_s'range);


--============================================================================
-- ! architecture begin
--============================================================================
begin


  --============================================================================
  -- Generic component instantiation
  --! 8b10b decoder declaration
  --============================================================================        
  gen_cmp_8b10b : for i in 0 to (g_NBR_8B10B_DEC-1) generate
  begin
    cmp_decoder8b10b : decoder8b10b
      port map(
        datain   => datain_i((i+1)*10-1 downto i*10),
        dispin   => disp_in_s(i),
        dataout  => dataout_s((i+1)*8-1 downto i*8),
        commaout => dataisk_s(i),
        dispout  => disp_out_s(i),
        code_err => codeerr_s(i),
        disp_err => disperr_s(i)
        );
  end generate gen_cmp_8b10b;

  disp_in_s(disp_in_s'left downto 1) <= disp_out_s(disp_in_s'left-1 downto 0);
  disp_in_s(0)                       <= disp_fbk_reg;

  --============================================================================
  -- Process p_feedback_disp
  --! Feedback register to keep disparity value of 8b10b decoding
  --! read: disp_out_s(g_NBR_8B10B_DEC-1) \n
  --! write: disp_fbk_reg \n
  --! r/w: \n
  --============================================================================        
  p_feedback_disp : process (clk_i)
  begin
    if clk_i'event and clk_i = '0' then
      if reset_i = '1' then
        disp_fbk_reg <= '0';
      else
        disp_fbk_reg <= disp_out_s(g_NBR_8B10B_DEC-1);
      end if;
    end if;
  end process;

  --============================================================================
  -- Process p_output_reg
  --! Output Register
  --! read:  dataout_s, codeerr_s, disperr_s\n
  --! write: dataout_o, codeerr_o, disperr_o\n
  --! r/w: \n
  --============================================================================        
  p_output_reg : process (clk_i)
  begin
    if clk_i'event and clk_i = '0' then
      if(reset_i = '1') then
        dataout_o <= (others => '0');
        dataisk_o <= (others => '0');
        codeerr_o <= (others => '0');
        disperr_o <= (others => '0');
      else
        dataout_o <= dataout_s;
        dataisk_o <= dataisk_s;
        codeerr_o <= codeerr_s;
        disperr_o <= disperr_s;
      end if;
    end if;
  end process p_output_reg;
  
end structural;
