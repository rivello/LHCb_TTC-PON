--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file frame_data_mux.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: Frame data mux (frame_data_mux)
--
--! @brief Muxes frame data received from 4 word aligner modules
--! (Detailed description will come here.)
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--! @date 05-09-2014
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 05-09-2014 CS  Created\n
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for frame_data_mux
--============================================================================
entity frame_data_mux is
  
  port (
    clk_i        : in  std_logic;
    din_p1_i       : in  std_logic_vector;
    pos_p1_i       : in  std_logic_vector;
    comma_p1_i     : in  std_logic;
    dvalid_p1_i    : in  std_logic;
    din_p2_i       : in  std_logic_vector;
    pos_p2_i       : in  std_logic_vector;
    comma_p2_i     : in  std_logic;
    dvalid_p2_i    : in  std_logic;
    din_p3_i       : in  std_logic_vector;
    pos_p3_i       : in  std_logic_vector;
    comma_p3_i     : in  std_logic;
    dvalid_p3_i    : in  std_logic;
    din_p4_i       : in  std_logic_vector;
    pos_p4_i       : in  std_logic_vector;
    comma_p4_i     : in  std_logic;
    dvalid_p4_i    : in  std_logic;
    frame_length_i : in  std_logic_vector;
    dout_o         : out std_logic_vector;
    pos_o          : out std_logic_vector;
    comma_o        : out std_logic;
    frame_o        : out std_logic;
    phase_o        : out std_logic_vector
    );

end entity frame_data_mux;

--============================================================================
-- ! architecture declaration
--============================================================================  
architecture rtl of frame_data_mux is

  type   t_mux_states is (GAP, COMMA, COMMA1, PAYLOAD);
  signal mux_state : t_mux_states := GAP;

  signal frame_mux_sel      : integer range 0 to 11 := 0;
  signal frame_mux_sel_raux : integer range 0 to 11 := 0;

  signal din_p1_r : std_logic_vector(din_p1_i'range);
  signal din_p2_r : std_logic_vector(din_p2_i'range);
  signal din_p3_r : std_logic_vector(din_p3_i'range);
  signal din_p4_r : std_logic_vector(din_p4_i'range);
  signal pos_p1_r : std_logic_vector(pos_p1_i'range);
  signal pos_p2_r : std_logic_vector(pos_p2_i'range);
  signal pos_p3_r : std_logic_vector(pos_p3_i'range);
  signal pos_p4_r : std_logic_vector(pos_p4_i'range);


  signal din_p1_rr : std_logic_vector(din_p1_i'range);
  signal din_p2_rr : std_logic_vector(din_p2_i'range);
  signal din_p3_rr : std_logic_vector(din_p3_i'range);
  signal din_p4_rr : std_logic_vector(din_p4_i'range);
  signal pos_p1_rr : std_logic_vector(pos_p1_i'range);
  signal pos_p2_rr : std_logic_vector(pos_p2_i'range);
  signal pos_p3_rr : std_logic_vector(pos_p3_i'range);
  signal pos_p4_rr : std_logic_vector(pos_p4_i'range);

  signal comma_p1_r : std_logic;
  signal comma_p2_r : std_logic;
  signal comma_p3_r : std_logic;
  signal comma_p4_r : std_logic;

  signal dvalid_p1_r : std_logic;
  signal dvalid_p2_r : std_logic;
  signal dvalid_p3_r : std_logic;
  signal dvalid_p4_r : std_logic;


  signal din_p1_r3 : std_logic_vector(din_p1_i'range);
  signal din_p2_r3 : std_logic_vector(din_p2_i'range);
  signal din_p3_r3 : std_logic_vector(din_p3_i'range);
  signal din_p4_r3 : std_logic_vector(din_p4_i'range);
  signal pos_p1_r3 : std_logic_vector(pos_p1_i'range);
  signal pos_p2_r3 : std_logic_vector(pos_p2_i'range);
  signal pos_p3_r3 : std_logic_vector(pos_p3_i'range);
  signal pos_p4_r3 : std_logic_vector(pos_p4_i'range);

  signal comma_p1_r2 : std_logic;
  signal comma_p2_r2 : std_logic;
  signal comma_p3_r2 : std_logic;
  signal comma_p4_r2 : std_logic;

  signal dvalid_p1_r2 : std_logic;
  signal dvalid_p2_r2 : std_logic;
  signal dvalid_p3_r2 : std_logic;
  signal dvalid_p4_r2 : std_logic;

  signal din_p1_r4 : std_logic_vector(din_p1_i'range);
  signal din_p2_r4 : std_logic_vector(din_p2_i'range);
  signal din_p3_r4 : std_logic_vector(din_p3_i'range);
  signal din_p4_r4 : std_logic_vector(din_p4_i'range);
  signal pos_p1_r4 : std_logic_vector(pos_p1_i'range);
  signal pos_p2_r4 : std_logic_vector(pos_p2_i'range);
  signal pos_p3_r4 : std_logic_vector(pos_p3_i'range);
  signal pos_p4_r4 : std_logic_vector(pos_p4_i'range);

  signal comma_p1_r3 : std_logic;
  signal comma_p2_r3 : std_logic;
  signal comma_p3_r3 : std_logic;
  signal comma_p4_r3 : std_logic;

  signal dvalid_p1_r3 : std_logic;
  signal dvalid_p2_r3 : std_logic;
  signal dvalid_p3_r3 : std_logic;
  signal dvalid_p4_r3 : std_logic;


  signal din_p1_r5 : std_logic_vector(din_p1_i'range);
  signal din_p2_r5 : std_logic_vector(din_p2_i'range);
  signal din_p3_r5 : std_logic_vector(din_p3_i'range);
  signal din_p4_r5 : std_logic_vector(din_p4_i'range);
  signal pos_p1_r5 : std_logic_vector(pos_p1_i'range);
  signal pos_p2_r5 : std_logic_vector(pos_p2_i'range);
  signal pos_p3_r5 : std_logic_vector(pos_p3_i'range);
  signal pos_p4_r5 : std_logic_vector(pos_p4_i'range);


  signal com_dval_0edge : std_logic_vector(3 downto 0);
  signal com_dval_1edge : std_logic_vector(3 downto 0);
  signal com_dval_2edge : std_logic_vector(3 downto 0);

  signal com_dval_1edge_r : std_logic_vector(3 downto 0);
  signal com_dval_2edge_r : std_logic_vector(3 downto 0);

  signal good_0edge   : std_logic;
  signal good_1edge   : std_logic;
  signal good_2edge   : std_logic;
  signal good_1edge_r : std_logic;
  signal good_2edge_r : std_logic;

  signal frame_cntr : unsigned(frame_length_i'range) := (others => '0');
--============================================================================
-- ! architecture begin
--============================================================================          
begin  -- architecture rtl

  --comma arrived and there are no edges in data
  com_dval_0edge(3) <= (comma_p1_r3 and dvalid_p1_r3 and dvalid_p1_r2 and dvalid_p1_r and dvalid_p1_i);
  com_dval_0edge(2) <= (comma_p2_r3 and dvalid_p2_r3 and dvalid_p2_r2 and dvalid_p2_r and dvalid_p2_i);
  com_dval_0edge(1) <= (comma_p3_r3 and dvalid_p3_r3 and dvalid_p3_r2 and dvalid_p3_r and dvalid_p3_i);
  com_dval_0edge(0) <= (comma_p4_r3 and dvalid_p4_r3 and dvalid_p4_r2 and dvalid_p4_r and dvalid_p4_i);

  --comma arrived and there is a single edge in data
  com_dval_1edge(3) <= (comma_p1_r3 and ((dvalid_p1_i and dvalid_p1_r2 and dvalid_p1_r) or (dvalid_p1_r3 and dvalid_p1_r2 and dvalid_p1_r) or (dvalid_p1_r3 and dvalid_p1_r2 and dvalid_p1_i) or (dvalid_p1_r3 and dvalid_p1_r and dvalid_p1_i)));
  com_dval_1edge(2) <= (comma_p2_r3 and ((dvalid_p2_i and dvalid_p2_r2 and dvalid_p2_r) or (dvalid_p2_r3 and dvalid_p2_r2 and dvalid_p2_r) or (dvalid_p2_r3 and dvalid_p2_r2 and dvalid_p2_i) or (dvalid_p2_r3 and dvalid_p2_r and dvalid_p2_i)));
  com_dval_1edge(1) <= (comma_p3_r3 and ((dvalid_p3_i and dvalid_p3_r2 and dvalid_p3_r) or (dvalid_p3_r3 and dvalid_p3_r2 and dvalid_p3_r) or (dvalid_p3_r3 and dvalid_p3_r2 and dvalid_p3_i) or (dvalid_p3_r3 and dvalid_p3_r and dvalid_p3_i)));
  com_dval_1edge(0) <= (comma_p4_r3 and ((dvalid_p4_i and dvalid_p4_r2 and dvalid_p4_r) or (dvalid_p4_r3 and dvalid_p4_r2 and dvalid_p4_r) or (dvalid_p4_r3 and dvalid_p4_r2 and dvalid_p4_i) or (dvalid_p4_r3 and dvalid_p4_r and dvalid_p4_i)));

  --comma arrived and there are one or more edge in data
  com_dval_2edge(3) <= (comma_p1_r3 and not(dvalid_p1_r3 and dvalid_p1_r2 and dvalid_p1_r and dvalid_p1_i));
  com_dval_2edge(2) <= (comma_p2_r3 and not(dvalid_p2_r3 and dvalid_p2_r2 and dvalid_p2_r and dvalid_p2_i));
  com_dval_2edge(1) <= (comma_p3_r3 and not(dvalid_p3_r3 and dvalid_p3_r2 and dvalid_p3_r and dvalid_p3_i));
  com_dval_2edge(0) <= (comma_p4_r3 and not(dvalid_p4_r3 and dvalid_p4_r2 and dvalid_p4_r and dvalid_p4_i));

  good_0edge <= (com_dval_0edge(3) or com_dval_0edge(2)) or (com_dval_0edge(1) or com_dval_0edge(0));
  good_1edge <= (com_dval_1edge(3) or com_dval_1edge(2)) or (com_dval_1edge(1) or com_dval_1edge(0));
  good_2edge <= (com_dval_2edge(3) or com_dval_2edge(2)) or (com_dval_2edge(1) or com_dval_2edge(0));


  p_delay : process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      com_dval_1edge_r <= com_dval_1edge;
      com_dval_2edge_r <= com_dval_2edge;

      good_1edge_r <= good_1edge;
      good_2edge_r <= good_2edge;

      comma_p1_r <= comma_p1_i;
      comma_p2_r <= comma_p2_i;
      comma_p3_r <= comma_p3_i;
      comma_p4_r <= comma_p4_i;

      comma_p1_r2 <= comma_p1_r;
      comma_p2_r2 <= comma_p2_r;
      comma_p3_r2 <= comma_p3_r;
      comma_p4_r2 <= comma_p4_r;

      comma_p1_r3 <= comma_p1_r2;
      comma_p2_r3 <= comma_p2_r2;
      comma_p3_r3 <= comma_p3_r2;
      comma_p4_r3 <= comma_p4_r2;


      dvalid_p1_r <= dvalid_p1_i;
      dvalid_p2_r <= dvalid_p2_i;
      dvalid_p3_r <= dvalid_p3_i;
      dvalid_p4_r <= dvalid_p4_i;

      dvalid_p1_r2 <= dvalid_p1_r;
      dvalid_p2_r2 <= dvalid_p2_r;
      dvalid_p3_r2 <= dvalid_p3_r;
      dvalid_p4_r2 <= dvalid_p4_r;

      dvalid_p1_r3 <= dvalid_p1_r2;
      dvalid_p2_r3 <= dvalid_p2_r2;
      dvalid_p3_r3 <= dvalid_p3_r2;
      dvalid_p4_r3 <= dvalid_p4_r2;
    end if;
  end process;

  p_frame_mux : process (clk_i) is
  begin  -- process p_frame_mux
    if clk_i'event and clk_i = '1' then     -- rising clock edge
      comma_o <= '0';
      frame_o <= '0';
      case mux_state is
        when GAP =>
          if (good_0edge = '1') then  --at least one of the detected phases has no edges in the payload, take this phase
            case com_dval_0edge is
              when "0000" => mux_state <= GAP;  --this condition is impossible to happen       
              when "0001" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "0010" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0011" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0100" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0101" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0111" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "1000" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1001" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1010" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1011" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "1100" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1101" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "1111" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when others => mux_state <= GAP;
            end case;
          elsif(good_1edge = '1' or good_2edge = '1') then  --there was a detected comma in one of the phases but there are edges in payload
            --wait for next clock cycle to take decision   
            mux_state <= COMMA1;
          else
            mux_state <= GAP;
          end if;

        when COMMA1 =>
          if (good_0edge = '1') then
            case com_dval_0edge is
              when "0000" => mux_state <= GAP;  --this condition is impossible to happen        
              when "0001" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "0010" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0011" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0100" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0101" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0111" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "1000" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1001" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1010" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1011" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "1100" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1101" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "1111" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when others => mux_state <= GAP;
            end case;
          elsif(good_1edge = '1') then
            case com_dval_1edge is
              when "0000" => mux_state <= GAP;  --this condition is impossible to happen        
              when "0001" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "0010" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0011" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "0100" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0101" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "0111" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
              when "1000" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1001" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1010" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1011" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
              when "1100" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1101" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
              when "1110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when "1111" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
              when others => mux_state <= GAP;
            end case;
          elsif(good_1edge_r = '1') then
            case com_dval_1edge_r is
              when "0000" => mux_state <= GAP;  --this condition is impossible to happen        
              when "0001" => mux_state <= PAYLOAD; frame_mux_sel <= 3; comma_o <= '1';
              when "0010" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "0011" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "0100" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0101" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0110" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0111" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "1000" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1001" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1010" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1011" => mux_state <= PAYLOAD; frame_mux_sel <= 3; comma_o <= '1';
              when "1100" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1101" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1110" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "1111" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when others => mux_state <= GAP;
            end case;
          elsif(good_2edge_r = '1') then
            case com_dval_2edge_r is
              when "0000" => mux_state <= GAP;  --this condition is impossible to happen        
              when "0001" => mux_state <= PAYLOAD; frame_mux_sel <= 3; comma_o <= '1';
              when "0010" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "0011" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "0100" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0101" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0110" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "0111" => mux_state <= PAYLOAD; frame_mux_sel <= 2; comma_o <= '1';
              when "1000" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1001" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1010" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1011" => mux_state <= PAYLOAD; frame_mux_sel <= 3; comma_o <= '1';
              when "1100" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1101" => mux_state <= PAYLOAD; frame_mux_sel <= 0; comma_o <= '1';
              when "1110" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when "1111" => mux_state <= PAYLOAD; frame_mux_sel <= 1; comma_o <= '1';
              when others => mux_state <= GAP;
            end case;
          else
            mux_state <= GAP;
          end if;
          
        when COMMA =>
          comma_o       <= '1';
          frame_mux_sel <= frame_mux_sel_raux;
          mux_state     <= PAYLOAD;
          
        when PAYLOAD =>
          frame_o <= '1';

          if (frame_cntr >= unsigned(frame_length_i)) then
            if (good_0edge = '1') then
              case com_dval_0edge is
                when "0000" => mux_state <= GAP;  --this condition is impossible to happen        
                when "0001" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
                when "0010" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
                when "0011" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
                when "0100" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
                when "0101" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
                when "0110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
                when "0111" => mux_state <= COMMA; frame_mux_sel_raux <= 2;
                when "1000" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
                when "1001" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
                when "1010" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
                when "1011" => mux_state <= COMMA; frame_mux_sel_raux <= 3;
                when "1100" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
                when "1101" => mux_state <= COMMA; frame_mux_sel_raux <= 0;
                when "1110" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
                when "1111" => mux_state <= COMMA; frame_mux_sel_raux <= 1;
                when others => mux_state <= GAP;
              end case;
            elsif(good_1edge = '1' or good_2edge = '1') then
              mux_state <= COMMA1;
            else
              mux_state <= GAP;
            end if;
          end if;
          
          
        when others => mux_state <= GAP;
      end case;

      din_p1_r  <= din_p1_i;
      din_p2_r  <= din_p2_i;
      din_p3_r  <= din_p3_i;
      din_p4_r  <= din_p4_i;
      din_p1_rr <= din_p1_r;
      din_p2_rr <= din_p2_r;
      din_p3_rr <= din_p3_r;
      din_p4_rr <= din_p4_r;

      din_p1_r3 <= din_p1_rr;
      din_p2_r3 <= din_p2_rr;
      din_p3_r3 <= din_p3_rr;
      din_p4_r3 <= din_p4_rr;

      din_p1_r4 <= din_p1_r3;
      din_p2_r4 <= din_p2_r3;
      din_p3_r4 <= din_p3_r3;
      din_p4_r4 <= din_p4_r3;

      din_p1_r5 <= din_p1_r4;
      din_p2_r5 <= din_p2_r4;
      din_p3_r5 <= din_p3_r4;
      din_p4_r5 <= din_p4_r4;

      pos_p1_r  <= pos_p1_i;
      pos_p2_r  <= pos_p2_i;
      pos_p3_r  <= pos_p3_i;
      pos_p4_r  <= pos_p4_i;
      pos_p1_rr <= pos_p1_r;
      pos_p2_rr <= pos_p2_r;
      pos_p3_rr <= pos_p3_r;
      pos_p4_rr <= pos_p4_r;

      pos_p1_r3 <= pos_p1_rr;
      pos_p2_r3 <= pos_p2_rr;
      pos_p3_r3 <= pos_p3_rr;
      pos_p4_r3 <= pos_p4_rr;

      pos_p1_r4 <= pos_p1_r3;
      pos_p2_r4 <= pos_p2_r3;
      pos_p3_r4 <= pos_p3_r3;
      pos_p4_r4 <= pos_p4_r3;

      pos_p1_r5 <= pos_p1_r4;
      pos_p2_r5 <= pos_p2_r4;
      pos_p3_r5 <= pos_p3_r4;
      pos_p4_r5 <= pos_p4_r4;

      case frame_mux_sel is
        when 0      => dout_o <= din_p1_r5; pos_o <= pos_p1_r5; phase_o <= std_logic_vector(to_unsigned(frame_mux_sel, phase_o'length));
        when 1      => dout_o <= din_p2_r5; pos_o <= pos_p2_r5; phase_o <= std_logic_vector(to_unsigned(frame_mux_sel, phase_o'length));
        when 2      => dout_o <= din_p3_r5; pos_o <= pos_p3_r5; phase_o <= std_logic_vector(to_unsigned(frame_mux_sel, phase_o'length));
        when 3      => dout_o <= din_p4_r5; pos_o <= pos_p4_r5; phase_o <= std_logic_vector(to_unsigned(frame_mux_sel, phase_o'length));
        when others => null;
      end case;
      
    end if;
  end process p_frame_mux;

  p_frame_cntr : process(clk_i) is
  begin
    if (clk_i'event and clk_i = '1') then
      if(mux_state = PAYLOAD) then
        frame_cntr <= frame_cntr + 1;
      else
        frame_cntr <= to_unsigned(0, frame_cntr'length);
      end if;
    end if;
  end process p_frame_cntr;
  
end architecture rtl;
