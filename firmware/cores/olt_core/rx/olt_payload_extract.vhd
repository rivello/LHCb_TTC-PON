--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_payload_extract.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT Rx Payload Extract (olt_payload_extract)
--
--! @brief Extracts received words into frames
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 24\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 24\06\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo documentation \n
--! 
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_payload_extract
--============================================================================
entity olt_payload_extract is
  port (
    clk_i             : in  std_logic;
    reset_i           : in  std_logic;
    comma_osrx_i      : in  std_logic;
    position_osrx_i   : in  std_logic_vector(6 downto 0);
    position_osrx_o   : out std_logic_vector(6 downto 0);
    rx_k28_1_o        : out std_logic;
    rx_k28_5_o        : out std_logic;
    dataisk_dec_i     : in  std_logic_vector(0 downto 0);
    codeerr_dec_i     : in  std_logic_vector(0 downto 0);
    disperr_dec_i     : in  std_logic_vector(0 downto 0);
    decoded_data_i    : in  std_logic_vector(7 downto 0);
    rx_frame_length_i : in  std_logic_vector(3 downto 0);
    data_error_o      : out std_logic;
    data_strobe_o     : out std_logic;
    data_usr_o        : out std_logic_vector(55 downto 0);
    data_ctrl_o       : out std_logic_vector(7 downto 0);
    onu_addr_o        : out std_logic_vector(7 downto 0)
    );
end entity olt_payload_extract;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of olt_payload_extract is

  --! Functions

  --! Constants
  constant c_PIPE_DEPTH : integer := 10;

  --! Signal declaration
  signal comma_osrx_r : std_logic;
  signal rx_k28_1     : std_logic;
  signal rx_k28_5     : std_logic;

  signal rx_k28_1_pipe : std_logic_vector(c_PIPE_DEPTH-1 downto 0);
  signal rx_k28_5_pipe : std_logic_vector(c_PIPE_DEPTH-1 downto 0);

  signal or_codeerr   : std_logic;
  signal or_disperr   : std_logic;
  signal codeerr_pipe : std_logic_vector(c_PIPE_DEPTH-1 downto 0);
  signal disperr_pipe : std_logic_vector(c_PIPE_DEPTH-1 downto 0);

  type   t_frame_buf is array (c_PIPE_DEPTH-1 downto 0) of std_logic_vector(decoded_data_i'range);
  signal frame_buf : t_frame_buf;

  type   t_pos_buf is array (c_PIPE_DEPTH-1 downto 0) of std_logic_vector(position_osrx_i'range);
  signal position_osrx_buf : t_pos_buf;


  --! Component declaration

--============================================================================
-- architecture begin
--============================================================================
begin

  ----------------------------------------------------------------------------
  --! Process pipe
  --! read:  decoded_data_i\n
  --! write: frame_buf\n
  ----------------------------------------------------------------------------   
  p_pipe : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      rx_k28_5_pipe(rx_k28_5_pipe'left downto 1) <= rx_k28_5_pipe(rx_k28_5_pipe'left-1 downto 0);
      rx_k28_5_pipe(0)                             <= rx_k28_5;

      rx_k28_1_pipe(rx_k28_1_pipe'left downto 1) <= rx_k28_1_pipe(rx_k28_1_pipe'left-1 downto 0);
      rx_k28_1_pipe(0)                             <= rx_k28_1;

      frame_buf(frame_buf'left downto 1) <= frame_buf(frame_buf'left-1 downto 0);
      frame_buf(0)                         <= decoded_data_i;

      position_osrx_buf(position_osrx_buf'left downto 1) <= position_osrx_buf(position_osrx_buf'left-1 downto 0);
      position_osrx_buf(0)                                 <= position_osrx_i;

      codeerr_pipe(codeerr_pipe'left downto 1) <= codeerr_pipe(codeerr_pipe'left-1 downto 0);
      codeerr_pipe(0)                            <= or_codeerr;

      disperr_pipe(disperr_pipe'left downto 1) <= disperr_pipe(disperr_pipe'left-1 downto 0);
      disperr_pipe(0)                            <= or_disperr;

    end if;
  end process p_pipe;

  or_codeerr <= codeerr_dec_i(0);
  or_disperr <= disperr_dec_i(0);


  ----------------------------------------------------------------------------
  --! Process comma identification
  --! read:  clock_i, reset_i, comma_osrx_i, dataisk_dec_i\n
  --! write: rx_k28_1, rx_k28_5\n
  --! r/w:   comma_osrx_r\n
  ----------------------------------------------------------------------------   
  p_comma_identification : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if (reset_i = '1') then
        rx_k28_5 <= '0';
        rx_k28_1 <= '0';
      else
        comma_osrx_r <= comma_osrx_i;
        if(comma_osrx_r = '1' and dataisk_dec_i(0) = '1') then
          if(decoded_data_i(7 downto 0) = x"3C") then
            rx_k28_1 <= '1';
          else
            rx_k28_1 <= '0';
          end if;
          if(decoded_data_i(7 downto 0) = x"BC") then
            rx_k28_5 <= '1';
          else
            rx_k28_5 <= '0';
          end if;
        else
          rx_k28_1 <= '0';
          rx_k28_5 <= '0';
        end if;
      end if;
    end if;
  end process p_comma_identification;

  ----------------------------------------------------------------------------
  --! Process interface
  --! read:  clock_i, reset_i, rx_k28_1,rx_k28_5,rx_k28_1_pipe,rx_k28_5_pipe,frame_buf\n
  --! write: data_ctrl_o,position_osrx_o,frame_ctrl_strobe_o,\n
  --! r/w:   s_onu_addr\n
  ----------------------------------------------------------------------------   
  p_interface : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      
      if(rx_k28_1_pipe(7) = '1' or rx_k28_5_pipe(7) = '1') then
        data_usr_o    <= decoded_data_i&frame_buf(0)&frame_buf(1)&frame_buf(2)&frame_buf(3)&frame_buf(4)&frame_buf(5);
        data_strobe_o <= '1';

        data_ctrl_o     <= frame_buf(6);
        position_osrx_o <= position_osrx_buf(8);
        onu_addr_o      <= frame_buf(7);

        if(rx_frame_length_i = "0000") then
          data_error_o <= (or_codeerr or codeerr_pipe(0) or codeerr_pipe(1) or codeerr_pipe(2)) or 
                          (or_disperr or codeerr_pipe(0) or codeerr_pipe(1));
        else
          data_error_o <= (or_codeerr or codeerr_pipe(0)or codeerr_pipe(1)or codeerr_pipe(2) or codeerr_pipe(3)or codeerr_pipe(4) or codeerr_pipe(5) or codeerr_pipe(6)) or
                          (or_disperr or disperr_pipe(0)or disperr_pipe(1)or disperr_pipe(2) or disperr_pipe(3)or disperr_pipe(4));
        end if;
      else
        data_strobe_o <= '0';
        data_error_o  <= '0';
      end if;
    end if;
  end process p_interface;

  rx_k28_1_o <= rx_k28_1_pipe(8);
  rx_k28_5_o <= rx_k28_5_pipe(8);
  
end architecture rtl;
--============================================================================
-- architecture end
--============================================================================

