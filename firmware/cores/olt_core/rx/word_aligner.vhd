--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file word_aligner.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: Word aligner (word_aligner)
--
--! @brief Performs word alignement using a special character
--! The word alignment is performed by detecting special characters (commas) in
--! the downsampled data word. To avoid false comma detection, a state-machine
--! controls when the comma is being searched for.
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 07-01-2014
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07-01-2014 CS  Created\n
--! 27-01-2014 CS  Added FSM to control the word alignment\n
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for word_aligner
--============================================================================
entity word_aligner is
  generic (
    G_PCOMMA_CHAR   : std_logic_vector := "10101111";
    G_MCOMMA_CHAR   : std_logic_vector := "10101111";
    G_COMMA_MASK    : std_logic_vector := "11111111";
    G_PREAMBLE_CHAR : std_logic_vector := "01010101");
  port (
    clk_i        : in  std_logic;
    reset_i        : in  std_logic;
    en_pcomma_i    : in  std_logic;
    en_mcomma_i    : in  std_logic;
    frame_length_i : in  std_logic_vector;
    datain_i       : in  std_logic_vector;
    edges_i        : in  std_logic_vector;
    dataout_o      : out std_logic_vector;
    doutena_o      : out std_logic;
    comma_o        : out std_logic;
    frame_o        : out std_logic;
    position_o     : out std_logic_vector);

end entity word_aligner;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of word_aligner is

  function or_reduce(arg : std_logic_vector) return std_logic is
    variable result : std_logic;
  begin
    result := '0';
    for i in arg'range loop
      result := result or arg(i);
    end loop;
    return result;
  end;

  type   t_char_array is array (0 to datain_i'length-1) of std_logic_vector(G_PCOMMA_CHAR'length-1 downto 0);
  signal delayed_chars : t_char_array;

  signal datain_r      : std_logic_vector(datain_i'range);
  signal data_bits     : std_logic_vector(datain_i'length*2-1 downto 0);
  signal data_bits_r   : std_logic_vector(datain_i'length*2-1 downto 0);
  signal data_bits_rr  : std_logic_vector(datain_i'length*2-1 downto 0);
  signal edges_r       : std_logic_vector(datain_i'range);
  signal edge_bits     : std_logic_vector(datain_i'length*2-1 downto 0);
  signal edge_bits_r   : std_logic_vector(datain_i'length*2-1 downto 0);
  signal edge_bits_rr  : std_logic_vector(datain_i'length*2-1 downto 0);
  signal bit_select    : integer range 0 to 31;
  signal comma_found   : std_logic;
  signal comma_clean   : std_logic_vector(datain_i'length-1 downto 0);
  --signal address_clean : std_logic_vector(datain_i'length-1 downto 0);
  
  type t_word_align_states is (
    RESET,
    SEARCH,
    FRAME);
  signal word_align_state : t_word_align_states    := RESET;
  signal frame_counter    : integer range 0 to 255 := 0;

--============================================================================
-- ! architecture begin
--============================================================================  
begin  -- architecture rtl

  -- Pipeline registers
  p_data_reg : process (clk_i) is
  begin  -- process p_data_reg
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      datain_r     <= datain_i;
      data_bits_r  <= data_bits;
      data_bits_rr <= data_bits_r;
      edges_r      <= edges_i;
      edge_bits_r  <= edge_bits;
      edge_bits_rr <= edge_bits_r;
    end if;
  end process p_data_reg;
  data_bits <= datain_i & datain_r;
  edge_bits <= edges_i & edges_r;

  -- The word alignment is done by detecting comma characters in the input
  -- data stream. To avoid false comma detection the state-machine prevents
  -- comma searching for a number of clock cycles defined by G_FRAME_LENGTH.
  p_word_align_fsm : process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      if reset_i = '1' then
        word_align_state <= RESET;
        frame_counter    <= 0;
      else
        word_align_state <= word_align_state;
        case word_align_state is
          when RESET =>
            frame_counter    <= 0;
            word_align_state <= SEARCH;
          when SEARCH =>
            frame_counter <= 0;
            if comma_found = '1' then
              word_align_state <= FRAME;
            end if;
          when FRAME =>
            frame_counter <= frame_counter + 1;
            if frame_counter = to_integer(unsigned(frame_length_i)) then
              word_align_state <= SEARCH;
            end if;
          when others =>
            word_align_state <= RESET;
        end case;
      end if;
    end if;
  end process p_word_align_fsm;

  -- The comma character could be at any bit position within the input data
  -- word. In order to detect the correct position the input data is shifted
  -- at N different bit positions, where N is the number of bits in the input
  -- data word.
  p_char_samples : process (clk_i) is
  begin  -- process p_char_samples
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if reset_i = '1' then
        delayed_chars <= (others => (others => '0'));
        comma_clean   <= (others => '0');
        --address_clean <= (others => '0');
      else
        for i in 0 to datain_i'length-1 loop
          delayed_chars(i) <= data_bits(G_PCOMMA_CHAR'length-1+i downto i);
          comma_clean(i)   <= not or_reduce(edge_bits(G_PCOMMA_CHAR'length-1+i downto i));
          --address_clean(i) <= not or_reduce(edge_bits_r(G_PCOMMA_CHAR'length-1+i downto i));
        end loop;
      end if;
    end if;
  end process p_char_samples;

  -- The bit-shifted copies of the input data word are compared with the masked
  -- comma character, and when the comma is found the proper word alignment
  -- position is defined. The alignment takes place only when the state-machine
  -- is in the SEARCH state.
  p_char_select : process (clk_i) is
  begin  -- process p_char_select
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if reset_i = '1' then
        bit_select  <= 0;
        comma_found <= '0';
      else
        comma_found <= '0';

        for i in datain_i'length-1 downto 0 loop
          if ((((delayed_chars(i) and G_COMMA_MASK) = (G_PCOMMA_CHAR and G_COMMA_MASK)) and en_pcomma_i = '1') or (((delayed_chars(i) and G_COMMA_MASK) = (G_MCOMMA_CHAR and G_COMMA_MASK)) and en_mcomma_i = '1')) and
            comma_clean(i) = '1'  and comma_found = '0' then --and address_clean(i) = '1' then
            comma_found <= '1';
            bit_select  <= i;
          end if;
        end loop;
      end if;
    end if;
  end process p_char_select;

  -- The aligned output is generated using the correct bit location.
  process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      frame_o <= '0';
      if word_align_state = FRAME then
        frame_o <= '1';
      end if;
      comma_o   <= comma_found;
      dataout_o <= data_bits_rr(dataout_o'length-1+bit_select downto bit_select);
      doutena_o <= not or_reduce(edge_bits_r(dataout_o'length-1+bit_select downto bit_select));
    end if;
  end process;

  position_o <= std_logic_vector(to_unsigned(bit_select, position_o'length));

end architecture rtl;
