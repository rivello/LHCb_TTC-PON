--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file rx_frame_lock.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Receiver Frame Lock Logic for OLT Rx
--
--! @brief Lock Logic for OLT Rx
--! - Allows header detection (i.e. ONU detection) only in very specific intervals of time
--! - This block makes use of the cyclic nature of the TTC-PON framing scheme
--! - It enables the BOS only for three clock cycles every superframe
--! - In the future with a fully fixed phase PON, it is recommended to place ONU's in the middle of the clock cycle and enable only once for every superframe
--! - This can also be improved including an ONU address detection (i.e. the OLT does not only knows when it should detect an ONU but also which ONU should be detected)
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 06\11\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 06\11\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for rx_frame_lock
--==============================================================================
entity rx_frame_lock is
  generic(
    g_BURST_LENGTH_OLT_RXUSRCLK : integer := 30;   --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
    g_WINDOW_BURST_COUNTING     : integer := 32*4; --! number of bursts to be checked to consider locked
    g_GOOD_BURST_TO_LOCK        : integer := 32*2; --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
    g_GOOD_BURST_TO_UNLOCK      : integer := 32/2   --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
  );
  port (
    clk_i             : in  std_logic;  --! clock input
    reset_i           : in  std_logic;  --! active high sync. reset
    enable_i          : in  std_logic;  --! enables this block
    header_i          : in  std_logic;  --! header detected
    en_header_o       : out std_logic;  --! enables header detection
    locked_o          : out std_logic   --! locked output
    );
end rx_frame_lock;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of rx_frame_lock is

  --! Constant declaration

  --! Signal declaration
  -- en_header creation
  signal en_header_cntr      : integer range 0 to g_BURST_LENGTH_OLT_RXUSRCLK;
  signal en_header_isone     : std_logic;
  signal en_header_istwo     : std_logic;
  signal en_header_isthree   : std_logic;

  -- burst counting
  signal burst_cntr          : integer range 0 to 2*g_WINDOW_BURST_COUNTING;
  signal tick_burst_cntr     : std_logic; --burst_cntr reached g_WINDOW_BURST_COUNTING
  signal detected_burst_cntr : integer range 0 to 2*g_WINDOW_BURST_COUNTING;


  -- FSM CRC-framing
  -- principle:
  -- HUNT            : received at least g_GOOD_BURST_TO_LOCK/8     -> GOING_LOCK
  --                   otherwise                                    -> CHANGE_POSITION
  -- CHANGE_POSITION : retards the en_header_cntr by 1 and then     -> HUNT
  -- GOING_LOCK      : received at least g_GOOD_BURST_TO_LOCK       -> LOCK
  --                   otherwise                                    -> CHANGE_POSITION
  -- LOCK            : received less than g_GOOD_BURST_TO_UNLOCK    -> CHANGE_POSITION
  type t_FRAME_LOCK_FSM is (HUNT, CHANGE_POSITION, GOING_LOCK, LOCK);
  signal frame_lock_state : t_FRAME_LOCK_FSM;

  signal locked    : std_logic;
  signal en_header : std_logic;

begin

  --============================================================================
  -- Process p_en_header_cntr
  --! counter for header enable
  --! read: frame_lock_state\n
  --! write: -\n
  --! r/w: \n
  --============================================================================   
  p_en_header_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        en_header_cntr <= 0;
      elsif(frame_lock_state /= CHANGE_POSITION) then
        if(en_header_cntr = (g_BURST_LENGTH_OLT_RXUSRCLK-1)) then
          en_header_cntr <= 0;
        else
          en_header_cntr <= en_header_cntr + 1;
        end if;
      end if;
    end if;
  end process p_en_header_cntr;

  en_header_isone   <= '1' when (en_header_cntr=1) else '0';
  en_header_istwo   <= '1' when (en_header_cntr=2) else '0';
  en_header_isthree <= '1' when (en_header_cntr=3) else '0';

  --============================================================================
  -- Process p_burst_cntr
  --! counter for expected burst
  --! read: en_header_istwo, tick_burst_cntr\n
  --! write: -\n
  --! r/w: burst_cntr\n
  --============================================================================   
  p_burst_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1' or tick_burst_cntr='1') then
        burst_cntr <= 0;
      elsif(en_header_istwo='1') then
        burst_cntr <= burst_cntr + 1;	  
      end if;
    end if;
  end process p_burst_cntr;
  tick_burst_cntr <= '1' when  (burst_cntr = g_WINDOW_BURST_COUNTING) else '0';

  --============================================================================
  -- Process p_detected_burst_cntr
  --! counter for detected burst
  --! read: header_i, tick_burst_cntr\n
  --! write: -\n
  --! r/w: burst_cntr\n
  --============================================================================   
  p_detected_burst_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1' or tick_burst_cntr='1') then
        detected_burst_cntr <= 0;	
      elsif(header_i='1') then
        detected_burst_cntr <= detected_burst_cntr + 1;	  
      end if;
    end if;
  end process p_detected_burst_cntr;

--------------------------------------------------------------------------------------------------------------------
------------------------------------------------
  --============================================================================
  -- Process p_frame_lock_fsm
  --! FSM for rx locking procedure
  --! read:\n
  --! write: -\n
  --! r/w: frame_lock_state \n
  --============================================================================  
  p_frame_lock_fsm : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        frame_lock_state <= HUNT;
      else
        if(enable_i = '1') then
            case frame_lock_state is
              when HUNT =>
                if(tick_burst_cntr='1') then
                  if(detected_burst_cntr >= g_GOOD_BURST_TO_LOCK/8) then
                    frame_lock_state <= GOING_LOCK;
                  else
                    frame_lock_state <= CHANGE_POSITION;
                  end if;
                end if;            
            
              when CHANGE_POSITION =>
                frame_lock_state <= HUNT;  
            
              when GOING_LOCK =>
                if(tick_burst_cntr='1') then
                  if(detected_burst_cntr >= g_GOOD_BURST_TO_LOCK) then
                    frame_lock_state <= LOCK;
                  else
                    frame_lock_state <= CHANGE_POSITION;
                  end if;
                end if;
            
              when LOCK =>
                if(tick_burst_cntr='1') then
                  if(detected_burst_cntr < g_GOOD_BURST_TO_UNLOCK) then
                    frame_lock_state <= CHANGE_POSITION;
                  end if;
                end if;
            
              when others => frame_lock_state <= HUNT;
            end case;
        end if;
      end if;
    end if;
  end process p_frame_lock_fsm;

  en_header <= (en_header_istwo) when (frame_lock_state = HUNT or frame_lock_state = CHANGE_POSITION) else (en_header_isone or en_header_istwo or en_header_isthree);
  en_header_o <= en_header or (not enable_i) when rising_edge(clk_i);

  locked <= '1' when (frame_lock_state = LOCK or enable_i='0') else '0';
  locked_o <= locked when rising_edge(clk_i);

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

