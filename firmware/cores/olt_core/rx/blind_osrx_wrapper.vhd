--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file blind_osrx_wrapper.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: Blind oversampling receiver (wrapper)
--
--! @brief Receives oversampled data stream and identifies the best sampling phase
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 30-10-2014
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! downsample.vhd, word_aligner.vhd
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 30-10-2014 CS  Created\n
--------------------------------------------------------------------------------
--! @todo TODO\n
--! implement frame_length_i
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for blind_osrx_wrapper
--============================================================================
entity blind_osrx_wrapper is
  generic (
    ONU_PCOMMA_CHAR   : std_logic_vector := "0101111100";  -- k28.5 (force disparity for upstream commas)
    ONU_MCOMMA_CHAR   : std_logic_vector := "1001111100";  -- K28.1 
    ONU_COMMA_MASK    : std_logic_vector := "1111111111";
    ONU_PREAMBLE_CHAR : std_logic_vector := "0101010101"
    );
  port (
    clk_i : in std_logic;
    reset_i : in std_logic;

    en_pcomma_i    : in std_logic;
    en_mcomma_i    : in std_logic;
    frame_length_i : in std_logic_vector;

    pos_o   : out std_logic_vector;
    comma_o : out std_logic;
    frame_o : out std_logic;
    phase_o : out std_logic_vector;

    rxdata_i : in  std_logic_vector;
    dout_o   : out std_logic_vector
    );

end entity blind_osrx_wrapper;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of blind_osrx_wrapper is

  --! Constants
  constant SAMPLING_RATIO : integer := 4;
  constant C_WORD_SIZE    : integer := 40/SAMPLING_RATIO;

  --! Component declaration
  component downsample is
    port (
      clk_i     : in  std_logic;
      din_i     : in  std_logic_vector;
      dout_p1_o : out std_logic_vector;
      edge_p1_o : out std_logic_vector;
      dout_p2_o : out std_logic_vector;
      edge_p2_o : out std_logic_vector;
      dout_p3_o : out std_logic_vector;
      edge_p3_o : out std_logic_vector;
      dout_p4_o : out std_logic_vector;
      edge_p4_o : out std_logic_vector);
  end component downsample;

  component word_aligner is
    generic (
      G_PCOMMA_CHAR   : std_logic_vector := "10101111";
      G_MCOMMA_CHAR   : std_logic_vector := "10101111";
      G_COMMA_MASK    : std_logic_vector := "11111111";
      G_PREAMBLE_CHAR : std_logic_vector := "01010101");
    port (
      clk_i        : in  std_logic;
      reset_i        : in  std_logic;
      en_pcomma_i    : in  std_logic;
      en_mcomma_i    : in  std_logic;
      frame_length_i : in  std_logic_vector;
      datain_i       : in  std_logic_vector;
      edges_i        : in  std_logic_vector;
      dataout_o      : out std_logic_vector;
      doutena_o      : out std_logic;
      comma_o        : out std_logic;
      frame_o        : out std_logic;
      position_o     : out std_logic_vector);
  end component word_aligner;

  component frame_data_mux is
    port (
      clk_i        : in  std_logic;
      din_p1_i       : in  std_logic_vector;
      pos_p1_i       : in  std_logic_vector;
      comma_p1_i     : in  std_logic;
      dvalid_p1_i    : in  std_logic;
      din_p2_i       : in  std_logic_vector;
      pos_p2_i       : in  std_logic_vector;
      comma_p2_i     : in  std_logic;
      dvalid_p2_i    : in  std_logic;
      din_p3_i       : in  std_logic_vector;
      pos_p3_i       : in  std_logic_vector;
      comma_p3_i     : in  std_logic;
      dvalid_p3_i    : in  std_logic;
      din_p4_i       : in  std_logic_vector;
      pos_p4_i       : in  std_logic_vector;
      comma_p4_i     : in  std_logic;
      dvalid_p4_i    : in  std_logic;
      frame_length_i : in  std_logic_vector;
      dout_o         : out std_logic_vector;
      pos_o          : out std_logic_vector;
      comma_o        : out std_logic;
      frame_o        : out std_logic;
      phase_o        : out std_logic_vector
      );
  end component frame_data_mux;

  --! Signal declaration
  signal dout_p1            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal dout_p2            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal dout_p3            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal dout_p4            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal edge_p1            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal edge_p2            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal edge_p3            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal edge_p4            : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal rx_aligned_data_p1 : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal rx_pos_p1          : std_logic_vector(4 downto 0);
  signal rx_data_ena_p1     : std_logic;
  signal rx_comma_p1        : std_logic;
  signal rx_frame_p1        : std_logic;
  signal rx_aligned_data_p2 : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal rx_pos_p2          : std_logic_vector(4 downto 0);
  signal rx_data_ena_p2     : std_logic;
  signal rx_comma_p2        : std_logic;
  signal rx_frame_p2        : std_logic;
  signal rx_aligned_data_p3 : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal rx_pos_p3          : std_logic_vector(4 downto 0);
  signal rx_data_ena_p3     : std_logic;
  signal rx_comma_p3        : std_logic;
  signal rx_frame_p3        : std_logic;
  signal rx_aligned_data_p4 : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal rx_pos_p4          : std_logic_vector(4 downto 0);
  signal rx_data_ena_p4     : std_logic;
  signal rx_comma_p4        : std_logic;
  signal rx_frame_p4        : std_logic;

--============================================================================
-- architecture begin
--============================================================================  
begin  -- architecture structural

  --============================================================================
  -- Downsampler
  --============================================================================
  cmp_downsample : downsample
    port map (
      clk_i     => clk_i,
      din_i     => rxdata_i,
      dout_p1_o => dout_p1,
      edge_p1_o => edge_p1,
      dout_p2_o => dout_p2,
      edge_p2_o => edge_p2,
      dout_p3_o => dout_p3,
      edge_p3_o => edge_p3,
      dout_p4_o => dout_p4,
      edge_p4_o => edge_p4);

  --============================================================================
  -- Word aligner1
  --============================================================================
  cmp_word_aligner1 : word_aligner
    generic map (
      G_PCOMMA_CHAR   => ONU_PCOMMA_CHAR,
      G_MCOMMA_CHAR   => ONU_MCOMMA_CHAR,
      G_COMMA_MASK    => ONU_COMMA_MASK,
      G_PREAMBLE_CHAR => ONU_PREAMBLE_CHAR)
    port map (
      clk_i        => clk_i,
      reset_i        => reset_i,
      en_pcomma_i    => en_pcomma_i,
      en_mcomma_i    => en_mcomma_i,
      frame_length_i => frame_length_i,
      datain_i       => dout_p1,
      edges_i        => edge_p1,
      dataout_o      => rx_aligned_data_p1,
      doutena_o      => rx_data_ena_p1,
      comma_o        => rx_comma_p1,
      frame_o        => rx_frame_p1,
      position_o     => rx_pos_p1);

  --============================================================================
  -- Word aligner2
  --============================================================================
  cmp_word_aligner2 : word_aligner
    generic map (
      G_PCOMMA_CHAR   => ONU_PCOMMA_CHAR,
      G_MCOMMA_CHAR   => ONU_MCOMMA_CHAR,
      G_COMMA_MASK    => ONU_COMMA_MASK,
      G_PREAMBLE_CHAR => ONU_PREAMBLE_CHAR)
    port map (
      clk_i        => clk_i,
      reset_i        => reset_i,
      en_pcomma_i    => en_pcomma_i,
      en_mcomma_i    => en_mcomma_i,
      frame_length_i => frame_length_i,
      datain_i       => dout_p2,
      edges_i        => edge_p2,
      dataout_o      => rx_aligned_data_p2,
      doutena_o      => rx_data_ena_p2,
      comma_o        => rx_comma_p2,
      frame_o        => rx_frame_p2,
      position_o     => rx_pos_p2);

  --============================================================================
  -- Word aligner3
  --============================================================================
  cmp_word_aligner3 : word_aligner
    generic map (
      G_PCOMMA_CHAR   => ONU_PCOMMA_CHAR,
      G_MCOMMA_CHAR   => ONU_MCOMMA_CHAR,
      G_COMMA_MASK    => ONU_COMMA_MASK,
      G_PREAMBLE_CHAR => ONU_PREAMBLE_CHAR)
    port map (
      clk_i        => clk_i,
      reset_i        => reset_i,
      en_pcomma_i    => en_pcomma_i,
      en_mcomma_i    => en_mcomma_i,
      frame_length_i => frame_length_i,
      datain_i       => dout_p3,
      edges_i        => edge_p3,
      dataout_o      => rx_aligned_data_p3,
      doutena_o      => rx_data_ena_p3,
      comma_o        => rx_comma_p3,
      frame_o        => rx_frame_p3,
      position_o     => rx_pos_p3);

  --============================================================================
  -- Word aligner4
  --============================================================================
  cmp_word_aligner4 : word_aligner
    generic map (
      G_PCOMMA_CHAR   => ONU_PCOMMA_CHAR,
      G_MCOMMA_CHAR   => ONU_MCOMMA_CHAR,
      G_COMMA_MASK    => ONU_COMMA_MASK,
      G_PREAMBLE_CHAR => ONU_PREAMBLE_CHAR)
    port map (
      clk_i        => clk_i,
      reset_i        => reset_i,
      en_pcomma_i    => en_pcomma_i,
      en_mcomma_i    => en_mcomma_i,
      frame_length_i => frame_length_i,
      datain_i       => dout_p4,
      edges_i        => edge_p4,
      doutena_o      => rx_data_ena_p4,
      dataout_o      => rx_aligned_data_p4,
      comma_o        => rx_comma_p4,
      frame_o        => rx_frame_p4,
      position_o     => rx_pos_p4);

  --============================================================================
  -- Frame data choise and assembling
  --============================================================================
  cmp_frame_data_mux : frame_data_mux
    port map (
      clk_i        => clk_i,
      din_p1_i       => rx_aligned_data_p1,
      pos_p1_i       => rx_pos_p1,
      comma_p1_i     => rx_comma_p1,
      dvalid_p1_i    => rx_data_ena_p1,
      din_p2_i       => rx_aligned_data_p2,
      pos_p2_i       => rx_pos_p2,
      comma_p2_i     => rx_comma_p2,
      dvalid_p2_i    => rx_data_ena_p2,
      din_p3_i       => rx_aligned_data_p3,
      pos_p3_i       => rx_pos_p3,
      comma_p3_i     => rx_comma_p3,
      dvalid_p3_i    => rx_data_ena_p3,
      din_p4_i       => rx_aligned_data_p4,
      pos_p4_i       => rx_pos_p4,
      comma_p4_i     => rx_comma_p4,
      dvalid_p4_i    => rx_data_ena_p4,
      frame_length_i => frame_length_i,
      dout_o         => dout_o,
      pos_o          => pos_o,
      comma_o        => comma_o,
      frame_o        => frame_o,
      phase_o        => phase_o);


end architecture structural;
