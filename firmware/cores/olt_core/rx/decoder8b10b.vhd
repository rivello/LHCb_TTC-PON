--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file decoder8b10b.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: single 8b10b decoder(decoder8b10b)
--
--! @brief 8b10b decoder
-- obs: This module was developed in verilog (from http://asics.chuckbenz.com/#My_open_source_8b10b_encoderdecoder) and translated by EBSM into VHDL
--      The original copyright information can be found below:
--
--      Chuck Benz, Hollis, NH   Copyright (c)2002
--      
--      The information and description contained herein is the
--      property of Chuck Benz.
--      
--      Permission is granted for any reuse of this information
--      and description as long as this copyright notice is
--      preserved.  Modifications may be made as long as this
--      notice is preserved.
--      per Widmer and Franaszek
--
--! @author Chuck Benz
--! @date 2002
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Chuck Benz
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 2002 - Chuck Benz\n
--! 2016 - EBSM - translated into .vhd
--! <extended description>
-------------------------------------------------------------------------------
--! @todo \n
--! \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for decoder8b10b
--============================================================================
entity decoder8b10b is
  port(
    datain   : in  std_logic_vector(9 downto 0);
    dispin   : in  std_logic;
    dataout  : out std_logic_vector(7 downto 0);
    commaout : out std_logic;
    dispout  : out std_logic;
    code_err : out std_logic;
    disp_err : out std_logic
    );
end decoder8b10b;

--============================================================================
--! Architecture declaration for decoder8b10b
--============================================================================
architecture rtl of decoder8b10b is

  signal ai : std_logic;
  signal bi : std_logic;
  signal ci : std_logic;
  signal di : std_logic;
  signal ei : std_logic;
  signal ii : std_logic;
  signal fi : std_logic;
  signal gi : std_logic;
  signal hi : std_logic;
  signal ji : std_logic;

  signal aeqb : std_logic;
  signal ceqd : std_logic;
  signal p22  : std_logic;
  signal p13  : std_logic;
  signal p31  : std_logic;

  signal p40         : std_logic;
  signal p04         : std_logic;
  signal disp6a      : std_logic;
  signal disp6a2     : std_logic;
  signal disp6a0     : std_logic;
  signal disp6b      : std_logic;
  signal p22bceeqi   : std_logic;
  signal p22bncneeqi : std_logic;
  signal p13in       : std_logic;
  signal p31i        : std_logic;
  signal p13dei      : std_logic;
  signal p22aceeqi   : std_logic;
  signal p22ancneeqi : std_logic;
  signal p13en       : std_logic;
  signal anbnenin    : std_logic;
  signal abei        : std_logic;
  signal cdei        : std_logic;
  signal cndnenin    : std_logic;

  signal p22enin   : std_logic;
  signal p22ei     : std_logic;
  signal p31dnenin : std_logic;
  signal p31e      : std_logic;

  signal compa : std_logic;
  signal compb : std_logic;
  signal compc : std_logic;
  signal compd : std_logic;
  signal compe : std_logic;

  signal ao : std_logic;
  signal bo : std_logic;
  signal co : std_logic;
  signal do : std_logic;
  signal eo : std_logic;

  signal feqg    : std_logic;
  signal heqj    : std_logic;
  signal fghj22  : std_logic;
  signal fghjp13 : std_logic;
  signal fghjp31 : std_logic;


  signal ko : std_logic;

  signal alt7 : std_logic;

  signal k28    : std_logic;
  signal k28p   : std_logic;
  signal fo     : std_logic;
  signal go     : std_logic;
  signal ho     : std_logic;
  signal disp6p : std_logic;
  signal disp6n : std_logic;
  signal disp4p : std_logic;
  signal disp4n : std_logic;


--============================================================================
--! Architecture begin for decoder8b10b
--============================================================================  
begin
  
  ai <= datain(0);
  bi <= datain(1);
  ci <= datain(2);
  di <= datain(3);
  ei <= datain(4);
  ii <= datain(5);
  fi <= datain(6);
  gi <= datain(7);
  hi <= datain(8);
  ji <= datain(9);

  aeqb <= (ai and bi) or (not ai and not bi);
  ceqd <= (ci and di) or (not ci and not di);
  p22  <= (ai and bi and not ci and not di) or
          (ci and di and not ai and not bi) or
          (not aeqb and not ceqd);
  p13 <= (not aeqb and not ci and not di) or
         (not ceqd and not ai and not bi);
  p31 <= (not aeqb and ci and di) or
         (not ceqd and ai and bi);

  p40 <= ai and bi and ci and di;
  p04 <= not ai and not bi and not ci and not di;

  disp6a  <= p31 or (p22 and dispin);   -- pos disp if p22 and was pos, or p31.
  disp6a2 <= p31 and dispin;            -- disp is ++ after 4 bits
  disp6a0 <= p13 and not dispin;        -- -- disp after 4 bits

  disp6b <= (((ei and ii and not disp6a0) or (disp6a and (ei or ii)) or disp6a2 or
              (ei and ii and di)) and (ei or ii or di)) ;

  -- The 5B/6B decoding special cases where ABCDE not <= abcde

  p22bceeqi   <= p22 and bi and ci and ((ei and ii) or (not ei and not ii));
  p22bncneeqi <= p22 and not bi and not ci and ((ei and ii) or (not ei and not ii));
  p13in       <= p13 and not ii;
  p31i        <= p31 and ii;
  p13dei      <= p13 and di and ei and ii;
  p22aceeqi   <= p22 and ai and ci and ((ei and ii) or (not ei and not ii));
  p22ancneeqi <= p22 and not ai and not ci and ((ei and ii) or (not ei and not ii));
  p13en       <= p13 and not ei;
  anbnenin    <= not ai and not bi and not ei and not ii;
  abei        <= ai and bi and ei and ii;
  cdei        <= ci and di and ei and ii;
  cndnenin    <= not ci and not di and not ei and not ii;

  -- non-zero disparity cases:
  p22enin   <= p22 and not ei and not ii;
  p22ei     <= p22 and ei and ii;
  --p13in <= p12 and not ii ;
  --p31i <= p31 and ii ;
  p31dnenin <= p31 and not di and not ei and not ii;
  --p13dei <= p13 and di and ei and ii ;
  p31e      <= p31 and ei;

  compa <= p22bncneeqi or p31i or p13dei or p22ancneeqi or
           p13en or abei or cndnenin;
  compb <= p22bceeqi or p31i or p13dei or p22aceeqi or
           p13en or abei or cndnenin;
  compc <= p22bceeqi or p31i or p13dei or p22ancneeqi or
           p13en or anbnenin or cndnenin;
  compd <= p22bncneeqi or p31i or p13dei or p22aceeqi or
           p13en or abei or cndnenin;
  compe <= p22bncneeqi or p13in or p13dei or p22ancneeqi or
           p13en or anbnenin or cndnenin;

  ao <= ai xor compa;
  bo <= bi xor compb;
  co <= ci xor compc;
  do <= di xor compd;
  eo <= ei xor compe;

  feqg   <= (fi and gi) or (not fi and not gi);
  heqj   <= (hi and ji) or (not hi and not ji);
  fghj22 <= (fi and gi and not hi and not ji) or
            (not fi and not gi and hi and ji) or
            (not feqg and not heqj);
  fghjp13 <= (not feqg and not hi and not ji) or
             (not heqj and not fi and not gi);
  fghjp31 <= ((not feqg) and hi and ji) or
             (not heqj and fi and gi);

  dispout <= (fghjp31 or (disp6b and fghj22) or (hi and ji)) and (hi or ji);

  ko <= ((ci and di and ei and ii) or (not ci and not di and not ei and not ii) or
         (p13 and not ei and ii and gi and hi and ji) or
         (p31 and ei and not ii and not gi and not hi and not ji)) ;

  alt7 <= (fi and not gi and not hi and  -- 1000 cases, where disp6b is 1
           ((dispin and ci and di and not ei and not ii) or ko or
            (dispin and not ci and di and not ei and not ii))) or
          (not fi and gi and hi and      -- 0111 cases, where disp6b is 0
           ((not dispin and not ci and not di and ei and ii) or ko or
            (not dispin and ci and not di and ei and ii))) ;

  k28  <= (ci and di and ei and ii) or not (ci or di or ei or ii);
  -- k28 with positive disp into fghi - .1, .2, .5, and .6 special cases
  k28p <= not (ci or di or ei or ii);
  fo   <= (ji and not fi and (hi or not gi or k28p)) or
          (fi and not ji and (not hi or gi or not k28p)) or
          (k28p and gi and hi) or
          (not k28p and not gi and not hi);

  go <= (ji and not fi and (hi or not gi or not k28p)) or
        (fi and not ji and (not hi or gi or k28p)) or
        (not k28p and gi and hi) or
        (k28p and not gi and not hi);

  ho <= ((ji xor hi) and not ((not fi and gi and not hi and ji and not k28p) or (not fi and gi and hi and not ji and k28p) or
                              (fi and not gi and not hi and ji and not k28p) or (fi and not gi and hi and not ji and k28p))) or
        (not fi and gi and hi and ji) or (fi and not gi and not hi and not ji);

  disp6p <= (p31 and (ei or ii)) or (p22 and ei and ii);
  disp6n <= (p13 and not (ei and ii)) or (p22 and not ei and not ii);
  disp4p <= fghjp31;
  disp4n <= fghjp13;

  code_err <= p40 or p04 or (fi and gi and hi and ji) or (not fi and not gi and not hi and not ji) or
              (p13 and not ei and not ii) or (p31 and ei and ii) or
              (ei and ii and fi and gi and hi) or (not ei and not ii and not fi and not gi and not hi) or
              (ei and not ii and gi and hi and ji) or (not ei and ii and not gi and not hi and not ji) or
              (not p31 and ei and not ii and not gi and not hi and not ji) or
              (not p13 and not ei and ii and gi and hi and ji) or
              (((ei and ii and not gi and not hi and not ji) or
                (not ei and not ii and gi and hi and ji)) and
               not ((ci and di and ei) or (not ci and not di and not ei))) or
              (disp6p and disp4p) or (disp6n and disp4n) or
              (ai and bi and ci and not ei and not ii and ((not fi and not gi) or fghjp13)) or
              (not ai and not bi and not ci and ei and ii and ((fi and gi) or fghjp31)) or
              (fi and gi and not hi and not ji and disp6p) or
              (not fi and not gi and hi and ji and disp6n) or
              (ci and di and ei and ii and not fi and not gi and not hi) or
              (not ci and not di and not ei and not ii and fi and gi and hi);

  --dataout <= {ho, go, fo, eo, do, co, bo, ao};
  dataout(0) <= ao;
  dataout(1) <= bo;
  dataout(2) <= co;
  dataout(3) <= do;
  dataout(4) <= eo;
  dataout(5) <= fo;
  dataout(6) <= go;
  dataout(7) <= ho;

  commaout <= ko;

  -- my disp err fires for any legal codes that violate disparity, may fire for illegal codes
  disp_err <= ((dispin and disp6p) or (disp6n and not dispin) or
               (dispin and not disp6n and fi and gi) or
               (dispin and ai and bi and ci) or
               (dispin and not disp6n and disp4p) or
               (not dispin and not disp6p and not fi and not gi) or
               (not dispin and not ai and not bi and not ci) or
               (not dispin and not disp6p and disp4n) or
               (disp6p and disp4p) or (disp6n and disp4n)) ;

end architecture rtl;
