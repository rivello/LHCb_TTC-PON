--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file downsample.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: Downsampler (downsample)
--
--! @brief Produces down-sampled outputs from input raw data
--! (Detailed description will come here.)
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 26-08-2014
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 26-08-2014 CS  Created\n
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for downsample
--============================================================================
entity downsample is
  port (
    clk_i     : in  std_logic;
    din_i     : in  std_logic_vector;
    dout_p1_o : out std_logic_vector;
    edge_p1_o : out std_logic_vector;
    dout_p2_o : out std_logic_vector;
    edge_p2_o : out std_logic_vector;
    dout_p3_o : out std_logic_vector;
    edge_p3_o : out std_logic_vector;
    dout_p4_o : out std_logic_vector;
    edge_p4_o : out std_logic_vector);
end entity downsample;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of downsample is

  --! Signal declaration
  signal din_r     : std_logic_vector(din_i'range);
  signal din_p1    : std_logic_vector(din_i'range);
  signal din_p2    : std_logic_vector(din_i'range);
  signal din_p3    : std_logic_vector(din_i'range);
  signal din_p4    : std_logic_vector(din_i'range);
  signal dout_p1   : std_logic_vector(dout_p1_o'range);
  signal dout_p2   : std_logic_vector(dout_p2_o'range);
  signal dout_p3   : std_logic_vector(dout_p3_o'range);
  signal dout_p4   : std_logic_vector(dout_p4_o'range);
  signal edge_p1   : std_logic_vector(dout_p1_o'range);
  signal edge_p2   : std_logic_vector(dout_p2_o'range);
  signal edge_p3   : std_logic_vector(dout_p3_o'range);
  signal edge_p4   : std_logic_vector(dout_p4_o'range);

--============================================================================
-- ! architecture begin
--============================================================================  
begin  -- architecture rtl

  p_din_reg : process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then  -- rising clk_i edge
      din_r <= din_i;
    end if;
  end process p_din_reg;
  din_p1 <= din_i(din_i'left-3 downto 0)&din_r(din_r'left downto din_r'left-2);
  din_p2 <= din_i(din_i'left-2 downto 0)&din_r(din_r'left downto din_r'left-1);
  din_p3 <= din_i(din_i'left-1 downto 0)&din_r(din_r'left);
  din_p4 <= din_i(din_i'left downto 0);

  p_voter_p1 : process (clk_i) is
    variable slv4 : std_logic_vector(3 downto 0);
  begin  -- process p_voter
    if clk_i'event and clk_i = '1' then  -- rising clk_i edge
      for i in dout_p1'range loop
        slv4 := din_p1(i*4+3 downto i*4);
        case slv4 is
          when "0000" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "0001" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "0010" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "0011" => dout_p1(i) <= '0'; edge_p1(i) <= '1';
          when "0100" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "0101" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "0110" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when "0111" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when "1000" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "1001" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "1010" => dout_p1(i) <= '0'; edge_p1(i) <= '0';
          when "1011" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when "1100" => dout_p1(i) <= '0'; edge_p1(i) <= '1';
          when "1101" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when "1110" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when "1111" => dout_p1(i) <= '1'; edge_p1(i) <= '0';
          when others => null;
        end case;
      end loop;  -- i
    end if;
  end process p_voter_p1;
  dout_p1_o <= dout_p1;
  edge_p1_o <= edge_p1;

  p_voter_p2 : process (clk_i) is
    variable slv4 : std_logic_vector(3 downto 0);
  begin  -- process p_voter
    if clk_i'event and clk_i = '1' then  -- rising clk_i edge
      for i in dout_p2'range loop
        slv4 := din_p2(i*4+3 downto i*4);
        case slv4 is
          when "0000" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "0001" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "0010" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "0011" => dout_p2(i) <= '0'; edge_p2(i) <= '1';
          when "0100" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "0101" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "0110" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when "0111" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when "1000" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "1001" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "1010" => dout_p2(i) <= '0'; edge_p2(i) <= '0';
          when "1011" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when "1100" => dout_p2(i) <= '0'; edge_p2(i) <= '1';
          when "1101" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when "1110" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when "1111" => dout_p2(i) <= '1'; edge_p2(i) <= '0';
          when others => null;
        end case;
      end loop;  -- i
    end if;
  end process p_voter_p2;
  dout_p2_o <= dout_p2;
  edge_p2_o <= edge_p2;

  p_voter_p3 : process (clk_i) is
    variable slv4 : std_logic_vector(3 downto 0);
  begin  -- process p_voter
    if clk_i'event and clk_i = '1' then  -- rising clk_i edge
      for i in dout_p3'range loop
        slv4 := din_p3(i*4+3 downto i*4);
        case slv4 is
          when "0000" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "0001" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "0010" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "0011" => dout_p3(i) <= '0'; edge_p3(i) <= '1';
          when "0100" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "0101" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "0110" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when "0111" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when "1000" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "1001" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "1010" => dout_p3(i) <= '0'; edge_p3(i) <= '0';
          when "1011" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when "1100" => dout_p3(i) <= '0'; edge_p3(i) <= '1';
          when "1101" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when "1110" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when "1111" => dout_p3(i) <= '1'; edge_p3(i) <= '0';
          when others => null;
        end case;
      end loop;  -- i
    end if;
  end process p_voter_p3;
  dout_p3_o <= dout_p3;
  edge_p3_o <= edge_p3;

  p_voter_p4 : process (clk_i) is
    variable slv4 : std_logic_vector(3 downto 0);
  begin  -- process p_voter
    if clk_i'event and clk_i = '1' then  -- rising clk_i edge
      for i in dout_p4'range loop
        slv4 := din_p4(i*4+3 downto i*4);
        case slv4 is
          when "0000" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "0001" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "0010" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "0011" => dout_p4(i) <= '0'; edge_p4(i) <= '1';
          when "0100" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "0101" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "0110" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when "0111" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when "1000" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "1001" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "1010" => dout_p4(i) <= '0'; edge_p4(i) <= '0';
          when "1011" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when "1100" => dout_p4(i) <= '0'; edge_p4(i) <= '1';
          when "1101" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when "1110" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when "1111" => dout_p4(i) <= '1'; edge_p4(i) <= '0';
          when others => null;
        end case;
      end loop;  -- i
    end if;
  end process p_voter_p4;
  dout_p4_o <= dout_p4;
  edge_p4_o <= edge_p4;

end architecture rtl;
