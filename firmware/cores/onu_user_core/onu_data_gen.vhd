--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_data_gen.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU data generator for example design (onu_data_gen)
--
--! @brief ONU data generator for example design
--! 
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--
--! @date 19\07\2016
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 29\06\2016 - EBSM - \n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_data_gen
--============================================================================
entity onu_data_gen is
  generic(
    g_DATA_WIDTH : integer := 48
    );
  port (
    -- global input signals --
    clk_i   : in std_logic;
    reset_i : in std_logic;
    -------------------------

    -- status / control --
    interfacing_mode_i : in std_logic;  -- 0 = STROBE -> WAIT READY / 1 = WAIT READY -> STROBE
    sel_pattern_i      : in std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "10" constant, "11" counter mode
    constant_frame_i   : in std_logic_vector(g_DATA_WIDTH-1 downto 0);
    inject_error_i     : in std_logic;
    -------------------------

    -- data in/out --
    data_o        : out std_logic_vector(g_DATA_WIDTH-1 downto 0);
    data_ready_i  : in  std_logic;
    data_strobe_o : out std_logic
    -------------------------
    );
end entity onu_data_gen;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of onu_data_gen is

  --! Signal declaration
  signal enable_gen   : std_logic;
  signal byte_cntr    : unsigned(7 downto 0);
  signal data_frame   : std_logic_vector(g_DATA_WIDTH-1 downto 0);
  signal data_strobe  : std_logic;
  signal data_ready_r : std_logic;

  signal prbs_mode  : std_logic;
  signal frame_prbs : std_logic_vector(g_DATA_WIDTH-1 downto 0);

  signal error_injected  : std_logic;  -- STATE to know if error was already injected
  signal shift_reg_error : std_logic_vector(g_DATA_WIDTH-1 downto 0);
  signal error_data      : std_logic_vector(g_DATA_WIDTH-1 downto 0);

  --! Component declaration
  component prbs_data_gen is
    generic (
      G_DOUT_WIDTH   : integer := 20;
      G_PRBS_TAP     : integer := 0;
      G_REVERSE_BITS : integer := 0);
    port (
      clk_i : in  std_logic;
      mode_i  : in  std_logic;  -- '0' external seed, '1' internal loop-back
      ena_i   : in  std_logic;          -- '0' hold, '1' generate
      sel_i   : in  std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  : in  std_logic_vector(22 downto 0);
      dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));
  end component prbs_data_gen;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Process p_interfacing
  --! Generates data_strobe for onu-user interfacing
  --! read:  \n
  --! write: data_strobe\n
  --! r/w:   \n
  --============================================================================           
  p_interfacing : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        data_strobe  <= '0';
        data_ready_r <= '0';
        data_o       <= (others => '0');
      else
        data_ready_r <= data_ready_i;
        if(interfacing_mode_i = '0') then  -- STROBE -> WAIT READY 
          if(data_ready_i = '1') then
            data_strobe <= '0';
            data_o      <= (others => '0');
          elsif(data_ready_r = '0') then
            data_strobe <= '1';
            data_o      <= data_frame xor error_data;
          end if;
        else                            -- WAIT READY -> STROBE
          if(data_ready_r = '0' and data_ready_i = '1') then  -- ready became '1'
            data_strobe <= '1';
            data_o      <= data_frame xor error_data;
          else
            data_strobe <= '0';
            data_o      <= (others => '0');
          end if;
        end if;
      end if;
    end if;
  end process p_interfacing;

  data_strobe_o <= data_strobe;

  --============================================================================
  -- Process p_enable_gen
  --! Generates strobe for olt-user interfacing
  --! read:  \n
  --! write: enable_gen\n
  --! r/w:   \n
  --============================================================================           
  p_enable_gen : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        enable_gen <= '0';
      else
        if(data_strobe = '1' and data_ready_i = '1') then
          enable_gen <= '1';
        else
          enable_gen <= '0';
        end if;
      end if;
    end if;
  end process p_enable_gen;

  --============================================================================
  -- Component instantiation
  -- Generates data PRBS
  --! Component prbs_data_gen
  --============================================================================  
  cmp_prbs_data_gen : prbs_data_gen
    generic map(
      G_DOUT_WIDTH   => g_DATA_WIDTH,
      G_PRBS_TAP     => 0,
      G_REVERSE_BITS => 0)
    port map(
      clk_i => clk_i,
      mode_i  => prbs_mode,
      ena_i   => enable_gen,
      sel_i   => sel_pattern_i,         -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  => "00110100010101100111100",
      dout_o  => frame_prbs);

  prbs_mode <= not reset_i;

  --============================================================================
  -- Process p_byte_cntr
  --! Generates frame counter mode
  --! read:  enable_gen\n
  --! write: byte_cntr\n
  --! r/w:   \n
  --============================================================================        
  p_byte_cntr : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        byte_cntr <= (others => '0');
      elsif(enable_gen = '1') then
        byte_cntr <= byte_cntr + 1;
      end if;
    end if;
  end process p_byte_cntr;

  --============================================================================
  -- Process p_inject_error
  --! Generates frame counter mode
  --! read:  enable_gen, inject_error_i\n
  --! write: error_data\n
  --! r/w:   shift_reg_error, error_injected\n
  --============================================================================        
  p_inject_error : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        error_injected                                 <= '0';
        shift_reg_error(0)                             <= '1';
        shift_reg_error(shift_reg_error'left downto 1) <= (others => '0');
        error_data                                     <= (others => '0');
      elsif(enable_gen = '1') then
        if(error_injected = '0')then
          if(inject_error_i = '1') then
            error_data      <= shift_reg_error;
            shift_reg_error <= shift_reg_error(shift_reg_error'left-1 downto 0)&shift_reg_error(shift_reg_error'left);
            error_injected  <= '1';
          else
            error_data     <= (others => '0');
            error_injected <= '0';
          end if;
        else
          error_data <= (others => '0');
          if(inject_error_i = '0') then
            error_injected <= '0';
          end if;
        end if;
      end if;
    end if;
  end process p_inject_error;

  --============================================================================
  -- Process p_selection_pattern - combinatorial
  --! Multiplexer select frame type
  --! read:  byte_cntr, enable_gen, constant_frame_i, frame_prbs\n
  --! write: data_frame\n
  --! r/w:   \n
  --============================================================================           
  p_selection_pattern : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(enable_gen = '1') then
        case sel_pattern_i is
          when "11" =>
            for i in 0 to data_frame'length/8-1 loop
              data_frame(8*(i+1)-1 downto 8*i) <= std_logic_vector(byte_cntr);
            end loop;
          when "10" =>
            data_frame <= constant_frame_i;
          when others =>
            data_frame <= frame_prbs;
        end case;
      end if;
    end if;
  end process p_selection_pattern;

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
