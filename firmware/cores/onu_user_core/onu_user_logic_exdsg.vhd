--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_user_logic_exdsg.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU user example design logic (onu_user_logic_exdsg)
--
--! @brief ONU user example design logic
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19\07\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 17\08\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------
entity onu_user_logic_exdsg is
  port(
    -- Core connection --
    onu_clk_sys_i     : in  std_logic;
    onu_core_reset_o  : out std_logic;
    onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
    onu_rx40_locked_i : in  std_logic;

    onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
    onu_rx_data_strobe_i : in std_logic;
    onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
    onu_tx_data_ready_i  : in  std_logic;
    onu_tx_data_strobe_o : out std_logic;
    onu_tx_data_o        : out std_logic_vector(55 downto 0);

    onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address
    onu_mgt_phase_good_i      : in  std_logic;  --! MGT Tx and Rx clocks CDC phase scanning
    onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
    onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
    onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
    onu_interrupt_i           : in  std_logic;
    onu_sticky_status_i       : in  std_logic_vector(30 downto 0);

    onu_mgt_tx_ready_i   : in std_logic;
    onu_mgt_rx_ready_i   : in std_logic;
    onu_mgt_txpll_lock_i : in std_logic;
    onu_mgt_rxpll_lock_i : in std_logic;
    ---------------------

    -- Control AXI --
    s_onu_user_aclk : in  std_logic;
    ctrl_reg_i      : in  t_regbank32b(127 downto 0);
    stat_reg_o      : out t_regbank32b(127 downto 0);
    ---------------------------------

    -- Others --
    rx_match_flag_o : out std_logic;

    onu_status_o : out std_logic_vector(7 downto 0)
    --------------------------------
    );
end onu_user_logic_exdsg;

architecture rtl of onu_user_logic_exdsg is
  
  attribute mark_debug : string;
  attribute keep       : string;
  attribute async_reg  : string;

  --! Functions

  --! Constants

  --! Signal declaration
  signal onu_status_reg_async : std_logic_vector(7 downto 0);

  -- ONU status AXI sync
  signal onu_status_reg_axisync   : std_logic_vector(7 downto 0);
  signal onu_status_reg_axisync_r : std_logic_vector(7 downto 0);

  attribute async_reg of onu_status_reg_axisync   : signal is "true";
  attribute async_reg of onu_status_reg_axisync_r : signal is "true";

  signal onu_interrupt                  : std_logic;
  attribute mark_debug of onu_interrupt : signal is "true";
  attribute keep of onu_interrupt       : signal is "true";

  -- TX Path
  signal reset_txlogic      : std_logic;
  signal onu_tx_data_ready  : std_logic;
  signal onu_tx_data_strobe : std_logic;
  signal onu_tx_data        : std_logic_vector(onu_tx_data_o'range);

  attribute mark_debug of onu_tx_data_ready  : signal is "true";
  attribute mark_debug of onu_tx_data_strobe : signal is "true";
  attribute mark_debug of onu_tx_data        : signal is "true";

  attribute keep of onu_tx_data_ready  : signal is "true";
  attribute keep of onu_tx_data_strobe : signal is "true";
  attribute keep of onu_tx_data        : signal is "true";

  signal tx_interfacing_mode_async    : std_logic;
  signal sel_tx_pattern_async         : std_logic_vector(1 downto 0);
  signal tx_interfacing_mode_txsync   : std_logic;
  signal sel_tx_pattern_txsync        : std_logic_vector(1 downto 0);
  signal tx_interfacing_mode_txsync_r : std_logic;
  signal sel_tx_pattern_txsync_r      : std_logic_vector(1 downto 0);

  attribute async_reg of sel_tx_pattern_txsync      : signal is "true";
  attribute async_reg of sel_tx_pattern_txsync_r    : signal is "true";
  attribute async_reg of tx_interfacing_mode_txsync : signal is "true";

  signal tx_inject_error_async    : std_logic;
  signal tx_inject_error_txsync   : std_logic;
  signal tx_inject_error_txsync_r : std_logic;

  attribute async_reg of tx_inject_error_txsync   : signal is "true";
  attribute async_reg of tx_inject_error_txsync_r : signal is "true";

  -- RX Path
  signal reset_rxlogic      : std_logic;
  signal onu_rx_data_strobe : std_logic;
  signal onu_rx_data_frame  : std_logic_vector(onu_rx_data_frame_i'range);
  signal rx_frame           : std_logic_vector(onu_rx_data_frame_i'range);
  signal rx_frame_valid     : std_logic;
  signal rx_frame_refgen    : std_logic_vector(onu_rx_data_frame_i'range);
  signal rx_frame_prbs_lock : std_logic;
  signal rx_frame_errs      : std_logic;
  signal rx_frame_nb_errs   : std_logic_vector(8 downto 0);

  attribute mark_debug of onu_rx_data_strobe : signal is "true";
  attribute mark_debug of onu_rx_data_frame  : signal is "true";
  attribute mark_debug of rx_frame_refgen    : signal is "true";
  attribute mark_debug of rx_frame_prbs_lock : signal is "true";
  attribute mark_debug of rx_frame_errs      : signal is "true";

  attribute keep of onu_rx_data_strobe : signal is "true";
  attribute keep of onu_rx_data_frame  : signal is "true";
  attribute keep of rx_frame_refgen    : signal is "true";
  attribute keep of rx_frame_prbs_lock : signal is "true";
  attribute keep of rx_frame_errs      : signal is "true";

  -- RX-BERT
  signal bert_clear_acc_async : std_logic;
  signal bert_latch_acc_async : std_logic;

  signal bert_clear_acc_rxsync, bert_clear_acc_rxsync_r : std_logic;
  attribute async_reg of bert_clear_acc_rxsync          : signal is "true";
  attribute async_reg of bert_clear_acc_rxsync_r        : signal is "true";

  signal bert_latch_acc_rxsync, bert_latch_acc_rxsync_r : std_logic;
  attribute async_reg of bert_latch_acc_rxsync          : signal is "true";
  attribute async_reg of bert_latch_acc_rxsync_r        : signal is "true";

  signal error_sum         : unsigned(47 downto 0);
  signal bit_sum           : unsigned(47 downto 0);
  signal error_sum_latched : std_logic_vector(47 downto 0);
  signal bit_sum_latched   : std_logic_vector(47 downto 0);

  signal serror_corrected_sum         : unsigned(47 downto 0);
  signal derror_corrected_sum         : unsigned(47 downto 0);
  signal serror_corrected_sum_latched : std_logic_vector(47 downto 0);
  signal derror_corrected_sum_latched : std_logic_vector(47 downto 0);

  attribute mark_debug of error_sum_latched : signal is "true";
  attribute mark_debug of bit_sum_latched   : signal is "true";

  --! Components
  component onu_data_gen is
    generic(
      g_DATA_WIDTH : integer := 48
      );
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      -------------------------

      -- status / control --
      interfacing_mode_i : in std_logic;  -- 0 = STROBE -> WAIT READY / 1 = WAIT READY -> STROBE
      sel_pattern_i      : in std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "10" constant, "11" counter mode
      constant_frame_i   : in std_logic_vector(g_DATA_WIDTH-1 downto 0);
      inject_error_i     : in std_logic;
      -------------------------

      -- data in/out --
      data_o        : out std_logic_vector(g_DATA_WIDTH-1 downto 0);
      data_ready_i  : in  std_logic;
      data_strobe_o : out std_logic
      -------------------------
      );
  end component onu_data_gen;

  component prbs_data_check is
    generic (
      G_DIN_WIDTH         : integer := 32;
      G_REVERSE_BITS      : integer := 0;
      G_MAX_ERRS_OUTWIDTH : integer := 6
      );
    port (
      clk_i  : in  std_logic;
      reset_i  : in  std_logic;
      ena_i    : in  std_logic;
      sel_i    : in  std_logic_vector(1 downto 0);
      -- "00" PRBS-7, "01" PRBS-23
      -- "1x" fix
      din_i    : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
      lock_o   : out std_logic;
      error_o  : out std_logic;
      nerrs_o  : out std_logic_vector(G_MAX_ERRS_OUTWIDTH-1 downto 0);
      refgen_o : out std_logic_vector(G_DIN_WIDTH-1 downto 0));

  end component prbs_data_check;
  
begin

  onu_interrupt <= onu_interrupt_i;

  --============================================================================
  -- TX path
  --============================================================================ 
  reset_txlogic <= '1' when (onu_mgt_tx_ready_i = '0' or onu_operational_i = '0') else '0';

  --============================================================================
  -- Component instantiation
  --! Component onu_data_gen
  --============================================================================
  cmp_onu_data_gen : onu_data_gen
    generic map(
      g_DATA_WIDTH => onu_tx_data'length
      )
    port map(
      -- global input signals --
      clk_i   => onu_clk_trxusr240_i,
      reset_i => reset_txlogic,
      -------------------------

      -- status / control --
      interfacing_mode_i => tx_interfacing_mode_txsync_r,
      sel_pattern_i      => sel_tx_pattern_txsync_r,
      inject_error_i     => tx_inject_error_txsync_r,
      constant_frame_i   => x"0123456789abcd",
      -------------------------

      -- data in/out --
      data_o        => onu_tx_data,
      data_ready_i  => onu_tx_data_ready,
      data_strobe_o => onu_tx_data_strobe
      -------------------------
      );

  onu_tx_data_o        <= onu_tx_data;
  onu_tx_data_ready    <= onu_tx_data_ready_i;
  onu_tx_data_strobe_o <= onu_tx_data_strobe;


  --============================================================================
  -- Process p_txsync
  --! read:  -\n
  --! write: tx_interfacing_mode_txsync_r, sel_tx_pattern_txsync_r\n
  --! r/w:   tx_interfacing_mode_txsync,sel_tx_pattern_txsync\n
  --============================================================================    
  p_txsync : process(onu_clk_trxusr240_i) is
  begin
    if(onu_clk_trxusr240_i'event and onu_clk_trxusr240_i = '1') then
      tx_interfacing_mode_txsync   <= tx_interfacing_mode_async;
      tx_interfacing_mode_txsync_r <= tx_interfacing_mode_txsync;

      sel_tx_pattern_txsync   <= sel_tx_pattern_async;
      sel_tx_pattern_txsync_r <= sel_tx_pattern_txsync;

      tx_inject_error_txsync   <= tx_inject_error_async;
      tx_inject_error_txsync_r <= tx_inject_error_txsync;
    end if;
  end process p_txsync;

  --============================================================================
  -- RX path
  --============================================================================
  reset_rxlogic <= '1' when (onu_mgt_rx_ready_i = '0' or onu_rx_locked_i = '0') else '0';

  p_frame_rx : process(onu_clk_trxusr240_i) is
  begin
    if (onu_clk_trxusr240_i'event and onu_clk_trxusr240_i = '1') then
      rx_frame_valid <= '0';
      if onu_rx_data_strobe = '1' then
        rx_frame       <= onu_rx_data_frame;
        rx_frame_valid <= '1';

        if (onu_rx_data_frame(7 downto 0) = x"00") then
          rx_match_flag_o <= '1';
        else
          rx_match_flag_o <= '0';
        end if;
        
      end if;
    end if;
  end process p_frame_rx;

  onu_rx_data_frame  <= onu_rx_data_frame_i;
  onu_rx_data_strobe <= onu_rx_data_strobe_i;

  --============================================================================
  -- Component instantiation
  --! Component cmp_prbs_frame240
  --============================================================================      
  cmp_prbs7_frame240 : prbs_data_check
    generic map(
      G_DIN_WIDTH         => onu_rx_data_frame_i'length,
      G_REVERSE_BITS      => 0,
      G_MAX_ERRS_OUTWIDTH => rx_frame_nb_errs'length
      )
    port map (
      clk_i  => onu_clk_trxusr240_i,
      reset_i  => reset_rxlogic,
      ena_i    => rx_frame_valid,
      sel_i    => "00",
      din_i    => rx_frame,
      lock_o   => rx_frame_prbs_lock,
      error_o  => rx_frame_errs,
      nerrs_o  => rx_frame_nb_errs,
      refgen_o => rx_frame_refgen
      ); 

  --============================================================================
  -- Process p_bert_acc
  --! Error / Bit  48b-accumulator
  --! read:  bert_clear_acc_rxsync_r, bert_latch_acc_rxsync_r
  --! write: error_sum_latched, bit_sum_latched\n
  --! r/w:   error_sum,bit_sum\n
  --============================================================================        
  p_bert_acc : process(onu_clk_trxusr240_i) is
  begin
    if(onu_clk_trxusr240_i'event and onu_clk_trxusr240_i = '1') then
      if(bert_clear_acc_rxsync_r = '1') then
        error_sum         <= (others => '0');
        bit_sum           <= (others => '0');
        error_sum_latched <= (others => '0');
        bit_sum_latched   <= (others => '0');

        serror_corrected_sum         <= (others => '0');
        derror_corrected_sum         <= (others => '0');
        serror_corrected_sum_latched <= (others => '0');
        derror_corrected_sum_latched <= (others => '0');
        
      else
        error_sum <= error_sum + unsigned(rx_frame_nb_errs);
        if(rx_frame_valid = '1') then
          bit_sum <= bit_sum + to_unsigned(rx_frame'length, bit_sum'length);
        end if;

        if(onu_rx_serror_corrected_i = '1') then
          serror_corrected_sum <= serror_corrected_sum + 1;
        end if;

        if(onu_rx_derror_corrected_i = '1') then
          derror_corrected_sum <= derror_corrected_sum + 1;
        end if;

        if(bert_latch_acc_rxsync_r = '1') then
          error_sum_latched            <= std_logic_vector(error_sum);
          bit_sum_latched              <= std_logic_vector(bit_sum);
          serror_corrected_sum_latched <= std_logic_vector(serror_corrected_sum);
          derror_corrected_sum_latched <= std_logic_vector(derror_corrected_sum);
        end if;
      end if;
    end if;
  end process p_bert_acc;

  --============================================================================
  -- Process p_rxsync
  --! read:  bert_clear_acc_async, bert_latch_acc_async
  --! write: bert_clear_acc_rxsync_r, bert_latch_acc_rxsync_r\n
  --! r/w:   bert_clear_acc_rxsync, bert_latch_acc_rxsync\n
  --============================================================================        
  p_rxsync : process(onu_clk_trxusr240_i) is
  begin
    if(onu_clk_trxusr240_i'event and onu_clk_trxusr240_i = '1') then
      bert_clear_acc_rxsync   <= bert_clear_acc_async;
      bert_clear_acc_rxsync_r <= bert_clear_acc_rxsync;

      bert_latch_acc_rxsync   <= bert_latch_acc_async;
      bert_latch_acc_rxsync_r <= bert_latch_acc_rxsync;
    end if;
  end process p_rxsync;



  --============================================================================
  -- AXI CLK
  --============================================================================ 
  onu_status_reg_async(0) <= onu_rx40_locked_i;
  onu_status_reg_async(1) <= onu_mgt_phase_good_i;
  onu_status_reg_async(2) <= onu_rx_locked_i;
  onu_status_reg_async(3) <= onu_operational_i;
  onu_status_reg_async(4) <= onu_mgt_tx_ready_i;
  onu_status_reg_async(5) <= onu_mgt_rx_ready_i;
  onu_status_reg_async(6) <= onu_mgt_txpll_lock_i;
  onu_status_reg_async(7) <= onu_mgt_rxpll_lock_i;

  onu_status_o <= onu_status_reg_async;

  --============================================================================
  -- Process p_axisync
  --! read:  -\n
  --! write: onu_status_reg_axisync_r\n
  --! r/w:   onu_status_reg_axisync\n
  --============================================================================    
  p_axisync : process(s_onu_user_aclk) is
  begin
    if(s_onu_user_aclk'event and s_onu_user_aclk = '1') then
      onu_status_reg_axisync   <= onu_status_reg_async;
      onu_status_reg_axisync_r <= onu_status_reg_axisync;
    end if;
  end process p_axisync;

  --============================================================================
  -- CONTROL / STAT
  --============================================================================       
  --REGISTER0 / R/W - SYS CONTROL
  onu_core_reset_o <= ctrl_reg_i(0)(0);
  onu_address_o    <= ctrl_reg_i(0)(8 downto 1);

  stat_reg_o(0) <= ctrl_reg_i(0);

  --REGISTER1 / R/W - RX CONTROL   
  bert_clear_acc_async <= ctrl_reg_i(1)(0);
  bert_latch_acc_async <= ctrl_reg_i(1)(1);

  stat_reg_o(1) <= ctrl_reg_i(1);


  --REGISTER2 / R/W - TX CONTROL   
  sel_tx_pattern_async      <= ctrl_reg_i(2)(1 downto 0);
  tx_interfacing_mode_async <= ctrl_reg_i(2)(2);
  tx_inject_error_async     <= ctrl_reg_i(2)(3);

  stat_reg_o(2) <= ctrl_reg_i(2);

  --REGISTER3 / R - SYS STATUS
  stat_reg_o(3)(7 downto 0)  <= onu_status_reg_axisync_r;
  stat_reg_o(3)(31 downto 8) <= (others => '0');

  --REGISTER4/5 / R - BIT SUM              
  stat_reg_o(4)(31 downto 0)  <= bit_sum_latched(31 downto 0);
  stat_reg_o(5)(15 downto 0)  <= bit_sum_latched(47 downto 32);
  stat_reg_o(5)(31 downto 16) <= (others => '0');

  --REGISTER6/7 / R - ERROR SUM              
  stat_reg_o(6)(31 downto 0)  <= error_sum_latched(31 downto 0);
  stat_reg_o(7)(15 downto 0)  <= error_sum_latched(47 downto 32);
  stat_reg_o(7)(31 downto 16) <= (others => '0');

  --REGISTER8/9 / R - SINGLE ERROR CORRECTED SUM                 
  stat_reg_o(8)(31 downto 0)  <= serror_corrected_sum_latched(31 downto 0);
  stat_reg_o(9)(15 downto 0)  <= serror_corrected_sum_latched(47 downto 32);
  stat_reg_o(9)(31 downto 16) <= (others => '0');

  --REGISTER10/11 / R - DOUBLE ERROR CORRECTED SUM                 
  stat_reg_o(10)(31 downto 0)  <= derror_corrected_sum_latched(31 downto 0);
  stat_reg_o(11)(15 downto 0)  <= derror_corrected_sum_latched(47 downto 32);
  stat_reg_o(11)(31 downto 16) <= (others => '0');

  --REGISTER 12-15 - NOT USED (LOOPBACK)
  stat_reg_o(12) <= ctrl_reg_i(12);
  stat_reg_o(13) <= ctrl_reg_i(13);
  stat_reg_o(14) <= ctrl_reg_i(14);
  stat_reg_o(15) <= ctrl_reg_i(15);
  
end rtl;
