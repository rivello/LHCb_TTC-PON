--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file fec_package.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: FEC package (fec_package)
--
--! @brief  TTC_PON for FEC package definitions
--! This package should never be modified by the user
--! Any change in those parameters can be catastrophic for the system and extremely hard to debug
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Package definition for fec_package.vhd
--============================================================================
package fec_package is

  --============================================================================
  -- Component instantiations
  --============================================================================
  component poly_div is
    generic (
      g_DIVISOR_POLY   : std_logic_vector := "10010001";  -- Notation: 1 + 0 + 0 + x^3 + 0 + 0 + 0 + x^7
      g_DIVIDEND_WIDTH : integer          := 40
      );
    port (
      clk_i       : in  std_logic;      --! clock input
      reset_i     : in  std_logic;
      enable_i    : in  std_logic;
      dividend_i  : in  std_logic_vector(g_DIVIDEND_WIDTH-1 downto 0);
      quotient_o  : out std_logic_vector(g_DIVIDEND_WIDTH-1 downto 0);
      remainder_o : out std_logic_vector(g_DIVISOR_POLY'length-2 downto 0)
      );
  end component poly_div;

  --============================================================================
  -- Function declaration
  --============================================================================
  function fcn_xor_reduce(arg1          : std_logic_vector; arg_mask : std_logic_vector) return std_logic;
  function fcn_r3_to_s3(rema3           : std_logic_vector(6 downto 0)) return std_logic_vector;
  function fcn_gf_mult_2pow7 (a         : std_logic_vector(6 downto 0); b : std_logic_vector(6 downto 0)) return std_logic_vector;
  function fcn_gf_invert_2pow7(a        : std_logic_vector(6 downto 0)) return std_logic_vector;
  function fcn_chien_search (value_eval : std_logic_vector; value_square_eval : std_logic_vector; x1_dot_x2 : std_logic_vector; x1_plus_x2 : std_logic_vector) return std_logic;

  --============================================================================
  -- Constants
  --============================================================================
  constant c_CRC7_POLY   : std_logic_vector(7 downto 0) := "11101101";  -- 1 + x + x^2 + x^4 + x^5 + x^7
  constant c_CRC7_HEADER : std_logic_vector(6 downto 0) := "0111001";

  constant c_SCR_POLY       : std_logic_vector(43 downto 0) := "10000000000000000000000000000000000000000001";  -- 1 + x^43
  constant c_SCR_INIT_STATE : std_logic_vector(42 downto 0) := "1001010011100100100000000001010111011011110";

  constant c_BCH120_106_GEN1_POLY  : std_logic_vector(7 downto 0)  := "10010001";  -- 1 + x^3 + x^7
  constant c_BCH120_106_GEN3_POLY  : std_logic_vector(7 downto 0)  := "11110001";  -- 1 + x + x^2 + x^3 + x^7
  constant c_BCH120_106_GEN13_POLY : std_logic_vector(14 downto 0) := "111011101100001";  -- 1 + x + x^2 + x^4 + x^5 + x^6 + x^8 + x^9 + x^14

  type     t_chien_search_array is array (119 downto 0) of std_logic_vector(6 downto 0);
  constant c_VALUE_CHIEN_SEARCH : t_chien_search_array :=
    (
      119 => "1000000",
      118 => "0100000",
      117 => "0010000",
      116 => "0001000",
      115 => "0000100",
      114 => "0000010",
      113 => "0000001",
      112 => "1001000",
      111 => "0100100",
      110 => "0010010",
      109 => "0001001",
      108 => "1001100",
      107 => "0100110",
      106 => "0010011",
      105 => "1000001",
      104 => "1101000",
      103 => "0110100",
      102 => "0011010",
      101 => "0001101",
      100 => "1001110",
      99  => "0100111",
      98  => "1011011",
      97  => "1100101",
      96  => "1111010",
      95  => "0111101",
      94  => "1010110",
      93  => "0101011",
      92  => "1011101",
      91  => "1100110",
      90  => "0110011",
      89  => "1010001",
      88  => "1100000",
      87  => "0110000",
      86  => "0011000",
      85  => "0001100",
      84  => "0000110",
      83  => "0000011",
      82  => "1001001",
      81  => "1101100",
      80  => "0110110",
      79  => "0011011",
      78  => "1000101",
      77  => "1101010",
      76  => "0110101",
      75  => "1010010",
      74  => "0101001",
      73  => "1011100",
      72  => "0101110",
      71  => "0010111",
      70  => "1000011",
      69  => "1101001",
      68  => "1111100",
      67  => "0111110",
      66  => "0011111",
      65  => "1000111",
      64  => "1101011",
      63  => "1111101",
      62  => "1110110",
      61  => "0111011",
      60  => "1010101",
      59  => "1100010",
      58  => "0110001",
      57  => "1010000",
      56  => "0101000",
      55  => "0010100",
      54  => "0001010",
      53  => "0000101",
      52  => "1001010",
      51  => "0100101",
      50  => "1011010",
      49  => "0101101",
      48  => "1011110",
      47  => "0101111",
      46  => "1011111",
      45  => "1100111",
      44  => "1111011",
      43  => "1110101",
      42  => "1110010",
      41  => "0111001",
      40  => "1010100",
      39  => "0101010",
      38  => "0010101",
      37  => "1000010",
      36  => "0100001",
      35  => "1011000",
      34  => "0101100",
      33  => "0010110",
      32  => "0001011",
      31  => "1001101",
      30  => "1101110",
      29  => "0110111",
      28  => "1010011",
      27  => "1100001",
      26  => "1111000",
      25  => "0111100",
      24  => "0011110",
      23  => "0001111",
      22  => "1001111",
      21  => "1101111",
      20  => "1111111",
      19  => "1110111",
      18  => "1110011",
      17  => "1110001",
      16  => "1110000",
      15  => "0111000",
      14  => "0011100",
      13  => "0001110",
      12  => "0000111",
      11  => "1001011",
      10  => "1101101",
      9   => "1111110",
      8   => "0111111",
      7   => "1010111",
      6   => "1100011",
      5   => "1111001",
      4   => "1110100",
      3   => "0111010",
      2   => "0011101",
      1   => "1000110",
      0   => "0100011");

  constant c_VALUE_SQUARE_CHIEN_SEARCH : t_chien_search_array :=
    (
      119 => "1000000",
      118 => "0010000",
      117 => "0000100",
      116 => "0000001",
      115 => "0100100",
      114 => "0001001",
      113 => "0100110",
      112 => "1000001",
      111 => "0110100",
      110 => "0001101",
      109 => "0100111",
      108 => "1100101",
      107 => "0111101",
      106 => "0101011",
      105 => "1100110",
      104 => "1010001",
      103 => "0110000",
      102 => "0001100",
      101 => "0000011",
      100 => "1101100",
      99  => "0011011",
      98  => "1101010",
      97  => "1010010",
      96  => "1011100",
      95  => "0010111",
      94  => "1101001",
      93  => "0111110",
      92  => "1000111",
      91  => "1111101",
      90  => "0111011",
      89  => "1100010",
      88  => "1010000",
      87  => "0010100",
      86  => "0000101",
      85  => "0100101",
      84  => "0101101",
      83  => "0101111",
      82  => "1100111",
      81  => "1110101",
      80  => "0111001",
      79  => "0101010",
      78  => "1000010",
      77  => "1011000",
      76  => "0010110",
      75  => "1001101",
      74  => "0110111",
      73  => "1100001",
      72  => "0111100",
      71  => "0001111",
      70  => "1101111",
      69  => "1110111",
      68  => "1110001",
      67  => "0111000",
      66  => "0001110",
      65  => "1001011",
      64  => "1111110",
      63  => "1010111",
      62  => "1111001",
      61  => "0111010",
      60  => "1000110",
      59  => "1011001",
      58  => "0110010",
      57  => "1000100",
      56  => "0010001",
      55  => "0100000",
      54  => "0001000",
      53  => "0000010",
      52  => "1001000",
      51  => "0010010",
      50  => "1001100",
      49  => "0010011",
      48  => "1101000",
      47  => "0011010",
      46  => "1001110",
      45  => "1011011",
      44  => "1111010",
      43  => "1010110",
      42  => "1011101",
      41  => "0110011",
      40  => "1100000",
      39  => "0011000",
      38  => "0000110",
      37  => "1001001",
      36  => "0110110",
      35  => "1000101",
      34  => "0110101",
      33  => "0101001",
      32  => "0101110",
      31  => "1000011",
      30  => "1111100",
      29  => "0011111",
      28  => "1101011",
      27  => "1110110",
      26  => "1010101",
      25  => "0110001",
      24  => "0101000",
      23  => "0001010",
      22  => "1001010",
      21  => "1011010",
      20  => "1011110",
      19  => "1011111",
      18  => "1111011",
      17  => "1110010",
      16  => "1010100",
      15  => "0010101",
      14  => "0100001",
      13  => "0101100",
      12  => "0001011",
      11  => "1101110",
      10  => "1010011",
      9   => "1111000",
      8   => "0011110",
      7   => "1001111",
      6   => "1111111",
      5   => "1110011",
      4   => "1110000",
      3   => "0011100",
      2   => "0000111",
      1   => "1101101",
      0   => "0111111");

end fec_package;

package body fec_package is

  function fcn_xor_reduce(arg1 : std_logic_vector; arg_mask : std_logic_vector) return std_logic is
    variable result : std_logic := '0';
  begin
    for i in arg1'range loop
      result := result xor (arg1(i) and arg_mask(i));
    end loop;

    return result;

  end fcn_xor_reduce;

  function fcn_r3_to_s3(rema3 : std_logic_vector(6 downto 0)) return std_logic_vector is
    variable s3 : std_logic_vector(rema3'range);
  begin
    s3(6) := rema3(6) xor rema3(1);
    s3(5) := rema3(2) xor rema3(1);
    s3(4) := rema3(3);
    s3(3) := rema3(5) xor rema3(1) xor rema3(0);
    s3(2) := rema3(2) xor rema3(0);
    s3(1) := rema3(3) xor rema3(2);
    s3(0) := rema3(4) xor rema3(0);
    return s3;
  end function fcn_r3_to_s3;

  function fcn_gf_mult_2pow7 (a : std_logic_vector(6 downto 0); b : std_logic_vector(6 downto 0)) return std_logic_vector is
    variable c : std_logic_vector(a'range);
  begin
    c(6) := (a(6) and b(6)) xor (a(5) and b(0)) xor (a(4) and b(1)) xor (a(3) and b(2)) xor (a(2) and b(3)) xor (a(1) and b(4)) xor (a(1) and b(0)) xor (a(0) and b(5)) xor (a(0) and b(1));
    c(5) := (a(6) and b(5)) xor (a(5) and b(6)) xor (a(4) and b(0)) xor (a(3) and b(1)) xor (a(2) and b(2)) xor (a(1) and b(3)) xor (a(0) and b(4)) xor (a(0) and b(0));
    c(4) := (a(6) and b(4)) xor (a(5) and b(5)) xor (a(4) and b(6)) xor (a(3) and b(0)) xor (a(2) and b(1)) xor (a(1) and b(2)) xor (a(0) and b(3));
    c(3) := (a(6) and b(3)) xor (a(5) and b(4)) xor (a(5) and b(0)) xor (a(4) and b(5)) xor (a(4) and b(1)) xor (a(3) and b(6)) xor (a(3) and b(2)) xor (a(2) and b(3)) xor (a(2) and b(0)) xor (a(1) and b(4)) xor (a(1) and b(1)) xor (a(1) and b(0)) xor (a(0) and b(5)) xor (a(0) and b(2)) xor (a(0) and b(1));
    c(2) := (a(6) and b(2)) xor (a(5) and b(3)) xor (a(4) and b(4)) xor (a(4) and b(0)) xor (a(3) and b(5)) xor (a(3) and b(1)) xor (a(2) and b(6)) xor (a(2) and b(2)) xor (a(1) and b(3)) xor (a(1) and b(0)) xor (a(0) and b(4)) xor (a(0) and b(1)) xor (a(0) and b(0));
    c(1) := (a(6) and b(1)) xor (a(5) and b(2)) xor (a(4) and b(3)) xor (a(3) and b(4)) xor (a(3) and b(0)) xor (a(2) and b(5)) xor (a(2) and b(1)) xor (a(1) and b(6)) xor (a(1) and b(2)) xor (a(0) and b(3)) xor (a(0) and b(0));
    c(0) := (a(6) and b(0)) xor (a(5) and b(1)) xor (a(4) and b(2)) xor (a(3) and b(3)) xor (a(2) and b(4)) xor (a(2) and b(0)) xor (a(1) and b(5)) xor (a(1) and b(1)) xor (a(0) and b(6)) xor (a(0) and b(2));
    return c;
  end function fcn_gf_mult_2pow7;

  function fcn_gf_invert_2pow7(a : std_logic_vector(6 downto 0)) return std_logic_vector is
    variable c : std_logic_vector(a'range);
  begin
    case a is
      when "1000000" => c := "1000000";
      when "0100000" => c := "0010001";
      when "1100000" => c := "0001111";
      when "0010000" => c := "0100010";
      when "1010000" => c := "0001010";
      when "0110000" => c := "0011110";
      when "1110000" => c := "0111101";
      when "0001000" => c := "1000100";
      when "1001000" => c := "1011001";
      when "0101000" => c := "0010100";
      when "1101000" => c := "1010111";
      when "0011000" => c := "0111100";
      when "1011000" => c := "0110101";
      when "0111000" => c := "1111010";
      when "1111000" => c := "0001100";
      when "0000100" => c := "0011001";
      when "1000100" => c := "0001000";
      when "0100100" => c := "0100011";
      when "1100100" => c := "0000001";
      when "0010100" => c := "0101000";
      when "1010100" => c := "0010111";
      when "0110100" => c := "0111111";
      when "1110100" => c := "0100110";
      when "0001100" => c := "1111000";
      when "1001100" => c := "0111010";
      when "0101100" => c := "1101010";
      when "1101100" => c := "1101110";
      when "0011100" => c := "1100101";
      when "1011100" => c := "0010101";
      when "0111100" => c := "0011000";
      when "1111100" => c := "1110101";
      when "0000010" => c := "0110010";
      when "1000010" => c := "0101001";
      when "0100010" => c := "0010000";
      when "1100010" => c := "1001010";
      when "0010010" => c := "1000110";
      when "1010010" => c := "0100001";
      when "0110010" => c := "0000010";
      when "1110010" => c := "1101001";
      when "0001010" => c := "1010000";
      when "1001010" => c := "1100010";
      when "0101010" => c := "0101110";
      when "1101010" => c := "0101100";
      when "0011010" => c := "1111110";
      when "1011010" => c := "0111011";
      when "0111010" => c := "1001100";
      when "1111010" => c := "0111000";
      when "0000110" => c := "1100001";
      when "1000110" => c := "0010010";
      when "0100110" => c := "1110100";
      when "1100110" => c := "1111111";
      when "0010110" => c := "1000101";
      when "1010110" => c := "1110001";
      when "0110110" => c := "1001101";
      when "1110110" => c := "0101101";
      when "0001110" => c := "1011011";
      when "1001110" => c := "1001011";
      when "0101110" => c := "0101010";
      when "1101110" => c := "1101100";
      when "0011110" => c := "0110000";
      when "1011110" => c := "1111101";
      when "0111110" => c := "1111011";
      when "1111110" => c := "0011010";
      when "0000001" => c := "1100100";
      when "1000001" => c := "1100011";
      when "0100001" => c := "1010010";
      when "1100001" => c := "0000110";
      when "0010001" => c := "0100000";
      when "1010001" => c := "1001111";
      when "0110001" => c := "0000101";
      when "1110001" => c := "1010110";
      when "0001001" => c := "0011101";
      when "1001001" => c := "0110111";
      when "0101001" => c := "1000010";
      when "1101001" => c := "1110010";
      when "0011001" => c := "0000100";
      when "1011001" => c := "1001000";
      when "0111001" => c := "1000011";
      when "1111001" => c := "0010011";
      when "0000101" => c := "0110001";
      when "1000101" => c := "0010110";
      when "0100101" => c := "1010101";
      when "1100101" => c := "0011100";
      when "0010101" => c := "1011100";
      when "1010101" => c := "0100101";
      when "0110101" => c := "1011000";
      when "1110101" => c := "1111100";
      when "0001101" => c := "1101101";
      when "1001101" => c := "0110110";
      when "0101101" => c := "1110110";
      when "1101101" => c := "0001101";
      when "0011101" => c := "0001001";
      when "1011101" => c := "1110111";
      when "0111101" => c := "1110000";
      when "1111101" => c := "1011110";
      when "0000011" => c := "1010011";
      when "1000011" => c := "0111001";
      when "0100011" => c := "0100100";
      when "1100011" => c := "1000001";
      when "0010011" => c := "1111001";
      when "1010011" => c := "0000011";
      when "0110011" => c := "1101111";
      when "1110011" => c := "0101011";
      when "0001011" => c := "0011011";
      when "1001011" => c := "1001110";
      when "0101011" => c := "1110011";
      when "1101011" => c := "0101111";
      when "0011011" => c := "0001011";
      when "1011011" => c := "0001110";
      when "0111011" => c := "1011010";
      when "1111011" => c := "0111110";
      when "0000111" => c := "0100111";
      when "1000111" => c := "1011111";
      when "0100111" => c := "0000111";
      when "1100111" => c := "0011111";
      when "0010111" => c := "1010100";
      when "1010111" => c := "1101000";
      when "0110111" => c := "1001001";
      when "1110111" => c := "1011101";
      when "0001111" => c := "1100000";
      when "1001111" => c := "1010001";
      when "0101111" => c := "1101011";
      when "1101111" => c := "0110011";
      when "0011111" => c := "1100111";
      when "1011111" => c := "1000111";
      when "0111111" => c := "0110100";
      when "1111111" => c := "1100110";
      when others    => c := "0000000";
    end case;
    return c;
  end function fcn_gf_invert_2pow7;

  function fcn_chien_search (value_eval : std_logic_vector; value_square_eval : std_logic_vector; x1_dot_x2 : std_logic_vector; x1_plus_x2 : std_logic_vector) return std_logic is
    variable c : std_logic;

    variable value_dot_x1_plus_x2 : std_logic_vector(value_eval'range);
    variable poly_result          : std_logic_vector(value_eval'range);

  begin

    value_dot_x1_plus_x2 := fcn_gf_mult_2pow7(value_eval, x1_plus_x2);
    poly_result          := value_dot_x1_plus_x2 xor value_square_eval xor x1_dot_x2;

    if(poly_result = "0000000") then
      c := '1';
    else
      c := '0';
    end if;

    return c;
  end function fcn_chien_search;

end fec_package;
