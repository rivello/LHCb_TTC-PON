--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file CountOnes.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Counter of ones (CountOnes)
--
--! @brief Counts number of ones in a vector
--! Based on a recursive technique, this might give some warnings depending on the platform where the design is being compiled
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - Created\n
-------------------------------------------------------------------------------
--! @todo - \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for CountOnes
--============================================================================
entity CountOnes is
  generic (SIZE        : positive := 32;
           MAXOUTWIDTH : positive := 6);
  port (clk_i : in  std_logic;
        I     : in  std_logic_vector (SIZE-1 downto 0);
        O     : out std_logic_vector (MAXOUTWIDTH-1 downto 0));
end CountOnes;

--============================================================================
--! Architecture declaration
--============================================================================
architecture RTL of CountOnes is

  component CountOnes is
    generic (SIZE        : positive;
             MAXOUTWIDTH : positive
             );
    port (clk_i : in  std_logic;
          I     : in  std_logic_vector(SIZE-1 downto 0);
          O     : out std_logic_vector(MAXOUTWIDTH-1 downto 0));
  end component;

  signal UpperOutput : std_logic_vector(O'length-2 downto 0);
  signal LowerOutput : std_logic_vector(O'length-2 downto 0);

--============================================================================
--! Architecture begin
--============================================================================
begin

  GT1 : if SIZE > 1 generate
    UpperHalf : CountOnes
      generic map (SIZE        => SIZE-SIZE/2,
                   MAXOUTWIDTH => MAXOUTWIDTH-1)
      port map (
        clk_i => clk_i,
        I     => I(SIZE-1 downto SIZE/2),
        O     => UpperOutput);

    LowerHalf : CountOnes
      generic map (SIZE        => SIZE/2,
                   MAXOUTWIDTH => MAXOUTWIDTH-1)
      port map (
        clk_i => clk_i,
        I     => I(SIZE/2-1 downto 0),
        O     => LowerOutput);

    clk : process(clk_i)
    begin
      if rising_edge(clk_i) then
        O <= std_logic_vector(unsigned('0' & UpperOutput) +
                              unsigned(LowerOutput));
      end if;
    end process;
  end generate;

  EQ1 : if SIZE = 1 generate
    process(clk_i)
    begin
      if rising_edge(clk_i) then
        O <= (others => '0');
        if I(0) = '1' then
          O(0) <= '1';
        end if;
      end if;
    end process;
  end generate;

end RTL;
