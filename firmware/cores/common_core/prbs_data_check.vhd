--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file prbs_data_check.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: PRBS checker (prbs_data_check)
--
--! @brief  PRBS data checker
--
--! @author Csaba Soos - csaba.soos@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - CS - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for PRBS checker
--==============================================================================
entity prbs_data_check is
  generic (
    G_DIN_WIDTH         : integer := 32;
    G_REVERSE_BITS      : integer := 0;
    G_MAX_ERRS_OUTWIDTH : integer := 6
    );
  port (
    clk_i  : in  std_logic;
    reset_i  : in  std_logic;
    ena_i    : in  std_logic;
    sel_i    : in  std_logic_vector(1 downto 0);
    -- "00" PRBS-7, "01" PRBS-23
    -- "1x" fix
    din_i    : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
    lock_o   : out std_logic;
    error_o  : out std_logic;
    nerrs_o  : out std_logic_vector(G_MAX_ERRS_OUTWIDTH-1 downto 0);
    refgen_o : out std_logic_vector(G_DIN_WIDTH-1 downto 0));

end entity prbs_data_check;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture RTL of prbs_data_check is

  signal ZEROS : std_logic_vector(127 downto 0);

  function slv_bitreverse (slv : in std_logic_vector) return std_logic_vector is
    variable slv_ret : std_logic_vector(slv'range);
    alias slv_rr     : std_logic_vector(slv'reverse_range) is slv;
  begin
    for i in slv'range loop
      slv_ret(i) := slv_rr(i);
    end loop;
    return slv_ret;
  end function slv_bitreverse;

  function bitcount (signal slv : std_logic_vector(3 downto 0)) return integer is
  begin  -- bitcount
    case slv is
      when "0000" => return 0;
      when "0001" => return 1;
      when "0010" => return 1;
      when "0011" => return 2;
      when "0100" => return 1;
      when "0101" => return 2;
      when "0110" => return 2;
      when "0111" => return 3;
      when "1000" => return 1;
      when "1001" => return 2;
      when "1010" => return 2;
      when "1011" => return 3;
      when "1100" => return 2;
      when "1101" => return 3;
      when "1110" => return 3;
      when "1111" => return 4;
      when others => return 0;
    end case;
  end bitcount;

  component prbs_data_gen
    generic (
      G_DOUT_WIDTH   : integer := 20;
      G_PRBS_TAP     : integer := 0;
      G_REVERSE_BITS : integer := 0);
    port (
      clk_i : in  std_logic;
      mode_i  : in  std_logic;  -- '0' external seed, '1' internal loop-back
      ena_i   : in  std_logic;          -- '0' hold, '1' generate
      sel_i   : in  std_logic_vector(1 downto 0);
      seed_i  : in  std_logic_vector(22 downto 0);
      dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));

  end component prbs_data_gen;

  component CountOnes
    generic (SIZE        : positive := 32;
             MAXOUTWIDTH : positive := 6);
    port (clk_i : in  std_logic;
          I     : in  std_logic_vector (SIZE-1 downto 0);
          O     : out std_logic_vector (MAXOUTWIDTH-1 downto 0));
  end component CountOnes;

  signal refgen_mode : std_logic;
  signal refgen_ena  : std_logic;
  signal refgen_sel  : std_logic_vector(1 downto 0);
  signal refgen_seed : std_logic_vector(22 downto 0);
  signal refgen_dout : std_logic_vector(G_DIN_WIDTH-1 downto 0);
  signal datain      : std_logic_vector(G_DIN_WIDTH-1 downto 0);
  signal datain_r    : std_logic_vector(G_DIN_WIDTH-1 downto 0);
  signal ena_r       : std_logic;
  signal ena_rr      : std_logic;
  signal data_error  : std_logic;
  signal search_mode : std_logic;

  -- ASK CSABA
  signal errd_bits : std_logic_vector(datain_r'range);
  --signal errd_bits     : std_logic_vector(95 downto 0);

--==============================================================================
-- architecture begin
--==============================================================================
begin  -- architecture RTL

  ZEROS <= (others => '0');

  no_reverse : if G_REVERSE_BITS = 0 generate
    datain <= din_i;
  end generate no_reverse;

  reverse : if G_REVERSE_BITS = 1 generate
    datain <= slv_bitreverse(din_i);
  end generate reverse;

  p_datain_reg : process(clk_i, reset_i) is
  begin
    if reset_i = '1' then
      datain_r <= (others => '1');
      ena_r    <= '0';
      ena_rr   <= '0';
    elsif clk_i'event and clk_i = '1' then
      datain_r <= datain;
      ena_r    <= ena_i;
      ena_rr   <= ena_r;
    end if;
  end process p_datain_reg;

  DIN_WIDER : if G_DIN_WIDTH >= 23 generate
    refgen_seed <= datain(refgen_seed'range);
  end generate DIN_WIDER;
  SEED_WIDER : if G_DIN_WIDTH < 23 generate
    refgen_seed <= datain_r((23-G_DIN_WIDTH)-1 downto 0) & datain;
  end generate SEED_WIDER;
  refgen_mode <= not search_mode;
  refgen_ena  <= ena_i;
  refgen_sel  <= sel_i;
  cmp_prbs_gen : prbs_data_gen
    generic map (
      G_DOUT_WIDTH => G_DIN_WIDTH,
      G_PRBS_TAP   => 0)
    port map (
      clk_i => clk_i,
      mode_i  => refgen_mode,
      ena_i   => refgen_ena,
      sel_i   => refgen_sel,
      seed_i  => refgen_seed,
      dout_o  => refgen_dout);

  p_compare : process (clk_i, reset_i) is
    variable good_data_count : integer range 0 to 15;
    variable bad_data_count  : integer range 0 to 15;
  begin  -- process p_regs
    if reset_i = '1' then               -- asynchronous reset (active high)
      search_mode     <= '1';
      data_error      <= '1';
      good_data_count := 0;
      bad_data_count  := 0;
    elsif clk_i'event and clk_i = '1' then  -- rising clk_i edge
      if ena_r = '0' then
        data_error <= '0';
      elsif datain_r /= refgen_dout then
        data_error <= '1';
      else
        data_error <= '0';
      end if;
      if search_mode = '1' and data_error = '0' and ena_rr = '1' then
        -- in searching mode and good data
        if good_data_count < 8 then
          good_data_count := good_data_count + 1;
        else
          search_mode     <= '0';
          good_data_count := 0;
          bad_data_count  := 0;
        end if;
      elsif search_mode = '1' and data_error = '1' and ena_rr = '1' then
        -- in searching mode and bad data
        good_data_count := 0;
      elsif search_mode = '0' and data_error = '1' and ena_rr = '1' then
        -- in locked state and bad data
        if bad_data_count < 8 then
          bad_data_count := bad_data_count + 1;
        else
          search_mode     <= '1';
          bad_data_count  := 0;
          good_data_count := 0;
        end if;
      elsif search_mode = '0' and data_error = '0' and ena_rr = '1' then
        -- in locked state and good data
        bad_data_count := 0;
      end if;
    end if;
  end process p_compare;
  lock_o  <= not search_mode;
  error_o <= data_error;


  p_error_detect : process (clk_i, reset_i)
  begin  -- process p_error_count
    if reset_i = '1' then               -- asynchronous reset (active high)
      errd_bits <= (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clk_i edge
      errd_bits <= (others => '0');
      if ena_r = '1' then
        errd_bits(datain_r'range) <= datain_r xor refgen_dout;
      end if;

    end if;
  end process p_error_detect;


  cmp_countones : CountOnes
    generic map(
      SIZE        => G_DIN_WIDTH,
      MAXOUTWIDTH => G_MAX_ERRS_OUTWIDTH
      )
    port map (
      clk_i => clk_i,
      I     => errd_bits,
      O     => nerrs_o
      );

  refgen_o <= refgen_dout;
  
end architecture RTL;
--==============================================================================
-- architecture end
--==============================================================================
