--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_onu_package_static.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: PON ONU package (pon_onu_package_static)
--
--! @brief  TTC_PON for ONU package definitions
--! Those parameters shall never be changed by the user
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Package definition for pon_onu_package_static.vhd
--============================================================================
package pon_onu_package_static is

  --! Those parameters should not be changed by user    
  --============================================================================
  -- ONU Data in/out
  --============================================================================   
  constant c_ONU_TX_MGT_DATA_WIDTH : integer := 10;
  constant c_ONU_RX_MGT_DATA_WIDTH : integer := 40;
  constant c_ONU_RX_USR_DATA_WIDTH : integer := 200;
  constant c_ONU_TX_USR_DATA_WIDTH : integer := 56;

  constant c_ONU_TX_CTRL_DATA_WIDTH : integer := 8;
  constant c_ONU_RX_CTRL_DATA_WIDTH : integer := 4;
  --============================================================================

  --============================================================================
  -- MANAGEMENT - CONTROL
  --============================================================================
  constant c_ONU_NBR_CTRL_BYTES : integer   := 512;
  --============================================================================

  --============================================================================
  -- ONU SFP Physical Parameters
  --============================================================================   
  constant c_PREAMBLE_LEN_TX_CLK_CYCLES : integer := 13;  -- Preamble duration
  constant c_BURST_EN_LIMIT             : integer := 25;  -- Duration of SFP_BURST_ENABLE
  constant c_SFP_EN_DELAY_PIPE_SIZE     : integer := 18;  -- given in RXCLK_PERIOD, corresponds to the delay given by the ONU core to the SFP enable signal to compensate latency of transceiver
  --============================================================================      

end pon_onu_package_static;



