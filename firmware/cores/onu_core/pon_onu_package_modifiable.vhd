--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_onu_package_modifiable.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: PON ONU package (pon_onu_package_modifiable)
--
--! @brief  ONU package which can be modified by user if needed
--! Those parameters can be changed by the user (but only if needed)
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Package definition for pon_onu_package_modifiable.vhd
--============================================================================
package pon_onu_package_modifiable is

  --============================================================================
  -- Simulation speed-up
  --============================================================================  
  constant c_IS_SIMULATION   : boolean := true;  -- speeds up simulation

  --============================================================================
  -- MANAGEMENT - CONTROL
  --============================================================================
  constant c_ONU_ENABLE_MANAGER : std_logic := '1';  -- to disable put '0'
  --============================================================================  

  --============================================================================
  -- MGT Related attributes
  --============================================================================   
  constant c_ONU_PERIOD_SYS_CLK : integer := 10;  -- This parameter is used by the MGT initialization, if using a frequency for clk_sys_i
                                                  -- other than 100MHz, change this value accordingly

  --============================================================================
  -- SCL PERIOD FOR IIC
  --============================================================================  
  constant c_ONU_SCL_PERIOD   : integer := 10000;  -- SCL period in ns

end pon_onu_package_modifiable;



