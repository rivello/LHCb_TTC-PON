--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file dec_bch120_106.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: BCH(120,106) decoder design (dec_bch120_106)
--
--! @brief Forward Error Correction of type BCH(120,106) shortened from BCH(127,113)
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 02\09\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 02\09\2016 - EBSM - Created\n
--! 27\02\2017 - EBSM - Added package, added error-corrected flags\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for dec_bch120_106
--==============================================================================
entity dec_bch120_106 is
  port (
    clk_i              : in  std_logic;  --! clock input
    reset_i            : in  std_logic;  --! active high sync. reset
    start_dec_i        : in  std_logic;  --! control scheduling
    data_i             : in  std_logic_vector(39 downto 0);   --! input data
    data_undecoded_o   : out std_logic_vector(105 downto 0);  --! output data undecoded
    data_o             : out std_logic_vector(105 downto 0);  --! output data decoded
    derror_corrected_o : out std_logic;  --! double error corrected
    serror_corrected_o : out std_logic  --! single error corrected
    );
end dec_bch120_106;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of dec_bch120_106 is

  --! Signal declaration
  -- Syndrome s1,s3 calculation
  signal rem1 : std_logic_vector(6 downto 0);
  signal rem3 : std_logic_vector(6 downto 0);
  signal s1   : std_logic_vector(6 downto 0);
  signal s3   : std_logic_vector(6 downto 0);

  signal s1_invert        : std_logic_vector(6 downto 0);
  signal s1_invert_dot_s3 : std_logic_vector(6 downto 0);
  signal s1_square        : std_logic_vector(6 downto 0);
  signal s1_triple        : std_logic_vector(6 downto 0);

  -- pipeline level inserted to meet timing  
  signal s1_r : std_logic_vector(6 downto 0);
  signal s3_r : std_logic_vector(6 downto 0);

  signal s1_invert_r        : std_logic_vector(6 downto 0);
  signal s1_invert_dot_s3_r : std_logic_vector(6 downto 0);
  signal s1_square_r        : std_logic_vector(6 downto 0);
  signal s1_triple_r        : std_logic_vector(6 downto 0);

  signal x1_dot_x2  : std_logic_vector(6 downto 0);
  signal x1_plus_x2 : std_logic_vector(6 downto 0);

  signal start_dec_r : std_logic;

  signal error_location : std_logic_vector(119 downto 0);

  signal data_buf : std_logic_vector(119 downto 0);

begin

  -- Syndrome s1,s3 calculation
  --============================================================================
  -- Component instantiation
  --! Component cmp_poly_div
  --! Calculates r1(x) = rem(r(x)/g1(x))
  --============================================================================   
  cmp_poly_div : poly_div
    generic map(
      g_DIVISOR_POLY   => c_BCH120_106_GEN1_POLY,
      g_DIVIDEND_WIDTH => 40
      )
    port map(
      clk_i       => clk_i,
      reset_i     => start_dec_i,
      enable_i    => '1',
      dividend_i  => data_i,
      quotient_o  => open,
      remainder_o => rem1
      );

  --============================================================================
  -- Component instantiation
  --! Component cmp_poly_div2
  --! Calculates r3(x) = rem(r(x)/g3(x))
  --============================================================================   
  cmp_poly_div2 : poly_div
    generic map(
      g_DIVISOR_POLY   => c_BCH120_106_GEN3_POLY,
      g_DIVIDEND_WIDTH => 40
      )
    port map(
      clk_i       => clk_i,
      reset_i     => start_dec_i,
      enable_i    => '1',
      dividend_i  => data_i,
      quotient_o  => open,
      remainder_o => rem3
      );

  -- s1 = r1(a) / s3 = r3(a.^3) 
  s1 <= rem1;
  s3 <= fcn_r3_to_s3(rem3);

  -- Calculation of error-locator polynomial coefficients
  s1_invert        <= fcn_gf_invert_2pow7(s1);
  s1_invert_dot_s3 <= fcn_gf_mult_2pow7(s1_invert, s3);
  s1_square        <= fcn_gf_mult_2pow7(s1, s1);
  s1_triple        <= fcn_gf_mult_2pow7(s1_square, s1);

  p_pipe_level0 : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      s1_r               <= s1;
      s3_r               <= s3;
      s1_invert_r        <= s1_invert;
      s1_invert_dot_s3_r <= s1_invert_dot_s3;
      s1_square_r        <= s1_square;
      s1_triple_r        <= s1_triple;
      start_dec_r        <= start_dec_i;
    end if;
  end process p_pipe_level0;

  x1_dot_x2  <= s1_invert_dot_s3_r xor s1_square_r;
  x1_plus_x2 <= s1_r;

  -- Root finding by using exhaustive chien-search method
  gen_chien_search : for k in 0 to 119 generate
    error_location(k) <= fcn_chien_search(c_VALUE_CHIEN_SEARCH(k), c_VALUE_SQUARE_CHIEN_SEARCH(k), x1_dot_x2, x1_plus_x2);
  end generate gen_chien_search;

  --============================================================================
  -- Process p_data_buf
  --! Data buffer and output register
  --! read:  clk_i, reset_i, error_location\n
  --! write: -\n
  --! r/w:   data_buf\n
  --============================================================================         
  p_data_buf : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      data_buf <= data_i & data_buf(119 downto 40);
    end if;
  end process p_data_buf;

  data_o           <= (data_buf(105 downto 0) xor error_location(105 downto 0));
  data_undecoded_o <= data_buf(105 downto 0);

  --============================================================================
  -- Process p_error_corrected_flag
  --! Generates a pulse when single-error or double-error is corrected
  --! read:  clk_i, reset_i, s3_r, s1_triple_r, s1_r\n
  --! write: serror_corrected_o\n
  --! r/w:   -\n
  --============================================================================         
  p_error_corrected_flag : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        serror_corrected_o <= '0';
        derror_corrected_o <= '0';
      elsif(start_dec_r = '1') then
        if(s1_r = "0000000" and s3_r = "0000000") then
          serror_corrected_o <= '0';
          derror_corrected_o <= '0';
        else
          if(s3_r = s1_triple_r) then
            serror_corrected_o <= '1';
            derror_corrected_o <= '0';
          else
            serror_corrected_o <= '0';
            derror_corrected_o <= '1';
          end if;
        end if;
      else
        serror_corrected_o <= '0';
        derror_corrected_o <= '0';
      end if;
    end if;
  end process p_error_corrected_flag;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
