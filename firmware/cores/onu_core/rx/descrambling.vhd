--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file descrambling.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Self-synchronous descrambler (descrambling)
--
--! @brief Self-synchronous descrambler
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 02\09\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 02\09\2016 - EBSM - Created\n
--! 27\02\2017 - EBSM - Added package\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for descrambling
--==============================================================================
entity descrambling is
  generic (
    g_PARAL_FACTOR  : integer := 40;
    g_HEADER_LENGTH : integer := 8
    );
  port (
    clk_i            : in  std_logic;   --! clock input
    en_i             : in  std_logic;   --! enable input
    data_is_header_i : in  std_logic;  --! data is header -> bypass descrambler
    data_i           : in  std_logic_vector(g_PARAL_FACTOR-1 downto 0);  --! input data
    data_o           : out std_logic_vector(g_PARAL_FACTOR-1 downto 0)  --! descrambled output data
    );
end descrambling;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of descrambling is

  --! Constant declaration  
  constant c_MINIMAL_POLY_REDUCE : std_logic_vector(c_SCR_POLY'length-2 downto 0) := c_SCR_POLY(c_SCR_POLY'length-1 downto 1);

  --! Signal declaration
  type   t_node_array is array (g_PARAL_FACTOR-1 downto 0) of std_logic_vector(c_SCR_POLY'length-2 downto 0);
  signal node_array : t_node_array;

  signal result  : std_logic_vector(g_PARAL_FACTOR-1 downto 0);
  signal node_ff : std_logic_vector(c_SCR_POLY'length-2 downto 0);

begin

  gen_shift_lsfr : for k in 0 to (g_PARAL_FACTOR-1) generate

    gen_first_state : if k = 0 generate  --first state
      node_array(0) <= node_ff;
    end generate gen_first_state;


    gen_other_states : if k = g_HEADER_LENGTH generate
      node_array(k)(c_SCR_POLY'length-2 downto 1) <= node_array(k-1)(c_SCR_POLY'length-3 downto 0) when data_is_header_i = '0' else node_FF(c_SCR_POLY'length-2 downto 1);
      node_array(k)(0)                            <= data_i(k-1)                                     when data_is_header_i = '0' else node_FF(0);
    end generate gen_other_states;

    gen_header_state : if (k /= 0 and k /= g_HEADER_LENGTH) generate
      node_array(k)(c_SCR_POLY'length-2 downto 1) <= node_array(k-1)(c_SCR_POLY'length-3 downto 0);
      node_array(k)(0)                            <= data_i(k-1);
    end generate gen_header_state;

    gen_header_feedthrough : if k < g_HEADER_LENGTH generate
      result(k) <= data_i(k) xor (fcn_xor_reduce(node_array(k), c_MINIMAL_POLY_REDUCE) and not data_is_header_i);
    end generate;

    gen_other_results : if k >= g_HEADER_LENGTH generate
      result(k) <= data_i(k) xor fcn_xor_reduce(node_array(k), c_MINIMAL_POLY_REDUCE);
    end generate;

  end generate gen_shift_lsfr;

  data_o <= result;

  process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(en_i = '1') then
        node_ff(node_ff'left downto 1) <= node_array(g_PARAL_FACTOR-1)(c_SCR_POLY'length-3 downto 0);
        node_ff(0)                     <= data_i(g_PARAL_FACTOR-1);
      end if;
    end if;
  end process;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

