--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_phy_control.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.pon_onu_package_static.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU PHY Control top design (onu_phy_control)
--
--! @brief ONU PHY Control top design for TTC-PON
--! <further description>
--! @author 
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author:
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_phy_control
--============================================================================
entity onu_phy_control is
  generic(
    g_CLK_SYS_PERIOD : integer range 5 to 20 := 10  -- clock period in ns
    );
  port (
    -- global input signals --
    clk_sys_i      : in std_logic;
    clk_trxusr240_i : in std_logic;

    core_reset_i : in std_logic;
    -------------------------

    -- global output signals --  
    reset_rxclk_o : out std_logic;
    reset_txclk_o : out std_logic;
    reset_mgt_o   : out std_logic;
    -------------------------

    -- status / control --
    -- from mgt
    mgt_rx_ready_i : in std_logic;
    mgt_tx_ready_i : in std_logic;

    -- from onu_rx
    header_locked_i : in std_logic;
    rx_mgt_reset_i  : in std_logic;

    -- onu_ctrl
    tx_ena_i           : in std_logic;
    sfp_enable_delay_i : in std_logic_vector(3 downto 0);
    sfp_tx_cont_en_i   : in std_logic;
    sfp_tx_off_i       : in std_logic;

    -- sfp
    sfp_rx_los_i   : in  std_logic;
    sfp_tx_fault_i : in  std_logic;
    sfp_mod_abs_i  : in  std_logic;
    sfp_tx_sd_i    : in  std_logic;
    sfp_pdown_o    : out std_logic;
    sfp_tx_dis_o   : out std_logic;

    --i2c interface
    i2c_ctrl_i    : in  std_logic_vector(31 downto 0);
    i2c_ctrl_en_i : in  std_logic;
    i2c_stat_o    : out std_logic_vector(31 downto 0);

    -- user interface
    i2c_req_o      : out std_logic;
    i2c_rnw_o      : out std_logic;
    i2c_rb2_o      : out std_logic;
    i2c_slv_addr_o : out std_logic_vector(6 downto 0);
    i2c_reg_addr_o : out std_logic_vector(7 downto 0);
    i2c_wr_data_o  : out std_logic_vector(7 downto 0);

    i2c_done_i     : in std_logic;
    i2c_error_i    : in std_logic;
    i2c_drop_req_i : in std_logic;
    i2c_rd_data_i  : in std_logic_vector(15 downto 0)
    -------------------------
    );
end entity onu_phy_control;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of onu_phy_control is

  attribute async_reg  : string;
  attribute mark_debug : string;

  --! Functions

  --! Constants

  --! Signal declaration

  --============================================================================
  -- SFP TX Enable logic
  --============================================================================  
  signal sfp_enable_pipe     : std_logic_vector(c_SFP_EN_DELAY_PIPE_SIZE-1 downto 0);
  signal sfp_enable          : std_logic;
  

  --============================================================================
  -- Reset scheme
  --============================================================================
  signal reset_mgt_pipe                 : std_logic_vector(3 downto 0) := "0000";  -- core_reset synchronizer to clk_sys_i
  attribute ASYNC_REG of reset_mgt_pipe : signal is "true";  
  signal reset_mgt                      : std_logic;  -- sync to clk_sys_i
  signal reset_txclk                    : std_logic;
  signal reset_rxclk                    : std_logic;
  ----------------------------------------------------------

  --============================================================================
  -- I2C master
  --============================================================================   
  --! Component declaration
  component i2c_rssi_ctrl is
    generic (
      g_CLK_SYS_PERIOD : integer range 5 to 20 := 10;   -- clock period in ns
      g_RSSI_TRG_WIDTH : integer               := 600;  -- RSSI trigger pulse width in ns
      g_RSSI_WAIT_TIME : integer               := 500000;  -- RSSI conversion time in ns
      g_RSSI_ENABLE    : boolean               := true  -- ENABLE RSSI LOGIC (false for ONU, true for OLT)
      );
    port (
      -- global input
      clk_sys_i : in std_logic;
      reset_i   : in std_logic;

      -- reg interface
      i2c_rssi_ctrl_i    : in  std_logic_vector(31 downto 0);
      i2c_rssi_ctrl_en_i : in  std_logic;
      i2c_rssi_stat_o    : out std_logic_vector(31 downto 0);

      -- user interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0);

      -- RSSI trigger
      rssi_trg_o : out std_logic
      );
  end component i2c_rssi_ctrl;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- SFP Enable logic
  --============================================================================
  --============================================================================
  -- Process p_sfp_enable
  --! sfp enable generation
  --! read:  tx_ena_i, sfp_tx_off_i, header_locked_i, sfp_tx_cont_en_i\n
  --! write: sfp_enable, sfp_tx_dis_o\n
  --! r/w:   sfp_enable_pipe\n
  --============================================================================           
  p_sfp_enable : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      sfp_tx_dis_o <= not sfp_enable;
      sfp_pdown_o  <= '1';

      if tx_ena_i = '1' then
        sfp_enable_pipe <= sfp_enable_pipe(sfp_enable_pipe'left-1 downto 0)&'1';
      else
        sfp_enable_pipe <= sfp_enable_pipe(sfp_enable_pipe'left-1 downto 0)&'0';
      end if;

      if (sfp_tx_off_i = '1' or header_locked_i = '0' or reset_rxclk='1' or reset_txclk='1') then
        sfp_enable <= '0';
      else
        case sfp_enable_delay_i is
          when "0000" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-16) or sfp_tx_cont_en_i;
          when "0001" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-15) or sfp_tx_cont_en_i;
          when "0010" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-14) or sfp_tx_cont_en_i;
          when "0011" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-13) or sfp_tx_cont_en_i;
          when "0100" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-12) or sfp_tx_cont_en_i;
          when "0101" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-11) or sfp_tx_cont_en_i;
          when "0110" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-10) or sfp_tx_cont_en_i;
          when "0111" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-9) or sfp_tx_cont_en_i;
          when "1000" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-8) or sfp_tx_cont_en_i;
          when "1001" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-7) or sfp_tx_cont_en_i;
          when "1010" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-6) or sfp_tx_cont_en_i;
          when "1011" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-5) or sfp_tx_cont_en_i;
          when "1100" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-4) or sfp_tx_cont_en_i;
          when "1101" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-3) or sfp_tx_cont_en_i;
          when "1110" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-2) or sfp_tx_cont_en_i;
          when "1111" =>
            sfp_enable <= sfp_enable_pipe(c_SFP_EN_DELAY_PIPE_SIZE-1) or sfp_tx_cont_en_i;
          when others =>
            sfp_enable <= '0';
        end case;
      end if;
    end if;
  end process p_sfp_enable;

  --============================================================================
  -- Reset Scheme
  --============================================================================
  --============================================================================
  -- Process p_reset_mgt_pipe
  --! System Reset synchronizer
  --! read:  rx_mgt_reset_i\n
  --! write: \n
  --! r/w:   reset_mgt_pipe\n
  --============================================================================            
  p_reset_mgt_pipe : process (clk_sys_i)
  begin  -- process p_reset_mgt_pipe
    if clk_sys_i'event and clk_sys_i = '1' then  -- rising clock edge
      reset_mgt_pipe <= reset_mgt_pipe(2 downto 0)&rx_mgt_reset_i;
    end if;
  end process p_reset_mgt_pipe;

  --reset_mgt <= '1' when reset_mgt_pipe = "1111" else '0';
  reset_mgt <= reset_mgt_pipe(reset_mgt_pipe'left);
  
  reset_mgt_o <= reset_mgt or core_reset_i or sfp_rx_los_i;

  reset_txclk <= not mgt_tx_ready_i;  --Note: mgt_tx_ready_i is synchronized to clk_txusr120_i in the mgt_wrapper 
  reset_rxclk <= not mgt_rx_ready_i;  --Note: mgt_rx_ready_i is synchronized to clk_trxusr240_i in the mgt_wrapper

  reset_txclk_o <= reset_txclk;
  reset_rxclk_o <= reset_rxclk;

  --============================================================================
  -- I2C master
  --============================================================================
  cmp_i2c_ctrl : i2c_rssi_ctrl
    generic map(
      g_CLK_SYS_PERIOD => g_CLK_SYS_PERIOD,  -- clock period in ns
      g_RSSI_TRG_WIDTH => 0,                 -- RSSI trigger pulse width in ns
      g_RSSI_WAIT_TIME => 0,                 -- RSSI conversion time in ns
      g_RSSI_ENABLE    => false
      )
    port map(
      -- global input
      clk_sys_i => clk_sys_i,
      reset_i   => core_reset_i,

      -- reg interface
      i2c_rssi_ctrl_i    => i2c_ctrl_i,
      i2c_rssi_ctrl_en_i => i2c_ctrl_en_i,
      i2c_rssi_stat_o    => i2c_stat_o,

      -- user interface
      i2c_req_o      => i2c_req_o,
      i2c_rnw_o      => i2c_rnw_o,
      i2c_rb2_o      => i2c_rb2_o,
      i2c_slv_addr_o => i2c_slv_addr_o,
      i2c_reg_addr_o => i2c_reg_addr_o,
      i2c_wr_data_o  => i2c_wr_data_o,

      i2c_done_i     => i2c_done_i,
      i2c_error_i    => i2c_error_i,
      i2c_drop_req_i => i2c_drop_req_i,
      i2c_rd_data_i  => i2c_rd_data_i,

      -- RSSI trigger
      rssi_trg_o => open
      );

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
