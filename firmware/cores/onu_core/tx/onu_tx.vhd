--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_tx.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU TX top design (onu_tx)
--
--! @brief ONU TX top design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! 14\01\2018 - EBSM - Modified MGT datapath to 10b\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo documentation \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_tx
--============================================================================
entity onu_tx is
  generic(
    g_PREAMBLE_LEN_CLK_CYCLES : integer := 13;
    g_DATA_MGT_WIDTH          : integer := 10;
    g_DATA_USR_WIDTH          : integer := 48;
    g_DATA_CTRL_WIDTH         : integer := 16
    );
  port (
    -- global input signals --
    clk_trxusr240_i : in std_logic;
    reset_i        : in std_logic;
    -------------------------     

    -- status / control --
    frame_ena_i         : in std_logic;  --! tx burst enable
    calibration_ena_i   : in std_logic;  --! calibration on
    slow_beat_i         : in std_logic;  --! slow beat during calibration (K28.1)
    fast_beat_i         : in std_logic;  --! fast beat during calibration (K28.5)
    tx_barrel_pos_i     : in std_logic_vector(3 downto 0);  --! 10b barrel shifter position
    onu_addr_i          : in std_logic_vector(7 downto 0);  --! onu address
    interface_usr_ena_i : in std_logic;  --! enable user interface data
    -------------------------     

    -- data in/out --
    data_usr_strobe_i : in  std_logic;
    data_usr_ready_o  : out std_logic;
    data_ctrl_ready_o : out std_logic;
    data_usr_ovr_i    : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
    data_usr_i        : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
    data_ctrl_i       : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
    data_mgt_o        : out std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0)
    -------------------------

    );
end entity onu_tx;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of onu_tx is

  --! Functions

  --! Constants

  --! Signal declaration
  --onu_tx_interface <-> onu_frame_gen  
  signal data_from_tx_interface     : std_logic_vector(g_DATA_USR_WIDTH+g_DATA_CTRL_WIDTH-1 downto 0);
  signal data_accept_from_frame_gen : std_logic;

  -- onu_frame_gen <-> 8b10b_encoder 
  signal tx8b10bbypass_from_frame_gen  : std_logic_vector(0 downto 0);
  signal txchardispmode_from_frame_gen : std_logic_vector(0 downto 0);
  signal txchardispval_from_frame_gen  : std_logic_vector(0 downto 0);
  signal txcharisk_from_frame_gen      : std_logic_vector(0 downto 0);
  signal data_from_frame_gen           : std_logic_vector(7 downto 0);

  -- 8b10b_encoder <-> barrel_shifter_20b   
  signal data_from_8b10b_encoder : std_logic_vector(data_mgt_o'range);

  --! Component declaration
  component onu_tx_interface is
    generic(
      g_DATA_USR_WIDTH  : integer := 48;
      g_DATA_CTRL_WIDTH : integer := 16
      );
    port (
      clk_i               : in  std_logic;
      reset_i             : in  std_logic;
      data_accept_i       : in  std_logic;  --! from frame generator      
      interface_usr_ena_i : in  std_logic;  --! enable user interface
      data_usr_strobe_i   : in  std_logic;  --! from user interface
      data_usr_ready_o    : out std_logic;  --! to user interface
      data_ctrl_ready_o   : out std_logic;  --! to control block
      data_usr_i          : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
      data_usr_ovr_i      : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);  --! when user interface is disabled, send this data
      data_ctrl_i         : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
      data_o              : out std_logic_vector(g_DATA_USR_WIDTH+g_DATA_CTRL_WIDTH-1 downto 0)
      );
  end component onu_tx_interface;

  component onu_frame_gen is
    generic(
      g_DATA_IN_WIDTH_BITS      : integer := 64;  --! payload bits per burst (multiple 16b)
      g_PREAMBLE_LEN_CLK_CYCLES : integer := 13    --! given in clock cycles
      );
    port (
      clk_i             : in  std_logic;
      reset_i           : in  std_logic;
      calibration_ena_i : in  std_logic;
      tx_ena_i          : in  std_logic;
      slow_beat_i       : in  std_logic;
      fast_beat_i       : in  std_logic;
      onu_addr_i        : in  std_logic_vector(7 downto 0);
      data_accept_o     : out std_logic;
      tx8b10bbypass_o   : out std_logic_vector(0 downto 0);
      txchardispmode_o  : out std_logic_vector(0 downto 0);
      txchardispval_o   : out std_logic_vector(0 downto 0);
      txcharisk_o       : out std_logic_vector(0 downto 0);
      data_i            : in  std_logic_vector(g_DATA_IN_WIDTH_BITS-1 downto 0);
      data_o            : out std_logic_vector(7 downto 0)
      );
  end component onu_frame_gen;

  component enc8b10b_wrapper is
    generic(
      g_NBR_8B10B_ENC : integer := 2    -- >= 2
      );
    port(
      clk_i           : in  std_logic;  -- clock input - rising edge triggered
      reset_i         : in  std_logic;  -- sync reset - active high                                       
      k_i             : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Control (K) input(active high)
      force_disp_i    : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Force disparity
      disp_val_i      : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Disparity value (if force disparity)
      tx8b10bbypass_i : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- bypass 8b10b encoding                                           
      data_i          : in  std_logic_vector((g_NBR_8B10B_ENC*8-1) downto 0);  -- user input data
      data_o          : out std_logic_vector((g_NBR_8B10B_ENC*10-1) downto 0)  -- encoded output data 
      );
  end component enc8b10b_wrapper;

  component barrel_shifter_10b is
    port(
      clk_i      : in  std_logic;
      nb_shift_i : in  std_logic_vector(3 downto 0);
      data_i     : in  std_logic_vector(9 downto 0);
      data_o     : out std_logic_vector(9 downto 0)
      );
  end component barrel_shifter_10b;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_tx_interface
  --============================================================================   
  cmp_onu_tx_interface : onu_tx_interface
    generic map(
      g_DATA_USR_WIDTH  => g_DATA_USR_WIDTH,
      g_DATA_CTRL_WIDTH => g_DATA_CTRL_WIDTH
      )
    port map(
      clk_i               => clk_trxusr240_i,
      reset_i             => reset_i,
      data_accept_i       => data_accept_from_frame_gen,
      interface_usr_ena_i => interface_usr_ena_i,
      data_usr_strobe_i   => data_usr_strobe_i,
      data_usr_ready_o    => data_usr_ready_o,
      data_ctrl_ready_o   => data_ctrl_ready_o,
      data_usr_i          => data_usr_i,
      data_usr_ovr_i      => data_usr_ovr_i,
      data_ctrl_i         => data_ctrl_i,
      data_o              => data_from_tx_interface
      );

  --============================================================================
  -- Component instantiation
  --! Component frame_generator
  --============================================================================   
  cmp_onu_frame_gen : onu_frame_gen
    generic map(
      g_DATA_IN_WIDTH_BITS      => (g_DATA_CTRL_WIDTH+g_DATA_USR_WIDTH),
      g_PREAMBLE_LEN_CLK_CYCLES => g_PREAMBLE_LEN_CLK_CYCLES
      )
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_i,
      calibration_ena_i => calibration_ena_i,
      tx_ena_i          => frame_ena_i,
      slow_beat_i       => slow_beat_i,
      fast_beat_i       => fast_beat_i,
      onu_addr_i        => onu_addr_i,
      data_accept_o     => data_accept_from_frame_gen,
      tx8b10bbypass_o   => tx8b10bbypass_from_frame_gen,
      txchardispmode_o  => txchardispmode_from_frame_gen,
      txchardispval_o   => txchardispval_from_frame_gen,
      txcharisk_o       => txcharisk_from_frame_gen,
      data_i            => data_from_tx_interface,
      data_o            => data_from_frame_gen
      );

  --============================================================================
  -- Component instantiation
  --! Component 8b10b encoder
  --============================================================================   
  cmp_8b10b : enc8b10b_wrapper
    generic map(
      g_NBR_8B10B_ENC => 1
      )
    port map(
      clk_i           => clk_trxusr240_i,
      reset_i         => reset_i,
      k_i             => txcharisk_from_frame_gen,
      force_disp_i    => txchardispmode_from_frame_gen,
      disp_val_i      => txchardispval_from_frame_gen,
      tx8b10bbypass_i => tx8b10bbypass_from_frame_gen,
      data_i          => data_from_frame_gen,
      data_o          => data_from_8b10b_encoder
      );

  --============================================================================
  -- Component instantiation
  --! Component 10b barrel shifter
  --============================================================================   
  cmp_barrel_shifter : barrel_shifter_10b
    port map(
      clk_i      => clk_trxusr240_i,
      nb_shift_i => tx_barrel_pos_i,
      data_i     => data_from_8b10b_encoder,
      data_o     => data_mgt_o
      );

end architecture structural;
--============================================================================
-- architecture end
--============================================================================
