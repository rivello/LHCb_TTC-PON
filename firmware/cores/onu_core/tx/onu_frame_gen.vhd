--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_frame_gen.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU frame generator (onu_frame_gen)
--
--! @brief ONU frame generator
--! Frame formatting ONU
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--! @date 26\06\2009
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--!
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 26\06\2009 - CS   - Created\n
--! 14\01\2018 - EBSM - Modified datapath to 10b\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo documentation \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_frame_gen
--============================================================================
entity onu_frame_gen is
  generic(
    g_DATA_IN_WIDTH_BITS      : integer := 64;  --! payload bits per burst (multiple 16b)
    g_PREAMBLE_LEN_CLK_CYCLES : integer := 13    --! given in clock cycles
    );
  port (
    clk_i             : in  std_logic;
    reset_i           : in  std_logic;
    calibration_ena_i : in  std_logic;
    tx_ena_i          : in  std_logic;
    slow_beat_i       : in  std_logic;
    fast_beat_i       : in  std_logic;
    onu_addr_i        : in  std_logic_vector(7 downto 0);
    data_accept_o     : out std_logic;
    tx8b10bbypass_o   : out std_logic_vector(0 downto 0);
    txchardispmode_o  : out std_logic_vector(0 downto 0);
    txchardispval_o   : out std_logic_vector(0 downto 0);
    txcharisk_o       : out std_logic_vector(0 downto 0);
    data_i            : in  std_logic_vector(g_DATA_IN_WIDTH_BITS-1 downto 0);
    data_o            : out std_logic_vector(7 downto 0)
    );
end entity onu_frame_gen;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of onu_frame_gen is

  --! Functions

  --! Constants
  constant c_PAYLOAD_LEN_CLK_CYCLES : integer := (g_DATA_IN_WIDTH_BITS/data_o'length);

  --! Signal declaration
  type   t_frame_ctrl_states is (TX_IDLE, TX_ENABLE_ZEROS, TX_INIT_PREAMBLE, TX_PREAMBLE, TX_SOF_COMMA, TX_SOF_ADDRESS, TX_PAYLOAD, TX_GAP, TX_GAP2);
  signal frame_ctrl_fsm_state : t_frame_ctrl_states;  --! onu frame FSM

  signal tx_k28_1 : std_logic;          --! transmit k28.1 for calibration

  signal preamble_cntr : integer range 0 to g_PREAMBLE_LEN_CLK_CYCLES-1;
  signal payload_cntr  : integer range 0 to c_PAYLOAD_LEN_CLK_CYCLES-1;

  --! Component declaration

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Process p_frame_control_fsm
  --! FSM responsible for onu framing
  --! read: calibration_ena_i, fast_beat_i, slow_beat_i, payload_cntr, preamble_cntr\n
  --! write: \n
  --! r/w: frame_ctrl_fsm_state\n
  --============================================================================          
  p_frame_control_fsm : process(clk_i)
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        frame_ctrl_fsm_state <= TX_IDLE;
      else
        frame_ctrl_fsm_state <= frame_ctrl_fsm_state;
        case frame_ctrl_fsm_state is
          when TX_IDLE =>
            if calibration_ena_i = '1' and fast_beat_i = '1' then
              frame_ctrl_fsm_state <= TX_PREAMBLE;
            elsif calibration_ena_i = '0' and tx_ena_i = '1' then
              frame_ctrl_fsm_state <= TX_ENABLE_ZEROS;
            end if;
            
          when TX_ENABLE_ZEROS =>
            frame_ctrl_fsm_state <= TX_INIT_PREAMBLE;
            
          when TX_INIT_PREAMBLE =>
            frame_ctrl_fsm_state <= TX_PREAMBLE;
          when TX_PREAMBLE =>
            if (preamble_cntr = (g_PREAMBLE_LEN_CLK_CYCLES-1) or calibration_ena_i = '1') then
              frame_ctrl_fsm_state <= TX_SOF_COMMA;
            else
              frame_ctrl_fsm_state <= TX_PREAMBLE;
            end if;
            
          when TX_SOF_COMMA =>
            frame_ctrl_fsm_state <= TX_SOF_ADDRESS;

          when TX_SOF_ADDRESS =>
            frame_ctrl_fsm_state <= TX_PAYLOAD;
            
          when TX_PAYLOAD =>
            if calibration_ena_i = '1' and (fast_beat_i = '1' or slow_beat_i = '1') then
              frame_ctrl_fsm_state <= TX_PREAMBLE;
            else
              if(payload_cntr = (c_PAYLOAD_LEN_CLK_CYCLES-1)) then
                frame_ctrl_fsm_state <= TX_GAP;
              else
                frame_ctrl_fsm_state <= TX_PAYLOAD;
              end if;
            end if;
            
          when TX_GAP =>
            frame_ctrl_fsm_state <= TX_GAP2;

          when TX_GAP2 =>
            frame_ctrl_fsm_state <= TX_IDLE;
            
          when others =>
            frame_ctrl_fsm_state <= TX_IDLE;
            
        end case;
      end if;
    end if;
  end process p_frame_control_fsm;

  --============================================================================
  -- Process p_frame_control_cntr
  --! Variable preamble/payload bursts
  --! read: frame_ctrl_fsm_state, calibration_ena_i\n
  --! write: \n
  --! r/w: preamble_cntr, payload_cntr\n   
  --============================================================================           
  p_frame_control_cntr : process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      if reset_i = '1' then
        preamble_cntr <= 0;
        payload_cntr  <= 0;
      else
        if (calibration_ena_i = '1') then
          preamble_cntr <= 0;
          payload_cntr  <= 0;
        else
          if (preamble_cntr = (g_PREAMBLE_LEN_CLK_CYCLES-1)) then
            preamble_cntr <= 0;
          elsif frame_ctrl_fsm_state = TX_PREAMBLE then
            preamble_cntr <= preamble_cntr+1;
          else
            preamble_cntr <= 0;
          end if;

          if (payload_cntr = (c_PAYLOAD_LEN_CLK_CYCLES-1)) then
            payload_cntr <= 0;
          elsif frame_ctrl_fsm_state = TX_PAYLOAD then
            payload_cntr <= payload_cntr+1;
          else
            payload_cntr <= 0;
          end if;
        end if;
      end if;
    end if;
  end process p_frame_control_cntr;

  --============================================================================
  -- Process p_header_control
  --! Variable preamble/payload bursts
  --! read: slow_beat_i, calibration_ena_i\n
  --! write: tx_k28_1\n
  --============================================================================            
  p_header_control : process (clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      if reset_i = '1' then
        tx_k28_1 <= '0';
      else
        if slow_beat_i = '1' and calibration_ena_i = '1' then
          tx_k28_1 <= '1';
        elsif frame_ctrl_fsm_state = TX_SOF_COMMA then
          tx_k28_1 <= '0';
        end if;
      end if;
    end if;
  end process p_header_control;

  --============================================================================
  -- Process p_frame_gen
  --! Data out burst
  --! read: frame_ctrl_fsm_state, payload_cntr\n
  --! write: data_o, txcharisk_o, tx8b10bbypass_o, txchardispmode_o, txchardispval_o\n
  --! r/w:   \n   
  --============================================================================                
  p_frame_gen : process (clk_i)
  begin  -- process p_frame_gen
    if (clk_i'event and clk_i = '1') then
      case frame_ctrl_fsm_state is
        when TX_IDLE =>
          data_o           <= X"78";  --keep DC balance
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_ENABLE_ZEROS =>
          data_o           <= X"00";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "1";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_INIT_PREAMBLE =>
          data_o           <= X"00";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "1";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_PREAMBLE =>
          data_o           <= X"B5";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_SOF_COMMA =>
          if tx_k28_1 = '1' then
            data_o (7 downto 0) <= X"3C";
          else
            data_o (7 downto 0) <= X"BC";
          end if;
          txcharisk_o      <= "1";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "1";     --we force RD for the comma and address
          txchardispval_o  <= "0";
        when TX_SOF_ADDRESS =>
          data_o (7 downto 0) <= onu_addr_i;
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "1";     --we force RD for the comma and address
          txchardispval_o  <= "0";		  
        when TX_PAYLOAD =>
          data_o           <= data_i(((payload_cntr+1)*8-1) downto (payload_cntr)*8);
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_GAP =>
          data_o           <= X"00";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "1";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when TX_GAP2 =>
          data_o           <= X"00";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "1";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
        when others =>
          data_o           <= X"78";
          txcharisk_o      <= "0";
          tx8b10bbypass_o  <= "0";
          txchardispmode_o <= "0";
          txchardispval_o  <= "0";
      end case;
    end if;
  end process p_frame_gen;

  data_accept_o <= '1' when frame_ctrl_fsm_state = TX_PREAMBLE else '0';
  
end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
