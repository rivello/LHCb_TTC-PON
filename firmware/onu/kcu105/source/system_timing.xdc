create_clock -period 4.167 -name FMC_HPC_GBTCLK0_M2C_C_P -waveform {0.000 2.083} [get_ports FMC_HPC_GBTCLK0_M2C_C_P]
create_clock -period 4.167 -name FMC_HPC_GBTCLK1_M2C_C_P -waveform {0.000 2.083} [get_ports FMC_HPC_GBTCLK1_M2C_C_P]
create_clock -period 3.300 -name clk_sys [get_ports CLK_300MHZ_P]