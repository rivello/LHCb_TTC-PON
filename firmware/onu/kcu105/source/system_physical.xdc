# System 300MHz clock
set_property IOSTANDARD LVDS [get_ports CLK_300MHZ_P]
set_property PACKAGE_PIN AK17 [get_ports CLK_300MHZ_P]

# GTH TX refclk
set_property PACKAGE_PIN K5 [get_ports FMC_HPC_GBTCLK0_M2C_C_N]
set_property PACKAGE_PIN K6 [get_ports FMC_HPC_GBTCLK0_M2C_C_P]

# GTH RX refclk
set_property PACKAGE_PIN H5 [get_ports FMC_HPC_GBTCLK1_M2C_C_N]
set_property PACKAGE_PIN H6 [get_ports FMC_HPC_GBTCLK1_M2C_C_P]

# Recovered 40MHz clock to FMC
set_property IOSTANDARD LVDS [get_ports {FMC_HPC_LA00_CC_N[0]}]
set_property PACKAGE_PIN H11 [get_ports {FMC_HPC_LA00_CC_P[0]}]
set_property PACKAGE_PIN G11 [get_ports {FMC_HPC_LA00_CC_N[0]}]
set_property IOSTANDARD LVDS [get_ports {FMC_HPC_LA00_CC_P[0]}]

# FMC I2C on FMC_HPC_LA01_CC_P/N
set_property PACKAGE_PIN G9 [get_ports iic_fmc_scl_io]
set_property IOSTANDARD LVCMOS18 [get_ports iic_fmc_scl_io]
set_property PACKAGE_PIN F9 [get_ports iic_fmc_sda_io]
set_property IOSTANDARD LVCMOS18 [get_ports iic_fmc_sda_io]

# FMC I2C_RESET
set_property PACKAGE_PIN D13 [get_ports {iic_fmc_gpo[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {iic_fmc_gpo[0]}]
# FMC PLL_RESET
set_property PACKAGE_PIN C13 [get_ports {iic_fmc_gpo[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {iic_fmc_gpo[1]}]

# FMC-SFP control connections
# TX DIS
set_property PACKAGE_PIN J10 [get_ports FMC_HPC_LA02_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA02_N]

# PDOWN
set_property PACKAGE_PIN K10 [get_ports FMC_HPC_LA02_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA02_P]

# RX LOS
set_property PACKAGE_PIN A12 [get_ports FMC_HPC_LA03_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA03_N]

# TX FAULT
set_property PACKAGE_PIN A13 [get_ports FMC_HPC_LA03_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA03_P]


# MOD ABS
set_property PACKAGE_PIN K12 [get_ports FMC_HPC_LA04_N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA04_N]

# TX SD
set_property PACKAGE_PIN L12 [get_ports FMC_HPC_LA04_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA04_P]

# PLL LOL
set_property PACKAGE_PIN L13 [get_ports FMC_HPC_LA05_P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_HPC_LA05_P]

#GPIO LEDs
set_property PACKAGE_PIN AP8 [get_ports {GPIO_LED[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[0]}]
set_property PACKAGE_PIN H23 [get_ports {GPIO_LED[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[1]}]
set_property PACKAGE_PIN P20 [get_ports {GPIO_LED[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[2]}]
set_property PACKAGE_PIN P21 [get_ports {GPIO_LED[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[3]}]
set_property PACKAGE_PIN N22 [get_ports {GPIO_LED[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[4]}]
set_property PACKAGE_PIN M22 [get_ports {GPIO_LED[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[5]}]
set_property PACKAGE_PIN R23 [get_ports {GPIO_LED[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {GPIO_LED[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[7]}]

#GPIO SMA - RX MATCH FLAG
set_property PACKAGE_PIN G27 [get_ports USER_SMA_GPIO_N]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_N]


#MGT location constraint
set_property LOC GTHE3_CHANNEL_X0Y16 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[0].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
set_property PACKAGE_PIN E3 [get_ports FMC_HPC_DP0_M2C_N]


# ILA FOR EXAMPLE DESIGN
