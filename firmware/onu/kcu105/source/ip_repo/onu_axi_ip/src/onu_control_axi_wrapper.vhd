--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_control_axi_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU control AXI4-Lite (onu_control_axi_wrapper)
--
--! @brief AXI4-Lite to ONU control interface
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_control_axi_wrapper
--============================================================================
entity onu_control_axi_wrapper is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface ONU_AXI
    C_ONU_AXI_DATA_WIDTH : integer := 32;
    C_ONU_AXI_ADDR_WIDTH : integer := 9
    );
  port (
    -- Users to add ports here
    stat_reg_i        : in  t_regbank32b(127 downto 0);
    ctrl_reg_o        : out std_logic_vector(31 downto 0);
    ctrl_reg_strobe_o : out std_logic_vector(127 downto 0);

    -- User ports ends
    -- Do not modify the ports beyond this line

    -- Ports of Axi Slave Bus Interface ONU_AXI
    onu_axi_aclk    : in  std_logic;
    onu_axi_aresetn : in  std_logic;
    onu_axi_awaddr  : in  std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
    onu_axi_awprot  : in  std_logic_vector(2 downto 0);
    onu_axi_awvalid : in  std_logic;
    onu_axi_awready : out std_logic;
    onu_axi_wdata   : in  std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
    onu_axi_wstrb   : in  std_logic_vector((C_ONU_AXI_DATA_WIDTH/8)-1 downto 0);
    onu_axi_wvalid  : in  std_logic;
    onu_axi_wready  : out std_logic;
    onu_axi_bresp   : out std_logic_vector(1 downto 0);
    onu_axi_bvalid  : out std_logic;
    onu_axi_bready  : in  std_logic;
    onu_axi_araddr  : in  std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
    onu_axi_arprot  : in  std_logic_vector(2 downto 0);
    onu_axi_arvalid : in  std_logic;
    onu_axi_arready : out std_logic;
    onu_axi_rdata   : out std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
    onu_axi_rresp   : out std_logic_vector(1 downto 0);
    onu_axi_rvalid  : out std_logic;
    onu_axi_rready  : in  std_logic
    );
end onu_control_axi_wrapper;

--============================================================================
--! Architecture declaration for onu_control_axi_wrapper
--============================================================================
architecture rtl of onu_control_axi_wrapper is

  -- AXI4LITE signals
  signal axi_awaddr  : std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
  signal axi_awready : std_logic;
  signal axi_wready  : std_logic;
  signal axi_bresp   : std_logic_vector(1 downto 0);
  signal axi_bvalid  : std_logic;
  signal axi_araddr  : std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
  signal axi_arready : std_logic;
  signal axi_rdata   : std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
  signal axi_rresp   : std_logic_vector(1 downto 0);
  signal axi_rvalid  : std_logic;

  -- Example-specific design signals
  -- local parameter for addressing 32 bit / 64 bit C_ONU_AXI_DATA_WIDTH
  -- ADDR_LSB is used for addressing 32/64 bit registers/memories
  -- ADDR_LSB = 2 for 32 bits (n downto 2)
  -- ADDR_LSB = 3 for 64 bits (n downto 3)
  constant ADDR_LSB          : integer := (C_ONU_AXI_DATA_WIDTH/32)+ 1;
  constant OPT_MEM_ADDR_BITS : integer := 6;
  -- User registers
  signal   slv_reg_rden      : std_logic;
  signal   slv_reg_wren      : std_logic;
  signal   reg_data_out      : std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
  signal   byte_index        : integer;

  -- USER SIGNALS
  signal slv_reg_strobe : std_logic_vector(127 downto 0);
  signal slv_reg_wr_aux : std_logic_vector(31 downto 0);

--============================================================================
--! Architecture begin for onu_control_axi_wrapper
--============================================================================
begin

  ----------------------------------------------------------------
  -- AXI logic
  ----------------------------------------------------------------
  ONU_AXI_AWREADY <= axi_awready;
  ONU_AXI_WREADY  <= axi_wready;
  ONU_AXI_BRESP   <= axi_bresp;
  ONU_AXI_BVALID  <= axi_bvalid;
  ONU_AXI_ARREADY <= axi_arready;
  ONU_AXI_RDATA   <= axi_rdata;
  ONU_AXI_RRESP   <= axi_rresp;
  ONU_AXI_RVALID  <= axi_rvalid;
  -- Implement axi_awready generation
  -- axi_awready is asserted for one ONU_AXI_ACLK clock cycle when both
  -- ONU_AXI_AWVALID and ONU_AXI_WVALID are asserted. axi_awready is
  -- de-asserted when reset is low.

  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_awready <= '0';
      else
        if (axi_awready = '0' and ONU_AXI_AWVALID = '1' and ONU_AXI_WVALID = '1') then
          -- slave is ready to accept write address when
          -- there is a valid write address and write data
          -- on the write address and data bus. This design 
          -- expects no outstanding transactions. 
          axi_awready <= '1';
        else
          axi_awready <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_awaddr latching
  -- This process is used to latch the address when both 
  -- ONU_AXI_AWVALID and ONU_AXI_WVALID are valid. 

  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_awaddr <= (others => '0');
      else
        if (axi_awready = '0' and ONU_AXI_AWVALID = '1' and ONU_AXI_WVALID = '1') then
          -- Write Address latching
          axi_awaddr <= ONU_AXI_AWADDR;
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_wready generation
  -- axi_wready is asserted for one ONU_AXI_ACLK clock cycle when both
  -- ONU_AXI_AWVALID and ONU_AXI_WVALID are asserted. axi_wready is 
  -- de-asserted when reset is low. 

  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_wready <= '0';
      else
        if (axi_wready = '0' and ONU_AXI_WVALID = '1' and ONU_AXI_AWVALID = '1') then
          -- slave is ready to accept write data when 
          -- there is a valid write address and write data
          -- on the write address and data bus. This design 
          -- expects no outstanding transactions.           
          axi_wready <= '1';
        else
          axi_wready <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement memory mapped register select and write logic generation
  -- The write data is accepted and written to memory mapped registers when
  -- axi_awready, ONU_AXI_WVALID, axi_wready and ONU_AXI_WVALID are asserted. Write strobes are used to
  -- select byte enables of slave registers while writing.
  -- These registers are cleared when reset (active low) is applied.
  -- Slave register write enable is asserted when valid address and data are available
  -- and the slave is ready to accept the write address and write data.
  slv_reg_wren <= axi_wready and ONU_AXI_WVALID and axi_awready and ONU_AXI_AWVALID;

  -- edited on 08/07 by EBSM
  process (ONU_AXI_ACLK)
    variable loc_addr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        slv_reg_wr_aux <= (others => '0');
        slv_reg_strobe <= (others => '0');
      else
        loc_addr       := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
        slv_reg_strobe <= (others => '0');

        if (slv_reg_wren = '1') then
          for byte_index in 0 to (C_ONU_AXI_DATA_WIDTH/8-1) loop
            if (ONU_AXI_WSTRB(byte_index) = '1') then
              -- Respective byte enables are asserted as per write strobes                   
              -- slave registor 0
              slv_reg_wr_aux(byte_index*8+7 downto byte_index*8) <= ONU_AXI_WDATA(byte_index*8+7 downto byte_index*8);
            end if;
          end loop;

          slv_reg_strobe(to_integer(unsigned(loc_addr))) <= '1';
          
        else
          slv_reg_strobe <= (others => '0');
        end if;
      end if;
    end if;
  end process;

  ctrl_reg_o        <= slv_reg_wr_aux;
  ctrl_reg_strobe_o <= slv_reg_strobe;

  -- Implement write response logic generation
  -- The write response and response valid signals are asserted by the slave 
  -- when axi_wready, ONU_AXI_WVALID, axi_wready and ONU_AXI_WVALID are asserted.  
  -- This marks the acceptance of address and indicates the status of 
  -- write transaction.

  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_bvalid <= '0';
        axi_bresp  <= "00";             --need to work more on the responses
      else
        if (axi_awready = '1' and ONU_AXI_AWVALID = '1' and axi_wready = '1' and ONU_AXI_WVALID = '1' and axi_bvalid = '0') then
          axi_bvalid <= '1';
          axi_bresp  <= "00";
        elsif (ONU_AXI_BREADY = '1' and axi_bvalid = '1') then  --check if bready is asserted while bvalid is high)
          axi_bvalid <= '0';  -- (there is a possibility that bready is always asserted high)
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_arready generation
  -- axi_arready is asserted for one ONU_AXI_ACLK clock cycle when
  -- ONU_AXI_ARVALID is asserted. axi_awready is 
  -- de-asserted when reset (active low) is asserted. 
  -- The read address is also latched when ONU_AXI_ARVALID is 
  -- asserted. axi_araddr is reset to zero on reset assertion.

  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_arready <= '0';
        axi_araddr  <= (others => '1');
      else
        if (axi_arready = '0' and ONU_AXI_ARVALID = '1') then
          -- indicates that the slave has acceped the valid read address
          axi_arready <= '1';
          -- Read Address latching 
          axi_araddr  <= ONU_AXI_ARADDR;
        else
          axi_arready <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_arvalid generation
  -- axi_rvalid is asserted for one ONU_AXI_ACLK clock cycle when both 
  -- ONU_AXI_ARVALID and axi_arready are asserted. The slave registers 
  -- data are available on the axi_rdata bus at this instance. The 
  -- assertion of axi_rvalid marks the validity of read data on the 
  -- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
  -- is deasserted on reset (active low). axi_rresp and axi_rdata are 
  -- cleared to zero on reset (active low).  
  process (ONU_AXI_ACLK)
  begin
    if rising_edge(ONU_AXI_ACLK) then
      if ONU_AXI_ARESETN = '0' then
        axi_rvalid <= '0';
        axi_rresp  <= "00";
      else
        if (axi_arready = '1' and ONU_AXI_ARVALID = '1' and axi_rvalid = '0') then
          -- Valid read data is available at the read data bus
          axi_rvalid <= '1';
          axi_rresp  <= "00";           -- 'OKAY' response
        elsif (axi_rvalid = '1' and ONU_AXI_RREADY = '1') then
          -- Read data is accepted by the master
          axi_rvalid <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement memory mapped register select and read logic generation
  -- Slave register read enable is asserted when valid address is available
  -- and the slave is ready to accept the read address.
  slv_reg_rden <= axi_arready and ONU_AXI_ARVALID and (not axi_rvalid);

  process (stat_reg_i, axi_araddr, ONU_AXI_ARESETN, slv_reg_rden)
    variable loc_addr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
  begin
    -- Address decoding for reading registers
    loc_addr     := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
    reg_data_out <= stat_reg_i(to_integer(unsigned(loc_addr)));
  end process;

  -- Output register or memory read data
  process(ONU_AXI_ACLK) is
  begin
    if (rising_edge (ONU_AXI_ACLK)) then
      if (ONU_AXI_ARESETN = '0') then
        axi_rdata <= (others => '0');
      else
        if (slv_reg_rden = '1') then
          -- When there is a valid read address (ONU_AXI_ARVALID) with 
          -- acceptance of read address by the slave (axi_arready), 
          -- output the read dada 
          -- Read address mux
          axi_rdata <= reg_data_out;    -- register read data
        end if;
      end if;
    end if;
  end process;
  
end architecture rtl;
