### ONU-IP CORE CONSTRAINTS ###
# Clocks
# Input clocks are constrained in top level (otherwise, there might happen a clock path break)

# Mailbox constraining
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_a2b_togflag_reg/C} ] -to [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_b_ready_reg*/D}]
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_a_reg*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_mailbox*/*data_b_reg*/D}] 4.000

set_false_path -to [get_pins -hier -filter {NAME =~ *axi_rdata_reg*/D}] 

########### ONU constraints ###########
# MGT reset synchronizer from ONU_RX
set_false_path -to [get_pins -hier -filter {NAME =~ */cmp_onu_phy_control/reset_mgt_pipe_reg[0]/D}]

# MMCM Control synchronizers
set_false_path -to [get_pins -hier -filter {NAME =~ *clkdiv_phase_ctrl/*meta*/D}]

# False paths related to synchronizers
set_false_path -to [get_pins -hier -filter {NAME =~ *cmp_onu_control/*trxsync_meta*/D}]
set_false_path -to [get_pins -hier -filter {NAME =~ *cmp_onu_control/*syssync_meta*/D}]
set_false_path -to [get_pins -hier -filter {NAME =~ *cmp_onu_control/*mngrsync_meta*/D}]
set_false_path -to [get_pins -hier -filter {NAME =~ *cmp_onu_control/*mngrsync_meta*/CE}]

# Latched scheme false paths
set_false_path -from [get_cells -hier -filter {NAME =~ *onu_control/rx_crc_errdetect_cntr_latched_reg*}]
set_false_path -from [get_cells -hier -filter {NAME =~ *onu_control/rx_derror_correc_cntr_latched_reg*}]
set_false_path -from [get_cells -hier -filter {NAME =~ *onu_control/rx_serror_correc_cntr_latched_reg*}]

# MGT exceptions
# False paths related to synchronizers
set_false_path -to [get_cells -hier -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
set_false_path -to [get_cells -hier -filter {NAME =~ *reset_synchronizer*inst/rst_in_*_reg}]
set_false_path -to [get_cells -hier -filter {NAME =~ *userclk_tx_active_meta*_reg}]
set_false_path -to [get_cells -hier -filter {NAME =~ *userclk_rx_active_meta*_reg}]

########### Tx Phase Aligner Constraints ###########
# Synchronizers internal to tx_phase_aligner
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_tx_phase_aligner/*meta*/D}]

# Latched with a done signal
set_false_path -to [get_pins -hier -filter  {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_o*/D}]

# Reset fifo fill pd after changing value of phase_detector_max from FSM
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/*cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/phase_detector_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/*cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/hits_acc_reg*/CE}]
set_false_path -from [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/*cmp_tx_phase_aligner_fsm/*/C}] -to [get_pins -hier -filter {NAME =~ *cmp_tx_phase_aligner/cmp_fifo_fill_level_acc/done_reg/D}]











