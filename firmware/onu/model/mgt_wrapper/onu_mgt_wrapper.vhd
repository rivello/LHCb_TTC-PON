--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_mgt_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU High Level MGT for simulation (onu_mgt_wrapper)
--
--! @brief ONU MultiGigabitTransceiver wrapper for simulation 
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 07\09\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\09\2018 - EBSM - Created
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_mgt_wrapper
--============================================================================
entity onu_mgt_wrapper is
  generic(
    g_SPEED_FOR_SIMULATION : boolean := true;  --! If true, rx slide only moves clock every two pulses, yielding in a always even number of rx slide
    g_UI_DOWN_PERIOD       : time    := 0.100 ns  --! UI_PERIOD: this generic should be EXACTLY equal to the period of clk_rxref240_i/40
    );
  port (
    -- global input signals --          
    clk_sys_i      : in std_logic;
    clk_rxref240_i : in std_logic;
    clk_txref240_i : in std_logic;
    mgt_reset_i    : in std_logic;
    -------------------------

    -- global output signals -- 
    clk_trxusr240_o : out std_logic;
    -------------------------

    -- status/control --
    tx_external_locked_i : in std_logic;
    rx_slide_i           : in std_logic;

    rxfsmrstdone_o : out std_logic;
    txfsmrstdone_o : out std_logic;
    mgt_txpll_o    : out std_logic;
    mgt_rxpll_o    : out std_logic;
    phase_good_o   : out std_logic;

    drp_wr_i                : in  std_logic_vector(31 downto 0);
    drp_wr_strobe_i         : in  std_logic;
    drp_monitor_o           : out std_logic_vector(31 downto 0);
    mgt_rx_equalizer_ctrl_i : in  std_logic_vector(31 downto 0);
    mgt_rx_equalizer_stat_o : out std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_i     : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_o     : out std_logic_vector(31 downto 0);
    -------------------------              

    -- data in/out --   
    tx_data_i : in  std_logic_vector(9 downto 0);
    rx_data_o : out std_logic_vector(39 downto 0);
    rx_p_i    : in  std_logic;
    rx_n_i    : in  std_logic;
    tx_p_o    : out std_logic;
    tx_n_o    : out std_logic
    -------------------------              
    );
end onu_mgt_wrapper;

--============================================================================
--! Architecture declaration for onu_mgt_wrapper
--============================================================================
architecture high_level of onu_mgt_wrapper is

  signal tx_data_r    : std_logic_vector(tx_data_i'range);
  signal clk_rxusr240 : std_logic;

--============================================================================
--! Architecture begin
--============================================================================  
begin

  -- transmitter
  p_serializer : process
    variable v_sercount : integer range 0 to tx_data_i'left := 0;
  begin
    v_sercount     := 0;
    txfsmrstdone_o <= '0';
    mgt_txpll_o    <= '0';
    phase_good_o   <= '0';
    wait until rising_edge(clk_rxusr240);
    wait for 1*g_UI_DOWN_PERIOD*4/2;
    while(mgt_reset_i = '0' and tx_external_locked_i = '1') loop
      tx_p_o <= tx_data_r(v_sercount);
      --wait for g_UI_DOWN_PERIOD*4; -- upstream_data_rate = downstream_data_rate/4
      if(v_sercount = tx_data_i'left) then
        v_sercount     := 0;
        txfsmrstdone_o <= '1';
        mgt_txpll_o    <= '1';
        phase_good_o   <= '1';		
      else
        v_sercount := v_sercount + 1;
      end if;
      wait for g_UI_DOWN_PERIOD*4;  -- upstream_data_rate = downstream_data_rate/4               
    end loop;
  end process p_serializer;

  tx_data_r <= tx_data_i when rising_edge(clk_rxusr240);
  tx_n_o         <= '0';

  -- receiver
  p_deserializer : process
    variable v_desercount : integer range 0 to rx_data_o'left := 0;
    variable v_deser_reg  : std_logic_vector(rx_data_o'range);

    variable v_RX_SLIDE_WAS_LOW : boolean := false;

    -- a random number is generated in order to give an initial value for v_deser_reg upon reset
    -- this is used to give a consistent even/odd number of initial clock phase positions required by the ONU logic
    variable seed1  : positive;
    variable seed2  : positive;
    variable randnb : real;
  begin

    uniform(seed1, seed2, randnb);
    if(g_SPEED_FOR_SIMULATION) then
      v_desercount := 0;
    else
      v_desercount := integer(floor(randnb*40.0));
    end if;

    rxfsmrstdone_o <= '0';
    mgt_rxpll_o    <= '0';
    clk_rxusr240 <= '0';
    wait until rising_edge(rx_p_i);     -- CDR
    wait for 1*g_UI_DOWN_PERIOD/2;      -- CDR
    while(mgt_reset_i = '0') loop
      v_deser_reg(v_desercount) := rx_p_i;
      wait for g_UI_DOWN_PERIOD*3/4;  -- avoid delta-delay mismatch between clk and data (we delay slightly the clock w.r.t. data so we have a consistent sampling inside logic)
      if(rx_slide_i = '1' and v_RX_SLIDE_WAS_LOW) then  -- RX_SLIDE
        v_desercount := v_desercount;
        wait for g_UI_DOWN_PERIOD/4;
      else
        if(v_desercount = rx_data_o'left) then
          v_desercount   := 0;
          rx_data_o      <= v_deser_reg;
          wait for g_UI_DOWN_PERIOD/4;
          clk_rxusr240 <= '1';
          rxfsmrstdone_o <= '1';
          mgt_rxpll_o    <= '1';
        elsif(v_desercount = (rx_data_o'left+1)/2 - 1) then
          wait for g_UI_DOWN_PERIOD/4;
          clk_rxusr240 <= '0';
          v_desercount   := v_desercount + 1;
        else
          wait for g_UI_DOWN_PERIOD/4;
          v_desercount := v_desercount + 1;
        end if;
      end if;
      if(rx_slide_i = '0') then
        v_RX_SLIDE_WAS_LOW := true;
      else
        v_RX_SLIDE_WAS_LOW := false;
      end if;
    end loop;
  end process p_deserializer;

  clk_trxusr240_o <= clk_rxusr240;

  drp_monitor_o           <= (others => '0');
  mgt_rx_equalizer_stat_o <= (others => '0');
  mgt_tx_phase_stat_o     <= (others => '0');

end high_level;
