--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_mgt_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
--use work.pon_onu_package.all;

-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU MGT Wrapper (onu_mgt_wrapper)
--
--! @brief onu MultiGigabitTransceiver wrapper for Arria10 design
--! Arria10 - MGT:
--! - two XCVR simplex instantiated in the same channel
--! - PHY reset controller 
--! - ATXPLL instantiating master clock generation block in order to perform feedback compensation bonding (fixed latency)
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 26\10\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 26\10\2016 - EBSM - Created\n
--! 28\11\2016 - EBSM - Registered rx_parallel_data to improve timing\n
--! 16\01\2018 - EBSM - Modified design to support 10b interface\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_mgt_wrapper
--============================================================================
entity onu_mgt_wrapper is
  port (
    -- reconfig interface --
    plls0_clk           : in  std_logic                     := '0';
    plls0_reset         : in  std_logic                     := '0';
    plls0_waitrequest   : out std_logic;
    plls0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
    plls0_write         : in  std_logic                     := '0';
    plls0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
    plls0_read          : in  std_logic                     := '0';
    plls0_readdata      : out std_logic_vector(31 downto 0);
    plls0_readdatavalid : out std_logic;

    txs0_clk           : in  std_logic                     := '0';
    txs0_reset         : in  std_logic                     := '0';
    txs0_waitrequest   : out std_logic;
    txs0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
    txs0_write         : in  std_logic                     := '0';
    txs0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
    txs0_read          : in  std_logic                     := '0';
    txs0_readdata      : out std_logic_vector(31 downto 0);
    txs0_readdatavalid : out std_logic;
    -------------------------

    -- global input signals --          
    clk_sys_i      : in std_logic;
    clk_rxref240_i : in std_logic;
    clk_txref240_i : in std_logic;
    mgt_reset_i    : in std_logic;
    -------------------------

    -- global output signals -- 
    clk_trxusr240_o : out std_logic;
    -------------------------

    -- status/control --
    tx_external_locked_i : in  std_logic;
    rx_slide_i           : in  std_logic;
    rxfsmrstdone_o       : out std_logic;
    txfsmrstdone_o       : out std_logic;
    mgt_txpll_o          : out std_logic;
    mgt_rxpll_o          : out std_logic;
	phase_good_o         : out std_logic;
    disable_phase_good_i : in  std_logic;

    -- specific A10
    rx_set_locktodata_i  : in  std_logic;
    rx_set_locktoref_i   : in  std_logic;
    -------------------------              

    -- data in/out --   
    tx_data_i : in  std_logic_vector(9 downto 0);
    rx_data_o : out std_logic_vector(39 downto 0);
    rx_p_i    : in  std_logic;
    rx_n_i    : in  std_logic;
    tx_p_o    : out std_logic;
    tx_n_o    : out std_logic
    -------------------------              
    );
end onu_mgt_wrapper;

--============================================================================
--! Architecture declaration for onu_mgt_wrapper
--============================================================================
architecture rtl of onu_mgt_wrapper is

  signal tied_to_ground_i     : std_logic;
  signal tied_to_ground_vec_i : std_logic_vector(127 downto 0);
  signal tied_to_vcc_i        : std_logic;
  signal tied_to_vcc_vec_i    : std_logic_vector(127 downto 0);

------------------------------------------------------------------------


  -- XCVR PHY connections
  signal rx_analogreset          : std_logic_vector(0 downto 0);  -- rx_analogreset
  signal rx_cal_busy             : std_logic_vector(0 downto 0);  -- rx_cal_busy
  signal rx_cdr_refclk0          : std_logic;                     -- clk
  signal rx_clkout               : std_logic_vector(0 downto 0);  -- clk
  signal rx_coreclkin            : std_logic_vector(0 downto 0);  -- clk
  signal rx_digitalreset         : std_logic_vector(0 downto 0);  -- rx_digitalreset
  signal rx_is_lockedtodata      : std_logic_vector(0 downto 0);  -- rx_is_lockedtodata
  signal rx_is_lockedtoref       : std_logic_vector(0 downto 0);  -- rx_is_lockedtoref
  signal rx_parallel_data        : std_logic_vector(39 downto 0);  -- rx_parallel_data
  signal rx_pma_clkslip          : std_logic_vector(0 downto 0);  -- rx_pma_clkslip
  signal rx_serial_data          : std_logic_vector(0 downto 0);  -- rx_serial_data
  signal s_txs0_waitreq          : std_logic;
  signal tx_analogreset          : std_logic_vector(0 downto 0);  -- tx_analogreset
  signal tx_bonding_clocks       : std_logic_vector(5 downto 0);  -- clk
  signal tx_cal_busy             : std_logic_vector(0 downto 0);  -- tx_cal_busy
  signal tx_clkout               : std_logic_vector(0 downto 0);  -- clk
  signal tx_coreclkin            : std_logic_vector(0 downto 0);  -- clk
  signal tx_digitalreset         : std_logic_vector(0 downto 0);  -- tx_digitalreset
  signal tx_parallel_data        : std_logic_vector(9 downto 0);  -- tx_parallel_data
  signal tx_serial_data          : std_logic_vector(0 downto 0);  -- tx_serial_data
  signal unused_rx_parallel_data : std_logic_vector(87 downto 0);  -- unused_rx_parallel_data
  signal unused_tx_parallel_data : std_logic_vector(117 downto 0);  -- unused_tx_parallel_data

  signal s_plls0_waitreq : std_logic;
  signal clock           : std_logic;                     -- clk
  signal pll_locked      : std_logic_vector(0 downto 0);  -- pll_locked
  signal pll_powerdown   : std_logic_vector(0 downto 0);  -- pll_powerdown
  signal pll_select      : std_logic_vector(0 downto 0);  -- pll_select
  signal reset           : std_logic;                     -- reset
  signal tx_reset        : std_logic;                     -- transmitter reset
  signal rx_ready        : std_logic_vector(0 downto 0);  -- rx_ready
  signal cal_busy        : std_logic_vector(0 downto 0);  -- tx_cal_busy
  signal tx_ready        : std_logic_vector(0 downto 0);  -- tx_ready

  signal mcgb_rst      : std_logic;     -- mcgb_rst
  signal pll_cal_busy  : std_logic;     -- pll_cal_busy
  signal pll_refclk0   : std_logic;     -- clk
  signal tx_serial_clk : std_logic;     -- clk

  signal rx_parallel_data_r      : std_logic_vector(39 downto 0);
  signal rx_parallel_data_nedger : std_logic_vector(39 downto 0);
------------------------------------------------------------------------

  signal txusrclk : std_logic;
  signal rxusrclk : std_logic;

  signal gt0_rxfsmresetdone_r  : std_logic;
  signal gt0_txfsmresetdone_r  : std_logic;
  signal gt0_rxfsmresetdone_r2 : std_logic;
  signal gt0_txfsmresetdone_r2 : std_logic;
  signal reset_fifo_meta       : std_logic;
  signal reset_fifo_r          : std_logic;
  
  component fifo_sync is
    generic(
      g_PHASE_GOOD_LOCK    : integer := 31;   --! number of consecutive good phase detected to lock phase_good_o output
      g_PHASE_NGOOD_UNLOCK : integer := 3;    --! number of consecutive bad phase detected to unlock phase_good_o output 
      g_DATA_WIDTH         : integer := 10    --! data width
      );
    port (
      clk_wr_i            : in  std_logic;                                 --! clock input domain write
      data_wr_i           : in  std_logic_vector(g_DATA_WIDTH-1 downto 0); --! data from domain write	
      phase_good_o        : out std_logic;                                 --! Phase relation between clocks is good	
      clk_rd_i            : in  std_logic;                                 --! clock input domain read	
  	  disable_phase_good_i: in  std_logic;                                 --! disable reset fifo with phase good	
      reset_i             : in  std_logic;                                 --! reset synchronous (synchronous to read pointer)	
      data_rd_o           : out std_logic_vector(g_DATA_WIDTH-1 downto 0) --! data to domain read
      );
  end component fifo_sync;

  component xcvr_rx_a10 is
    port (
      rx_analogreset          : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_analogreset
      rx_cal_busy             : out std_logic_vector(0 downto 0);  -- rx_cal_busy
      rx_cdr_refclk0          : in  std_logic                    := 'X';  -- clk
      rx_clkout               : out std_logic_vector(0 downto 0);  -- clk
      rx_coreclkin            : in  std_logic_vector(0 downto 0) := (others => 'X');  -- clk
      rx_digitalreset         : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_digitalreset
	  rx_set_locktodata       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_set_locktodata
	  rx_set_locktoref        : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_set_locktoref
      rx_is_lockedtodata      : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata
      rx_is_lockedtoref       : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref
      rx_parallel_data        : out std_logic_vector(39 downto 0);  -- rx_parallel_data
      rx_pma_clkslip          : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_pma_clkslip
      rx_serial_data          : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_serial_data
      unused_rx_parallel_data : out std_logic_vector(87 downto 0)  -- unused_rx_parallel_data
      );
  end component xcvr_rx_a10;

  component xcvr_tx_a10 is
    port (
      s0_write                : in  std_logic_vector(0 downto 0)   := (others => '0');  -- s0.write
      s0_read                 : in  std_logic_vector(0 downto 0)   := (others => '0');  --   .read
      s0_address              : in  std_logic_vector(9 downto 0)   := (others => '0');  --   .address
      s0_writedata            : in  std_logic_vector(31 downto 0)  := (others => '0');  --   .writedata
      s0_readdata             : out std_logic_vector(31 downto 0);  --   .readdata
      s0_waitrequest          : out std_logic_vector(0 downto 0);  --   .waitrequest
      s0_clk_clk              : in  std_logic_vector(0 downto 0)   := (others => '0');  -- s0_clk.clk
      s0_reset_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- s0_reset.reset
      tx_analogreset          : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- tx_analogreset
      tx_bonding_clocks       : in  std_logic_vector(5 downto 0)   := (others => 'X');  -- clk
      tx_cal_busy             : out std_logic_vector(0 downto 0);  -- tx_cal_busy
      tx_clkout               : out std_logic_vector(0 downto 0);  -- clk
      tx_coreclkin            : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- clk
      tx_digitalreset         : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- tx_digitalreset
      tx_parallel_data        : in  std_logic_vector(9 downto 0)  := (others => 'X');  -- tx_parallel_data
      tx_serial_data          : out std_logic_vector(0 downto 0);  -- tx_serial_data
      unused_tx_parallel_data : in  std_logic_vector(117 downto 0) := (others => 'X')  -- unused_tx_parallel_data
      );
  end component xcvr_tx_a10;

--  component xcvr_reset_control_a10 is
--    port (
--      clock              : in  std_logic                    := 'X';  -- clk
--      pll_locked         : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_locked
--      pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
--      pll_select         : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_select
--      reset              : in  std_logic                    := 'X';  -- reset
--      rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset
--      rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_cal_busy
--      rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset
--      rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_is_lockedtodata
--      rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready
--      tx_analogreset     : out std_logic_vector(0 downto 0);  -- tx_analogreset
--      tx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => 'X');  -- tx_cal_busy
--      tx_digitalreset    : out std_logic_vector(0 downto 0);  -- tx_digitalreset
--      tx_ready           : out std_logic_vector(0 downto 0)   -- tx_ready
--      );
--  end component xcvr_reset_control_a10;

  component xcvr_tx_reset_control_a10 is
    port (
      clock           : in  std_logic                    := 'X';  -- clk
      pll_locked      : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_locked
      pll_powerdown   : out std_logic_vector(0 downto 0);  -- pll_powerdown
      pll_select      : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_select
      reset           : in  std_logic                    := 'X';  -- reset
      tx_analogreset  : out std_logic_vector(0 downto 0);  -- tx_analogreset
      tx_cal_busy     : in  std_logic_vector(0 downto 0) := (others => 'X');  -- tx_cal_busy
      tx_digitalreset : out std_logic_vector(0 downto 0);  -- tx_digitalreset
      tx_ready        : out std_logic_vector(0 downto 0)   -- tx_ready
      );
  end component xcvr_tx_reset_control_a10;

  component xcvr_rx_reset_control_a10 is
    port (
      clock              : in  std_logic                    := 'X';  -- clk
      pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
      reset              : in  std_logic                    := 'X';  -- reset
      rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset
      rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_cal_busy
      rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset
      rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => 'X');  -- rx_is_lockedtodata
      rx_ready           : out std_logic_vector(0 downto 0)   -- rx_ready
      );
  end component xcvr_rx_reset_control_a10;

  component atx_pll_a10 is
    port (
      mcgb_rst          : in  std_logic                     := 'X';  -- mcgb_rst
      pll_cal_busy      : out std_logic;                      -- pll_cal_busy
      pll_locked        : out std_logic;                      -- pll_locked
      pll_powerdown     : in  std_logic                     := 'X';  -- pll_powerdown
      pll_refclk0       : in  std_logic                     := 'X';  -- clk
      s0_write          : in  std_logic                     := '0';  -- s0.write
      s0_read           : in  std_logic                     := '0';  -- .read
      s0_address        : in  std_logic_vector(9 downto 0)  := (others => '0');  -- .address
      s0_writedata      : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
      s0_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
      s0_waitrequest    : out std_logic;                      -- .waitrequest
      s0_clk_clk        : in  std_logic                     := '0';  -- s0_clk.clk
      s0_reset_reset    : in  std_logic                     := '0';  -- s0_reset.reset
      tx_bonding_clocks : out std_logic_vector(5 downto 0);   -- clk
      tx_serial_clk     : out std_logic                       -- clk
      );
  end component atx_pll_a10;

--============================================================================
--! Architecture begin
--============================================================================  
begin
  --  Static signal Assigments
  tied_to_ground_i     <= '0';
  tied_to_ground_vec_i <= (others => '0');
  tied_to_vcc_i        <= '1';
  tied_to_vcc_vec_i    <= (others => '1');

  -- Transceiver connections

  xcvr_rx_simplex : component xcvr_rx_a10
    port map (
      rx_analogreset          => rx_analogreset ,
      rx_cal_busy             => rx_cal_busy ,
      rx_cdr_refclk0          => rx_cdr_refclk0 ,
      rx_clkout               => rx_clkout ,
      rx_coreclkin            => rx_coreclkin ,
      rx_digitalreset         => rx_digitalreset ,
      rx_set_locktodata(0)    => rx_set_locktodata_i,
      rx_set_locktoref(0)     => rx_set_locktoref_i,
      rx_is_lockedtodata      => rx_is_lockedtodata ,
      rx_is_lockedtoref       => rx_is_lockedtoref ,
      rx_parallel_data        => rx_parallel_data ,
      rx_pma_clkslip          => rx_pma_clkslip ,
      rx_serial_data          => rx_serial_data ,
      unused_rx_parallel_data => unused_rx_parallel_data
      );          

  xcvr_tx_simplex : component xcvr_tx_a10
    port map (
      s0_clk_clk(0)           => txs0_clk ,
      s0_reset_reset(0)       => txs0_reset ,
      s0_waitrequest(0)       => s_txs0_waitreq ,
      s0_address              => txs0_address ,
      s0_write(0)             => txs0_write ,
      s0_writedata            => txs0_writedata ,
      s0_read(0)              => txs0_read ,
      s0_readdata             => txs0_readdata ,
      tx_analogreset          => tx_analogreset ,
      tx_bonding_clocks       => tx_bonding_clocks ,
      tx_cal_busy             => tx_cal_busy ,
      tx_clkout               => tx_clkout ,
      tx_coreclkin            => tx_coreclkin ,
      tx_digitalreset         => tx_digitalreset ,
      tx_parallel_data        => tx_parallel_data ,
      tx_serial_data          => tx_serial_data ,
      unused_tx_parallel_data => unused_tx_parallel_data
      );

  txs0_waitrequest   <= s_txs0_waitreq;
  txs0_readdatavalid <= txs0_read and not s_txs0_waitreq and not txs0_reset when rising_edge (txs0_clk);


  txusrclk          <= tx_clkout(0);
  rxusrclk          <= rx_clkout(0);
  rx_coreclkin      <= rx_clkout;
  tx_coreclkin      <= tx_clkout;
  clk_trxusr240_o    <= rxusrclk;

  -- reset for FIFO
  process(txusrclk, tx_ready(0))
  begin
    if(tx_ready(0) = '0') then
      reset_fifo_meta  <= '1';
      reset_fifo_r     <= '1';
    elsif (txusrclk'event and txusrclk = '1') then
      reset_fifo_meta  <= '0';
      reset_fifo_r     <= reset_fifo_meta;
    end if;
  end process;

  cmp_fifo_sync : fifo_sync
    generic map(
      g_PHASE_GOOD_LOCK    => 63,
      g_PHASE_NGOOD_UNLOCK => 3,
      g_DATA_WIDTH         => 10    --! data width
      )
    port map(
      clk_wr_i            => rxusrclk,
      data_wr_i           => tx_data_i,
      phase_good_o        => phase_good_o,
      clk_rd_i            => txusrclk,
      disable_phase_good_i => disable_phase_good_i,
      reset_i             => reset_fifo_r,
      data_rd_o           => tx_parallel_data
    );

  rx_cdr_refclk0    <= clk_rxref240_i;
  mgt_txpll_o       <= pll_locked(0);
  mgt_rxpll_o       <= rx_is_lockedtodata(0);
  rx_pma_clkslip(0) <= rx_slide_i;

  unused_tx_parallel_data <= tied_to_ground_vec_i(unused_tx_parallel_data'range);

  rx_parallel_data_nedger <= rx_parallel_data        when falling_edge(rxusrclk);
  rx_parallel_data_r      <= rx_parallel_data_nedger when rising_edge(rxusrclk);

  rx_data_o <= rx_parallel_data_r;

  rx_serial_data(0) <= rx_p_i;
  tx_p_o            <= tx_serial_data(0);

--  cmp_xcvr_reset_control_a10 : xcvr_reset_control_a10
--    port map(
--      clock              => clk_sys_i ,
--      pll_locked         => pll_locked ,
--      pll_powerdown      => pll_powerdown ,
--      pll_select         => pll_select ,
--      reset              => reset ,
--      rx_analogreset     => rx_analogreset ,
--      rx_cal_busy        => rx_cal_busy ,
--      rx_digitalreset    => rx_digitalreset ,
--      rx_is_lockedtodata => rx_is_lockedtodata ,
--      rx_ready           => rx_ready ,
--      tx_analogreset     => tx_analogreset ,
--      tx_cal_busy        => cal_busy ,
--      tx_digitalreset    => tx_digitalreset ,
--      tx_ready           => tx_ready
--      );

  cmp_xcvr_tx_reset_control_a10 : xcvr_tx_reset_control_a10
    port map(
      clock           => clk_sys_i ,
      pll_locked      => pll_locked ,
      pll_powerdown   => pll_powerdown ,
      pll_select      => pll_select ,
      reset           => tx_reset ,
      tx_analogreset  => tx_analogreset ,
      tx_cal_busy     => cal_busy ,
      tx_digitalreset => tx_digitalreset ,
      tx_ready        => tx_ready
      );

  cmp_xcvr_rx_reset_control_a10 : xcvr_rx_reset_control_a10
    port map(
      clock              => clk_sys_i ,
      pll_powerdown      => open ,
      reset              => reset ,
      rx_analogreset     => rx_analogreset ,
      rx_cal_busy        => rx_cal_busy ,
      rx_digitalreset    => rx_digitalreset ,
      rx_is_lockedtodata => rx_is_lockedtodata ,
      rx_ready           => rx_ready
      );

  reset    <= mgt_reset_i;
  tx_reset <= reset or (not tx_external_locked_i);

  pll_select  <= tied_to_ground_vec_i(pll_select'range);
  cal_busy(0) <= tx_cal_busy(0) or pll_cal_busy;

  cmp_atx_pll_a10 : atx_pll_a10
    port map(
      s0_clk_clk        => plls0_clk ,
      s0_reset_reset    => plls0_reset ,
      s0_waitrequest    => s_plls0_waitreq ,
      s0_address        => plls0_address ,
      s0_write          => plls0_write ,
      s0_writedata      => plls0_writedata ,
      s0_read           => plls0_read ,
      s0_readdata       => plls0_readdata ,
      mcgb_rst          => mcgb_rst ,
      pll_cal_busy      => pll_cal_busy ,
      pll_locked        => pll_locked(0) ,
      pll_powerdown     => pll_powerdown(0) ,
      pll_refclk0       => pll_refclk0 ,
      tx_bonding_clocks => tx_bonding_clocks ,
      tx_serial_clk     => open
      );

  plls0_waitrequest   <= s_plls0_waitreq;
  plls0_readdatavalid <= plls0_read and not s_plls0_waitreq and not plls0_reset when rising_edge (plls0_clk);

  pll_refclk0 <= clk_txref240_i;
  mcgb_rst    <= pll_powerdown(0);

  --============================================================================
  --! FSM Reset Done - * rising edge Synchronizers
  --===========================================================================
  process(rxusrclk, tx_ready(0))
  begin
    if(tx_ready(0) = '0') then
      gt0_txfsmresetdone_r  <= '0';
      gt0_txfsmresetdone_r2 <= '0';
    elsif (rxusrclk'event and rxusrclk = '1') then
      gt0_txfsmresetdone_r  <= tx_ready(0);
      gt0_txfsmresetdone_r2 <= gt0_txfsmresetdone_r;
    end if;
  end process;

  process(rxusrclk, rx_ready(0))
  begin
    if(rx_ready(0) = '0') then
      gt0_rxfsmresetdone_r  <= '0';
      gt0_rxfsmresetdone_r2 <= '0';
    elsif (rxusrclk'event and rxusrclk = '1') then
      gt0_rxfsmresetdone_r  <= rx_ready(0);
      gt0_rxfsmresetdone_r2 <= gt0_rxfsmresetdone_r;
    end if;
  end process;


  rxfsmrstdone_o <= gt0_rxfsmresetdone_r2;
  txfsmrstdone_o <= gt0_txfsmresetdone_r2;
  
end rtl;
