--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file fifo_sync.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Synchronous FIFO for mesochronous CDC (fifo_sync)
--
--! @brief Synchronous FIFO for mesochronous clock domain-crossing with a fixed-phase
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 16\01\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 16\01\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for fifo_sync
--==============================================================================
entity fifo_sync is
  generic(
    g_PHASE_GOOD_LOCK    : integer := 31;   --! number of consecutive good phase detected to lock phase_good_o output
    g_PHASE_NGOOD_UNLOCK : integer := 3;    --! number of consecutive bad phase detected to unlock phase_good_o output 
    g_DATA_WIDTH         : integer := 10    --! data width
    );
  port (
    clk_wr_i            : in  std_logic;                                 --! clock input domain write
    data_wr_i           : in  std_logic_vector(g_DATA_WIDTH-1 downto 0); --! data from domain write	
    phase_good_o        : out std_logic;                                 --! Phase relation between clocks is good	
    clk_rd_i            : in  std_logic;                                 --! clock input domain read	
	disable_phase_good_i: in  std_logic;                                 --! disable reset fifo with phase good	
    reset_i             : in  std_logic;                                 --! reset synchronous (synchronous to read pointer)	
    data_rd_o           : out std_logic_vector(g_DATA_WIDTH-1 downto 0) --! data to domain read
    );
end fifo_sync;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of fifo_sync is

  --! Constant declaration
  constant c_SIZE_FIFO : integer := 6;

  --! Signal declaration
  signal ptr_wr  : integer range 0 to c_SIZE_FIFO-1;
  signal flag_wr : std_logic_vector(c_SIZE_FIFO/2-2 downto 0);

  signal ptr_rd    : integer range 0 to c_SIZE_FIFO-1;
  signal flag_rd   : std_logic;
  signal flag_rd_r : std_logic;

  type t_FIFO_MEM is array (0 to c_SIZE_FIFO-1) of std_logic_vector(g_DATA_WIDTH-1 downto 0);
  signal fifo_mem : t_FIFO_MEM;
  
  -- FSM phase good locking to provide some hysteresis
  -- principle:
  -- HUNT            : received a correct phase_good                       -> GOING_SYNC
  --                   received a wrong phase_good                         -> HUNT
  -- GOING_SYNC      : received a consecutive number of correct phase_good -> SYNC
  --                   received a wrong phase_good                         -> HUNT   
  -- SYNC            : received a wrong phase_good                         -> GOING_HUNT
  -- GOING_HUNT      : received a consecutive number of wrong phase_good   -> HUNT
  --                   received a correct phase_good                       -> SYNC
  type   t_PHASE_GOOD_STATE is (HUNT, GOING_SYNC, SYNC, GOING_HUNT);
  signal phase_good_state : t_PHASE_GOOD_STATE;

  signal phase_good_locked   : std_logic;
  signal phase_good_locked_r : std_logic;

  signal phase_good_cntr     : integer range 0 to g_PHASE_GOOD_LOCK;
  signal phase_bad_cntr      : integer range 0 to g_PHASE_NGOOD_UNLOCK;

  signal phase_good_locked_wr_meta : std_logic;
  signal phase_good_locked_wr_r    : std_logic;

  signal disable_phase_good_meta : std_logic;
  signal disable_phase_good_r    : std_logic;
  
begin

  --============================================================================
  -- Process p_ptr_wr
  --! FIFO write process
  --! read: data_wr_i\n
  --! write: fifo_mem\n
  --! r/w: ptr_wr, flag_wr\n
  --============================================================================     
  p_ptr_wr : process(clk_wr_i)
  begin
    if(clk_wr_i'event and clk_wr_i = '1') then
      if(ptr_wr >= c_SIZE_FIFO-1) then
        ptr_wr     <= 0;
        flag_wr(0) <= '1';
      else
        ptr_wr     <= ptr_wr + 1;
        flag_wr(0) <= '0';		
      end if;
      flag_wr(flag_wr'left downto 1) <= flag_wr(flag_wr'left-1 downto 0);
      fifo_mem(ptr_wr) <= data_wr_i;
    end if;
  end process p_ptr_wr;

  --============================================================================
  -- Process p_ptr_rd
  --! FIFO read process
  --! read: fifo_mem, phase_good_locked_r, flag_rd\n
  --! write: data_rd_o\n
  --! r/w: ptr_rd \n
  --============================================================================
  flag_rd   <= flag_wr(flag_wr'left) when rising_edge(clk_rd_i);
  p_ptr_rd : process(clk_rd_i)
  begin
    if(clk_rd_i'event and clk_rd_i = '1') then
      if(ptr_rd >= c_SIZE_FIFO-1 or (phase_good_locked_r = '0' and flag_rd = '1' and disable_phase_good_r='0')) then
        ptr_rd     <= 0;
      else
        ptr_rd     <= ptr_rd + 1;
      end if;
      data_rd_o <= fifo_mem(ptr_rd);
    end if;
  end process p_ptr_rd;

  flag_rd_r <= flag_rd when rising_edge(clk_rd_i);
  disable_phase_good_meta <= disable_phase_good_i when rising_edge(clk_rd_i);
  disable_phase_good_r    <= disable_phase_good_meta when rising_edge(clk_rd_i);

  --============================================================================
  -- Process p_phase_good_fsm
  --! FSM for phase_good locking procedure with hysteresis
  --! read: ptr_rd, flag_rd_r\n
  --! write: -\n
  --! r/w: phase_good_state, phase_good_cntr, phase_bad_cntr\n
  --============================================================================  
  p_phase_good_fsm : process(clk_rd_i)
  begin
    if(clk_rd_i'event and clk_rd_i = '1') then
      if(reset_i = '1') then
        phase_good_state <= HUNT;
        phase_good_cntr  <= 0;
        phase_bad_cntr   <= 0;
      else
        case phase_good_state is
          when HUNT =>
            if(ptr_rd = 0 and flag_rd_r = '1') then -- phase_good
              phase_good_state <= GOING_SYNC;
            end if;
          when GOING_SYNC =>
            if((ptr_rd /= 0 and flag_rd_r = '1') or (ptr_rd=0 and flag_rd_r='0')) then -- not phase_good
			  phase_good_cntr  <= 0;
              phase_good_state <= HUNT;
			elsif((ptr_rd=0 and flag_rd_r='1')) then -- phase_good
              if(phase_good_cntr >= g_PHASE_GOOD_LOCK-1) then
                phase_good_state <= SYNC;
			    phase_good_cntr <= 0;
              else
			    phase_good_cntr <= phase_good_cntr + 1;
              end if;			  
			end if;
          when SYNC =>
            if((ptr_rd /= 0 and flag_rd_r = '1') or (ptr_rd=0 and flag_rd_r='0')) then -- not phase_good
              phase_good_state <= GOING_HUNT;
            end if;
          when GOING_HUNT =>
            if((ptr_rd /= 0 and flag_rd_r = '1') or (ptr_rd=0 and flag_rd_r='0')) then -- not phase_good
              if(phase_bad_cntr >= g_PHASE_NGOOD_UNLOCK-1) then
                phase_good_state <= HUNT;
			    phase_bad_cntr  <= 0;
              else
			    phase_bad_cntr <= phase_bad_cntr + 1;
              end if;	
			elsif((ptr_rd=0 and flag_rd_r='1')) then -- phase_good
			  phase_bad_cntr  <= 0;
              phase_good_state <= SYNC;		  
			end if;
          when others => phase_good_state <= HUNT;
        end case;
      end if;
    end if;
  end process p_phase_good_fsm;

  phase_good_locked   <= '1' when (phase_good_state = SYNC or phase_good_state = GOING_HUNT) else '0';
  phase_good_locked_r <= phase_good_locked when rising_edge(clk_rd_i);
  
  phase_good_locked_wr_meta <= phase_good_locked_r when rising_edge(clk_wr_i);
  phase_good_locked_wr_r    <= phase_good_locked_wr_meta when rising_edge(clk_wr_i);
  phase_good_o              <= phase_good_locked_wr_r;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

