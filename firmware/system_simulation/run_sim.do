vlib work

## Compile TTC-PON core files
## Common files
vcom -explicit  -93 "../cores/common_core/common_package.vhd"
vcom -explicit  -93 "../cores/common_core/protocol_package.vhd"
vcom -explicit  -93 "../cores/common_core/prbs_data_check.vhd" 
vcom -explicit  -93 "../cores/common_core/prbs_data_gen.vhd"
vcom -explicit  -93 "../cores/common_core/poly_div.vhd"
vcom -explicit  -93 "../cores/common_core/bbert_top.vhd"
vcom -explicit  -93 "../cores/common_core/cdc_mailbox.vhd"
vcom -explicit  -93 "../cores/common_core/CountOnes.vhd"
vcom -explicit  -93 "../cores/common_core/fec_package.vhd" 
vcom -explicit  -93 "../cores/common_core/i2c_interface.vhd"
vcom -explicit  -93 "../cores/common_core/i2c_rssi_ctrl.vhd"

## OLT core
vcom -explicit  -93 "../cores/olt_core/pon_olt_package_static.vhd"
vcom -explicit  -93 "../cores/olt_core/pon_olt_package_modifiable.vhd"

vcom -explicit  -93 "../cores/olt_core/ctrl/pon_down_sc_tx.vhd"
vcom -explicit  -93 "../cores/olt_core/ctrl/pon_up_sc_rx.vhd"
vcom -explicit  -93 "../cores/olt_core/ctrl/olt_control.vhd"
vcom -explicit  -93 "../cores/olt_core/ctrl/rx_fine_phase_measurement.vhd"

vcom -explicit  -93 "../cores/olt_core/phy_ctrl/olt_phy_control.vhd"

vcom -explicit  -93 "../cores/olt_core/rx/rx_frame_lock.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/decoder8b10b.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/8b10b_dec_wrapper.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/word_aligner.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/downsample.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/frame_data_mux.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/blind_osrx_wrapper.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/olt_payload_extract.vhd"
vcom -explicit  -93 "../cores/olt_core/rx/olt_rx.vhd"

vcom -explicit  -93 "../cores/olt_core/tx/enc_bch120_106.vhd"
vcom -explicit  -93 "../cores/olt_core/tx/scrambling.vhd"
vcom -explicit  -93 "../cores/olt_core/tx/olt_tx.vhd"

vcom -explicit  -93 "../cores/olt_core/olt_core.vhd"

vcom -explicit  -93 "../olt/model/mgt_wrapper/olt_mgt_wrapper.vhd"
vcom -explicit  -93 "../olt/model/pon_olt_model_wrapper.vhd"

## ONU core
vcom -explicit  -93 "../cores/onu_core/pon_onu_package_static.vhd"
vcom -explicit  -93 "../cores/onu_core/pon_onu_package_modifiable.vhd"

vcom -explicit  -93 "../cores/onu_core/ctrl/onu_tdm_timer.vhd"
vcom -explicit  -93 "../cores/onu_core/ctrl/pon_down_sc_rx.vhd"
vcom -explicit  -93 "../cores/onu_core/ctrl/pon_up_sc_tx.vhd"
vcom -explicit  -93 "../cores/onu_core/ctrl/onu_control.vhd"

vcom -explicit  -93 "../cores/onu_core/phy_ctrl/onu_phy_control.vhd"

vcom -explicit  -93 "../cores/onu_core/rx/dec_bch120_106.vhd"
vcom -explicit  -93 "../cores/onu_core/rx/descrambling.vhd"
vcom -explicit  -93 "../cores/onu_core/rx/onu_word_align.vhd"
vcom -explicit  -93 "../cores/onu_core/rx/new_barrel.vhd"
vcom -explicit  -93 "../cores/onu_core/rx/onu_rx.vhd"

vcom -explicit  -93 "../cores/onu_core/tx/8b10b_enc_modif.vhd"
vcom -explicit  -93 "../cores/onu_core/tx/8b10b_enc_wrapper.vhd"
vcom -explicit  -93 "../cores/onu_core/tx/barrel_shifter_10b.vhd"
vcom -explicit  -93 "../cores/onu_core/tx/onu_frame_gen.vhd"
vcom -explicit  -93 "../cores/onu_core/tx/onu_tx_interface.vhd"
vcom -explicit  -93 "../cores/onu_core/tx/onu_tx.vhd"

vcom -explicit  -93 "../cores/onu_core/onu_core.vhd"

vcom -explicit  -93 "../onu/model/mgt_wrapper/onu_mgt_wrapper.vhd"
vcom -explicit  -93 "../onu/model/clk40_wrapper/clkdiv_phase_ctrl.vhd"
vcom -explicit  -93 "../onu/model/pon_onu_model_wrapper.vhd"

## OLT user logic
vcom -explicit  -93 "../cores/olt_user_core/olt_data_gen.vhd"
vcom -explicit  -93 "../cores/olt_user_core/olt_user_logic_exdsg.vhd"

## ONU user logic
vcom -explicit  -93 "../cores/onu_user_core/onu_data_gen.vhd"
vcom -explicit  -93 "../cores/onu_user_core/onu_user_logic_exdsg.vhd"

## Test bench file
vcom -explicit  -93 "./tb_pon_system.vhd"

# Start simulation
vsim -gui -t ps -novopt work.tb_pon_system



view wave
view structure
view signals
do "./wave_config.do"
log -r *
run 300 us

