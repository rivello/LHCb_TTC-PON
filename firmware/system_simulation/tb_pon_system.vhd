--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file tb_pon_system.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_olt_package_static.all;
use work.pon_olt_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: TTC-PON system test-bench (tb_pon_system)
--
--! @brief TTC-PON system test-bench to help user system-level simulation
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 07/09/2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\09\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for tb_pon_system
--============================================================================
entity tb_pon_system is
end entity tb_pon_system;

--============================================================================
--! Architecture declaration for tb_pon_system
--============================================================================
architecture tb of tb_pon_system is

  --! Functions
  function fcn_or_reduce(arg : std_logic_vector) return std_logic is
    variable result : std_logic;
  begin
    result := '0';
    for i in arg'range loop
      result := result or arg(i);
    end loop;
    return result;
  end;
  
  --! Constants
  constant c_UI_PERIOD_DOWNSTREAM : time := 0.100 ns;
  constant c_OLT_CLK_REF_PERIOD   : time := c_UI_PERIOD_DOWNSTREAM * 40;
  constant c_OLT_CLK_SYS_PERIOD   : time := 10.0 ns;
  
  constant c_NUMBER_ONU              : integer range 1 to 64 := 3;
  constant c_ONU_CLK_SYS_PERIOD      : time := 10.0 ns;
  constant c_ONU_CLK_RX_REF_PERIOD   : time := c_UI_PERIOD_DOWNSTREAM * 40; 
  constant c_ONU_CLK_TX_REF_PERIOD   : time := c_UI_PERIOD_DOWNSTREAM * 4 * 10; 

  constant c_ONU_EXTERNAL_PLL_DELAY  : time := 2.0 ns; -- used to ensure phase_good = 1
                                                       -- In a real PON system, the ONU has a local initialization procedure
                                                       -- This procedure is further explained in the TTC-PON Firmware and Software User Guide													   

  --! Signal declaration
  ------- OLT signals (begin)
  signal olt_clk_manager_i     : std_logic := '0';
  signal olt_ctrl_reg_i        : std_logic_vector(31 downto 0)  := (others => '0');
  signal olt_ctrl_reg_strobe_i : std_logic_vector(127 downto 0) := (others => '0');	
  signal olt_stat_reg_o        : t_regbank32b(127 downto 0);
  signal olt_clk_sys_i         : std_logic := '0';
  signal olt_core_reset_i      : std_logic := '0';
  signal olt_clk_ref240_i      : std_logic := '0';
  signal olt_interrupt_o       : std_logic;
  signal olt_status_sticky_o   : std_logic_vector(30 downto 0);
  signal olt_clk_trxusr240_o   : std_logic;
  signal olt_tx_data_i         : std_logic_vector(199 downto 0) := (others => '0');
  signal olt_tx_data_strobe_i  : std_logic := '0';
  signal olt_rx_locked_o       : std_logic;
  signal olt_rx_data_o         : std_logic_vector(55 downto 0);
  signal olt_rx_onu_addr_o     : std_logic_vector(7 downto 0);
  signal olt_rx_data_strobe_o  : std_logic;
  signal olt_rx_data_error_o   : std_logic;
  signal olt_mgt_tx_ready_o    : std_logic;
  signal olt_mgt_rx_ready_o    : std_logic;
  signal olt_mgt_pll_lock_o    : std_logic;
  signal olt_tx_p_o            : std_logic;
  signal olt_tx_n_o            : std_logic;         --! not used simulation
  signal olt_rx_p_i            : std_logic := '0';
  signal olt_rx_n_i            : std_logic := '0';  --! not used simulation
  signal olt_sfp_rx_sd_i       : std_logic := '0';  --! not implemented high_level model
  signal olt_sfp_tx_fault_i    : std_logic := '0';  --! not implemented high_level model
  signal olt_sfp_mod_abs_i     : std_logic := '0';  --! not implemented high_level model
  signal olt_sfp_rx_reset_o    : std_logic;
  signal olt_sfp_rssi_tri_o    : std_logic;
  signal olt_sfp_tx_dis_o      : std_logic;
  signal olt_i2c_sda_io        : std_logic;         --! not implemented high_level model
  signal olt_i2c_scl_io        : std_logic;         --! not implemented high_level model
  ------- OLT signals (end)
  
  ------- OLT user logic signals (begin)
  signal olt_user_logic_ctrl_reg : t_regbank32b(127 downto 0) := (others => (others => '0'));
  signal olt_user_logic_stat_reg : t_regbank32b(127 downto 0);
  signal olt_user_tx_match_flag    : std_logic;
  ------- OLT user logic signals (end)  
  
  ------- ONU signals (begin)
  -- shared  
  signal onu_clk_manager_i         : std_logic;	
  signal onu_clk_sys_i             : std_logic;

  type t_ONU_SIGNALS is record
    onu_stat_reg_o            : t_regbank32b(127 downto 0);
    onu_ctrl_reg_i            : std_logic_vector(31 downto 0);
    onu_ctrl_reg_strobe_i     : std_logic_vector(127 downto 0);           
    onu_core_reset_i          : std_logic;
    onu_clk_rxref240_i        : std_logic;  --! MGT Rx reference clock - 240MHz      
    onu_clk_txref240_i        : std_logic;  --! MGT Tx reference clock - 240MHz (has to be derived from downstream recovered clock - synchronization required for TTC-PON)        
    onu_clk_rx40_o            : std_logic;
    onu_rx40_locked_o         : std_logic;
    onu_clk_trxusr240_o        : std_logic;  --! User_TRx 240MHz clock
    onu_rx_data_strobe_o      : std_logic;
    onu_rx_data_frame_o       : std_logic_vector(199 downto 0);
    onu_tx_data_strobe_i      : std_logic;
    onu_tx_data_ready_o       : std_logic;
    onu_tx_data_i             : std_logic_vector(55 downto 0);
    onu_address_i             : std_logic_vector(7 downto 0) ;  --! ONU address
    onu_mgt_phase_good_o      : std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning       
    onu_rx_locked_o           : std_logic;  --! downstream header aligned
    onu_operational_o         : std_logic;  --! given by onu once system init (calibration) is done
    onu_rx_serror_corrected_o : std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_o : std_logic;  --! a double-error was corrected by the FEC        
    onu_mgt_tx_ready_o        : std_logic;
    onu_mgt_rx_ready_o        : std_logic;
    onu_mgt_txpll_lock_o      : std_logic;
    onu_mgt_rxpll_lock_o      : std_logic;
    onu_interrupt_o           : std_logic;
    onu_status_sticky_o       : std_logic_vector(30 downto 0);
    onu_tx_p_o                : std_logic;
    onu_tx_n_o                : std_logic;
    onu_rx_p_i                : std_logic;
    onu_rx_n_i                : std_logic;
    onu_sfp_rx_los_i          : std_logic;
    onu_sfp_tx_fault_i        : std_logic;
    onu_sfp_mod_abs_i         : std_logic;
    onu_sfp_tx_sd_i           : std_logic;
    onu_sfp_pdown_o           : std_logic;
    onu_sfp_tx_dis_o          : std_logic;
    onu_i2c_sda_io            : std_logic;
    onu_i2c_scl_io            : std_logic;
  end record t_ONU_SIGNALS;

  type t_ONU_ARRAY is array (natural range <>) of t_ONU_SIGNALS;

  signal onu_array_signals : t_ONU_ARRAY(1 to c_NUMBER_ONU);
  ------- ONU signals (end)

  
  ------- ONU user logic signals (begin)
  type t_ONU_USER_SIGNALS is record
    rx_match_flag            :  std_logic;
    onu_status               :  std_logic_vector(7 downto 0);
    onu_user_logic_ctrl_reg  :  t_regbank32b(127 downto 0);
    onu_user_logic_stat_reg  :  t_regbank32b(127 downto 0);
  end record t_ONU_USER_SIGNALS;

  type t_ONU_USER_ARRAY is array (natural range <>) of t_ONU_USER_SIGNALS;

  signal onu_user_array_signals : t_ONU_USER_ARRAY(1 to c_NUMBER_ONU);  
  ------- ONU user logic signals (end)

  ------- ONU upstream channel (begin)
  signal   onu_tx_channel : std_logic_vector(1 to c_NUMBER_ONU);
  ------- ONU upstream channel (end)

  -- signal to help user going through the different stages of the simulation  
  signal tb_STATE_STRING : string(1 to 20) := "IDLE                ";

  --! Component declaration    
  component pon_olt_model_wrapper is
    generic (
      g_UI_PERIOD : time := 0.100 ns
      );
    port (
      -- Control interface
      olt_clk_manager_i     : in  std_logic;
      olt_ctrl_reg_i        : in  std_logic_vector(31 downto 0);
      olt_ctrl_reg_strobe_i : in  std_logic_vector(127 downto 0);	
      olt_stat_reg_o        : out t_regbank32b(127 downto 0);
  
      -- Sys clk / reset
      olt_clk_sys_i    : in std_logic;
      olt_core_reset_i : in std_logic;
  
      -- mgt 240MHz ref clk
      olt_clk_ref240_i : in std_logic;
  
      -- Soft-configurable interrupt output
      olt_interrupt_o     : out std_logic;
      olt_status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------
  
      -- data in/out
      olt_clk_trxusr240_o   : out std_logic;
      olt_tx_data_i        : in  std_logic_vector(199 downto 0);
      olt_tx_data_strobe_i : in  std_logic;
  
      olt_rx_locked_o      : out std_logic;	  
      olt_rx_data_o        : out std_logic_vector(55 downto 0);
      olt_rx_onu_addr_o    : out std_logic_vector(7 downto 0);
      olt_rx_data_strobe_o : out std_logic;
      olt_rx_data_error_o  : out std_logic;
  
      -- status
      olt_mgt_tx_ready_o : out std_logic;
      olt_mgt_rx_ready_o : out std_logic;
      olt_mgt_pll_lock_o : out std_logic;
  
      -- serial
      olt_tx_p_o         : out std_logic;
      olt_tx_n_o         : out std_logic;
      olt_rx_p_i         : in  std_logic;
      olt_rx_n_i         : in  std_logic;
      olt_sfp_rx_sd_i    : in  std_logic;
      olt_sfp_tx_fault_i : in  std_logic;
      olt_sfp_mod_abs_i  : in  std_logic;
      olt_sfp_rx_reset_o : out std_logic;
      olt_sfp_rssi_tri_o : out std_logic;
      olt_sfp_tx_dis_o   : out std_logic;
  
      -- i2c interfacing --
      olt_i2c_sda_io : inout std_logic; --! not implemented in high_level model
      olt_i2c_scl_io : inout std_logic  --! not implemented in high_level model
      -------------------------             
      );
  end component pon_olt_model_wrapper;

  component olt_user_logic_exdsg is
    port(
      -- core connections
      olt_clk_sys_i        : in  std_logic;
      olt_core_reset_o     : out std_logic;
      olt_interrupt_i      : in  std_logic;
      olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
      olt_clk_trxusr240_i   : in  std_logic;
      olt_tx_data_o        : out std_logic_vector(199 downto 0);
      olt_tx_data_strobe_o : out std_logic;
      olt_rx_locked_i	   : in  std_logic;	  
      olt_rx_data_i        : in  std_logic_vector(55 downto 0);
      olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
      olt_rx_data_strobe_i : in  std_logic;
      olt_rx_data_error_i  : in  std_logic;
      olt_mgt_pll_lock_i   : in  std_logic;
      olt_mgt_tx_ready_i   : in  std_logic;
      olt_mgt_rx_ready_i   : in  std_logic;
  
      -- Control --
      s_olt_user_axi_aclk : in  std_logic;
      ctrl_reg_i          : in  t_regbank32b(127 downto 0);
      stat_reg_o          : out t_regbank32b(127 downto 0);
      ---------------------------------
  
      -- Others ----------
      tx_match_flag_o : out std_logic
      ----------------------------------

      );
  end component olt_user_logic_exdsg;

  component pon_onu_model_wrapper is
    generic(
      g_UI_DOWN_PERIOD : time := 0.100 ns --! UI_PERIOD: this generic should be EXACTLY equal to the period of clk_rxref240_i/40
    );  
    port (
      -- Control interface
      onu_clk_manager_i     : in  std_logic;	
      onu_stat_reg_o        : out t_regbank32b(127 downto 0);
      onu_ctrl_reg_i        : in  std_logic_vector(31 downto 0);
      onu_ctrl_reg_strobe_i : in  std_logic_vector(127 downto 0);
  
      -- Clk sys/reset              
      onu_clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      onu_core_reset_i : in std_logic;
  
      -- MGT ref clocks
      onu_clk_rxref240_i : in std_logic;  --! MGT Rx reference clock - 240MHz      
      onu_clk_txref240_i : in std_logic;  --! MGT Tx reference clock - 120MHz (has to be derived from downstream recovered clock - synchronization required for TTC-PON)        
      onu_tx_external_locked_i : in std_logic;
      -- Recovered downstream 40MHz clock
      onu_clk_rx40_o    : out std_logic;
      onu_rx40_locked_o : out std_logic;
  
      -- Data in/out
      onu_clk_trxusr240_o   : out std_logic;  --! User_TRx 240MHz clock
      onu_rx_data_strobe_o : out std_logic;
      onu_rx_data_frame_o  : out std_logic_vector(199 downto 0);
      onu_tx_data_strobe_i : in  std_logic;
      onu_tx_data_ready_o  : out std_logic;
      onu_tx_data_i        : in  std_logic_vector(55 downto 0);
      onu_address_i        : in  std_logic_vector(7 downto 0);  --! ONU address
  
      -- Status
      onu_mgt_phase_good_o      : out std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning       
      onu_rx_locked_o           : out std_logic;  --! downstream header aligned
      onu_operational_o         : out std_logic;  --! given by onu once system init (calibration) is done
      onu_rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
      onu_rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC        
      onu_mgt_tx_ready_o        : out std_logic;
      onu_mgt_rx_ready_o        : out std_logic;
      onu_mgt_txpll_lock_o      : out std_logic;
      onu_mgt_rxpll_lock_o      : out std_logic;
  
      -- Soft-configurable interrupt output
      onu_interrupt_o     : out std_logic;
      onu_status_sticky_o : out std_logic_vector(30 downto 0);
      ------------------------
  
      -- serial
      onu_tx_p_o : out std_logic;
      onu_tx_n_o : out std_logic;
      onu_rx_p_i : in  std_logic;
      onu_rx_n_i : in  std_logic;
  
      onu_sfp_rx_los_i   : in  std_logic;
      onu_sfp_tx_fault_i : in  std_logic;
      onu_sfp_mod_abs_i  : in  std_logic;
      onu_sfp_tx_sd_i    : in  std_logic;
      onu_sfp_pdown_o    : out std_logic;
      onu_sfp_tx_dis_o   : out std_logic;
  
      --i2c interface
      onu_i2c_sda_io : inout std_logic;
      onu_i2c_scl_io : inout std_logic
      -------------------------
  
      );
  end component pon_onu_model_wrapper;

  component onu_user_logic_exdsg is
    port(
      -- Core connection --
      onu_clk_sys_i     : in  std_logic;
      onu_core_reset_o  : out std_logic;
      onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
      onu_rx40_locked_i : in  std_logic;
  
      onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
      onu_rx_data_strobe_i : in std_logic;
      onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
  
      onu_tx_data_ready_i  : in  std_logic;
      onu_tx_data_strobe_o : out std_logic;
      onu_tx_data_o        : out std_logic_vector(55 downto 0);
  
      onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address
      onu_mgt_phase_good_i      : in  std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning
      onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
      onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
      onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
      onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
      onu_interrupt_i           : in  std_logic;
      onu_sticky_status_i       : in  std_logic_vector(30 downto 0);
  
      onu_mgt_tx_ready_i   : in std_logic;
      onu_mgt_rx_ready_i   : in std_logic;
      onu_mgt_txpll_lock_i : in std_logic;
      onu_mgt_rxpll_lock_i : in std_logic;
      ---------------------
  
      -- Control AXI --
      s_onu_user_aclk : in  std_logic;
      ctrl_reg_i      : in  t_regbank32b(127 downto 0);
      stat_reg_o      : out t_regbank32b(127 downto 0);
      ---------------------------------
  
      -- Others --
      rx_match_flag_o : out std_logic;
  
      onu_status_o : out std_logic_vector(7 downto 0)
      --------------------------------
      );
  end component onu_user_logic_exdsg;

--============================================================================
-- architecture begin
--============================================================================
begin
  --! OLT model wrapper instantiation
  cmp_pon_olt_model_wrapper : pon_olt_model_wrapper
    generic map(
      g_UI_PERIOD => c_UI_PERIOD_DOWNSTREAM
      )
    port map(
      -- Control interface
      olt_clk_manager_i     => olt_clk_manager_i,     
      olt_ctrl_reg_i        => olt_ctrl_reg_i,        
      olt_ctrl_reg_strobe_i => olt_ctrl_reg_strobe_i, 	
      olt_stat_reg_o        => olt_stat_reg_o,  
  
      -- Sys clk / reset
      olt_clk_sys_i         => olt_clk_sys_i,   
      olt_core_reset_i      => olt_core_reset_i,
  
      -- mgt 240MHz ref clk
      olt_clk_ref240_i      => olt_clk_ref240_i,
  
      -- Soft-configurable interrupt output
      olt_interrupt_o       => olt_interrupt_o,    
      olt_status_sticky_o   => olt_status_sticky_o,
      -------------------------
  
      -- data in/out
      olt_clk_trxusr240_o   => olt_clk_trxusr240_o   ,  
      olt_tx_data_i        => olt_tx_data_i        , 
      olt_tx_data_strobe_i => olt_tx_data_strobe_i ,

      olt_rx_locked_o      => olt_rx_locked_o      ,
      olt_rx_data_o        => olt_rx_data_o        ,
      olt_rx_onu_addr_o    => olt_rx_onu_addr_o    ,
      olt_rx_data_strobe_o => olt_rx_data_strobe_o ,
      olt_rx_data_error_o  => olt_rx_data_error_o  ,
  
      -- status
      olt_mgt_tx_ready_o   => olt_mgt_tx_ready_o, 
      olt_mgt_rx_ready_o   => olt_mgt_rx_ready_o, 
      olt_mgt_pll_lock_o   => olt_mgt_pll_lock_o, 
  
      -- serial
      olt_tx_p_o           => olt_tx_p_o,        
      olt_tx_n_o           => olt_tx_n_o,        
      olt_rx_p_i           => olt_rx_p_i,        
      olt_rx_n_i           => olt_rx_n_i,        
      olt_sfp_rx_sd_i      => olt_sfp_rx_sd_i,   
      olt_sfp_tx_fault_i   => olt_sfp_tx_fault_i,
      olt_sfp_mod_abs_i    => olt_sfp_mod_abs_i, 
      olt_sfp_rx_reset_o   => olt_sfp_rx_reset_o,
      olt_sfp_rssi_tri_o   => olt_sfp_rssi_tri_o,
      olt_sfp_tx_dis_o     => olt_sfp_tx_dis_o,  
  
      -- i2c interfacing --
      olt_i2c_sda_io       => olt_i2c_sda_io,
      olt_i2c_scl_io       => olt_i2c_scl_io
      -------------------------             
      );

  cmp_olt_user_logic_exdsg : olt_user_logic_exdsg
    port map(
      -- core connections
      olt_clk_sys_i        => olt_clk_sys_i        ,
      olt_core_reset_o     => olt_core_reset_i     ,
      olt_interrupt_i      => olt_interrupt_o      ,
      olt_status_sticky_i  => olt_status_sticky_o  ,
      olt_clk_trxusr240_i   => olt_clk_trxusr240_o   ,
      olt_tx_data_o        => olt_tx_data_i        ,
      olt_tx_data_strobe_o => olt_tx_data_strobe_i ,
      olt_rx_locked_i	   => olt_rx_locked_o      ,
      olt_rx_data_i        => olt_rx_data_o        ,
      olt_rx_onu_addr_i    => olt_rx_onu_addr_o    ,
      olt_rx_data_strobe_i => olt_rx_data_strobe_o ,
      olt_rx_data_error_i  => olt_rx_data_error_o  ,
      olt_mgt_pll_lock_i   => olt_mgt_pll_lock_o   ,
      olt_mgt_tx_ready_i   => olt_mgt_tx_ready_o   ,
      olt_mgt_rx_ready_i   => olt_mgt_rx_ready_o   ,
  
      -- Control --
      s_olt_user_axi_aclk  => olt_clk_manager_i    ,
      ctrl_reg_i           => olt_user_logic_ctrl_reg,
      stat_reg_o           => olt_user_logic_stat_reg,
      ---------------------------------
  
      -- Others ----------
      tx_match_flag_o     => olt_user_tx_match_flag
      ----------------------------------

      );


    --! ONU model wrapper instantiation
    gen_onu_structural: for i in 1 to c_NUMBER_ONU generate

      cmp_pon_onu_model_wrapper : pon_onu_model_wrapper
        generic map(
          g_UI_DOWN_PERIOD => c_UI_PERIOD_DOWNSTREAM
        )
        port map(
          -- Control interface
          onu_clk_manager_i     => onu_clk_manager_i                         ,
          onu_stat_reg_o        => onu_array_signals(i).onu_stat_reg_o       ,
          onu_ctrl_reg_i        => onu_array_signals(i).onu_ctrl_reg_i       ,
          onu_ctrl_reg_strobe_i => onu_array_signals(i).onu_ctrl_reg_strobe_i,
      
          -- Clk sys/reset              
          onu_clk_sys_i         => onu_clk_sys_i                        ,   
          onu_core_reset_i      => onu_array_signals(i).onu_core_reset_i, 
      
          -- MGT ref clocks
          onu_clk_rxref240_i    => onu_array_signals(i).onu_clk_rxref240_i ,
          onu_clk_txref240_i    => onu_array_signals(i).onu_clk_txref240_i ,       
          onu_tx_external_locked_i => onu_array_signals(i).onu_rx_locked_o,

          -- Recovered downstream 40MHz clock
          onu_clk_rx40_o        => onu_array_signals(i).onu_clk_rx40_o   ,
          onu_rx40_locked_o     => onu_array_signals(i).onu_rx40_locked_o,
      
          -- Data in/out
          onu_clk_trxusr240_o    => onu_array_signals(i).onu_clk_trxusr240_o   , 
          onu_rx_data_strobe_o  => onu_array_signals(i).onu_rx_data_strobe_o ,
          onu_rx_data_frame_o   => onu_array_signals(i).onu_rx_data_frame_o  ,
          onu_tx_data_strobe_i  => onu_array_signals(i).onu_tx_data_strobe_i, 
          onu_tx_data_ready_o   => onu_array_signals(i).onu_tx_data_ready_o , 
          onu_tx_data_i         => onu_array_signals(i).onu_tx_data_i       , 
          onu_address_i         => onu_array_signals(i).onu_address_i       , 
      
          -- Status
          onu_mgt_phase_good_o      => onu_array_signals(i).onu_mgt_phase_good_o      ,                
          onu_rx_locked_o           => onu_array_signals(i).onu_rx_locked_o           ,         
          onu_operational_o         => onu_array_signals(i).onu_operational_o         ,        
          onu_rx_serror_corrected_o => onu_array_signals(i).onu_rx_serror_corrected_o ,  
          onu_rx_derror_corrected_o => onu_array_signals(i).onu_rx_derror_corrected_o ,
          onu_mgt_tx_ready_o        => onu_array_signals(i).onu_mgt_tx_ready_o        ,
          onu_mgt_rx_ready_o        => onu_array_signals(i).onu_mgt_rx_ready_o        ,
          onu_mgt_txpll_lock_o      => onu_array_signals(i).onu_mgt_txpll_lock_o      ,
          onu_mgt_rxpll_lock_o      => onu_array_signals(i).onu_mgt_rxpll_lock_o      ,
      
          -- Soft-configurable interrupt output
          onu_interrupt_o     => onu_array_signals(i).onu_interrupt_o    ,
          onu_status_sticky_o => onu_array_signals(i).onu_status_sticky_o,
          ------------------------
      
          -- serial
          onu_tx_p_o         => onu_array_signals(i).onu_tx_p_o         ,
          onu_tx_n_o         => onu_array_signals(i).onu_tx_n_o         ,
          onu_rx_p_i         => onu_array_signals(i).onu_rx_p_i         ,
          onu_rx_n_i         => onu_array_signals(i).onu_rx_n_i         ,
      
          onu_sfp_rx_los_i   => onu_array_signals(i).onu_sfp_rx_los_i   , 
          onu_sfp_tx_fault_i => onu_array_signals(i).onu_sfp_tx_fault_i , 
          onu_sfp_mod_abs_i  => onu_array_signals(i).onu_sfp_mod_abs_i  , 
          onu_sfp_tx_sd_i    => onu_array_signals(i).onu_sfp_tx_sd_i    ,
          onu_sfp_pdown_o    => onu_array_signals(i).onu_sfp_pdown_o    ,
          onu_sfp_tx_dis_o   => onu_array_signals(i).onu_sfp_tx_dis_o   ,
      
          --i2c interface
          onu_i2c_sda_io     => onu_array_signals(i).onu_i2c_sda_io,     
          onu_i2c_scl_io     => onu_array_signals(i).onu_i2c_scl_io     
          -------------------------
          );

      cmp_onu_user_logic_exdsg : onu_user_logic_exdsg
        port map(
          -- Core connection --
          onu_clk_sys_i             => onu_clk_sys_i                          ,  
          onu_core_reset_o          => onu_array_signals(i).onu_core_reset_i  ,  
          onu_clk_rx40_i            => onu_array_signals(i).onu_clk_rx40_o    ,  
          onu_rx40_locked_i         => onu_array_signals(i).onu_rx40_locked_o ,  
                                    
          onu_clk_trxusr240_i        => onu_array_signals(i).onu_clk_trxusr240_o   ,
          onu_rx_data_strobe_i      => onu_array_signals(i).onu_rx_data_strobe_o , 
          onu_rx_data_frame_i       => onu_array_signals(i).onu_rx_data_frame_o  ,
          onu_tx_data_ready_i       => onu_array_signals(i).onu_tx_data_ready_o  ,
          onu_tx_data_strobe_o      => onu_array_signals(i).onu_tx_data_strobe_i ,
          onu_tx_data_o             => onu_array_signals(i).onu_tx_data_i        ,
      
          onu_address_o             => onu_array_signals(i).onu_address_i              ,
          onu_mgt_phase_good_i      => onu_array_signals(i).onu_mgt_phase_good_o       ,
          onu_rx_locked_i           => onu_array_signals(i).onu_rx_locked_o            , 
          onu_operational_i         => onu_array_signals(i).onu_operational_o          ,
          onu_rx_serror_corrected_i => onu_array_signals(i).onu_rx_serror_corrected_o  ,
          onu_rx_derror_corrected_i => onu_array_signals(i).onu_rx_derror_corrected_o  ,
          onu_interrupt_i           => onu_array_signals(i).onu_interrupt_o            , 
          onu_sticky_status_i       => onu_array_signals(i).onu_status_sticky_o        ,

          onu_mgt_tx_ready_i        => onu_array_signals(i).onu_mgt_tx_ready_o         ,
          onu_mgt_rx_ready_i        => onu_array_signals(i).onu_mgt_rx_ready_o         ,
          onu_mgt_txpll_lock_i      => onu_array_signals(i).onu_mgt_txpll_lock_o       ,
          onu_mgt_rxpll_lock_i      => onu_array_signals(i).onu_mgt_rxpll_lock_o       ,
          ---------------------
      
          -- Control AXI --
          s_onu_user_aclk           => onu_clk_manager_i                                  ,
          ctrl_reg_i                => onu_user_array_signals(i).onu_user_logic_ctrl_reg,
          stat_reg_o                => onu_user_array_signals(i).onu_user_logic_stat_reg,
          ---------------------------------
      
          -- Others --
          rx_match_flag_o           => onu_user_array_signals(i).rx_match_flag,
                                    
          onu_status_o              => onu_user_array_signals(i).onu_status
          --------------------------------
          );

         onu_array_signals(i).onu_rx_p_i                     <= olt_tx_p_o;	  
         onu_array_signals(i).onu_rx_n_i                     <= '0';

         onu_tx_channel(i) <= (onu_array_signals(i).onu_tx_p_o and (not onu_array_signals(i).onu_sfp_tx_dis_o));

    end generate gen_onu_structural;

	olt_rx_p_i <= fcn_or_reduce(onu_tx_channel);
	olt_rx_n_i <= '0';

    --OLT clocks
    p_olt_clk_sys : process
    begin
      olt_clk_sys_i<='0';
      wait for c_OLT_CLK_SYS_PERIOD/2;
      olt_clk_sys_i<='1';
      wait for c_OLT_CLK_SYS_PERIOD/2;
    end process;
    olt_clk_manager_i <= olt_clk_sys_i;

    p_olt_clk_ref240 : process
    begin
      olt_clk_ref240_i<='0';
      wait for c_OLT_CLK_REF_PERIOD/2;
      olt_clk_ref240_i<='1';
      wait for c_OLT_CLK_REF_PERIOD/2;
    end process;
	
	--ONU clocks
    p_onu_clk_sys : process
    begin
      onu_clk_sys_i<='0';
      wait for c_ONU_CLK_SYS_PERIOD/2;
      onu_clk_sys_i<='1';
      wait for c_ONU_CLK_SYS_PERIOD/2;
    end process;
    onu_clk_manager_i <= onu_clk_sys_i;
		
    gen_onu_clocks: for i in 1 to c_NUMBER_ONU generate
        p_onu_clk_ref_rx_240 : process
        begin
          onu_array_signals(i).onu_clk_rxref240_i <='0';
          wait for c_ONU_CLK_RX_REF_PERIOD/2;
          onu_array_signals(i).onu_clk_rxref240_i <='1';
          wait for c_ONU_CLK_RX_REF_PERIOD/2;
        end process;

        p_onu_clk_ref_tx_120 : process
        begin
          wait until rising_edge(onu_array_signals(i).onu_rx40_locked_o);	
          wait until rising_edge(onu_array_signals(i).onu_clk_rx40_o);
          wait for c_ONU_EXTERNAL_PLL_DELAY;
          while(onu_array_signals(i).onu_rx40_locked_o = '1') loop
              onu_array_signals(i).onu_clk_txref240_i <='0';
              wait for c_ONU_CLK_TX_REF_PERIOD/2;
              onu_array_signals(i).onu_clk_txref240_i <='1';
              wait for c_ONU_CLK_TX_REF_PERIOD/2;
          end loop;
        end process;

    end generate gen_onu_clocks;

	
  --stimulis
   p_stimulis:process
   
    variable v_ONU_READY : boolean := false;
	
    procedure olt_write_ctrl_reg(constant address : integer range 0 to olt_ctrl_reg_strobe_i'left; constant data : std_logic_vector(olt_ctrl_reg_i'range)) is
    begin
        wait until(rising_edge(olt_clk_manager_i));
        olt_ctrl_reg_i                 <= data;
        olt_ctrl_reg_strobe_i(address) <= '1';
        wait until(rising_edge(olt_clk_manager_i));
        olt_ctrl_reg_i                 <= (others => '0');
        olt_ctrl_reg_strobe_i(address) <= '0';	
    end procedure;

    procedure olt_user_write_ctrl_reg(constant address : integer range 0 to olt_user_logic_ctrl_reg'left; constant data : std_logic_vector(olt_user_logic_ctrl_reg(0)'range)) is
    begin
        wait until(rising_edge(olt_clk_manager_i));
        olt_user_logic_ctrl_reg(address) <= data;
        wait until(rising_edge(olt_clk_manager_i));	
    end procedure;

    procedure onu_write_ctrl_reg(constant onu_address : integer range 1 to c_NUMBER_ONU; constant address : integer range 0 to olt_ctrl_reg_strobe_i'left; constant data : std_logic_vector(olt_ctrl_reg_i'range)) is
    begin
        wait until rising_edge(onu_clk_manager_i);
        onu_array_signals(onu_address).onu_ctrl_reg_i                 <= data;
        onu_array_signals(onu_address).onu_ctrl_reg_strobe_i(address) <= '1';
        wait until rising_edge(onu_clk_manager_i);
        onu_array_signals(onu_address).onu_ctrl_reg_i                 <= (others => '0');
        onu_array_signals(onu_address).onu_ctrl_reg_strobe_i(address) <= '0';	
    end procedure;

    procedure onu_user_write_ctrl_reg(constant onu_address : integer range 1 to c_NUMBER_ONU; constant address : integer range 0 to olt_user_logic_ctrl_reg'left; constant data : std_logic_vector(olt_user_logic_ctrl_reg(0)'range)) is
    begin
        wait until rising_edge(onu_clk_manager_i);
        onu_user_array_signals(onu_address).onu_user_logic_ctrl_reg(address) <= data;
        wait until rising_edge(onu_clk_manager_i);	
    end procedure;

   begin
      -- Simulation Initialization (begin)   
      -- Init to 'Z' all record signals
      -- see https://forums.xilinx.com/t5/Welcome-Join/VHDL-question-signal-not-propagating/td-p/442562 for more information	  
      for i in 1 to c_NUMBER_ONU loop
         onu_array_signals(i).onu_stat_reg_o                 <= (others => (others => 'Z'));  
         onu_array_signals(i).onu_core_reset_i               <= 'Z';
         onu_array_signals(i).onu_clk_txref240_i             <= 'Z';	  	
         onu_array_signals(i).onu_clk_rxref240_i             <= 'Z';	  	
         onu_array_signals(i).onu_clk_rx40_o                 <= 'Z';	  
         onu_array_signals(i).onu_rx40_locked_o              <= 'Z';	  
         onu_array_signals(i).onu_clk_trxusr240_o             <= 'Z';	  
         onu_array_signals(i).onu_rx_data_strobe_o           <= 'Z';	  
         onu_array_signals(i).onu_rx_data_frame_o            <= (others => 'Z');  
         onu_array_signals(i).onu_tx_data_strobe_i           <= 'Z';	  
         onu_array_signals(i).onu_tx_data_ready_o            <= 'Z';	  
         onu_array_signals(i).onu_tx_data_i                  <= (others => 'Z');  
         onu_array_signals(i).onu_address_i                  <= (others => 'Z');  
         onu_array_signals(i).onu_mgt_phase_good_o           <= 'Z';	  
         onu_array_signals(i).onu_rx_locked_o                <= 'Z';	  
         onu_array_signals(i).onu_operational_o              <= 'Z';	  
         onu_array_signals(i).onu_rx_serror_corrected_o      <= 'Z';	  
         onu_array_signals(i).onu_rx_derror_corrected_o      <= 'Z';	  
         onu_array_signals(i).onu_mgt_tx_ready_o             <= 'Z';	  
         onu_array_signals(i).onu_mgt_rx_ready_o             <= 'Z';	  
         onu_array_signals(i).onu_mgt_txpll_lock_o           <= 'Z';	  
         onu_array_signals(i).onu_mgt_rxpll_lock_o           <= 'Z';	  
         onu_array_signals(i).onu_interrupt_o                <= 'Z';	  
         onu_array_signals(i).onu_status_sticky_o            <= (others => 'Z'); 
         onu_array_signals(i).onu_tx_p_o                     <= 'Z';	  
         onu_array_signals(i).onu_tx_n_o                     <= 'Z';
         onu_array_signals(i).onu_rx_p_i                     <= 'Z';
         onu_array_signals(i).onu_rx_n_i                     <= 'Z';
		 
         onu_array_signals(i).onu_sfp_rx_los_i               <= '0';	  
         onu_array_signals(i).onu_sfp_tx_fault_i             <= '0';	  
         onu_array_signals(i).onu_sfp_mod_abs_i              <= '0';	  
         onu_array_signals(i).onu_sfp_tx_sd_i                <= '1';	  
         onu_array_signals(i).onu_sfp_pdown_o                <= 'Z';	  
         onu_array_signals(i).onu_sfp_tx_dis_o               <= 'Z';	  
         onu_array_signals(i).onu_i2c_sda_io                 <= 'Z';	  
         onu_array_signals(i).onu_i2c_scl_io                 <= 'Z';
         onu_user_array_signals(i).rx_match_flag           <= 'Z';
         onu_user_array_signals(i).onu_status              <= (others => 'Z');
         onu_user_array_signals(i).onu_user_logic_stat_reg <= (others => (others => 'Z'));  
      end loop;
	  
      -- Init to '0' all ONU control signals
      for i in 1 to c_NUMBER_ONU loop
        onu_array_signals(i).onu_ctrl_reg_i                 <= (others => '0');
        onu_array_signals(i).onu_ctrl_reg_strobe_i          <= (others => '0');
        onu_user_array_signals(i).onu_user_logic_ctrl_reg <= (others => (others => '0'));	
      end loop;

      -- Init to '0' all OLT control signals
      olt_ctrl_reg_i          <= (others => '0');
	  olt_ctrl_reg_strobe_i   <= (others => '0');
      olt_user_logic_ctrl_reg <= (others => (others => '0'));	
      -- Simulation Initialization (end)  
	  
      -- System Reset (begin)
      tb_STATE_STRING <= "RESET SYS           ";  
	  --OLT reset
      olt_user_write_ctrl_reg(0,x"00000001");
      wait for 1000.0 ns;		  
      olt_user_write_ctrl_reg(0,x"00000000");
      wait for 1000.0 ns;	
	  --ONU reset + address assignment
      for i in 1 to c_NUMBER_ONU loop	  
        onu_user_write_ctrl_reg(i,0,x"00000001");
        wait for 1000.0 ns;			
        onu_user_write_ctrl_reg(i,0,x"00000000");
        wait for 1000.0 ns;			
        onu_user_write_ctrl_reg(i,0,std_logic_vector(to_unsigned(i*2, 32)));	 -- address for TDM	
        wait for 1000.0 ns;			
      end loop;
      -- System Reset (end)
	  
      -- Wait for ONU to be locked and ready (begin)
      tb_STATE_STRING <= "WAIT LOCK DOWNSTREAM";  
      v_ONU_READY := false;
      while(not v_ONU_READY) loop
          wait until rising_edge(onu_clk_sys_i);
          v_ONU_READY := true;		  
          for i in 1 to c_NUMBER_ONU loop	  
            if(onu_array_signals(i).onu_rx40_locked_o    = '0' or
		       onu_array_signals(i).onu_rx_locked_o      = '0' or
		       onu_array_signals(i).onu_mgt_tx_ready_o   = '0' or
		       onu_array_signals(i).onu_mgt_rx_ready_o   = '0' or
		       onu_array_signals(i).onu_mgt_txpll_lock_o = '0' or
		       onu_array_signals(i).onu_mgt_rxpll_lock_o = '0') then
		       v_ONU_READY := false;
		    end if;
          end loop;		  
      end loop;
      -- In a real system, the port onu_phase_good_o shall be monitored
	  -- Once the ONU is ready, the delay of the external PLL (here modelled as c_ONU_EXTERNAL_PLL_DELAY) has to be adjusted to ensure phase_good
      -- More information can be found in the TTC-PON Firmware and Software User Guide
	  
      -- Wait for ONU to be locked and ready (end)

      -- Dummy calibration for simulation (begin)
      tb_STATE_STRING <= "UPSTREAM INIT       ";  	  
      -- In the real system the procedure <full_calibration_run> should be used to ensure the TDM upstream is properly arbitrated
      -- This is an extremely simplified version of the calibration for simulation only purposes and shall not be used in a real PON system	  
      -- OLT heartbeat
      olt_write_ctrl_reg(1, std_logic_vector(to_unsigned(30*c_NUMBER_ONU, 32)) );
	  for i in 1 to c_NUMBER_ONU loop
        onu_write_ctrl_reg(i,0, std_logic_vector(to_unsigned(233, 32)) ); -- ONU operational
        onu_write_ctrl_reg(i,1, std_logic_vector(to_unsigned(30*c_NUMBER_ONU-1 + ((2**16)*30*(i-1)), 32)) ); -- ONU heartbeat and coarse offset
      end loop;	
      --onu_write_ctrl_reg(2,0, std_logic_vector(to_unsigned(0, 32)) ); -- ONU disable
      --onu_write_ctrl_reg(3,0, std_logic_vector(to_unsigned(0, 32)) ); -- ONU disable
      wait for 20000 ns; 	  
      -- Dummy calibration for simulation (end)

      -- Clear monitor status (begin)
      -- phy monitor	  
      olt_write_ctrl_reg(10, x"00000004"); 
      wait for 100.0 ns;		  
      olt_write_ctrl_reg(10, x"00000000"); 
      wait for 100.0 ns;		  
      -- 8b10b error monitor
      olt_write_ctrl_reg(15, x"00000001");
      wait for 100.0 ns;		  
      olt_write_ctrl_reg(15, x"00000000");
      wait for 100.0 ns;		  
	  for i in 1 to c_NUMBER_ONU loop
        -- phy monitor	  
        onu_write_ctrl_reg(i,5, x"00000001");
        wait for 100.0 ns;			
        onu_write_ctrl_reg(i,5, x"00000000");
        wait for 100.0 ns;			
        -- FEC/CRC error monitor
        onu_write_ctrl_reg(i,3, x"00000001");
        wait for 100.0 ns;			
        onu_write_ctrl_reg(i,3, x"00000000");
        wait for 100.0 ns;			
      end loop;
      -- Clear monitor status (end)

      tb_STATE_STRING <= "SYSTEM RUN          ";

      onu_user_write_ctrl_reg(1, 2, x"00000000"); -- prbs-7
      onu_user_write_ctrl_reg(2, 2, x"00000003"); -- counter
      onu_user_write_ctrl_reg(3, 2, x"00000002"); -- constant

      wait;
	  
   end process;

end tb;
