add wave -position insertpoint  \
sim:/tb_pon_system/c_NUMBER_ONU \
sim:/tb_pon_system/tb_STATE_STRING\
sim:/tb_pon_system/olt_clk_manager_i \
sim:/tb_pon_system/olt_clk_ref240_i \
sim:/tb_pon_system/olt_clk_sys_i \
sim:/tb_pon_system/olt_clk_trxusr240_o \
sim:/tb_pon_system/olt_core_reset_i \
sim:/tb_pon_system/olt_ctrl_reg_i \
sim:/tb_pon_system/olt_ctrl_reg_strobe_i \
sim:/tb_pon_system/olt_interrupt_o \
sim:/tb_pon_system/olt_mgt_pll_lock_o \
sim:/tb_pon_system/olt_mgt_rx_ready_o \
sim:/tb_pon_system/olt_mgt_tx_ready_o \
sim:/tb_pon_system/olt_rx_locked_o \
sim:/tb_pon_system/olt_rx_data_error_o \
sim:/tb_pon_system/olt_rx_data_o \
sim:/tb_pon_system/olt_rx_data_strobe_o \
sim:/tb_pon_system/olt_rx_n_i \
sim:/tb_pon_system/olt_rx_onu_addr_o \
sim:/tb_pon_system/olt_rx_p_i \
sim:/tb_pon_system/olt_stat_reg_o \
sim:/tb_pon_system/olt_status_sticky_o \
sim:/tb_pon_system/olt_tx_data_i \
sim:/tb_pon_system/olt_tx_data_strobe_i \
sim:/tb_pon_system/olt_tx_n_o \
sim:/tb_pon_system/olt_tx_p_o \
sim:/tb_pon_system/olt_user_logic_ctrl_reg \
sim:/tb_pon_system/olt_user_logic_stat_reg \
sim:/tb_pon_system/olt_user_tx_match_flag \
sim:/tb_pon_system/onu_array_signals \
sim:/tb_pon_system/onu_clk_manager_i \
sim:/tb_pon_system/onu_clk_sys_i \
sim:/tb_pon_system/onu_tx_channel \
sim:/tb_pon_system/onu_user_array_signals