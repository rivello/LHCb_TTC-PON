# rev_1_0_2

![TTC_PON](doc_img/TTC_PON_Official_Logo.png)

# TTC-PON Basics

The TTC-PON system was developed at CERN in the context of phase-1 and 2 upgrades of the LHC experiments in order to propose a new generation TTC system. The system is responsible for delivering timing, trigger and control from a central trigger unit to the detector sub-partitions in the back-end part of the LHC experiments as shown in the figure below. TTC-PON is based on passive optical networks technology, widely adopted in the telecommunications industry. 

![Diagram](doc_img/image_PON.png)

TTC-PON is a point-to-multipoint bidirectional optical communication system in which a master node called OLT (Optical Line Terminal) and slave nodes called ONU (Optical Network Unit) can communicate through the same fiber using wavelength division multiplexing access. In the downstream direction, the OLT broadcasts information to the ONUs and in the upstream direction, the ONUs can send information to the OLT  using a time division multiplexing scheme.  The downstream data transmission has a line rate of 9.6Gb/s and a wavelength of 1577nm. The upstream data transmission scheme is fully synchronous to the downstream transmission with a line rate of 2.4Gb/s and a wavelength of 1270nm.

# This repository

In this repository you can find:

- TTC-PON firmware and software user guide (TTC_PON_fw_sw_user_guide.pdf)

- TTC-PON FMC V3 schematic under hardware (example of hardware required to have a TTC-PON node in your system)

- Example design for OLT in KCU105 and A10GX boards with quick start guides

- Example design for ONU in KCU105 and A10GX boards with quick start guides

- TTC-PON system simulation to help users get ease with TTC-PON cores

For any additional question, please contact eduardo.brandao.de.souza.mendes@cern.ch or sophie.baron@cern.ch
